#!/bin/bash

set -xu

# To compile Grundpraktikum I:
#PARTS=teilI
#MAINFILE=main

PARTS=fp
MAINFILE=main

LATEX="xelatex -interaction=nonstopmode"

if [ ! -d .git ]; then
    echo "must run in EXPERIMENTect root"
    exit 1;
fi

function compile_one() {
    EXPERIMENT=${1}
    EXPERIMENT=`dirname ${EXPERIMENT}`
    EXPERIMENT_FOLDER=$(dirname ${EXPERIMENT})
    mkdir -p ${BASEDIR}/artifacts/${PART}/${EXPERIMENT_FOLDER}
    pushd $EXPERIMENT
    pwd
    UPTODATE=2
    while [ 0 -ne $UPTODATE ]; do
        if [ -e ${MAINFILE}.tex ]; then
            $LATEX ${MAINFILE}.tex
            cmp -s ${MAINFILE}.aux old.aux
            UPTODATE=$?
            cp ${MAINFILE}.aux old.aux;
        else
            exit 1;
        fi
    done;
    rm old.aux
    cp ${MAINFILE}.pdf ${BASEDIR}/artifacts/${PART}/${EXPERIMENT}.pdf
    popd
}

function compile_all() {

    BASEDIR=$(pwd)

    for PART in $PARTS; do
        pushd $PART
        for EXPERIMENT in `find . -name ${MAINFILE}.tex`; do
            compile_one ${EXPERIMENT}
        done;
        popd
    done
}

compile_all

wait

