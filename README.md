# Physik-Praktikum

RWTH-Gitlab: https://git.rwth-aachen.de/lewin/Praktikum

Gitlab.com: https://gitlab.com/dermesser/praktikum-ci (Mirror, nur für CI)

Gitlab.com ist so konfiguriert, dass jeder gepushte Commit automatisch neue PDFs
für alle "Projekte" (siehe unten) generiert: [Aktuelle automatisch generierte
PDF-Dateien](https://gitlab.com/dermesser/praktikum-ci/-/jobs/artifacts/master/browse/artifacts?job=artifacts_main)

## Fonts und Pakete

Über texlive o.ä. installieren:

* `physics`, `graphicx`, `hyperref`, `geometry`, `koma-script`, `mathdesign`,
  `stix`

## Teil I (Anfängerpraktikum) 2020

Damit das automatische Generieren von PDFs funktioniert, und für allgemeine Ordnung, sollten wir
dieser Struktur für alle Dateien folgen:

* In `teilI`: Ein Verzeichnis pro Thema
  * In jedem Versuchsverzeichnis: Ein Verzeichnis pro Gruppe (von jeweils 2
      Leuten)
    * Dort ein Unterverzeichnis für binäre Dateien wie Bilder, Messdaten, etc.
    * und eine `main.tex`-Datei mit dem eigentlichen Dokument
* Verzeichnisse ohne `main.tex`-Datei sind z.B. für andere Inhalte wie Skripte
o.ä. nützlich.
* In `teil2`: Ein Verzeichnis pro Person, darin ein Verzeichnis pro Experiment.
Jedes Verzeichnis sollte eine `Protokoll.tex`-Datei enthalten.

Vorlagen sind im `base`-Verzeichnis.

```
teilI/
├── base
│   └── demo
│       ├── img
│       │   └── file.png
│       └── main.tex
└── versuch1
    ├── gruppe1
    │   └── main.tex
    └── gruppe2
        └── main.tex
teil2/
├── base
│   ├── nice_plots.py
│   ├── protokoll.cls
│   └── Protokoll.tex
└── lewin
    └── atomphysik
        ├── analyse
        │   └── nice_plots.py
        ├── protokoll.cls
        └── Protokoll.tex
└── alex
    └── atomphysik
        ├── analyse
        │   └── nice_plots.py
        ├── protokoll.cls
        └── Protokoll.tex  
```

Wenn dann das Skript `auto/compile_all.sh` ausgeführt wird, werden alle
`main.tex`-Dateien nacheinander kompiliert und die resultierenden PDFs mit
passenden Namen in den `artifacts`-Ordner kopiert. Nach einem Push können die
Dateien auch von
[Gitlab](https://gitlab.com/dermesser/praktikum-ci/-/jobs/artifacts/master/browse/artifacts?job=artifacts_main)
heruntergeladen werden. Das Skript kann zwischen `teilI/teil2` umgestellt
werden.

LaTeX-Ordner sollten immer eine `main.tex`/`Protokoll.tex`-Datei enthalten; ansonsten werden sie
nicht gefunden. Dadurch können z.B. Skripte in eigene Ordner gelegt werden, und
stören die automatische Kompilierung nicht.

## Fortgeschrittenenpraktikum (2021)

Das Prinzip ist gleich wie vorher. `main.tex`-Dateien in allen Ordnern unterhalb von `fp/` werden gefunden und kompiliert.
