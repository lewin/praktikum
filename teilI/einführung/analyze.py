#!/usr/bin/env python3

from praktikum import analyse, cassy
import matplotlib.pyplot as plt
import numpy as np

"""
# CASSY-Info für cassy.schwingung.lab:

Lese CASSY-1 Textdatei:  cassy.schwingung.lab

Messung #1:  (Datenreihen: n, t, I_A1, U_B1)
Index n                                  501 Werte von 1 bis 501
Zeit t/ms                                501 Werte von 0 bis 0.01
Strom I_A1/A                             501 Werte von -0.03835 bis 0.04685
Spannung U_B1/V                          501 Werte von -4.68 bis 5.875
"""

def setup_matplotlib():
    plt.rcParams['font.size'] = 12.0
    plt.rcParams['font.family'] = 'sans-serif'
    plt.rcParams['font.sans-serif'] = 'Arial'
    plt.rcParams['font.weight'] = 'regular'
    plt.rcParams['axes.labelsize'] = 'medium'
    plt.rcParams['axes.labelweight'] = 'bold'
    plt.rcParams['axes.linewidth'] = 1.2
    plt.rcParams['lines.linewidth'] = 2.0

def find_zero_intersect(time, series):
    """Find intersections of a signal with zero.

    Arguments:
            time: A numpy or python array with time marks.
            series: A value/data series.
    Returns:
            A [(index, time)] list of positions of minimal distance from 0
    """
    assert(time.shape == series.shape)
    minima = []
    last = 0
    for (i,v) in enumerate(series):
        if (last > 0 and v < 0) or (last < 0 and v > 0):
            if abs(last) < abs(v):
                minima.append(i-1)
            else:
                minima.append(i)
        last = v
    return [(m, time[m]) for m in minima]


def analyze():
    fig = plt.figure(dpi=150)
    data = cassy.CassyDaten('cassy.schwingung.lab')
    t = data.messung(1).datenreihe('t').werte
    U_1 = data.messung(1).datenreihe('U_B1').werte
    I_1 = data.messung(1).datenreihe('I_A1').werte
    p1 = fig.add_subplot(2,1,1)
    iline = p1.plot(data.messung(1).datenreihe('t').werte, I_1, label='I')
    right = p1.twinx()
    uline = right.plot(data.messung(1).datenreihe('t').werte, U_1, 'r', label='U')
    right.set_ylabel('I/A')
    p1.set_ylabel('U/V')
    p1.grid(True)
    p1.legend([iline[0], uline[0]], ['I', 'U'])

    p2 = fig.add_subplot(2,1,2)
    freq, amp = analyse.fourier(t, U_1)
    p2.plot(freq, amp)
    p2.set_xlim(left=0, right=2000)
    p2.grid(True)

    fig.show()
    print("sigma U: ", np.std(U_1, ddof=1))

def main():
    setup_matplotlib()


main()
analyze()

if __name__ == '__main__':
    main()