# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 14:55:47 2020

@author: Max_b
"""


from praktikum import cassy
from praktikum import analyse
import numpy as np
import matplotlib.pyplot as plt

lengthE = [65.2, 58.2, 54.9, 48.9, 43.6, 41.2]
freqE = [82.16, 92.17, 97.79, 110.24, 123.18, 130.39]
errorE = [0.13, 0.13, 0.12, 0.13, 0.13, 0.13]
lengthA = [65.1, 61.5, 58.1, 51.8, 46.2, 43.6]
freqA = [110.243, 116.836, 123.916, 138.811, 155.537, 164.694]
errorA = [0.122,0.123,0.123,0.128,0.123,0.122]

lengthE = np.array(lengthE)/100
freqE = np.array(freqE)
errorE = np.array(errorE)
lengthA = np.array(lengthA)/100
freqA = np.array(freqA)
errorA = np.array(errorA)
errorLengthE = (0.001/np.sqrt(12))*(1/(2*lengthE))
errorLengthA = (0.001/np.sqrt(12))*(1/(2*lengthA))

xaxisE = 1/(2*lengthE)
xaxisA = 1/(2*lengthA)

xaxisMeanE = analyse.mittelwert_stdabw(xaxisE)
xaxisMeanA = analyse.mittelwert_stdabw(xaxisA)
freqEMean  = analyse.mittelwert_stdabw(freqE)
freqAMean = analyse.mittelwert_stdabw(freqA)

xaxisE-= xaxisMeanE[0]
xaxisA-= xaxisMeanA[0]
freqE -= freqEMean[0]
freqA -= freqAMean[0]

eString = analyse.lineare_regression_xy(xaxisE, freqE, errorLengthE, errorE)
aString = analyse.lineare_regression_xy(xaxisA, freqA, errorLengthA, errorA)

xaxisE+= xaxisMeanE[0]
xaxisA+= xaxisMeanA[0]
freqE += freqEMean[0]
freqA += freqAMean[0]

xaxislineE = np.linspace(0.76,1.23, 1000)
yaxislineE = xaxislineE*eString[0]+ eString[2]
xaxisLineA = np.linspace(0.77, 1.15, 1000)
yaxisLineA = xaxisLineA*aString[0]+aString[2]

plt.figure()
plt.xlabel('x in 1/m')
plt.ylabel('frequency in Hz')
plt.errorbar(xaxisE,freqE, xerr=errorLengthE,yerr=errorE ,fmt='o')
plt.plot(xaxislineE,yaxislineE, color='red')
plt.show()

plt.figure()
plt.xlabel('x in 1/m')
plt.ylabel('frequency in Hz')
plt.errorbar(xaxisA, freqA, xerr=errorLengthA, yerr = errorA, fmt='o')
plt.plot(xaxisLineA, yaxisLineA, color='red')
plt.show()

freqE = freqE -eString[0]*xaxisE-eString[2]
plt.figure()
plt.xlabel('x in 1/m')
plt.ylabel('frequency in Hz')
plt.errorbar(xaxisE, freqE, xerr=errorLengthE, yerr=errorE, fmt='o')
plt.show()

freqA = freqA - aString[0]*xaxisA -aString[2]
plt.figure()
plt.xlabel('x in 1/m')
plt.ylabel('frequency in Hz')
plt.errorbar(xaxisA, freqA, xerr=errorLengthA, yerr=errorA, fmt='o')
plt.show()

print(eString)
print(aString)