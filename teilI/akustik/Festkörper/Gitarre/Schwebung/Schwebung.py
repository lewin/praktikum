# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 17:34:55 2020

@author: Max_b
"""


from praktikum import cassy
from praktikum import analyse
import numpy as np
import matplotlib.pyplot as plt

class PeakCounter():
    lastpeaktime = 0
    peakcounter =0
    peaktimes =0
    timearray = 0
    peakamplitude =0
    beatpeaktimes = 0
    mintime = 0
    
    def peakcounterfunc(self, time, voltage):
        self.timearray = np.zeros(len(time))
        for i in range(len(time)):
            if(i != 0 and i != len(time)-1 and voltage[i]>voltage[i-1] and voltage[i]>voltage[i+1]): 
                self.peakcounter += 1
                self.lastpeaktime = time[i]
                self.timearray[i] = time[i]
        
        self.timearray = self.timearray[self.timearray>0]
        n = len(self.timearray)
        for i in range(n):
            if(i<n-1 and abs(self.timearray[i]-self.timearray[i+1])<self.mintime ):
                if(self.timearray[i]>self.timearray[i+1]):
                    self.timearray = np.delete(self.timearray, i+1)
                    n -=1
                    self.peakcounter-=1
                else:
                    self.timearray = np.delete(self.timearray, i)
                    n -=1
                    self.peakcounter-=1
        
        return self.peakcounter, self.timearray
        
    def beatcounterfunc(self, time, voltage, mintime, maxtime, n):
        
        self.beatpeaktimes = np.zeros(len(time))
        if(len(time) != len(voltage)):
            return 0
        
        self.peaktimes = time
        self.peakamplitude = np.zeros(len(voltage))
        for i in range(len(time)):
            if (i != 0 and i != len(time)-1 and voltage[i]>voltage[i-1] and voltage[i]>voltage[i+1] and abs(self.lastpeaktime-time[i])>0.0048):
                self.peakamplitude[i] = voltage[i]
        index = np.where(self.peakamplitude > 0)
        for i in range(len(index[0])):
         
            bigger = False
            if(i>n and i<len(index[0])-n): 
                for j in range(n):
                    if(voltage[index[0][i-j]]<=voltage[index[0][i]] and voltage[index[0][i+j]] <= voltage[index[0][i]]):
                        bigger = True
                    else:
                        bigger = False
                        break
                if(bigger):     
                    self.beatpeaktimes[i] = time[index[0][i]]
                  
                    
        self.beatpeaktimes = self.beatpeaktimes[self.beatpeaktimes>0]
        return self.beatpeaktimes
            
    def timecalculator (self, time):
        size = time.size -1
        first = time[0]
        last = time[size]
        numberOfFronts = size/2
        timeDiff = last-first
        timeDiff = timeDiff/numberOfFronts
        return 1/timeDiff
        
        
             


name = '1.Messung/4.lab'
datei  = cassy.CassyDaten(name).messung(1)

time = datei.datenreihe('t').werte
voltage = datei.datenreihe('U_A1').werte
fastft0,fastft1 = analyse.fourier_fft(time,voltage)
fastft0 = fastft0[:int(len(fastft0)/2):]
fastft1 = fastft1[:int(len(fastft1)/2):]
#plt.figure()
#plt.plot(fastft0,fastft1)
#plt.xlabel('frequency/Hz', fontsize=16)
#plt.ylabel('amplitude', fontsize=16)
#plt.show()
peak = PeakCounter()
time = time[time<1]
voltage = voltage[:len(time):]

#plt.figure()
#plt.plot(time, voltage)
#plt.xlabel('time/s', fontsize=16)
#plt.ylabel('voltage/V', fontsize=16)

#plt.show()


peaks, peaktime = peak.peakcounterfunc(time, voltage)
error = np.linspace(0.9425, 0.9425, peaks)
peaks = np.arange(peaks)

peaksMean = analyse.mittelwert_stdabw(peaks)
peaktimeMean = analyse.mittelwert_stdabw(peaktime)

peaks = peaks.astype(float)

peaks -= peaksMean[0]
peaktime -= peaktimeMean[0]

regre = analyse.lineare_regression(peaktime, peaks, error )
print(regre)

peaks += peaksMean[0]
peaktime += peaktimeMean[0]

xaxisline = np.linspace(0,1,1000)
yaxisline = xaxisline*regre[0]+regre[2]

plt.figure()
plt.xlabel('time in s')
plt.ylabel('count')
plt.errorbar(peaktime, peaks,error,fmt='o')
plt.plot(xaxisline, yaxisline, '.')
plt.show()

peaks = peaks -peaktime*regre[0]-regre[2]
plt.figure()
plt.xlabel('time in s')
plt.ylabel('count')
plt.errorbar(peaktime, peaks, error, fmt='o')
plt.show()


peaktimer =peak.beatcounterfunc(time, voltage, 0, 0,9)
tmptime = peaktimer/0.0005
tmptime = tmptime.astype(int)
linregvoltage = voltage[tmptime]
counter = (np.arange(len(tmptime))+1) /2
error = [0.021, 0.0219, 0.0215, 0.021, 0.0215]
error = np.array(error)

timeMean = analyse.mittelwert_stdabw(time[tmptime])
counterMean = analyse.mittelwert_stdabw(counter)

time[tmptime] -= timeMean[0]
counter -= counterMean[0]

regre1= analyse.lineare_regression(time[tmptime],counter, error)
print(regre1)

time[tmptime] += timeMean[0]
counter += counterMean[0]

xaxis = np.linspace(0,1,1000)
yaxis = xaxis*regre1[0]+regre1[2]

plt.figure()
plt.xlabel('time in s')
plt.ylabel('count')
plt.plot(xaxis, yaxis, color='red')
plt.errorbar(time[tmptime], counter, error, fmt='o')
plt.show()

counter = counter -time[tmptime]*regre1[0]-regre1[2]
plt.figure()
plt.xlabel('time in s')
plt.ylabel('count')
plt.errorbar(time[tmptime], counter, error, fmt='o')
plt.show()

peaktimer = peak.timecalculator(peaktimer)
print('peaktimer with timecalculatr:' ,peaktimer)


