\documentclass[a4paper,12pt]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage{xcolor}
\usepackage[margin=2.5cm]{geometry}
\usepackage[german]{varioref}
\usepackage{physics}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{float}

\usepackage[no-math]{fontspec}
\setmainfont{STIX}
\usepackage[notext]{stix}

\hypersetup{colorlinks,breaklinks,urlcolor=blue,linkcolor=blue}
\restylefloat{table}

\newcommand{\TODO}[1]{{\large \textcolor{red}{\textbf{TODO:}}} \emph{#1}}

%opening
\subject{Physikalisches Grundpraktikum\\RWTH Aachen}
\title{Schallgeschwindigkeit in Festkörpern \& Physik der Gitarre}
\author{Maximilian Bartel, Alexander Kohlgraf}
\date{\today}

\begin{document}

\maketitle

\section{Schallgeschwindigkeit in Festkörpern}

\subsection{Versuchsbeschreibung}

Es wird eine Metallstange unbekannten Materials gewählt (Nummerierung 1-12). Diese wird in eine Halterung eingespannt welche freies Schwingen ermöglicht.
Das Gewicht, die Länge und der Durchmesser der Stange (für Durchmesser mehrere Messungen) werden gemessen.
Mithilfe eines Mikrofons, welches auf das eine Ende der Stange gerichtet wird, misst man die longitudinale Schwingung der Stange. Diese wird durch einen parallelen Schlag auf das andere Ende der Stange mit einem Gummihammer erzeugt.
Diese Schwingung wird ausgewertet, um die Frequenz zu bestimmen (mehrere Messungen derselben Stange). 
Mit dieser Frequenz und den anderen Messwerten der Stange können nun die Schallgeschwindigkeit, Dichte und das Elastizitätsmodul bestimmt werden.


\subsubsection{Ziel}

Wir wollen in diesem Versuch, die Schallgeschwindigkeit, die Dichte und das Elastizitätsmodul bestimmen und diese mit den Literaturwerten vergleichen.
Über diesen Vergleich erhoffen wir, das Material der Stangen zu bestimmen, welche wir vermessen haben.

\subsection{Grundlagen}

Die Schallgeschwindigkeit sollte in verschiedenen Materialien unterschiedlich sein.
Das Elastizitätsmodul beschreibt die Proportionalität von angreifender Zugspannung und relativer Längenausdehnung und wird wie folgt definiert:
\[
E = \frac{(F/A)}{\Delta L/L}
\]
Dabei gilt, wie leicht zu sehen, dass bei einem hohen Elastizitätsmodul, das Material sich weniger stark verformt.
Das Elastizitätsmodul gibt diese Beziehung nur in einem gewissen Bereich wieder. Dieser Bereich wird auch Hookescher Bereich genannt. Dies rührt daher, dass das Material bei einer Verlängerung oder Zusammenstauchung auch seinen Durchmesser oder Kurzzeitig Dichte verändern muss. Bei einer zu starken Verlängerung kann somit das Material immer dünner werden, bis das Phänomen des Fließens einsetzt, wo das Material so dünn wird, dass es eben nicht mehr sich elastisch zurückbewegen kann. In diesem Experiment bewegen wir uns jedoch in Bereichen, wo diese Gleichung Aussagekräftig ist.
Insbesondere da wir Stäbe und keine Drähte nutzen, bei welchen die Durchmesseränderung vernachlässigt werden kann.
Weiterführend gilt die Gleichung der longitudinalen Schallgeschwindigkeit in Festkörpern, welche aufgrund des geringen Durchmessers unserer Versuchsobjekte (D << $\lambda$) vereinfacht werden kann zu:
\[
v_l = \sqrt{\frac{E}{\rho}}
\]
Da unser Stab aufgrund des Aufbaus frei Schwingt, entspricht die Grundschwingung der longitudinalen Wellenlänge Lambda, zwei mal der Länge des Stabes. Am Ende des Stabes ist jeweils ein Druckknoten:
\[
\lambda_0 = 2L
\]
Es gilt auch die Grundgleichung der Wellengeschwindigkeit:
\[
v_l = \lambda_0 \cdot f_0
\]
Somit folgt direkt aus den vorherigen beiden Gleichungen eine unserer wichtigen Gleichungen für die Auswertung:

\begin{equation}
\label{eq:v_l}
v_l = 2 \cdot L \cdot f_0
\end{equation}

Die Dichte der Stangen sollte sich unterscheiden und ist somit ein gutes Maß zur Materialabschätzung. Sie ist die Masse geteilt durch das Volumen des Körpers. Da unsere Stange ein Zylinder ist gilt also:

\begin{equation}
\label{eq:rho}
\rho = \frac{M}{\pi L \qty(\frac{D}{2})^2} = \frac{4M}{\pi L D^2}
\end{equation}

Zuletzt stellt man die Formel der Schallgeschwindigkeit nach dem Elastizitätsmodul um und setzt sowohl die Dichte, als auch die Schallgeschwindigkeit ein. So erhält man das Elastizitätsmodul abhängig von Größen die sich Messen lassen:
\begin{equation}
\label{eq:E}
E = \frac{16}{\pi} f_0^2 L M \frac{1}{D^2}
\end{equation}

\subsection{Versuchsaufbau}
\label{sec:aufbau}

\begin{figure}[H]
\centering
\caption{Aufbau; entnommen aus Praktikumsanleitung}
\centering
\includegraphics[width=\textwidth]{Stange/img/Aufbau}
\end{figure}

Es wird eine Metallstange aus dem zugewiesenen Ort entnommen. Diese kann vor oder nach dem Versuch vermessen werden (Masse, Dicke, Länge; Bei Versuchsdurchführung mehr dazu). Man Befestigt nun die Kreuzmuffe an der kurzen Metallstange mit der Tischklemme am Tisch. Sie sollte gut festgezogen werden, damit man im späteren verlauf des Versuches sie weder verrücken noch lösen kann. Man darf sie aber auch nicht zu fest ziehen, da man sie sonst überdreht (siehe Sprichwort: "Nach fest kommt ab"), \emph{dies gilt für alle Schraubhalterungen}.
\\\\
Man spanne nun die Bereit gelegte Stange in diese Halterung. Sie soll nur an zwei stellen Kontakt haben, um ein freies Longitudinales Schwingen zu ermöglichen. Sie soll Parallel zu Tischkante und Tischplatte eingelegt werden, damit das Mikrophon an die richtige Stelle kann. Um unnötige Spannungen im Material zu verhindern muss die Stange mittig eingespannt werden. Es empfiehlt sich, dass eine Person die Stange hält, während die andere die Schrauben der Kreuzmuffe festzieht.
\\\\
Nun wird das Mikrophon aufgestellt. Es wird ebenfalls mit einer Tischklemme befestigt. Man stelle es circa 5mm vom Stabende auf, sodass das Rohr direkt auf das Ende gerichtet ist. Wieder ist es hilfreich wenn einer das Mikrophon korrekt hält, während der andere, diesmal die Tischschrauben, festzieht.
\\\\
Die vorherigen beiden Absätze werden für jede neue Stange wiederholt, da die Längen der Stangen unterschiedlich sind.
 Man schließe das bereitgestellte Sensor CASSY an. Dafür versorgt man es mit Strom (12V Stecker oder weiteres CASSY Modul), und verbindet es über die Serielle Schnittstelle mit einem Computer. Es soll Spannung auf die Y-Achse und Zeit auf die X-Achse gelegt werden. Es soll auch eine FFT der Daten direkt durchgeführt werden, anhand derer wir die Qualität unserer Messung schnell prüfen können. Am Mikrophon wähle man den Amplitudenmodus, indem man bei den uns vorgelegten Mikrophonen den Regler auf ~ stellt. Zuletzt verbinde man die Kabel des Mikrophons mit einem der Spannungseingänge des Sensor CASSY. Wir haben U\_A1 verwendet. Kurz vor der Durchführung schalte man das Mikrophon an. \emph{Dieses schaltet sich nach 10 Minuten automatisch wieder aus.} Man lege für den Versuch einen Gummihammer bereit.
\\
\begin{table}[H]
\label{tab:messparameter1}
\centering
\begin{tabular}{c|c}
Messparameter & Wert\\
\hline
Messbereich & $\pm$3 V\\
Messintervall & 100 $\mu$s\\
Messpunkte & 16000\\
Messzeit & 1.6 s\\
Kanal & U\_A1
\end{tabular}
\caption{Messparameter}
\end{table}

\subsection{Versuchsdurchführung}

Nachdem man geprüft hat, ob das Messgerät aufzeichnet, Teilt man sich auf. Eine Person stellt sich an das Stangenende gegenüber des Mikrophons, die andere an den Computer um die Messung zu starten. Es wird nun Möglichst parallel gegen das Ende der Stange mit dem Hammer geschlagen. Man sollte davon absehen, mit dem Handgelenk Schwung zu nehmen, da dies leicht zu einem transversalen Anteil in der Schwingung führt. Transversale Anteile ruinieren die Messwerte und sollten unbedingt minimiert werden. Wenn man sie bereits beim Anschlag mit dem Auge erkennen kann, sollte man die Messung neu Starten. Wenn man nun einen zufriedenstellenden Schlag gemacht hat, sollte der Partner am Computer nach einem kurzen Augenblick die Messung starten. Diese Verzögerung dient dazu den Schlag selbst nicht mit aufzuzeichnen und könnte auch dadurch ersetzt werden, dass man die erste Sekunde aus den Messwerten entfernt. Man beachte, dass niemand anderes im Raum dieses Experiment mit dem gleichen Material durchführt, da auch dies die Werte verunreinigt. Nachdem die Messung beendet ist, schaue man sich die FFT der Werte an. Es sollte eine klar erkennbare Frequenz, welche größer als 100 Hz ist, erkennbar sein. Ist dies nicht der Fall, verwirft man die Messung. Dies wiederholt man, bis 5 Messungen vorhanden sind. Diese speichert man jedes mal ab. Zuletzt spannt man die Stange wieder aus und vermisst sie. Zu diesem Zweck nutzt man die im Versuchsraum gestellte Waage. Man vermisst die Länge mit einem Maßband. Der Durchmesser der Stange wird mithilfe einer Mikrometerschraube vermessen. Dazu misst man an verschiedenen Stellen und in verschiedenen Ausrichtungen. Wenn man nur die die Position der Mikrometerschraube verändert, bemerkt man nicht, wenn die Stange der ganzen Länge entlang elliptisch ist. 10 Messwerte für den Durchmesser sollten genügen. Man errechnet aus ihnen den Mittelwert, dies ist der Durchmesser. Man räumt die alte Stange nun weg und nimmt eine neue. Die genannten Schritte aus \autoref{sec:aufbau} muss man wiederholen.

\subsection{Auswertung}

\subsubsection{Rohdaten}

\begin{figure}[h]
\centering
\caption{Rohdaten Schallgeschwindigkeit Festkörper}
\label{fig:rawRod}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Stange/img/Stange1}
\caption{Stange 1}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Stange/img/Stange6}
\caption{Stange 1}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Stange/img/Stange12}
\caption{Stange 12}
\end{subfigure}
\end{figure}

\autoref{fig:rawRod}
Hier werden exemplarisch für jede unserer Stangen eine FFT angegeben, sowie den durch den Peakfinder der Praktikumsbibliothek höchsten Peak. Die Daten die man sehen kann sind bereits bereinigt, sodass man Frequenzen unter 100 Hz nicht sehen kann. Dies musste gemacht werden, da der höchste Peak der Messung unter anderem die Netzspannung mit 50 Hz seien kann. Das X.lab steht dafür, die wievielte Messung es jeweils war. Die genauen Datensätze findet man unter Festkörper/Stange/Stäbe.

\subsubsection{Analyse}

Für die Analyse Verwenden wir ein Python Skript. Dieses führt dieselben Berechnungen bei allen Datensätzen durch, welche hier erklärt werden.
Auf die Länge liegt ein statistischer Ablesefehler von circa 0.29 mm, sowie ein systematischer von circa 0.4 mm. Auf der Masse liegt ein statistischer Fehler von 0.1 g. Die Werte des Durchmessers werden manuell eingelesen und mithilfe der Praktikumsbibliothek werden Mittelwert und Standardabweichung und daraus der Fehler des Mittelwertes bestimmt. Die Frequenz wird mithilfe der Funktionen fourier\_fft, untermenge\_daten und peakfinder\_schwerpunkt der Praktikumsbibliothek bestimmt. Dafür führen wir die FFT auf unseren Datensätzen durch und schneiden danach alle Frequenzen unter 100 Hz ab. Diese benötigen wir nicht, da die Frequenz des Metalls hörbar größer als 1000 Hz ist. Mithilfe des peakfinders wird nun die Frequenz der Stange bestimmt und für jede Messung abgespeichert. Um nun Frequenz und Fehler zu bestimmen, wird wieder die Funktion mittelwert\_stdabw verwendet und der Mittelwert Fehler bestimmt.
\\
So haben wir nun alle Größen mit Fehler bestimmt, aus denen wir unsere Zielgrößen bestimmen können.
Für die Dichte nutzen wir \autoref{eq:rho}.
Die Fehler der Dichte berechnen wir mithilfe der Fehlerfortpflanzung:
\[
\sigma_\rho^{stat} = \rho \cdot \sqrt{\qty(\frac{\sigma_M}{M})^2 + \qty(\frac{\sigma_L}{L})^2 + 4 \cdot \qty(\frac{\sigma_D}{D})^2}
\]
\[
\sigma_\rho^{sys} = \rho \cdot \frac{\sigma_L^{sys}}{L}
\]
Für die Schallgeschwindigkeit nutzen wir \autoref{eq:v_l}.
Für dessen Fehler:
\[
\sigma_v^{stat} = v \cdot \sqrt{\qty(\frac{\sigma_f}{f})^2 + \qty(\frac{\sigma_L}{L})^2 }
\]
\[
\sigma_v^{sys} = v \cdot \frac{\sigma_L^{sys}}{L}
\]
Für das Elastizitätsmodul nutzen wir \autoref{eq:E}.
Für dessen Fehler:
\[
\sigma_E^{stat} = E \cdot \sqrt{4 \cdot \qty(\frac{\sigma_f}{f})^2 + \qty(\frac{\sigma_L}{L})^2 + \qty(\frac{\sigma_M}{M})^2 + 4 \cdot \qty(\frac{\sigma_D}{D})^2}
\]
\[
\sigma_E^{sys} = E \cdot \frac{\sigma_L^{sys}}{L}
\]
So gibt uns das Programm alle Fehler und Daten. Im folgenden werden diese Analysedateien für jede einzelne Stange angegeben. Das Ergebnis und der Fehler werden auf Sinnvolle Werte gekürzt.

\paragraph{Stange 6}

Farbeinschätzung: Messing

\begin{table}[H]
\label{tab:paraStange6}
\centering
\begin{tabular}{c|c}
Parameter & Wert\\
\hline
Länge & 1.299 m\\
Statistischer Fehler & 0.3 mm\\
Systematischer Fehler & 0.4 mm\\
Masse & 1.2371 kg\\
Statistischer Fehler & 0.03 g\\
Durchmesser & 11.9976 mm\\
Statistischer Fehler & 0.0002 mm\\
Frequenz & 1348.801 Hz\\
Statistischer Fehler & 0.002 Hz\\
Dichte & 8424 $\mathrm{kg/m^3}$\\
Statistischer Fehler & 1.9 $\mathrm{kg/m^3}$\\
Systematischer Fehler & 2.6 $\mathrm{kg/m^3}$\\
Schallgeschwindigkeit & 3504.2 $\mathrm{m/s}$\\
Statistischer Fehler & 0.8 $\mathrm{m/s}$\\
Systematischer Fehler & 1.1 $\mathrm{m/s}$\\
Elastizitätsmodul & 103.44 GP\\
Statistischer Fehler & 0.02 GP\\
Systematischer Fehler & 0.03 GP
\end{tabular}
\caption{Parameter Stange 6}
\end{table}

\paragraph{Stange 1}

Farbeinschätzung: Eisen

\begin{table}[H]
\label{tab:paraStange1}
\centering
\begin{tabular}{c|c}
Parameter & Wert\\
\hline
Länge & 1.5 m\\
Statistischer Fehler & 0.3 mm\\
Systematischer Fehler & 0.4 mm\\
Masse & 1.3255 kg\\
Statistischer Fehler & 0.03 g\\
Durchmesser & 11.992 mm\\
Statistischer Fehler & 0.001 mm\\
Frequenz & 1728.564 Hz\\
Statistischer Fehler & 0.055 Hz\\
Dichte & 7823 $\mathrm{kg/m^3}$\\
Statistischer Fehler & 2.3 $\mathrm{kg/m^3}$\\
Systematischer Fehler & 2.1 $\mathrm{kg/m^3}$\\
Schallgeschwindigkeit & 5185.7 $\mathrm{m/s}$\\
Statistischer Fehler & 1.0 $\mathrm{m/s}$\\
Systematischer Fehler & 1.4 $\mathrm{m/s}$\\
Elastizitätsmodul & 210.39 GP\\
Statistischer Fehler & 0.06 GP\\
Systematischer Fehler & 0.06 GP
\end{tabular}
\caption{Parameter Stange 1}
\end{table}

\paragraph{Stange 12}

Farbeinschätzung: Kupfer

\begin{table}[H]
\label{tab:paraStange12}
\centering
\begin{tabular}{c|c}
Parameter & Wert\\
\hline
Länge & 1.299 m\\
Statistischer Fehler & 0.3 mm\\
Systematischer Fehler & 0.4 mm\\
Masse & 1.3024 kg\\
Statistischer Fehler & 0.03 g\\
Durchmesser & 11.967 mm\\
Statistischer Fehler & 0.010 mm\\
Frequenz & 1511.611 Hz\\
Statistischer Fehler & 0.001 Hz\\
Dichte & 8914 $\mathrm{kg/m^3}$\\
Statistischer Fehler & 6.2 $\mathrm{kg/m^3}$\\
Systematischer Fehler & 2.8 $\mathrm{kg/m^3}$\\
Schallgeschwindigkeit & 3927.2 $\mathrm{m/s}$\\
Statistischer Fehler & 0.9 $\mathrm{m/s}$\\
Systematischer Fehler & 1.2 $\mathrm{m/s}$\\
Elastizitätsmodul & 137 GP\\
Statistischer Fehler & 0.1 GP\\
Systematischer Fehler & 0.04 GP
\end{tabular}
\caption{Parameter Stange 12}
\end{table}


\subsubsection{Endergebnisse}

\paragraph{Stange 6}

Die Literaturwerte für Messing als eine Legierung sind recht weit gestreut. Somit konnten wir Leider nur eine Spanne von Elastizitätsmodulen auf Wikipedia finden \footnote{\url{https://de.wikipedia.org/wiki/Elastizit\%C3\%A4tsmodul}}
Hier wird diese Spanne als 70-120 GP angegeben. Unser Wert von 103 würde Hervorragend Hineinpassen, ist jedoch nur eine unzureichende Bestimmung. Auf einer anderen Seite \footnote{\url{https://de.wikibooks.org/wiki/Tabellensammlung_Chemie/_Dichte_fester_Stoffe}} findet man Dichtetabellen für verschiedenste Stoffe. Diese Tabelle wird auch für die folgenden Stangen genutzt. Es wird für Messing 8100-8700 $\mathrm{kg/m^3}$ angegeben. Unser Wert von 8424 $\mathrm{kg/m^3}$ würde also wieder als gutes Mittel in diese Spanne passen. Als letztes Maß haben wir die Schallgeschwindigkeit im Material. Unter \footnote{\url{https://www.schweizer-fn.de/stoff/akustik/schallgeschwindigkeit.php}} wird die Schallgeschwindigkeit in Messing als 3500 $\mathrm{m/s}$ bei 20°C gelistet. Unser Wert von 3504 $\mathrm{m/s}$ Passt diesmal sogar Hervorragend.
\\
Es ist somit sehr Wahrscheinlich, dass Stange 6 aus Messing besteht.

\paragraph{Stange 1}

Unsere Vermutung für Stange Nummer 1 war Eisen oder eine stark eisenhaltige Legierung wie Stahl.
Der Tabellenwert für das Elastizitätsmodul für Baustahl liegt bei 210 GP \footnotemark[1]. Unser Wert lag äußerst nah bei 210.39 GP. Die Dichte von unlegiertem Stahl ist nach \footnotemark[2] 7850 $\mathrm{kg/m^3}$ und die von legiertem Stahl 7900 $\mathrm{kg/m^3}$. Unser Wert von 7823 $\mathrm{kg/m^3}$ würde somit eher für unlegierten Stahl sprechen, allerdings ist dies aufgrund der starken Ungenauigkeiten (im folgenden mehr dazu) nicht aussagekräftig.
Unsere Schallgeschwindigkeit von 5185.7 $\mathrm{m/s}$ liegt zwischen den auf \footnotemark[3] vorkommenden Werten 5100 $\mathrm{m/s}$ und 5200 $\mathrm{m/s}$. Somit ist Stahl mit den 5100 $\mathrm{m/s}$ eine denkbare Möglichkeit, während Rotbuche (5100 $\mathrm{m/s}$), Weißtanne (5200 $\mathrm{m/s}$) und Lärche (5200 $\mathrm{m/s}$) aufgrund der geringen Dichte von Tannenholz \footnote{\url{https://de.wikipedia.org/wiki/Tannenholz}} von 320 - 710 $\mathrm{kg/m^3}$ recht unwahrscheinlich sind. 
\\
Es ist somit davon auszugehen, dass es sich bei dem Material von Stange 1 um Stahl handelt.

\paragraph{Stange 12}

Die Vermutung des Materials von Stange 12, begründet auf Farbe und Geruch, war Kupfer. Vergleicht man als ersten Literaturwert die Dichte, so erhält man aus \footnotemark[2] eine Dichte von 8920 - 8960 $\mathrm{kg/m^3}$. Unser Messergebnis von 8914 $\mathrm{kg/m^3}$ deckt sich sowohl mit dem Erwartetem Material, als auch mit Nickel, welches mit 8910 $\mathrm{kg/m^3}$ gelistet wird. Das Elastizitätsmodul von Kupfer liegt laut \footnotemark[1] bei 100 - 130 GP und für Gusseisen bei 90 - 145 GP. Unser Wert von 137 GP ist somit etwas hoch für Kupfer, aber dennoch möglich.
Als letztes Maß haben wir die Schallgeschwindigkeit. Wir haben dafür einen Wert von 3927.4 $\mathrm{m/s}$ gemessen. Dieser stimmt am ehesten mit Beton überein, was offensichtlich falsch ist. In userer Quelle \footnotemark[3] wid Kupfer mit 5010 $\mathrm{m/s}$ gelistet. Unter \footnote{\url{https://de.wikipedia.org/wiki/Schallgeschwindigkeit}} wird sie jedoch als 4660 $\mathrm{m/s}$ gelistet. Beides ist weit von unserem Wert entfernt. 
\\
Da die anderen Faktoren jedoch eine hohe Übereinstimmung hatten und sowohl die olfaktorische, als auch die optische Wahrnehmung diese Übereinstimmung unterstützen, nehmen wir an, dass wir irgendwo in der Aufnahme von Länge, Dichte und Masse einen Fehler gemacht haben. Wir vermuten weiterhin, dass es sich bei Stange 12 um Kupfer handelt.

\subsubsection{Diskussion}

Man sieht, dass unsere Fehler sehr gering sind, obwohl unsere Abweichungen vom Literaturwert hoch sind. Dies ist natürlich darauf zurückzuführen, dass wir viele Fehlerquellen nicht berücksichtigt haben. So ist die Temperatur ein wichtiger Faktor den es eigentlich zu berücksichtigen gilt, da sie aufgrund der Längenausdehnung sowohl auf L, als auch auf D und als Folge $\rho$ einen Fehler produziert.
\\
Ebenso haben wir einen unbekannten systematischen Messfehler auf M. Dieser würde alle Fehler in den resultierenden Werten erhöhen und auf realistischere Werte bringen.
\\
Wir haben die Form des Stabes als zylindrisch angenommen und versucht diesen Fehler über die Mittlung einiger Durchmesser werte auszugleichen. Diese Werte sind Voreingenommen, da wir sie stehend gemacht haben und sie somit wahrscheinlich nicht gleichmäßig entlang der Rotation des Zylinders verteilt sind. Sollte der Zylinder sogar Elliptisch sein, ist der Fehler noch größer. Dies ist besonders relevant, da der Durchmesser quadratisch in das Elastizitätsmodul und die Dichte eingeht. Der Fehler geht somit auch stärker gewichtet in die Fehler ein. So können wir unter anderem unsere geringen Fehler erklären.
\\
Wir haben nicht berücksichtigt ob die Stange eine Krümmung hat, wodurch die Messung von L nicht korrekt ist und die Schwingung nicht sauber getragen werden könnte.
\\
Wir können nicht wissen wie verunreinigt eine Stange ist mit anderen Materialien, oder wie viel Material fehlt. Dies würde die Dichte und die Schallgeschwindigkeit des Materials Ändern.
\\
All diese Fehler führen zu einer Fluktuation um die Literaturwerte die im Bereich des erwarteten liegt. Unsere Werte sind, ausgenommen Stab 12, hinreichend Zutreffend.

\section{Physik der Gitarre}

\subsection{Schwebung}

Bei diesem Versuch wurde eine Saite der Gitarre leicht verstimmt und gleichzeitig mit einer anderen Saite, auf der der ursprüngliche Ton gespielt wurde, angeschlagen.

\subsubsection{Ziel}

Ziel des Versuches ist die Untersuchung von Schwebungen, die entstehen, wenn zwei Wellen mit ähnlicher Frequenz gespielt werden.

\subsubsection{Grundlagen}

Werden zwei Schallwellen ausgesandt, überlagern sich die Schwingungen beim Empfänger. Da die Welle damit keine Ortskomponente mehr besitzt, muss nur noch der Zeitanteil der Störung betrachtet werden, womit aus der Welle eine Schwingung wird. 
Betrachten wir nun die Schwingungen $y_a(t)=y_{0a}sin(\omega_at)$ und $y_b(t)=y_{0a}sin(\omega_bt)$. O.B.d.A. nehmen wir an, dass $y_{0a}=y_{0b}$. Addieren wir diese erhalten wir:
\begin{align*}
	y_a(t)+y_b(t)&=y_{0a}sin(\omega_at)+y_{0a}sin(\omega_bt) \\
	&=y_{0}(sin(\omega_at)+sin(\omega_bt) \\
	&=2y_0sin(\frac{\omega_a+\omega_b}{2})cos(\frac{\omega_a-\omega_b}{2})
\end{align*}
Das Ergebnis kann als eine Schwingung mit der Frequenz $\frac{\omega_a+\omega_b}{2}$ interpretiert werden, die mit der Frequenz $\frac{\omega_a-\omega_b}{2}$ moduliert wird.


\subsubsection{Versuchsaufbau}

Eine Saite der Gitarre wird leicht verstimmt und es wird der entsprechende Bund markiert, um auf einer anderen Saite den gleichen Ton zu produzieren. Das Mikrofon wird mittig über das Schallloch platziert und mit dem Spannungseingang des Sensorcassys verbunden. Das Sensorcassy ist mit dem PC verbunden und 
folgende Messparameter werden voreingestellt:
\begin{table}[h]
\label{tab:messparameter2}
\centering
\begin{tabular}{c|c}
Messparamter & Wert\\
\hline

Messbereich & $\pm$1 V\\
Messintervall & 500 $\mu$s\\
Messpunkte & 16000\\
Messzeit & 8 s\\
Kanal & $\mathrm{U_{A1}}$

\end{tabular}
\caption{Messparameter}
\end{table}
Zusätzlich zur Spannung haben wir uns die FFT der Spannung ausgeben lassen.
\subsubsection{Versuchsdurchführung}

In unserem Fall entschieden wird uns für die leere D-Saite und den 5. Bund der A-Saite. Die D-Saite wurde leicht nach unten verstimmt. Beide Saiten wurden gleichzeitig möglichst gleich stark angeschlagen. Nach einer kurzen Verzögerung von ca. einer halben Sekunde wurde die Messung gestartet.



\subsubsection{Rohdaten}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Schwebung/Spannung-Zeit}
\caption{Spannung aufgetragen gegen Zeit}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Schwebung/FFT-Spannung-Zeit}
\caption{FFT der Spannung}
\end{subfigure}
\label{fig:vol/time}
\end{figure}

Hier aufgetragen ist ein Ausschnitt der Daten im Zeitbereich bis 1 s. Bei der Auswertung wurde auch nur dieser Zeitbereich betrachtet, da in den restlichen 7 s keine Schwebung mehr zu erkennen ist. Die Schwebung ist deutlich zu erkennen. Weiterhin sind auch sehr viele Artefakte im Datensatz zu sehen. Im Diagramm der FFT sind deutlich zwei sehr enge Peaks etwas unterhalb von 200 Hz zu sehen, aber auch ein weiterer sehr stark ausgeprägter bei 50 Hz. Die beiden Peaks haben laut FFT die Frequenzen 140,764 Hz und 146,258 Hz. Die FFT ist die einzige Messung, die die vollen 8 s ausgewertet hat.


\subsubsection{Modell}

Um die Daten auszuwerten benutzten wir eine lineare Regression. Wir schrieben ein Skript, welches die Peaks zählte und die jeweiligen Zeiten speicherte. Aufgetragen wurde dann die Anzahl der Peaks gegen Zeit und per linearer Regression wurde eine Gerade angepasst. Die Steigung dieser war die resultierende Frequenz. Um Korrelationen zu vermeiden transformierten wir die Werte symmetrisch um den Nullpunkt.

Die Schwebungsfrequenz wurde auch mit Hilfe einer linearen Regression bestimmt. Hier erstellten wir ein Pythonskript, das den höchsten Peak in einer Umgebung von n Peaks bestimmen konnte und ließen uns wieder die Zeiten dazu ausgeben. Auch hier wurden die Punkte gegen die Zeit aufgetragen und eine Gerade angepasst. Auch diese Werte wurden vorher mit Hilfe der Mittelwerte transformiert. Auch hier war die Steigung die gesuchte Frequenz.

\subsubsection{Analyse und Fehler}

Da wir nie einen bestimmten Ton treffen mussten und die Frequenzen relativ zu einander betrachten, können wir systematische Fehler außer Acht lassen.
 
Bei der linearen Regression der Daten für die resultierende Frequenz haben wir einerseits einen Fehler durch die Abtastrate des Cassy und andererseits den Fehler den das Pythonskript hinzufügt. Um die Artefakte zu minimieren haben wir einen Mindestabstand festgelegt, den zwei Peaks haben müssen, um als Peak aufgefasst zu werden. Der Fehler auf die Zeit durch die Intervallbreite ist $\sigma_1=\frac{0.0005}{\sqrt{12}}$ s.
Auf den Mindestabstand im Pythonskript den Fehler zu ermitteln ist deutlich schwieriger. Letztendlich benutzten wir die Anzahl der Peaks auf dem Zeitraum 0-1 s mal den Mindestabstand, weil das unser maximaler Fehler ist. Also $\sigma_2=145*0.0065\;\text{s}=0.9425$ s. Insgesamt erhalten also einen Fehler von $\sigma_{sys}=\sqrt{\sigma_1^2+\sigma_2^2}=0.9465011\;\text{s}$. Die relevanten Stellen der Fehler sind aber nur die ersten 4 Nachkommastellen, da unsere Messung auch nur auf 4 Nachkommastellen genau war. Nach der linearen Regression sind unsere Ergebnisse:
\begin{table}[h]

\centering
\begin{tabular}{c|c}
Parameter & Wert\\
\hline
Ordinatenabschnitt & 0\\
Ordinatenabschnitt Fehler & 0.0783\\
Steigung & 143.1553 Hz\\
Steigung Fehler & 0.2679 Hz\\
\#Freiheitsgrade & 143\\
$\chi^2$ & 400.73\\
$\frac{\chi^2}{\#Freiheitsgrade}$ & 2.8
\end{tabular}
\label{tab:ergebnisse1}
\caption{Parameter der resultierenden Frequenz}
\end{table}

Bei der Schwebungsfrequenz gingen wir genauso vor wie bei der resultierenden Frequenz. Der Fehler auf die Intervallbreite ist auch hier $\sigma_1=\frac{0.0005}{\sqrt{12}}$ s. Für den Fehler auf die Maxima haben wir hier jeweils den größeren Abstand zu einem Maxima auf ähnlicher Höhe benutzt, welche per Hand aus den Daten ausgewertet wurde, da nur 5 Bäuche betrachtet werden. Auch hier sind diese Fehler sehr viel größer als die Ablesefehler, sodass nur die im Pythonskript jeweils benutzten Fehler pro Peak beachtet werden muss.

\begin{table}[h]
\centering
\begin{tabular}{c|c}
Parameter & Wert\\
\hline
Ordinatenabschnitt & 0.0004\\
Ordinatenabschnitt Fehler & 0.0096\\
Steigung & 2.74 Hz\\
Steigung Fehler & 0.0369 Hz\\
\#Freiheitsgrade & 3\\
$\chi^2$ & 15.092\\
$\frac{\chi^2}{\#Freiheitsgrade}$ & 5.031
\end{tabular}
\label{tab:ergebnisse2}
\caption{Parameter der Schwebungsfrequenz}
\end{table}

Als Fehler auf die FFT wurde die Intervallbreite genommen die jeweils 0.122 Hz beträgt.

Um aus den beiden Frequenzen der FFT die ausgerechneten Frequenzen zu bestimmen gehen wir nach der oben genannten Rechnung vor. Damit ergibt sich aus der FFT 2.747$\pm$0.173 Hz  und 143.511$\pm$0.173 Hz.

\subsubsection{Endergebnisse}

Aus unserer Messung haben wir folgende Ergebnisse:
\begin{itemize}
\item Aus den Regressionen: 
\begin{itemize}
\item resultierende Frequenz 143.1553$\pm$0.2679 Hz
\begin{itemize}
\item $\frac{\chi^2}{\#Freiheitsgrade}=2.8$
\end{itemize}
\item Schwebungsfrequenz 2.74$\pm$0.0369 Hz
\begin{itemize}
\item $\frac{\chi^2}{\#Freiheitsgrade}=5.031$
\end{itemize}
\end{itemize}
\item Aus der FFT:
\begin{itemize}
\item resultierende Frequenz 143.511$\pm$0.173 Hz
\item Schwebungsfrequenz 2.747$\pm$0.173 Hz
\end{itemize}
\end{itemize}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Schwebung/resultierende}
\caption{Messpunkte mit berechneter Geraden}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Schwebung/schwebung}
\caption{Schwebung mit Geraden}
\end{subfigure}
\label{fig:schwebung}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Schwebung/resulerror}
\caption{Residuenplot}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Schwebung/schwebungerror}
\caption{Residuenplot}
\end{subfigure}
\label{fig:schwebungerror}
\end{figure}

Man sieht stark, dass die resultierende Frequenz wahrscheinlich noch eine Modulation enthält.
\subsubsection{Diskussion}

Unsere Daten passen durchaus mit den Erwartungen zusammen. Das einzige Problem sind unsere $\frac{\chi^2}{\#Freiheitsgrade}$, die mit 2.8 und 5.031 etwas zu groß sind. Dies liegt wahrscheinlich an den Pythonskripten und die Fehlerabschätzung auf diese. Auch die überlagerte Frequenz von 50 Hz könnte eine Rolle spielen. 

\subsection{Massebelag}

Bei diesem Versuch wurde eine Saite in mehreren Bünden gespielt und mit der Bundlänge verglichen.

\subsubsection{Ziel}

Ziel des Versuchs war es, den Massebelag, den der Hersteller angibt, mit unseren gemessenen Werten zu vergleichen.

\subsubsection{Grundlagen}

Letztendlich folgt aus Gleichung (1) und (2) die Gleichung $f_n=\frac{n}{2L}\sqrt{\frac{T}{\mu}}$. Dabei ist $n$ die $n$. Oberschwingung der Saite, $L$ die Länge der Saite, $T$ die Saitenspannung und $\mu$ der Massebelag. Aus unseren Messungen werden wir nur den Quotient $\frac{T}{\mu}$ bestimmen können.

\subsubsection{Aufbau}

Der Aufbau ist der selbe wie in Versuch 2.1, nur dass diesmal die Gitarre so genau wie möglich auf die Standardstimmung gebracht wird. Die Voreingestellten Messparameter sind wieder:
\begin{table}[h]
\label{tab:messparameter3}
\centering
\begin{tabular}{c|c}
Messparamter & Wert\\
\hline

Messbereich & $\pm$1 V\\
Messintervall & 500 $\mu$s\\
Messpunkte & 16000\\
Messzeit & 8 s\\
Kanal & $\mathrm{U_{A1}}$

\end{tabular}
\caption{Messparameter}
\end{table}


\subsubsection{Durchführung}

Zuerst haben wir die Längen der einzelnen Bünde der E- und A-Saite bis zum 10 Bund ausgemessen und notiert. Danach haben wir auf einer Saite Bünde ausgesucht, diese runtergedrückt und den Ton gespielt. Ca. eine halbe Sekunde nach anschlagen der Saite haben wir die Aufnahme mit dem Mikrofon gestartet. Dies haben wir für beide Saiten für jeweils 6 mal getan und die jeweiligen Daten abgespeichert.

\subsubsection{Rohdaten}
\begin{table}[h]
\label{tab:daten2}
\centering
\begin{tabular}{c|c|c|c|c}
Bund & Länge E-Saite [cm] &Frequenz E-Saite [Hz]&Länge A-Saite [cm]&Frequenz A-Saite [Hz]\\
\hline 
0 & 65.2 & 82.16 & 65.1 & 110.243\\
1 &  &  & 61.5 & 116.836\\
2 & 58.2& 92.7&58.1&123.916\\
3 & 54.9&97.79& &  \\
4& & &51.8&138.811\\
5&48.9&110.24& &  \\
6& & &46.2&155.537\\
7&43.6&123.18&43.6&164.693\\
8&41.2&130.39& &   \\

\end{tabular}
\caption{Rohdaten}
\end{table}

Die Längen wurden jeweils mit einem Maßband gemessen und die Frequenzen aus der FFT bestimmt.

\subsubsection{Modell}

Zur Auswertung haben wir jeweils eine lineare Regression angewandt. Wir schrieben ein Pythonskript, das jeweils die Messwerte und die Fehler als Listen enthält. Aufgetragen wurden jeweils $f_1$ gegen $\frac{1}{2L}$, wobei $L$ die Länge vom Steg bis zum gegriffenem Bund ist. Die Steigung dieser Geraden ist der gesuchte Faktor $\sqrt{\frac{T}{\mu}}$.
Um Korrelationen zu vermeiden haben wir beide Werte mit Hilfe des Mittelwertes um den Nullpunkt transformiert.

\subsubsection{Analyse und Fehler}

Hier haben wir jeweils einen Fehler auf die Längenmessung mit dem Maßband und auf die Frequenzen aus der FFT. 
Da bei der Messung mit dem Maßband für die spätere Auswertung nur die Unterscheide relativ zueinander wichtig sind, können wir den systematischen Fehler außer Acht lassen. Der statistische Fehler bei der Längenmessung beträgt $\sigma_L\equiv\frac{1}{\sqrt{12}}$ mm. Da wir aber der Fehler auf $x\equiv\frac{1}{2L}$ brauchen, müssen wir diesen mit Hilfe der Fehlerfortpflanzung berechnen. Der Fehler ist dann $\sigma_{sys_x}=\frac{\sigma_L}{2L}$. Damit ist der Fehler für jede gemessene Länge anders. Dies ist im Pythonskript berücksichtigt.
Auf den Fehler der FFT haben wir wie schon im vorigen Versuch jeweils den maximalen Abstand zum nächsten Messpunkt der FFT benutzt. Auch diese sind als Liste im Pythonskript berücksichtigt worden.

Nach durchführen der linearen Regression mit Fehler auf beiden Achsen erhalten wir folgende Werte:

\begin{table}[h]
\centering
\begin{tabular}{c|c|c}
Parameter & Wert E-Saite & Wert A-Saite\\
\hline
Ordinatenabschnitt & 0 Hz & -0.001 Hz\\
Ordinatenabschnitt Fehler & 0.053 Hz & 0.053 Hz\\
Steigung & 108 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}} $& 143.589 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}} $\\
Steigung Fehler & 0.35 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}} $& 0.38 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}} $\\
\#Freiheitsgrade & 4 & 4\\
$\chi^2$ & 9.89 & 5.706\\
$\frac{\chi^2}{\#Freiheitsgrade}$ & 2.47 & 1.426
\end{tabular}
\label{tab:ergebnisse3}
\caption{Parameter der Messung}
\end{table}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Saitenspannung/E-Saite}
\caption{E-Saite}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Saitenspannung/A-Saite}
\caption{A-Saite}
\end{subfigure}
\label{fig:strings}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Saitenspannung/E-Error}
\caption{Residuenplot}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Saitenspannung/A-Error}
\caption{Residuenplot}
\end{subfigure}
\label{fig:stringerror}
\end{figure}



\subsubsection{Endergebnisse}

Die relevanten Werte sind jeweils die Steigung und der Fehler auf diese, sowie die $\frac{\chi^2}{\#Freiheitsgrade}$.
Für die E-Saite bekommen wir einen Faktor von 108$\pm$0.35 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}}$ und $\frac{\chi^2}{\#Freiheitsgrade}$ von 2.47.
Für die A-Saite bekommen wir einen Faktor von 143.589$\pm$0.38 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}} $ und $\frac{\chi^2}{\#Freiheitsgrade}$ von 1.426.

\subsubsection{Diskussion}

Die angegebenen Literaturwerte für die E-Saite sind 105.82 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}}$ und für die A-Saite 141.27 $\sqrt{\frac{\mathrm{Nm}}{\mathrm{kg}}}$. Unsere Werte inklusive Fehler liegen nicht in diesem Bereich sondern nur in der Nähe. Was wir nun natürlich nicht wissen, ist welche Toleranzen der Hersteller bei den Saiten akzeptiert, sowie ob das Alter der Saiten diese Werte verändert. Insgesamt sind unsere Messungen hinreichend.

\subsection{Frequenzspektrum}

Bei diesem Versuch wurde eine leere Saite an verschiedenen Stellen angeschlagen und das Frequenzspektrum aufgenommen.

\subsubsection{Ziel}

Das Ziel war zu beobachten, dass sich das Frequenzspektrum sichtbar sowie hörbar unterscheidet, wenn man die Saite an unterschiedlichen Stellen anschlägt. Auch sollten sich die Frequenzen nach der Formel $d=\frac{L}{n}$ verhalten, wobei dies bedeutet, dass wenn man die Saite bei einem Abstand $d$ von einem Ende anschlägt, das $n$-te Maximum verschwinden sollte.

\subsubsection{Grundlagen}

Die Auslenkung einer Saite kann man als Summe ihrer Einzelmoden $y(x,t)=\sum_{i=1}^{N}A_ncos(\omega_nt+\Phi_n)sin(k_nx)$ schreiben. Es bildet sich also eine Überlagerung stehender Wellen aus, die dadurch gebildet wird, dass die Welle an Steg und Brücke reflektiert wird und dabei einen Phasensprung von $180^\circ$ entsteht.
Dabei sollte sich das Frequenzspektrum nach der oben genannten Formel verhalten, da z.B. bei Anschlag auf $\frac{1}{10}$ der Saite, das begrenzende Bauteil genau an der Stelle wäre, an der sich das Maximum der 10. Oberschwingung ausbilden sollte. Da das Bauteil aber fest ist, kann sich kein Wellenbauch ausbilden - Die Mode sollte verschwinden.

\subsubsection{Aufbau}
Genau wie im vorigen Versuch.

\subsubsection{Durchführungen}

Die Saite wird an einem abgemessenen Punkt angeschlagen. Kurz nach anschlagen wird das Mikrofon gestartet und eine Messung durchgeführt. Von den Daten wird eine FFT gebildet und die Frequenzspektren miteinander verglichen.


\subsubsection{Modell}

Die Werte werden mit einer FFT per Hand verglichen.

\subsubsection{Analyse und Fehler}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Spektrum/12Bund}
\caption{Frequenzspektrum bei halber Länge}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gitarre/Spektrum/fünftel}
\caption{Frequenzspektrum bei Fünftel der Länge}
\end{subfigure}
\label{fig:spektrum}
\end{figure}

Die FFT hat den schon genannten Fehler des Abstandes zum nächsten Messpunkt. 
Zwar entsteht beim Abmessen mit dem Maßband auch ein Fehler, dieser ist aber zu vernachlässigen im Vergleich zu dem Fehler, wenn man die Saite zwischen die Finger nimmt und dann loslässt, damit sie anfängt zu schwingen. Im Endeffekt müsste man hier die halbe Daumenbreite als Fehler nehmen, sowie einrechnen, dass die Saite nicht mit einem spitzen Wellenberg startet.
Da hier die Ergebnisse nur qualitativ betrachten werden sollten, haben wir auf eine explizite Fehlerrechnung verzichtet.

\subsubsection{Endergebnisse}

Wie man sieht, ist der fünfte Peak in unserer Messung zwar kleiner, aber nicht verschwunden. Dieses Ergebnis ergab sich auch nach wiederholter Messung, in dieser ist der Peak sogar am schwächsten ausgeprägt. Zwar können wir die Formel damit nicht bestätigen, aber Tendenzen sind zu erkennen. Subjektiv war der Ton deutlich verändert. Ich persönlich würde den Klang bei Anschlag in der Mitte als warm und weich beschreiben, wohingegen er nah am Ende sehr hart wurde.

\subsubsection{Diskussion}

Zwar wurde dieser Versuch nicht quantitativ betrachtet, aber schon qualitativ kann man sehen, dass solch eine Messung sehr suboptimal ist. Einerseits war es im Raum sehr laut, was bei einer Messung des gesamten Spektrums sehr schlecht ist, andererseits sind Musikinstrumente sehr komplexe Gegenstände, die bei Anschlag der Saite auch selber im Holz beispielsweise stehende Wellen ausbilden, die das Ergebnis der reinen Saitenmessung beeinflussen können. Auch der Anschlag mit dem Finger ist suboptimal, weil man so nur sehr ungenau arbeiten kann.

\end{document}
