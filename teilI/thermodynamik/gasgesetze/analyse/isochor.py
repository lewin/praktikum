# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 11:22:09 2020

@author: Max_b
"""
import lib

import math

import numpy as np
import matplotlib.pyplot as plt
import pandas
import praktikum.analyse as analyse
import praktikum.literaturwerte as lw

def read_isochor(file):
    """Read CSV file and return columns (numpy arrays): T/Celsius, p/Pa"""
    d = pandas.read_csv(file)
    return (np.array(d['T/C'].values, dtype=np.float32), np.array(d['p/Pa'].values, dtype=np.float32))


ISOCHOR_DATA = ['../daten/isochor.csv']

data = read_isochor(ISOCHOR_DATA[0])

tempInC = data[0]
pressInhPa = data[1]/100
errorOfTemp = np.array(0.4*np.ones(len(tempInC)))
errorOfPress = np.array(3/np.sqrt(12)*np.ones(len(pressInhPa)))

a,errora, b, errorb, chiq, korr = analyse.lineare_regression_xy( tempInC, pressInhPa, errorOfTemp, errorOfPress)
print('Regression parameters:', a,errora, b, errorb, chiq, korr)

# Shift method in y
press_plus = pressInhPa + errorOfPress
press_minus = pressInhPa - errorOfPress
params_plus = analyse.lineare_regression_xy(tempInC, press_plus, errorOfTemp, errorOfPress)
params_minus = analyse.lineare_regression_xy(tempInC, press_minus, errorOfTemp, errorOfPress)
ea_sys = (params_plus[1]-params_minus[1])/2
print('Systematic error on a (y-shift): ea_sys =', ea_sys)

# Shift method in x
temp_plus = tempInC + errorOfTemp
temp_minus = tempInC - errorOfTemp
params_plus = analyse.lineare_regression_xy(temp_plus, pressInhPa, errorOfTemp, errorOfPress)
params_minus = analyse.lineare_regression_xy(temp_minus, pressInhPa, errorOfTemp, errorOfPress)
ea_sys = (params_plus[1]-params_minus[1])/2
print('Systematic error on a (x-shift): ea_sys =', ea_sys)

residy = pressInhPa -a*tempInC-b
residyerror = np.sqrt(errorOfPress**2+(a*errorOfTemp)**2)
xaxis = np.linspace(0,100, 1000)
yaxis = a*xaxis+b
#plt.plot(xaxis,yaxis)
fig = plt.figure(dpi=300, figsize=(12,6))
sp = fig.add_subplot(111)
sp.grid(True)
sp.set_xlabel('t / Celsius')
sp.set_ylabel('p / mbar')
sp.errorbar( tempInC,pressInhPa, yerr= errorOfPress, xerr=errorOfTemp, fmt='.')
#plt.show()
lib.plot_with_residue(tempInC,pressInhPa, errorOfTemp, errorOfPress, residy, residyerror, m=a, b=b, xlabel='t / Celsius', ylabel ='p / mbar', title='Isochor', size=(12,6))

errorOfT0 =b/a* np.sqrt((errora/a)**2-2*korr*errora*errorb/(a*b)+(errorb/b)**2)
output = 'T_0 is {0:16.2f} \nerror of T_0 is {1:5.2f}'.format(b/a, errorOfT0)
print(output)