#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 13:06:53 2020

@author: lbo
"""


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.spines as spine

def plot_absolute_zero():
    # Illustration for description of pV = nRT experiment.
    m = 1/5
    b = 1
    t_cel = np.linspace(-7, 3)
    p = t_cel * m + b

    fig = plt.figure(dpi=300)
    sp = fig.add_subplot(111)
    sp.grid(True)
    sp.plot(t_cel, p)
    sp.spines['left'].set_position(('data', 0))
    sp.spines['bottom'].set_position(('data',0))
    sp.set_xlabel('T')
    sp.set_ylabel('p')
    sp.set_xlim(-8, 4)
    sp.set_ylim(-1, 3)
    sp.xaxis.set_label_coords(.95,.32)
    sp.tick_params(labelbottom=False, labelleft=False)
    sp.yaxis.set_label_coords(.55, .95)
    plt.savefig('../img/absolutezero.svg')
    return sp

plot_absolute_zero()