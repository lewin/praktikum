#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 14:32:36 2020

@author: lbo
"""

import lib

import math

import numpy as np
import matplotlib.pyplot as plt
import pandas
import praktikum.analyse as analyse
import praktikum.literaturwerte as lw

ISOTHERM_DATA = ['../daten/isotherm_kalt.csv', '../daten/isotherm_warm.csv']
ISOCHOR_DATA = ['../daten/isochor.csv']

def read_isotherm(file):
    """Read CSV file and return columns (numpy arrays): V/m^3, p/Pa"""
    d = pandas.read_csv(file)
    return (np.array(d['V_echt(m3)'].values,dtype=np.float32), np.array(d['p(Pa)'].values, dtype=np.float32))

def read_isochor(file):
    """Read CSV file and return columns (numpy arrays): T/Celsius, p/Pa"""
    d = pandas.read_csv(file)
    return (np.array(d['T/C'].values, dtype=np.float32), np.array(d['p/Pa'].values, dtype=np.float32))

def regress_isotherm(f):
    """Plot isothermal curves and return linreg parameters."""
    vol, pres = read_isotherm(f)


    fig = plt.figure(dpi=300, figsize=(12, 4))
    p = fig.add_subplot(122)
    p.set_xlabel('V0 / ml')
    p.set_ylabel('1/p / 1/mbar')
    p.set_title('1/p über V_0')
    p.grid(True)
    p2 = fig.add_subplot(121)
    p2.set_xlabel('V0 / ml')
    p2.set_ylabel('p / mbar')
    p2.set_title('p über V_0 (Isotherme)')
    p2.grid(True)

    # From here on: Use mbar/ml
    vol *= 1e6
    pres /= 1e2
    presinv = 1/pres

    # Estimate errors on pressure, volume
    preserr = np.array(3/math.sqrt(12)*np.ones(pres.shape))
    presinverr  = preserr / (pres**2)
    verr = np.array(0.1*np.ones(vol.shape))

    p.errorbar(vol, presinv, xerr=verr, yerr=presinverr, fmt='.')
    p2.errorbar(vol, pres, xerr=verr, yerr=preserr, fmt='.')

    parameters = analyse.lineare_regression_xy(vol, presinv, verr, presinverr)
    (a, ea, b, eb, chisq, corr) = parameters
    print('Isotherm regression:', parameters)

    # Residuals: 1/p = a V0 + b
    resid = presinv - (a*vol+b)
    residerr = np.sqrt(presinverr**2 + (a * verr)**2)

    lib.plot_with_residue(vol, presinv, verr, presinverr, resid, residerr,
                          xlabel='V0 / ml', ylabel='1/p / 1/mbar',
                          m=a, b=b) # draw linear regression

    # Shift method: vertical
    plus_params = analyse.lineare_regression_xy(vol, presinv+presinverr, verr, presinverr)
    minus_params = analyse.lineare_regression_xy(vol, presinv-presinverr, verr, presinverr)
    ea_sys = (plus_params[1]-minus_params[1])/2
    print('systematic error (y) on a:', ea_sys)

    plus_params = analyse.lineare_regression_xy(vol+verr, presinv, verr, presinverr)
    minus_params = analyse.lineare_regression_xy(vol-verr, presinv, verr, presinverr)
    ea_sys = (plus_params[1]-minus_params[1])/2
    print('systematic error (x) on a:', ea_sys)


    return parameters


def calculate_environment_conditions(parameters, temp, V0_ml=50, V_S=None, V_S_err=None, N=None):
    """Calculates number of particles, molar air mass, air density, boltzmann constant."""
    # Calculate molar mass and density
    humidity = 0.36
    p0 = 986
    t0 = temp
    M_L = lw.molare_masse_luft(humidity, p0, t0) / 1000
    density_L = lw.dichte_luft(humidity, p0, t0)

    # Convert to SI
    a = parameters[0] * 1e4
    ea = parameters[1] * 1e4
    b = parameters[2] / 1e2
    eb = parameters[3] / 1e2
    corr = parameters[5]
    vs = b/a

    # Error on V_S (eq. 3.18)
    vs_err = math.sqrt(vs**2 * ((ea/a)**2 - 2*corr*ea*eb/(a*b) + (eb/b)**2))
    print('V_S =', vs, 'error on V_S:', vs_err)

    vstart = V0_ml * 1e-6 + (V_S or vs)
    vstart_err = math.sqrt((0.1e-6)**2 + (V_S_err or vs_err)**2)
    print(vstart, vstart_err, vs, vs_err)

    # Calculate number of particles
    N_A = 6.022e23
    N = N or ((N_A * vstart * density_L) / M_L)
    # Boltzmann constant
    k_B = 1./(a*N*(273.15+t0))

    # Error on Boltzmann constant
    # vs_err, ea,
    t0_err = 0.1/math.sqrt(12)
    density_err = 0.005
    molar_err = 0.1e-3
    # Propagate error from measured sizes to N and k_B
    N_err = math.sqrt(((N_A*density_L)/M_L * vstart_err)**2 +
                      ((N_A * vstart)/M_L * density_err )**2 +
                      ((N_A*vstart*density_L)/M_L**2 * molar_err)**2)
    kb_err = math.sqrt((1/(a**2*N*t0) * ea)**2 + (1/(a*N**2*t0) * N_err)**2 +
                       (1/(a*N*t0**2) * t0_err)**2)

    print('a = {:.3f} +/- {:.4f} M_L = {:.6f} +/- {:.5f} rho_L = {:.3f} +/- {:.5f} N = {} +/- {} k_B = {} +/- {}'.format(
        a, ea, M_L, molar_err, density_L, density_err, N, N_err, k_B, kb_err))
    return (k_B, kb_err, vs, vs_err, N)


def calculate_isothermic():
    """Run calculations for all isothermic measurements we have"""

    print('Measurement 1 (cold)')
    # We will use N (number of molecules) and V_S (hose volume) from the first measurement,
    # by advice from tutors.
    parameters = regress_isotherm(ISOTHERM_DATA[0])
    (k_B1, kb_err1, V_S, V_S_err, N) = calculate_environment_conditions(parameters, 22.2)

    print('')
    print('Measurement 2 (hot)')
    # Reuse V_S, N from first run!
    parameters = regress_isotherm(ISOTHERM_DATA[1])
    (k_B2, kb_err2, V_S_, V_S_err_, N_) = calculate_environment_conditions(parameters, 80.5, V_S=V_S, V_S_err=V_S_err, N=N)

    kb_theoretical = 1.38065e-23
    kb_isochor = 1.27e-23
    kb_isochor_err = 0.04e-23
    kb_final = 1.381e-23
    kb_final_err = 0.04e-23

    plot_kb([kb_theoretical, kb_final, k_B1, k_B2, kb_isochor],
            [0, kb_final_err, kb_err1, kb_err2, kb_isochor_err],
            ['theorie', '(endergebnis)', '(kalt)', '(heiss)', '(isochor)'])

def plot_kb(values, errs, labels):
    """Plot k_b measurements vertically separated with error bars"""
    fig = plt.figure(dpi=300, figsize=(9,3))
    p = fig.add_subplot(111)
    p.set_xlabel('J / K')
    p.set_title('Messungen k_B (vertikale Trennung für Übersicht)')
    p.grid(True)

    ys = np.linspace(0, 1, len(values))
    p.errorbar(values, ys, xerr=errs, fmt='o')
    for i in range(len(values)):
        #p.annotate(labels[i], xy=(values[i], ys[i]), xycoords='data')
        p.text(values[i]+2e-25, ys[i]-(1/(4*len(values))), labels[i])

