#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 15:29:07 2020

@author: lbo
"""

import numpy as np
import matplotlib.pyplot as plt

def plot_with_residue(x, y, xerr, yerr, residy, residyerr, m=None, b=None,
                      fmt='.', xlabel='', ylabel='', title='', size=None):
    """Plot errorbar data with residual plot below it. Error arrays can be None.

    Returns:
        (figure, mainaxes, residualaxes) tuple.
    """
    fig = plt.figure(dpi=300, figsize=size)

    main = fig.add_axes([0.1, 0.25, 0.8, 0.7])
    main.set_xlabel(xlabel)
    main.set_ylabel(ylabel)
    main.set_title(title)
    main.grid(True)
    resid = fig.add_axes([0.1, 0.05, 0.8, 0.2])
    resid.spines['bottom'].set_position(('data', 0))
    resid.grid(True)

    main.errorbar(x, y, xerr=xerr, yerr=yerr, fmt=fmt,)
    resid.errorbar(x, residy, yerr=residyerr, fmt=fmt)

    if m:
        b = b or 0
        xs = [x[0], x[-1]]
        ys = [m*x[0]+b, m*x[-1]+b]
        main.plot(xs, ys, 'r-')


    return (fig, main, resid)

def test_resid_plot():
    x = np.array([1, 2, 3, 4, 5, 6, 7])
    y = np.array([3, 4, 2, 3, 1, 5, 6])
    xerr = np.array([1, 1, 1, 1, 1, 1, 1])
    yerr = np.array([0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5])
    residxerr = xerr
    residyerr = yerr
    residy = y - 3.5
    return plot_with_residue(x, y, xerr, yerr, residy, residyerr, title='aaa', ylabel='bbb')