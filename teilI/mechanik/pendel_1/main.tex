\documentclass[a4paper,12pt]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage{xcolor}
\usepackage[german]{varioref}
\usepackage{physics}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{colorlinks,breaklinks,urlcolor=blue,linkcolor=blue}

\usepackage{amssymb}
\usepackage{amsmath}

\usepackage[no-math]{fontspec}
\setmainfont{STIX}
\usepackage[notext]{stix}

\newcommand{\TODO}[1]{{\large \textcolor{red}{\textbf{TODO:}}} \emph{#1}}

\subject{Physikalisches Grundpraktikum\\RWTH Aachen}
\title{Messung der Erdbeschleunigung}
\author{Lewin Bormann/...}
\date{\today}

\begin{document}

\maketitle

\section{Versuchsbeschreibung}

Mit Schwerkraftpendeln, wie z.B. Feder- oder Physikalischen Pendeln, lässt sich die
Schwerebeschleunigung $g$ relativ genau messen. Der Versuch beruht darauf, dass die
Pendelfrequenz $f = 1/T$ für das physikalische und mathematische Pendel linear von der
Erdbeschleunigung abhängt. Misst man also die Frequenz und Pendelparameter (Länge, Masse,
Trägheitsmoment) hinreichend genau, so lässt sich ein entsprechend genauer Wert für die
Erdbeschleunigung ableiten.

\subsection{Grundlagen}

Für jedes Rotationspendel, ob mathematisch oder physikalisch, gilt zunächst \[M = I \ddot
\varphi = - \omega^2 \varphi\]

mit Drehmoment $M$, Auslenkung aus der Nulllage $\varphi$ und Kreisfrequenz $\omega$.
Diese Differentialgleichung zweiter Ordnung folgt allerdings nicht exakt aus der Mechanik
des Pendels.

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.3]{img/fadenpendel_tiplermosca_s427.png}
 % fadenpendel_tiplermosca_s427.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Kräfte am Fadenpendel ($\theta = \varphi$; \emph{aus: "`Physik"', Tipler/Mosca,
7. Aufl., S. 427})}
 \label{fig:fadenkraefte}
\end{figure}

Stattdessen muss beachtet werden, dass wie in der Skizze \vref{fig:fadenkraefte}, die
Relation $F_R = m g \sin(\varphi)$ gilt. Im Gegensatz zum idealen Pendel ist also die
Rückstellkraft bei größeren Winkel ($\gtrsim 14\%$) nicht mehr linear. Für kleine Winkel
kann sie jedoch als näherungsweise linear betrachtet werden. Daraus ergibt sich dann mit
dieser Näherung: \[M = I \ddot \varphi = - m g l \sin(\varphi) \approx - m g \varphi\]

Für das mathematische Pendel, also eine Punktmasse an einem masselosen Faden, ist $I = m
l^2$ (Abstand Aufhängung - Masse $l$), während beim physikalischen Pendel keine
Einschränkungen bezüglich der Form existieren, $l = l_S$ dann der Abstand zwischen
Drehachse und dem Schwerpunkt (Massemittelpunkt) ist und $I$ das Trägheitsmoment bezogen
auf den Aufhängungspunkt.

Mit $M_M = I \ddot \varphi = m l^2 \ddot \varphi$ für das mathematische Pendel oder $M_P
= I \ddot \varphi$ ist dann \[\ddot \varphi = - \frac{g}{l} \varphi\] und für das
physikalische Pendel allgemein \[\ddot \varphi = - \frac{m g l_s}{I} \varphi\]

Es lässt sich also identifizieren: $\omega_M^2 = g/l$ und $\omega_P^2 = \frac{m g
l_s}{I}$ für das mathematische bzw. physikalische Pendel. Hierin ist bereits $g$
enthalten, das später gesucht wird. Die Schwingungsdauer $T$ ist bestimmt durch $T = 2\pi
/ \omega$.

Die Lösung für eine solche DGL ist \[\varphi(t) = a \cos(\omega t) + b \sin(\omega t),\]
wobei $a$ und $b$ durch die Anfangsbedingungen bestimmt sind. Ist $\varphi(t=0) =
\varphi_{max}$, dann ist also offensichtlich $a = \varphi_{max}$ und $b = 0$.

\subsection{Bestimmung des Trägheitsmoments}

Da das mathematische Pendel nicht oder nur annähernd existiert, müssen wir mit dem
physikalischen Pendel rechnen. Die größte Schwierigkeit ist hierbei die Bestimmung des
Trägheitsmoments $I$; für eine einfache Stange mit Masse lässt sich das noch annähernd
erreichen, in unserem Fall kommt aber noch ein komplexer Kopf mit Aufhängung hinzu.

Der in der Praktikumsanleitung beschriebene Weg ist daher praktisch, um diese
Schwierigkeit zu umgehen, und stattdessen durch eine zweite Frequenzmessung ersetzt. Dazu
messen wir zunächst die Schwingungsfrequenz $\omega_s$ der Stange alleine, d.h. ohne
angehängte Masse. Setze nun das Direktionsmoment (Rückstellmoment) der "`nackten"'
Stange auf $D_s = m_s g l_s$. Dann setzt man wieder die Pendelmasse $m_p$ an die Stange,
und verschiebt sie, bis das Pendel bei einem Abstand $l_p$ der Masse von der
Rotationsachse die gleiche Frequenz $\omega_p = \omega_s$ aufweist. Da nun für die Stange
allein $\omega_s^2 = D_s/I_s$ und für die Pendelmasse $\omega_p^2 = D_p/I_p = \omega_s^2$
gilt, ist auch $D_p/D_s = I_p/I_s$. Da das Verhältnis gleich ist, gilt also Gleichung
(1.11) in der Anleitung: \[\omega^2 = \frac{D_s + D_p}{I_s + I_p} = \omega_s^2 \frac{1 +
D_p/D_s}{1 + I_p/I_s} = \omega_s^2\].

Da nun das mathematische Pendel der Masse $m_p$ unbeeinflusst von der Stange ist, lässt
sich das Trägheitsmoment mit dem Satz von Steiner zu \[I_p = \frac{m_p r_p^2}{2} + m_p
l_p^2\] festlegen. Mit $\omega^2 = \omega_p^2 = D_p/I_p$ ist dann \[\omega^2 = \frac{m_p
g l_p}{1/2~m_p r_p^2 + m_p l_p^2}\] (Gleichung (1.13) in der Anleitung)

Umgestellt wird dann

\begin{equation}
g = \frac{\omega^2 (1/2~r_p^2 + l_p^2)}{l_p} = \omega^2 l_p \left(\frac{r_p^2}{2 l_p^2} +
1\right)
\end{equation}

zu Gleichung (1.14).

\section{Versuchsaufbau}

\TODO{Fotos einfügen}

\emph{Genaue Beschreibung der verwendeten Aufbauten unter Verwendung von Skizzen oder
Photos.}

\section{Versuchsdurchführung}

\emph{Beschreibung der Messwerterfassungseinstellungen (eingestellte Messzeiten,
Messbedingungen, Trigger, Anzahl der Messungen) und der Durchführung der Versuche.}

\section{Auswertung}

\subsection{Rohdaten}

\emph{Als Tabelle, o.ä.}

\begin{table}[h]
\centering
\begin{tabular}{lll}
X & Y & Z\\
\hline
1 & 1 & 1\\
2 & 1.5 & 1.33\\
3 & 2 & 1.66\\
87 & 22 & 21.2
\end{tabular}
\caption{test}
\end{table}

\subsection{Modell}

\emph{Welches Modell wurde verwendet? Vor-/Nachteile, Modellanpassung, tabellarische oder
grafische Form}

\subsection{Analyse}

\emph{Fehlerrechnung, Residuenplots, Fehlerquellen}

\subsection{Endergebnisse}

\emph{nur signifikante Stellen!}

\subsection{Diskussion}

\emph{Wie passen die Ergebnisse mit Vorhersagen und Literaturwerten zusammen? Wieso sind
sie nicht gleich? Was kann verbessert werden?}

\end{document}
