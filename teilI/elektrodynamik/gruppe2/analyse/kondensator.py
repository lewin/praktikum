#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 17:27:51 2020

@author: lbo
"""

import nice_plots
from praktikum import analyse, cassy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import pandas

from collections import namedtuple
import math

# Run in `analyse` directory.

CHARGE_FILES = ['../data/kondensator/1000ohm10mf{}aufladung.lab'.format(i) for i in range(1,5)]
DISCHARGE_FILES = ['../data/kondensator/1000ohm10mf{}entladung.lab'.format(i) for i in range(1,5)]

def cassyinfo(f):
    cassy.CassyDaten(f).info()

ChargeData = namedtuple('ChargeData', ('t', 'I', 'U'))

def extract_data(f):
    """Return rows from CASSY file as ChargeData"""
    cd = cassy.CassyDaten(f)
    assert cd.anzahl_messungen() == 1
    return ChargeData(cd.messung(1).datenreihe('t').werte,
                      cd.messung(1).datenreihe('I_A1').werte,
                      cd.messung(1).datenreihe('U_B1').werte)

def plot_raw_cap(files, discharge=False, use_first_fraction=1/4, U_off=0.04):
    """Just plot raw data for all measurements.

    Arguments:
        files: CASSY file paths.
        discharge: True if looking at a discharging capacitor, otherwise False.
        use_first_fraction: Only use this fraction of data, by time. 1/4 => only use the first quarter of measurements
        U_off: absolute voltage offset, discovered empirically. Final value of discharging capacitor.
    """
    fig = plt.figure(figsize=(20,15), tight_layout={'h_pad': 0.4})
    base = 421

    for i, f in enumerate(files):
        (t, I, U) = extract_data(f)
        U = U - U_off
        U_max = U[-1]

        t = t[0:int(t.size*use_first_fraction)]
        I = I[0:int(I.size*use_first_fraction)]
        U = U[0:int(U.size*use_first_fraction)]

        p = fig.add_subplot(base+2*i)
        p.set_xlabel('t / ms')
        p.set_ylabel('U / V')
        p.grid(True)
        p.plot(t*1e3, U, 'b')

        iscale = p.twinx()
        iscale.set_ylabel('I / mA')
        iscale.plot(t*1e3, I*1e3, 'r')

        # Proxy artists
        red = lines.Line2D([], [], color='red')
        blue = lines.Line2D([], [], color='blue')
        p.legend(handles=(blue, red), labels=('U', 'I'),
                 loc='center right')

        if discharge:
            logU = np.log(U)
        else:
            logU = np.log(np.abs(U_max - U))
        p = fig.add_subplot(base+2*i+1)
        p.set_ylabel('ln(U / V)')
        p.set_xlabel('t / ms')
        p.plot(t*1e3, logU, 'b')
        p.legend(handles=(blue,), labels=('U',),
                 loc='center right')

def capacitor_linreg(file, discharge=False, use_first_fraction=1/4,
                     U_measure_max=10, R=991.6, sR=10.94, ssysR=12.8, step=50,
                     save=None):
    (t, I, U) = extract_data(file)

    # U_limit is the voltage reached for long times.
    # For discharge: This is the measurement offset of the device.
    # For charge: This is U_0.
    U_limit = np.mean(U[-50:-1])
    sU_limit = max(np.std(U[-50:-1], ddof=1), 10/2048)
    print('Voltage limit: {} +/- {}'.format(U_limit, sU_limit))

    t = t[10:int(t.size*use_first_fraction):step]
    I = I[10:int(I.size*use_first_fraction):step]
    U = U[10:int(U.size*use_first_fraction):step]

    logU = np.log(abs(abs(U)-abs(U_limit)))

    # Statistical errors: Combine errors on U_limit and U. 0.005 is the
    # digitization error.
    sigmaU = U*np.sqrt((0.005)**2 + (sU_limit)**2)
    sLogU = (sigmaU/U)  # See widerstaende.py: preprocess_raw_data()

    a, ea, b, eb, chiq, corr = nice_plots.nice_regression_plot(
        t*1e3, logU,
        xerror=np.ones(logU.shape)*1e-6, yerror=sLogU,
        ylabel='ln(U / V)', xlabel='t / ms',
        ylabelresidue='ln(U / V) - (a t + b) / V',
        save=save)

    # Shift method using systematic error in U.
    sysU = (0.01*U + 0.005*U_measure_max)/math.sqrt(3)
    sysLogU = sysU/U
    plus_params = analyse.lineare_regression(t*1e3, logU+sysLogU, sLogU)
    minus_params = analyse.lineare_regression(t*1e3, logU-sysLogU, sLogU)
    sys_ea = (abs(plus_params[0]-a)+abs(minus_params[0]-a))/2
    print('systematic error on a: sys_ea =', sys_ea)

    # Propagate systematic and statistical errors
    cap = -1e3/(R*a)
    ecap = abs(1e3*ea/(a**2*R))
    sysecap = math.sqrt((1e3*ssysR/(R**2*a))**2 + (1e3*sys_ea/(a**2*R))**2)
    print('Calculated capacity: C = -1/(m R) = {:.3f} +/- {:.3f} (stat) +/- {:.3f} (sys) µF'.format(
        cap, ecap, sysecap))
    print()

def plot_cap_results(f='weighted.csv'):
    csv = pandas.read_csv(f)
    val = csv['Messungen'].values
    sc = csv['sigma'].values
    ns = np.arange(1, val.size+1)
    mid = csv['weighted'].values
    print(val, sc, ns, mid)

    fig = plt.figure(figsize=(20,8))
    sp = fig.add_subplot(111)
    sp.grid(True)
    sp.errorbar(ns, val, fmt='bo', yerr=sc, label='Messungen',)
    sp.set_xlabel('Messung (n)')
    sp.set_ylabel('C / µF')
    sp.plot(ns, mid, 'r', label='Gew. Durchschn.')
    sp.legend()