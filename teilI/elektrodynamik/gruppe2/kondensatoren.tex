\section{Charakterisierung: Kondensator}
\label{sec:kondensator}

\subsection{Versuchsbeschreibung}

\subsubsection{Ziel}

Bei der Charakterisierung messen wir die Kapazität eines Kondensators, indem wir die
Ladungs- und Entladungskurven bei der Entladung über einen Widerstand betrachten. Die
Kapazität kann über die Zeitkonstante $\tau = RC$ bestimmt werden, mit der sich der
Kondensator auf- und entlädt. Mit dem bekannten Widerstand $R$ kann $C$ bestimmt werden.

Der hier ausgewählte Widerstand $R$ wird Unterversuch (\autoref{sec:widerstaende}) genau
gemessen. Entgegen der Reihenfolge dieses Protokolls wurde dieser Versuch zuerst
durchgeführt, da dieser Versuch vom absoluten Wert des Widerstands abhängt, der uns eine
genaue Messung erlauben soll.

\subsubsection{Grundlagen}

Für den Schaltkreis \autoref{fig:circuitentladung} gilt nach der Maschenregel jeweils mit
$C = Q/U$:

\begin{align}
 U_C + U_R &= U_0 \qq{(Aufladung)} \\
 U_C(t) + R I &= U_0 \\
 U_C(t) + R C \dv{U_C(t)}{t} &= U_0
 \label{eqn:dglaufladung}
\end{align}

und ganz analog

\begin{align}
 U_C + U_R &= 0 \qq{(Entladug)} \\
 U_C(t) + R \dv{Q}{t} &= 0 \\
 U_C(t) + R C \dv{U_C}{t} &= 0
 \label{eqn:dglentladung}
\end{align}

Die beiden DGLs werden gelöst durch

\begin{align}
 \text{\autoref{eqn:dglaufladung}: } U_C(t) &= U_0 (1 - e^{-\frac{t}{RC}}) = U_0
(1-e^{-t/\tau})\\
 \text{\autoref{eqn:dglentladung}: } U_C(t) &= U_0 e^{-\frac{t}{RC}} = U_0 e^{-t/\tau}
\end{align}

mit $\tau := R C$ als der Zeitkonstante, d.h. der Zeit, nach der die Spannung auf $1/e$
der Quellspannung $U_0$ abgefallen bzw. auf $\frac{e-1}{e}$ angestiegen ist.
Die Zeitkonstante $\tau$ kann gemessen werden, indem die Spannungswerte zu zwei
Zeitpunkten $t_1$, $t_2$ verglichen werden:

\begin{align}
 \ln(U_C) = -\frac{t}{RC} &\Rightarrow \ln(\frac{U_2}{U_1}) = - \frac{t_2 - t_1}{R C} \\
 &\Rightarrow RC = \frac{t_1 - t_2}{\ln{U_2/U_1}} \hat = - \frac{\Delta t}{\Delta
(\ln(U))} \\
 &\Rightarrow \frac{1}{RC} = - \frac{\Delta \ln(U)}{\Delta t} =: a
\label{eqn:linregherleitung}
\end{align}

\autoref{eqn:linregherleitung} kann als die Steigung $a$ der Geraden $a t + b$ einer
linearen Regression über die gegen die Zeit aufgetragene logarithmierte Spannung
betrachtet werden.

\subsection{Versuchsaufbau}

\begin{figure}[h]
\centering
\begin{subfigure}{0.49\textwidth}
\begin{circuitikz}[european resistors]
\draw (0,0) -- (0,2) to[battery1, l=$U_0$] (0,3) -- (0,6) -- (2,6) to[C, l=$C$] (2,4) --
(2,2) to[R,
l=$R$] (2,0) -- (0,0);
% U_C
\draw (2,6) -- (4,6) to[voltmeter, l=$U_C$] (4,4) -- (2,4);
% U_R
\draw (2,3) to[ammeter, l=$I$] (2,2);
% button
\draw (0,2) -- (-2,2) to[push button, l={B}] (-2, 3) -- (0,3);
\end{circuitikz}
\caption{Schaltkreis für Messung der Ladung/Entladung.}
\label{fig:circuitentladung}
\end{subfigure}
\begin{subfigure}{0.49\textwidth}
 \centering
 \includegraphics[scale=0.3]{img/kondensator_aufbau_cassy.jpg}
 % kondensator_aufbau_cassy.jpg: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Aufbau des RC-Schaltkreises auf Rasterbrett.}
 \label{fig:rcaufbauphoto}
\end{subfigure}
\caption{Aufbau: RC-Schaltung.}
\label{fig:rcaufbau}
\end{figure}

\begin{table}[h]
\centering
 \begin{tabular}{ll}
  Gegenstand & Anzahl \\
  \hline
  \rule{0pt}{2.5ex}Sensor CASSY & 1 \\
  Oszilloskop & 1 \\
  Widerstand, \SI{1}{\kilo\ohm} & 1 \\
  Kondensator, \SI{10}{\mu\farad} & 1 \\
  Paar Kabel rot/blau, \SI{50}{\centi\meter} & 3 \\
  Taster (Knopf B) & 1 \\
 \end{tabular}
 \caption{Benötigte Gegenstände.}
 \label{tab:kondensatorinventar}
\end{table}

Der Aufbau in \autoref{fig:rcaufbau} setzt das Schaltbild \autoref{fig:circuitentladung}
um.  Der Knopf B schließt die Spannungsquelle kurz und entlädt den Kondensator. Zur
Beobachtung der Aufladung wird der Knopf gedrückt gehalten und bei Beginn der Messung
losgelassen. Um den Kondensator zu entladen, wird die Messung beim Drücken des Knopfes
gestartet.

Zunächst wurde der Aufbau an das Oszilloskop angeschlossen, wobei ein Kanal die Spannung
$U_C$ und ein anderer Kanal den Spannungsabfall an $R$ $U_R$ misst (letzterer als
indirekte Größe, aus der der Strom berechnet werden kann). Am CASSY wurde der Kreis dann
wie in \autoref{fig:circuitentladung} mit den Spannungs- und Stromeingängen am CASSY
verbunden.

Die Trigger wurden jeweils so eingestellt, dass die Messung bei Unterschreiten einer
Triggerspannung (abfallende Flanke) für die Entladung, bzw. Überschreiten einer
Triggerspannung (steigende Flanke) für die Ladung, gestartet wird. Beim Oszilloskop wird
dabei der komplette Prozess aufgezeichnet, bei CASSY nur ab dem Triggerzeitpunkt.

\subsection{Versuchsdurchführung}
\label{sec:kondensatordurchfuehrung}

Aus den ersten Messungen mit dem Oszilloskop ließ sich in der Schnellauswertung prüfen,
ob wir erwartete Werte messen, und ob der Tastschalter den Kondensator korrekt
auf-/entlädt. Außerdem wurde hier ausgesucht, welcher Widerstand $R$ eingesetzt werden
sollte; wir haben $R = \SI{1}{\kilo\ohm}$ gewählt, um eine möglichst langsame
(Ent-)Ladung zu erreichen, die man entsprechend besser messen kann. Mit dem gewählten
Kondensator $C \approx \SI{10}{\mu\farad}$ erhalten wir eine ungefähre Zeitkonstante $\tau
= RC = \SI{10}{\milli\second}$; in dieser Größenordnung werden sich also Lade- und
Entladevorgang abspielen.

Nach Bestätigung der Funktionstüchtigkeit des gebauten Schaltkreises schlossen wir die
Kabel zur Spannungsmessung an den Spannungs- und Stromeingang des Sensor-CASSY an. Die
Strommessung, die am Oszilloskop über den Spannungsabfall am Widerstand $R$ durchgeführt
wurde, wird nun über das Amperemeter am Eingang A des CASSY erreicht.

Am CASSY wurden Messreihen der gesamten Entladung/Aufladung in einem Messbereich
0..\SI{10}{\volt} bei einem Messintervall von \SI{10}{\mu\second}, einer Messdauer
von \SI{160}{\milli\second}, und \num{16000} Messpunkten aufgezeichnet. Das ist etwa
viermal länger, als die eigentliche Auf- und Entladung dauerte, und entspricht der
höchsten Samplerate, die CASSY unterstützt. Die gleiche Samplerate wurde bei der Messung
des Widerstands (\autoref{sec:widerstaende}) eingesetzt, um vergleichbare Ergebnisse zu
messen.

Verlustfaktoren, wie in Kapitel 4.5 der Versuchsanleitung beschrieben, treten in diesem
Versuch nicht auf, da es sich um Gleichstrom handelt und die Umpolung nur einmal
stattfindet.

\subsection{Auswertung}

\subsubsection{Rohdaten}

In \autoref{fig:kondensatorroh} sind die erfassten Daten grafisch dargestellt. Die vier
Messungen unterscheiden sich jeweils nur minimal, und wären in einem Plot dargestellt
visuell nicht unterscheidbar. Es sind jeweils etwa das erste 1/4 der erfassten Daten
aufgetragen, um "`Rauschen"' in der logarithmischen Darstellung zu vermeiden, das
auftritt, wenn gegen Ende der Messung nur das Rauschen des Voltmeters gemessen wird (dies
ist links unten auf den Plots der rechten Seite bereits zu erahnen). Auf der gleiche
Messpunktmenge findet die Berechnung der linearen Regression im nächsten Abschnitt statt.

Die Reihenfolge der Rohdatenplots bestimmt auch im weiteren Text die Nummerierung, z.B.
"`Entladevorgang Nr. 3"'.

\begin{figure}[p]
\vspace{-3em}
\centering
\begin{subfigure}{\textwidth}
 \centering
 \includegraphics[scale=0.3]{img/kondensator_aufladen_rohdaten.png}
 % kondensator_aufladen_rohdaten.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Rohdaten der vier Messungen für die Aufladung des Kondensators.}
 \label{fig:kondensatoraufladenroh}
\end{subfigure}
\begin{subfigure}{\textwidth}
 \centering
 \includegraphics[scale=0.3]{img/kondensator_entladen_rohdaten.png}
 % kondensator_aufladen_rohdaten.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Rohdaten der vier Messungen für die Entladung des Kondensators.}
 \label{fig:kondensatorentladenroh}
\end{subfigure}
\caption{Messdaten für Auf-/Entladung, rechts mit Spannung logarithmisch aufgetragen.}
\label{fig:kondensatorroh}
\end{figure}

\subsubsection{Modell}

Rechts ist das in \autoref{eqn:linregherleitung} angedeutete lineare Verhältnis
aufgetragen, das uns die Nutzung einer linearen Regression erlaubt:

\begin{align}
 \frac{1}{RC} &= - \frac{\Delta (\ln(U))}{\Delta t} =: a \qq{mit} \\
 (\ln(U))(t) &= a t + b
\end{align}

\subsubsection{Analyse}

\paragraph{Messunsicherheiten}
\label{sec:kondensatorunsicherheit}

Für die Unsicherheit $\sigma_U$ von $U$ benutzen wir die im Widerstandsmessungsversuch
beobachtete statistische Schwankung von U. Diese beträgt, unabhängig vom absoluten Wert,
$\sigma_U \leq \SI{5}{\milli\volt}$ (in diesem Fall ist das unser
Digitalisierungsfehler, da die statistische Unsicherheit kleiner ist als diese). Außerdem
mussten zwei Arten von Offset korrigiert werden:

\begin{itemize}
 \item Bei der \textbf{Entladung} stellt sich die Spannung am Kondensator auf einen Wert
leicht größer als 0 ein; in unseren Versuchen konsistent etwa $U_{lim} =
\SI{0.04(2)}{\volt}$. Der genaue Wert mit Standardabweichung $\sigma_{U_{lim}} =
\max(\sigma_{U}, \sigma_{U, digit})$ für die jeweilige Auswertung wird durch Betrachtung
der letzten 50 Messwerte jeder Reihe bestimmt. Die zu analysierende Spannung $U'$ ist
dann gegeben durch $U' = U_C - U_{lim}$, und das Digitalisierungsintervall ist
$\sigma_{U, digit} = \SI{10}{\volt} / 2048 \approx 0.005$, da CASSY über das
Messintervall eine Auflösung von 12 bit abzüglich Vorzeichen hat, sodass $2^11 = 2048$
Schritte übrig bleiben. Eine genauere Auflösung als diese anzugeben ist unsinnig.

 \item Bei der \textbf{Aufladung} stellt sich die Spannung $U_C \longrightarrow_{t
\rightarrow \infty} U_0 \equiv U_{lim}$ ein. Um mit logarithmischer Auftragung eine
Gerade zu erhalten, muss aber $U' = U_{lim} - U_C$ betrachtet werden (s.
\autoref{fig:kondensatoraufladenroh}). Die Spannung $U_0$ und ihre stat. Unsicherheit
$\sigma_{U_{lim}}$ wurden wiederum ermittelt durch den Mittelwert und die
Stichprobenstandardabweichung der letzten 50 Messwerte jeder Reihe, wie bei der Entladung
im Punkt oben.
\end{itemize}

Die tatsächlich analysierte Spannung $U'$ wird in unserer statistischen Auswertung
konkret berechnet durch

\[
U' = \abs{\abs{U} - \abs{U_{lim}}} = \abs{U - U_{lim}} = U - U_{lim} \qq{(hier)}
\]

Diese Methode beseitigt jegliche Offsets, die vom Messgerät oder dem Versuchsaufbau
stammen können, da die "`Ruhespannung"' $U_{lim}$ berücksichtigt wird. Die
Standardabweichung von $U'$, $\sigma_{U'}$, berechnet sich durch quadratische Addition:

\begin{equation}
\sigma_{U'} = \sqrt{\sigma_{U_{lim}}^2 + \sigma_{U_C}^2}
\label{eqn:totaluerror}
\end{equation}

Damit erhält $U'$ unabhängig von Lade-/Entladevorgang die Form einer negativen
Exponentialfunktion $e^{-t/\tau}$, anhand derer wir die Zeitkonstante ermitteln können.
Da wir hierzu nicht $U'$  benutzen, sondern $\ln(U')$, pflanzt sich $\sigma_U$ fort mit
$\sigma_{\ln(U')} = \sigma_{U'} / U'$ ($\sigma_{U'}$ aus \autoref{eqn:totaluerror}).

Aus der beschriebenen Auftragung erhalten wir nach einer linearen Regression (siehe
nächster Abschnitt) die Steigung $a = -1/(RC)$, die statistische Unsicherheit der
Steigung $\sigma_a$, und einen systematische Fehler der Steigung $\sigma_{a, sys}$.
Letzteren gewinnen wir, indem wir die Verschiebemethode anwenden. Dabei wird $\ln(U)$ um
$\pm \frac{\sigma_{U,sys}}{U}$ verschoben (das ist die Herstellerangabe des
systematischen Fehlers $\sigma_{U,sys}$ fortgepflanzt auf $\ln(U)$). Für die beiden
verschobenen Geraden berechnen wir jeweils die lineare Regression mit den Steigungen
$a_+$/$a_-$, und erhalten den systematischen Fehler

\[
\sigma_{a,sys} = \frac{\abs{a_+ - a} + \abs{a_- - a}}{2}
\]

Die Kapazität $C$ des Kondensators ist gegeben durch

\[
C = -\frac{1}{a R}
\]

wobei der Widerstand $R$ in \autoref{sec:widerstaende} gemessen wurde. Die statistische
Unsicherheit ist dann

\[
\sigma_{C, stat} = \frac{\sigma_a}{a^2 R}
\]

Der systematische Fehler setzt sich zusammen aus dem systematischen Fehler von R
$\sigma_{R, sys}$ und dem mittels der Verschiebemethode ermittelten systematischen Fehler
$\sigma_{a, sys}$:

\[
\sigma_{C, sys} = \sqrt{\left(\frac{\sigma_{R, sys}}{a R^2}\right)^2 +
\left(\frac{\sigma_{a, sys}}{a^2 R}\right)^2}
\]

Die jeweiligen Unsicherheiten sind in \autoref{tab:kondensatorregressionen} beschrieben.
Die Messung von $I$ benutzen wir nicht. Da der auftretende Strom sehr klein war, siehe
Rohdaten, ist auch die Messunsicherheit verhältnismäßig groß. Das ist auch am Rauschen in
den Rohdaten zu sehen. Wir betrachten die Unsicherheit auf die Zeit nicht: $\sigma_t = 0$.

\paragraph{Regressionen}

Aus den acht Messungen (vier Entladevorgänge, vier Aufladevorgänge) präsentieren wir hier
jeweils eine ausgewählte grafisch in \autoref{fig:kondensatorregression}. Die anderen
Regressionen sind anhand ihrer Parameter in \autoref{tab:kondensatorregressionen}
aufgeführt, und unterscheiden sich nur wenig.

Die lineare Regression wurde mit jedem 50. Messpunkt im ersten 1/4 der Zeit berechnet.
Dies liegt an der extrem hohen Samplingrate, die wir gewählt haben, die viel redundante
Information speichert. Die Einbeziehung von weiteren Punkten, siehe
\autoref{fig:kondensatorregressiondicht}, macht nur systematische Effekte sichtbar,
ändert die berechneten Regressionsparameter aber nur unwesentlich.

Die Kapazität wurde aus der Zeitkonstante $\tau$ mit dem in \autoref{sec:widerstaende}
bestimmten Widerstand berechnet:

\[R = \SI[parse-numbers=false]{991.6 \pm 10.94~(stat) \pm 12.8~(sys)}{\ohm}\]

Eine Kurzmessung an der Messbrücke ergab als Vergleichswert eine Kapazität von $C_{MB} =
\SI{10.25(3)}{\mu\farad}$.

\begin{figure}[h]
 \begin{subfigure}{\textwidth}
  \hspace{-4em}
  \includegraphics[scale=0.38]{img/kondensator_entladen_regression_3.png}
  \caption{Entladevorgang Nr. 3}
 \end{subfigure}
 \begin{subfigure}{\textwidth}
  \hspace{-4em}
  \includegraphics[scale=0.38]{img/kondensator_aufladen_regression_2.png}
  \caption{Aufladevorgang Nr. 2}
 \end{subfigure}
 \caption{Grafische Darstellung der linearen Regressionen für RC-Bestimmung mit $\ln(U)
= a t + b$, $a = -1/(RC)$.}
 \label{fig:kondensatorregression}
\end{figure}

\begin{table}[h]
 \begin{subfigure}{\textwidth}
\hspace{-2em}
 \begin{tabular}{r|ccc|c}
  $n$ & $a / \si{\per\ohm\per\farad}$ & $b / 1$ & $\chi^2/\text{ndof}$ & $C = (-1/aR) /
\si{\mu\farad} $
\\
\hline
\rule{0pt}{2.5ex}1 & \num{-0.0974(1)} & \num{2.186(2)} & \num{57.0}/\num{78} = \num{0.73}
&
\SI[parse-numbers=false]{10.353 \pm~0.007~(stat)~\pm~0.330~(sys)}{\mu\farad} \\
  2 & \num{-0.0976(1)} & \num{2.188(2)} & \num{115.8}/\num{78} = \num{1.48} &
\SI[parse-numbers=false]{10.330 \pm~0.007~(stat)~\pm~0.330~(sys)}{\mu\farad} \\
  3 & \num{-0.0973(1)} & \num{2.184(2)} & \num{94.3}/\num{78} = \num{1.21} &
\SI[parse-numbers=false]{10.365 \pm~0.007~(stat)~\pm~0.330~(sys)}{\mu\farad} \\
  4 & \num{-0.0975(1)} & \num{2.186(2)} & \num{40.6}/\num{78} = \num{0.52} &
\SI[parse-numbers=false]{10.348 \pm~0.007~(stat)~\pm~0.330~(sys)}{\mu\farad} \\
\hline
& \multicolumn{3}{c|}{\rule{0pt}{2.5ex}$\sigma_{a, sys} =
\SI{0.003}{\per\ohm\per\farad}$}
& \\
\end{tabular}
 \caption{Entladevorgänge. $\sigma_{a,sys}$ ist im Rahmen der signifikanten Stellen für
alle Messungen gleich.}
 \end{subfigure}
 \begin{subfigure}{\textwidth}
\vspace{2em}
\hspace{-2em}
 \begin{tabular}{r|ccc|c}
%   $n$ & $a / \si{\per\ohm\per\farad}$ & $b / 1$ & $\chi^2/\text{ndof}$ & $C = -1/aR$
% \\ \hline
  5 & \num{-0.0975(1)} & \num{2.202(2)} & \num{33.8}/\num{78} =
\num{0.43} &
\SI[parse-numbers=false]{10.340 \pm~0.006~(stat)~\pm~0.171~(sys)}{\mu\farad} \\
  6 & \num{-0.0974(1)} & \num{2.201(2)} & \num{51.9}/\num{78} = \num{0.67} &
\SI[parse-numbers=false]{10.354 \pm~0.006~(stat)~\pm~0.171~(sys)}{\mu\farad} \\
  7 & \num{-0.0976(1)} & \num{2.202(2)} & \num{32.4}/\num{78} = \num{0.42} &
\SI[parse-numbers=false]{10.337 \pm~0.006~(stat)~\pm~0.171~(sys)}{\mu\farad} \\
  8 & \num{-0.0976(1)} & \num{2.203(2)} & \num{49.2}/\num{78} = \num{0.63} &
\SI[parse-numbers=false]{10.337 \pm~0.006~(stat)~\pm~0.171~(sys)}{\mu\farad} \\
\hline
& \multicolumn{3}{c|}{\rule{0pt}{2.5ex}$\sigma_{a, sys} =
\SI{0.001}{\per\ohm\per\farad}$}
& \\
 \end{tabular}
 \caption{Aufladevorgänge. $\sigma_{a,sys}$ ist im Rahmen der signifikanten Stellen für
alle Messungen gleich.}
 \end{subfigure}
 \caption{Regressionen: Parameter}
 \label{tab:kondensatorregressionen}
\end{table}

\begin{figure}[h]
\centering
 \begin{subfigure}{\textwidth}
  \hspace{-4em}
  \includegraphics[scale=0.38]{img/kondensator_entladen_regression_3_fine.png}
  \caption{Entladevorgang Nr. 3, mit jedem zehnten Messpunkt}
 \end{subfigure}
 \begin{subfigure}{\textwidth}
  \hspace{-4em}
  \includegraphics[scale=0.38]{img/kondensator_aufladen_regression_3_fine.png}
  \caption{Aufladevorgang Nr. 3, mit jedem zehnten Messpunkt}
 \end{subfigure}
 \caption{Grafische Darstellung wie in \autoref{fig:kondensatorregression}, aber mit
fünfmal mehr Datenpunkten. Man sieht im Residuenplot "`Bananen"' und "`Stufen"', die von
der logarithmierten Digitalisierungsauflösung stammen. Diese wird besonders im letzten
Drittel bei sehr kleinen Spannungen sichtbar.}
 \label{fig:kondensatorregressiondicht}
\end{figure}

\paragraph{Auffälligkeiten}

Die Residuenplots der linearen Regressionen in \autoref{fig:kondensatorregression} weisen
in der ersten Hälfte der Messung eine leicht fallende Tendenz auf. Außerdem werden die
Abweichungen von der Gerade gegen Ende der Messung jeweils viel größer (die Fehlerbalken
auch). Diese beiden Auffälligkeiten sind durch die Logarithmierung verursacht, die bei
kleinen Werten (gegen Ende der Messung) schon für kleine Schwankungen große Abweichungen
ergibt. Diese werden dann übermäßig gewichtet, und drehen die Ausgleichsgerade leicht
gegen den Uhrzeigersinn (da die Abweichungen tendenziell positiv sind), was zur
beobachteten schiefen Korrelation in der Residuenauftragung führt.

\subsubsection{Endergebnisse}

\begin{figure}[H]
 \hspace{-4em}
 \includegraphics[scale=0.38]{img/kondensator_gewichtet_vergleich.png}
 % kondensator_gewichtet_vergleich.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Vergleich der Messungen mit dem gewichteten Durchschnitt}
 \label{fig:kondensatorgewichtetvergleich}
\end{figure}

Wir können aus \autoref{fig:kondensatorregressiondicht} einen gewichteten Mittelwert
berechnen. Dieser ist mit seinen statistischen Unsicherheiten grafisch in
\autoref{fig:kondensatorgewichtetvergleich} dargestellt. Die Messwerte sind nicht
komplett kompatibel, weisen jedoch viele Überschneidungen auf. Die größte Menge
kompatibler Messwerte sind die Messungen ${1, 4, 5, 6, 7, 8}$, deren Fehlerbalken sich
überlappen, sodass wir nur diese zur Berechnung des gewichteten Mittelwerts heranziehen.
Damit erhalten wir einen gewichteten Mittelwert mit $\overline C = \sum_i
\frac{C_i}{\sigma_i^2} / \sum_i \frac{1}{\sigma_i^2}$ und $\sigma_{\overline C} = 1 /
\sum_i \frac{1}{\sigma_i^2}$:

% sigma-stat ist 5.18823529411765E-06
\[\overline C =
\SI[parse-numbers=false]{10,346~\pm0,000~(stat)~\pm0,019~(sys)~}{\mu\farad}\]

Bezüglich des gewichteten Mittelwerts erhalten wir für die sechs gewählten Messungen ein
$\chisq{6.14}{5}{1.23}$ mit $\chi^2 = \sum_i \frac{(C_i - \overline C)^2}{\sigma_i^2}$
und einem Freiheitsgrad.

Die Messbrücke liefert für den gleichen Kondensator eine Kapazität $C_{MB} =
\SI{10.25(3)}{\mu\farad}$. Diese Messung ist nicht direkt kompatibel mit unserer Messung;
sie liegt etwa $3 \sigma_{\overline C, sys}$ unterhalb (sie ist allerdings kompatibel mit
den Einzelmessungen, s. \autoref{tab:kondensatorregressionen}).

\subsubsection{Diskussion}

Die statistischen und systematischen Fehler dieses Versuchs sind relativ klein,
allerdings teilweise zu klein

Ein Grund für die Abweichung vom an der Messbrücke gemessenen Wert ist sicherlich, dass
die Verbindung des Kondensators über ein Rasterboard, das sehr viele Steckverbindungen
und relativ lange Leitungen aufweist, qualitativ nicht besonders hochwertig ist.
Insbesondere im Vergleich zu einem sehr genauen Gerät wie der Messbrücke sind hier
sicherlich Abweichungen zu erwarten; gerade die erhöhte Kapazität lässt sich mit der
Geometrie und fehlenden Eichung des Aufbaus erklären. Zum Beispiel lassen sich die
Stecker als parallel geschaltete Widerstände und Kondensatoren erklären, und in unserem
Aufbau gibt es relativ viele Steckverbindungen (mind. 14). Auch Kabel und Messgeräte
haben gewisse Kapazitäten. Diese fließen in unsere statistisch präzise Messung ein, und
erhöhen den gemessenen Wert.

\begin{figure}[H]
\centering
 \begin{circuitikz}[european resistors]
 \draw (0,0) -- (1,0) -- (1,1) to[C, l=$C$] (4,1) -- (4,0) -- (5,0);
 \draw (1,0) -- (1, -1) to[R, l=$R$] (4,-1) -- (4,0);
 \end{circuitikz}
 \caption{Ersatzschaltbild für Stecker.}
 \label{fig:kondensatorsteckerbild}
\end{figure}

\FloatBarrier
