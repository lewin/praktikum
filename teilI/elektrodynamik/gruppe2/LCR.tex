\section{Gekoppelte LCR-Kreise}

\emph{Dieser Versuch wurde zusammen mit Maximilian Bartel/Norman Thimm durchgeführt.}

\subsection{Versuchsbeschreibung}

Wie man mechanische Pendel durch Federn koppeln kann, so kann man zwei Schwingkreise über
ihre Ströme oder Magnetfelder koppeln. In diesem Versuch werden zwei Schwingkreise über
die Magnetfelder ihrer Spulen gekoppelt. Bei dem im Folgenden "`Schwebung"' genannten
Versuch wird ein Schwingkreis zunächst angeregt, indem sein Kondensator aufgeladen wird;
bei der sich ergebenen Schwingung wird im zweiten Schwingkreis eine Schwingung angeregt,
und die beiden Schwingkreise befinden sich in einer Schwebung.

Bei der "`Kopplung"' werden die Kondensatoren zweier Schwingkreise zunächst aufgeladen,
und die Schwingung möglichst simultan gestartet. Auch diese Schwingkreise sind gekoppelt,
und es stellen sich Wechselwirkungen ein, die im folgenden untersucht werden.

\subsubsection{Ziel}

Wir wollten die Eigenfrequenzen, die Schwebungsfrequenzen, und den Kopplungsgrad zweier
Schwingkreise bestimmen, und zusätzlich die Phasenbeziehungen untersuchen.

\subsubsection{Grundlagen}

\subsection{Versuchsaufbau}

Entlang der Verfügbarkeit der Materialien haben wir zwei identische Schwingkreise mit
Spulen von 500 Wicklungen und $L \approx \SI{9}{\milli\henry}$ und Kondensatoren von
\SI{2.2}{\mu\farad} aufgebaut.

\begin{figure}[h]
\begin{circuitikz}[european resistors, voltage dir=noold]
% base circuit 1
\draw (0,0) -- (0,1) to[capacitor, l=$C_1$] (0,3)
    -- (3,3) to[L, l=$L_1$] (3,0) to[push button] (0,0);
% voltage source 1
\draw (2,0) -- (2,-2) to[battery1, l=$U_{0,1}$] (1,-2) -- (1,0);
% voltmeter 1
\draw (0,1) -- (-2, 1) to[voltmeter, l=$U_{C,1}$] (-2,3) -- (0,3);

% base circuit 2
\draw (8,0) to[push button] (5,0) to[L, l=$L_2$] (5,3) -- (8,3) to[capacitor, l=$C_2$]
(8,1) -- (8,0);
% voltage source 2
\draw (6,0) -- (6,-2) to[battery1, l=$U_{0, 2}$] (7,-2) -- (7,0);
% voltmeter 2
\draw (8,3) -- (10,3) to[voltmeter, l=$U_{C,2}$] (10,1) -- (8,1);
\end{circuitikz}
\caption{Gekoppelte LCR-Kreise. Der Taster schließt jeweils einen Kreis. Gekoppelte
Taster schließen gleichzeitig. Je nach Spannungsquellenausrichtung laufen die
Schwingkreise gleich- oder gegenläufig.}
\label{fig:lcrcircuit}
\end{figure}

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.4]{img/kopplung_spule_cassy_compressed.jpg}
 % kopplung_spule_cassy.jpg: 3016x3488 px, 72dpi, 106.40x123.05 cm, bb=0 0 3016 3488
 \caption{Steckaufbau der gekoppelten Schwingkreise.}
 \label{fig:lcraufbaufoto}
\end{figure}

\subsection{Versuchsdurchführung}

Wir messen die Spannung über \SI{160}{\milli\second} an zwei CASSYs in einem Messbereich
von $\num{-10}..\SI{10}{\volt}$ mit einem Messintervall von \SI{10}{\mu\second}. Die
Messung wird gestartet durch einen Trigger an der steigenden Flanke von $U_{B1}$ bei
$U_{trig} = \SI{1}{\volt}$.

\paragraph{Schwebung}

Zunächst benutzen wir einen Aufbau analog zu Abb. 4.8 in der Anleitung, um die Schwebung
aufzunehmen. Die sieben Messreihen sind in \autoref{fig:lcrschwebungdata} gezeigt (in der
ersten Messung war der Trigger wohl auf "`fallende Flanke"' eingestellt). Die Messung
wurde zunächst in CASSY gestartet (\texttt{F9}), und durch Drücken des Kurzschlussknopfs
gestartet.

Bei zwei Messreihen wurden die beiden Spulen unterschiedlich stark durch einen Eisenkern
gekoppelt, der mehr oder weniger tief in beiden Spulen steckte.

\paragraph{Kopplung}

Die Konfiguration ist analog zum Schwebungsversuch. Allerdings werden die Spannungen in
beiden Kreisen gemessen, jeweils am Spannungskanal B zweier zusammengesteckter
CASSY-Module. Die beiden Spannungsverläufe sind in \autoref{fig:lcrkopplungdata} gezeigt.

Es wurden jeweils drei Messreihen aufgenommen für die gleichsinnige bzw. gegensinnige
Konfiguration. Bei einer zusätzlichen gleichsinnigen Messreihe wurden die beiden Kreise
zudem durch einen Eisenkern gekoppelt.

\paragraph{Messung der Bauteile}

An der verfügbaren Messbrücke haben wir die Kondensatoren und Spulen nochmals genau
vermessen, da die aufgedruckten Angaben deutlich ungenauer ($\pm 5\,\%$) sind als die
Messungen der Messbrücke ($\pm \num{0.25}\,\%$). Wir haben keinen Widerstand eingebaut,
als $R$ fungieren somit die internen Widerstände der Steckverbindungen und Spulen, die
wir hier nicht gemessen haben.

\begin{table}[h]
\centering
 \begin{tabular}{c|cc}
  Kreis & $C$ & $L$ \\
  \hline
  \rule{0pt}{2.5ex} 1 & \SI{2.234(6)}{\mu\farad} & \SI{9.095(23)}{\milli\henry} \\
  2 & \SI{2.271(6)}{\mu\farad} & \SI{8.997(224)}{\milli\henry} \\
 \end{tabular}
\end{table}


\subsection{Auswertung}

\subsubsection{Rohdaten}

Wir haben für Kopplung und Schwebung jeweils sieben Messreihen aufgenommen.

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.32]{img/schwebung_alle.png}
 % schwebung_alle.png: 1448x2122 px, 72dpi, 51.09x74.88 cm, bb=0 0 1448 2122
 \caption{Schwebungsversuche; die ersten vier Versuche sehen sehr gut aus,
\emph{halbhalbeisen} hatte einen halb-eingeschobenenen Eisenkern, \emph{naheisen} einen
jeweils komplett eingeschobenen Eisenkern.}
 \label{fig:lcrschwebungdata}
\end{figure}

\begin{figure}[h]
 %\hspace{-3em}
 \includegraphics[scale=0.32]{img/kopplung_alle.png}
 % kopplung_alle.png: 1428x2122 px, 72dpi, 50.39x74.88 cm, bb=0 0 1428 2122
 \caption{Kopplungsversuche. Versuch \emph{gegen\_3} und \emph{mit\_4} sind besonders gut
gelungen. \emph{gegen} bedeutet gegensinnig, \emph{mit} bedeutet gleichsinnig.}
 \label{fig:lcrkopplungdata}
\end{figure}

\FloatBarrier

\subsubsection{Modell}

Um die Frequenz der Schwingungen zu ermitteln, haben wir Fouriertransformationen über die
Daten ausgeführt. Für einige der besser gelungenen Schwingungen sind diese hier zu sehen:

\begin{figure}[h]
\hspace{-4em}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.28]{img/schwebung_ft_500n2_2mf_nah.png}
  \caption{Messung \emph{500n2\_2.2µf\_nah}.}
 \end{subfigure}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.28]{img/schwebung_ft_500n2_2mf_1cm.png}
  \caption{Messung \emph{500n2\_2.2µf\_1cm}.}
 \end{subfigure}

 \hspace{-4em}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.28]{img/schwebung_ft_500n2_2mf_2cm.png}
  \caption{Messung \emph{500n2\_2.2µf\_2cm}.}
 \end{subfigure}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.28]{img/schwebung_ft_500n2_2mf_3cm.png}
  \caption{Messung \emph{500n2\_2.2µf\_3cm}.}
 \end{subfigure}

 \hspace{-4em}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.28]{img/schwebung_ft_500n2_2mf_halbhalbeisen.png}
  \caption{Messung \emph{500n2\_2.2µf\_halbhalbeisen}, mit dem Eisenkern jeweils zur
Hälfte eingeschoben mit Abstand von wenigen Zentimetern.}
 \end{subfigure}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.28]{img/schwebung_ft_500n2_2mf_naheisen.png}
  \caption{Messung \emph{500n2\_2.2µf\_naheisen}, mit dem Eisenkern vollständig
eingeführt.}
 \end{subfigure}
 \caption{Fouriertransformierte der verschiedenen Schwebungen.}
 \label{fig:lcrschwebungfourier}
\end{figure}


\begin{figure}[h]
 \hspace{-4em}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.25]{img/kopplung_ft_gegensinnig_2.png}
  \caption{Messung \emph{gegen\_2}.}
 \end{subfigure}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.25]{img/kopplung_ft_gegensinnig_3.png}
  \caption{Messung \emph{gegen\_3}.}
 \end{subfigure}

 \hspace{-4em}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.25]{img/kopplung_ft_gleichsinnig_2.png}
  \caption{Messung \emph{mit\_2}.}
 \end{subfigure}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.25]{img/kopplung_ft_gleichsinnig_4.png}
  \caption{Messung \emph{mit\_4}.}
 \end{subfigure}

 \hspace{-4em}
 \begin{subfigure}{0.6\textwidth}
  \includegraphics[scale=0.25]{img/kopplung_ft_gleichsinnig_1.png}
  \caption{Messung \emph{mit\_1}}
 \end{subfigure}
 \begin{subfigure}{\textwidth}
  \includegraphics[scale=0.25]{img/kopplung_ft_gleichsinnig_eisen.png}
  \caption{Messung \emph{mit\_eisen}.}
 \end{subfigure}

 \caption{Fouriertransformierte der verschiedenen gekoppelten Schwingungen.}
 \label{fig:lcrkopplungfourier}
\end{figure}

Aus diesen erhalten wir die Eigenfrequenzen durch Nutzung des Peakfinders
\texttt{scipy.\-signal.\-find\_peaks} in \autoref{tab:lcreigenschwebung} für die
Schwebung und in \autoref{tab:lcreigenkoppel} für die gekoppelten Kreise.

Da unsere Fourieranalyse \num{16000} Punkten über eine Zeitspanne von
\SI{160}{\milli\second} bei einer Messfrequenz von \SI{1e5}{\hertz} einbezieht, beträgt
die Auflösung der Frequenz $f_{res} = \SI{6.25}{\hertz}$. Dadurch sind die Peaks, wie in
\autoref{fig:lcrkopplungfourier} und \autoref{fig:lcrschwebungfourier} gezeigt, auch in
engen Grenzen festgelegt, sodass wir $f_{res}$ als Messunsicherheit $\sigma_f$ benutzen
werden.

%% BERECHNUNGEN sind in analyse/analyse.ods zu finden.

\begin{table}[h]
\centering
 \begin{tabular}{r|cc|ccc}
  Messung & $f_+ / \si{\hertz} $ & $f_- / \si{\hertz}$ & $f_0 / \si{\hertz}$ & $k_1$ &
$k_2$ \\
  \hline
  \rule{0pt}{2.5ex} \emph{500N2\_2.2µf\_nah} & \num{1050.0} & \num{1193.7} &
\num{1121.9(42)} & \num{.142(3)} & \num{.117(2)} \\
  \emph{500N2\_2.2µf\_1cm} & \num{1062.4} & \num{1175.0} & \num{1118.7(42)} &
\num{0.109(2)} & \num{0.094(2)} \\
  \emph{500N2\_2.2µf\_2cm} & \num{1075.0} & \num{1162.4} & \num{1118.7(42)} &
\num{0.083(2)} & \num{0.074(2)} \\
  \emph{500N2\_2.2µf\_3cm} & \num{1081.2} & \num{1150.0} & \num{1115.6(42)} &
\num{0.065(1)} & \num{0.059(1)} \\

  \emph{500N2\_2.2µf\_halbhalbeisen} & \num{581.2} & \num{781.2} & \num{681.2(42)} &
\num{0.374(11)} & \num{0.240(7)} \\
  \emph{500N2\_2.2µf\_naheisen} & \num{412.5} & \num{706.2} & \num{559.3(42)} &
\num{0.839(32)} & \num{0.373(14)} \\
 \end{tabular}
 \caption{Eigenfrequenzen der Schwebung. Die Unsicherheit auf $f_\pm$ beträgt
\SI{6.25}{\hertz}.}
 \label{tab:lcreigenschwebung}
\end{table}

%% BERECHNUNGEN sind in analyse/analyse.ods zu finden.

\begin{table}[h]
\centering
 \begin{tabular}{rl|cc|ccc}
  Messung & $U$ & $f_+ / \si{\hertz} $ & $f_- / \si{\hertz}$ & $f_k / \si{\hertz}$ &
$f_{schw} / \si{\hertz} $ & $k$ \\
  \hline
  \rule{0pt}{2.5ex} \emph{gegen\_1} & 1 & \num{1056.2} & \si{1200.0} & \num{1128.1} &
\num{71.9} & \num{0.127(8)} \\
 & 2$^*$ & \num{1056.2} & $-$ & $-$ & $-$ & $-$ \\

 \emph{gegen\_2} & 1$^*$ & \num{1056.2} & $-$ & $-$ & $-$ & $-$ \\
                 & 2 & \num{1056.2} & \num{1181.2} & \num{1118.7} & \num{62.5} &
\num{0.111(8)} \\

 \emph{gegen\_3} & 1 & \num{1012.4} & \num{1187.4} & \num{1099.9} & \num{87.5} &
\num{0.111(8)} \\
                 & 2 & \num{1056.2} & \num{1187.4} & \num{1121.8} & \num{65.6} &
\num{0.117(8)} \\

\hline

 \rule{0pt}{2.5ex} \emph{mit\_1} & 1 & \num{1049.9} & \num{1187.4} & \num{1118.7} &
\num{137.5} & \num{0.122(8)} \\
               & 2 & \num{1056.2} & \num{1162.4} & \num{1109.3} & \num{53.1} &
\num{0.095(8)} \\

 \emph{mit\_2} & 1 & \num{1031.2} & \num{1187.4} & \num{1109.3} & \num{78.1} &
\num{0.140(8)} \\
               & 2 & \num{1062.4} & \num{1187.4} & \num{1125.0} & \num{62.5} &
\num{0.111(8)} \\

 \emph{mit\_4} & 1 & \num{1056.2} & \num{1237.4} & \num{1146.8} & \num{90.6} &
\num{0.157(8)} \\
               & 2 & \num{1056.2} & \num{1181.2} & \num{1118.7} & \num{62.5} &
\num{0.111(8)} \\

\hline

 \rule{0pt}{2.5ex} \emph{mit\_eisen} & 1 & \num{412.5} & \num{706.2} & \num{559.4} &
\num{146.9} &
\num{0.491(17)} \\
                      & 2 & \num{412.5} & \num{706.2} & \num{559.4} & \num{146.9} &
\num{0.491(17)} \\

 \end{tabular}
 \caption{Eigenfrequenzen der gekoppelten Kreise 1 und 2. \emph{gegen} = gegensinnig,
\emph{mit} = gleichsinnig angeregt. Die Messunsicherheit auf $f_k$ und $f_{schw}$
beträgt $\pm \SI{4.2}{\hertz}$. ($^*$ kein zweiter Peak)}
 \label{tab:lcreigenkoppel}
\end{table}

Die Eigenfrequenzen der magnetisch gekoppelten schwebenden Schwingkreise (d.h. ein
angeregter Schwingkreis, ein gekoppelter passiver Schwingkreis) sind in
\autoref{tab:lcreigenschwebung} aufgeführt. Die Werte auf der rechten Seite sind
berechnet mit

\begin{align}
f_0 &\approx \frac{f_+ + f_-}{2} \\
k_{1/2} &= \pm \left(\frac{f_0^2}{f_\pm^2} - 1\right)
\end{align}

Die Eigenfrequenzen der gekoppelten angeregten Schwingkreise sind in
\autoref{tab:lcreigenkoppel} aufgeführt. Dabei ist analog zum Skript

\begin{align}
 f_k &= \frac{f_- + f_+}{2} \\
 f_{schw} &= \frac{f_- - f_+}{2} \\
 k &= \frac{f_-^2 - f_+^2}{f_-^2 + f_+^2}
\end{align}

\FloatBarrier

\subsubsection{Analyse}

\emph{Fehlerrechnung, Residuenplots, Fehlerquellen}

\begin{equation}
 v = \SI{3.45(67)e12}{\milli\kelvin\per\kilo\watt}
\end{equation}

\subsubsection{Endergebnisse}

\emph{nur signifikante Stellen!}

\subsubsection{Diskussion}

\emph{Wie passen die Ergebnisse mit Vorhersagen und Literaturwerten zusammen? Wieso sind
sie nicht gleich? Was kann verbessert werden?}
