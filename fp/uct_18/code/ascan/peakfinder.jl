import DSP
import LsqFit
import Distributions
import Measurements
import Plots

const M = Measurements
const M64 = M.Measurement{Float64}

function envelope_of(ys)
    abs.(DSP.hilbert(ys))
end

function scaled_normal_model(x, params)::Vector{Float64}
    # Model to be used for curve fitting.
    scaled_normal(x, params[1], params[2], params[3])
end

function scaled_normal(x::Vector{Float64}, μ, σ, scale=1)::Vector{Float64}
    # A scaled normal distribution function, vectorized.
    map(xx -> Distributions.pdf(Distributions.Normal(μ, σ), xx)*scale, x)
end

function find_approx_max(xs, ys)
    # Returns (maxix, fwhm) of maximum of ys.
    maxix = argmax(ys)
    maxy = ys[maxix]
    hm = maxy/2
    left, right = ys[1:maxix], ys[maxix:end]
    leftfwhm, rightfwhm = argmin(abs.(left.-hm)), maxix+argmin(abs.(right.-hm))-1
    fwhm = (ys[rightfwhm]+ys[leftfwhm])/2
    return maxix, fwhm
end

function crop_data_around_max_ix(xs, ys)
    # Return left and right index of maximum peak in ys.
    maxix = argmax(ys)
    maxy = ys[maxix]
    qm = 3/4 * maxy
    left, right = ys[1:maxix], ys[maxix:end]
    lix, hix = argmin(abs.(left.-qm)), maxix+argmin(abs.(right.-qm))-1
    return lix, hix
end

function crop_data_around_max(xs, ys)
    # Crop xs and ys so that only the relevant part of the highest peak is left.
    lix, hix = crop_data_around_max_ix(xs, ys)
    return xs[lix:hix], ys[lix:hix]
end

function fit_gauss(xs, ys)
    # Returns μ, σ, scale array for given data.
    maxix, fwhm = find_approx_max(xs, ys)
    fit = LsqFit.curve_fit(scaled_normal_model, xs, ys, [xs[maxix], fwhm/2, 1.])
    covar = LsqFit.estimate_covar(fit)
    stds = [sqrt(covar[i,i]) for i = 1:size(covar)[1]]
    return fit.param, stds
end

function crop_and_fit_peak(xs, ys)
    # Automatically find highest peak and fit a normal distribution
    xs_, ys_ = crop_data_around_max(xs, ys)
    params, vars = fit_gauss(xs_, ys_)
    return params, vars
end

function check_fit_plot(xs, ys; kwargs...)
    # Fit normal distribution to highest peak and plot result for visual inspection
    lix, hix = crop_data_around_max_ix(xs, ys)
    params, vars = crop_and_fit_peak(xs, ys)
    uncertain_params = map(vs -> Measurements.measurement(vs[1], vs[2]), zip(params, vars))
    fitys = scaled_normal_model(xs, nv(uncertain_params))

    Plots.plot(xs[:], ys[:], label="Data"; kwargs...)
    Plots.plot!(xs[:], fitys[:], label="Fit: μ = $(round(params[1], digits=1)), σ = $(round(params[2], digits=1))")
end

function find_rise_threshold(xs, ys, threshold_frac=.25)
    threshold = threshold_frac * maximum(ys)
    for (x,y) = zip(xs, ys)
        if y >= threshold
            return x
        end
    end
end
