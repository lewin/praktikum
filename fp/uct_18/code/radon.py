#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 19:26:59 2021

@author: lbo
"""

import numpy as np
from numpy import fft
from scipy import ndimage

import math
import matplotlib.pyplot as plt

DIM = 100
FIGSIZE = (DIM/10, DIM/10)

# GENERATE SOURCE IMAGE

def generate_synthetic_data(l=DIM):
    """ Synthetic binary data

    from scikit-learn: https://scikit-learn.org/stable/auto_examples/applications/plot_tomography_l1_reconstruction.html"""
    rs = np.random.RandomState(0)
    n_pts = 36
    x, y = np.ogrid[0:l, 0:l]
    mask_outer = (x - l / 2.) ** 2 + (y - l / 2.) ** 2 < (l / 2.) ** 2
    mask = np.zeros((l, l))
    points = l * rs.rand(2, n_pts)
    mask[(points[0]).astype(int), (points[1]).astype(int)] = 1
    mask = ndimage.gaussian_filter(mask, sigma=l / n_pts)
    res = np.logical_and(mask > mask.mean(), mask_outer)
    return res #np.logical_xor(res, ndimage.binary_erosion(res))


def points_src_img(pos=[(.2,.2), (.5, .9), (.9, .2)], r=4):
    img = np.zeros((DIM,DIM))

    for p in pos:
        center = p[0]*DIM, p[1]*DIM
        pixels = [(x, y) for x in range(int(center[0]-r/2), int(center[0]+r/2)+1)
                  for y in range(int(center[1]-r/2), int(center[1]+r/2))]
        for pix in pixels:
            img[pix] = 1
    return img

def points_lewin_img(w=4):
    img = np.zeros((DIM,DIM))

    # L
    X = 20
    for y in range(X, DIM-X):
        for x in range(int(X-w/2), int(X+w/2)+1):
            img[x, y] = 1
    Y = 20
    for x in range(X, X+30):
        for y in range(int(Y-w/2), int(Y+w/2)+1):
            img[x, y] = 1

    # B
    X = 60
    for y in range(20, DIM-20):
        for x in range(int(X-w/2), int(X+w/2)+1):
            img[x, y] = 1

    def draw_circle(c, r):
        xys = [(x, y) for x in range(0, DIM) for y in range(0, DIM)
           if (r-int(w/2))**2 <= (x-c[0])**2 + (y-c[1])**2 <= (r+int(w/2))**2 and x > c[0] ]
        for x,y in xys:
            img[x,y] = 1

    draw_circle((60, 62), 15)
    draw_circle((60, 33), 15)

    return img

# PLOT STUFF

def draw_img(img, title='', save=None, dim=DIM):
    fig = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = fig.add_subplot(111)
    p.imshow(img.T, origin='lower', aspect='equal')
    p.set_xlabel('x')
    p.set_ylabel('y')

    p.set_xticks(np.linspace(0, dim, int(dim/10)+1))
    p.set_xticklabels(np.array(np.linspace(-dim/2, dim/2, int(dim/10)+1), dtype=np.int))
    p.set_yticks(np.linspace(0, dim, int(dim/10)+1))
    p.set_yticklabels(np.array(np.linspace(-dim/2, dim/2, int(dim/10)+1), dtype=np.int))

    if title:
        p.set_title(title)
    if save:
        fig.savefig(save)

def draw_projections(all_projections, title='', save=None):
    fig = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = fig.add_subplot(111)
    dim = int(-2*all_projections[:,0].min())

    maxint = all_projections[:, 2].max()
    valtocolor = lambda intval: (0,0,0, max(0, intval)/maxint)

    for proj in all_projections:
        l, theta, integral = proj
        ss = generate_scan_s(l, theta, dim)
        xys = lts_to_xy(l, theta, ss)

        p.plot([xys[0, 0], xys[-1, 0]], [xys[0, 1], xys[-1, 1]],
               color=valtocolor(integral))
    if title:
        p.set_title(title)
    p.set_xlabel('x')
    p.set_ylabel('y')

    if save:
        fig.savefig(save)

def draw_sinogramm(projections, save=None, dim=DIM):
    fig = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = fig.add_subplot(111)

    # x: theta, y: l (top to bottom)
    ls = np.unique(projections[:, 0])
    thetas = np.unique(projections[:, 1])

    img = np.zeros((thetas.size, ls.size))

    for x in range(0, thetas.size):
        for y in range(0, ls.size):
            mask = (projections[:, [0,1]] == [ls[y], thetas[x]]).all(axis=1)
            img[x,y] = projections[mask,2].sum()

    p.set_title('Sinogramm')
    p.set_xlabel('$\\theta$')
    p.set_ylabel('l')

    p.set_xticks(np.linspace(0, thetas.size, 10))
    xlabels = np.array(np.linspace(0, math.pi, 10)/math.pi * 180, dtype=np.int)
    p.set_xticklabels(xlabels)

    p.set_yticks(np.linspace(0, ls.size, int(dim/10)+1))
    ylabels = np.array(np.linspace(ls.min(), ls.max()+1, int(dim/10)+1), dtype=np.int)
    p.set_yticklabels(ylabels)

    p.imshow(img)

    if save:
        fig.savefig(save)

# CONVERSION

def coord_to_ix(xys, dim=DIM):
    x, y = xys[:, 0], xys[:,1]
    if (x < -DIM/2).any() or (x > DIM/2).any() or (y < -DIM/2).any() or (y > DIM/2).any():
        return None
    return np.array(xys+DIM/2, dtype=np.int)

def lt_to_xy_fn(l, theta):
    return lambda s: lts_to_xy(l, theta, s)

def lts_to_xy(l, theta, s):
    xs = l*np.cos(theta) - s*np.sin(theta)
    ys = l*np.sin(theta) + s*np.cos(theta)
    return np.array([xs, ys]).T

# GENERATE SAMPLING

def generate_scan_lt(n=100, lmax=DIM/2):
    """Generate (n, 2) array with (l, theta) pairs."""
    per_dim = 3*int(math.sqrt(n))
    lens = np.linspace(-lmax, lmax-1, per_dim)
    thetas = np.linspace(0, .99*math.pi, per_dim)
    return np.transpose([np.repeat(lens, len(thetas)), np.tile(thetas, len(lens))])

def generate_scan_s(l, theta, dim=DIM, nmax=DIM):
    """Generate valid s values along the line of measurement for given l, t."""
    ss = np.linspace(-dim/math.sqrt(2), dim/math.sqrt(2)-1, nmax)
    points = lts_to_xy(l, theta, ss)
    mask = [-dim/2 <= x <= dim/2 and -dim/2 <= y <= dim/2 for [x,y] in points]
    return ss[mask]

# "MEASURE"

def integrate_radon(img, l, theta, f=lambda x: x, ssteps=DIM):
    """Generate projection P(l, theta) for given image."""
    assert img.shape[0] == img.shape[1]
    ss = generate_scan_s(l, theta, img.shape[0], ssteps)
    if ss.size < 3:
        return 0
    ds = (ss[1:]-ss[:-1])[0]

    xys = lts_to_xy(l, theta, ss)
    ixs = coord_to_ix(xys)
    assert ixs is not None

    integral = sum(f(img[c[0], c[1]])*ds for c in ixs)
    return integral

def all_projections(img, n=DIM, lmax=DIM/2):
    """Calculate many projections for image. n is a measure for the number of projections.

    Returns array of [l, theta, integral] rows.
    """
    lt = generate_scan_lt(n, lmax)
    projections = []
    for l, theta in lt:
        projections.append([l, theta, integrate_radon(img, l, theta, ssteps=n)])
    return np.array(projections)

# CALCULATE ORIGINAL DISTRIBUTION

def fft_projection(all_projections, normalize=True):
    """Calculate FFT for all projections for a single angle."""
    thetas = np.unique(all_projections[:,1])

    transformed = np.copy(all_projections)

    for theta in thetas:
        projs = all_projections[all_projections[:,1] == theta, :]
        ls = projs[:,0]
        integrals = projs[:,2]
        # fourier is the fourier transform of the projection, a function of k, theta (fixed)
        fourier = fft.rfft(integrals)
        freqs = fft.rfftfreq(ls.size)
        filtered_freqs = np.abs(freqs)*fourier
        filtered_projection = fft.irfft(filtered_freqs)

        if normalize:
            filtered_projection -= filtered_projection.min()

        # plt.plot(integrals)
        # plt.plot(filtered_projection)
        # break

        transformed[transformed[:,1] == theta, 2] = filtered_projection

    return transformed

def transform_projections_to_xy(projections, dim=DIM, nmax=DIM):
    """Project data back to XY space. This is done by simply mapping integral strength
    to all points along a given line.

    nmax determines the number of samples taken for each line.

    Faster than reconstruct_from_samples, because we sample once for each
    projection and assign to the correct pixels.
    """
    sampled = np.zeros((dim,dim))
    for proj in projections:
        l, theta, integral = proj
        ss = generate_scan_s(l, theta, dim, nmax=nmax)
        xys = lts_to_xy(l, theta, ss)
        ixs = coord_to_ix(xys)

        for (x, y) in ixs:
            sampled[x,y] += max(integral, 0)
    sampled /= sampled.max()

    return sampled

def sample_at(x, y, projections, dim=DIM):
    """Integrate over measurements for sample point at x,y."""
    thetas = np.unique(projections[:, 1])
    x, y = x-dim/2, y-dim/2
    total = 0

    for theta in thetas:
        l0 = x*math.cos(theta)+y*math.sin(theta)
        mask = np.logical_and(l0-1/2 < projections[:, 0], projections[:, 0] < l0+1/2)
        mask = np.logical_and(mask, projections[:, 1] == theta)
        intsum = projections[mask,2].sum()
        total += intsum * math.pi/projections.shape[0]
    return total

def reconstruct_from_samples(projections, dim=DIM):
    """For each pixel, integrate along all angles and possible l's.

    This is analogous to the algorithm on p. 13 of the script.

    Slower than transform_projections_to_xy, because for each pixel we go
    through all projections, including those irrelevant to that pixel.
    """
    img = np.zeros((dim, dim))

    for x,y in ((x,y) for x in range(0, dim) for y in range(0, dim)):
        img[x,y] = sample_at(x, y, projections, dim=dim)
    return img

def example_sample_and_backtransform(img=points_src_img(), title='uct', n=DIM):
    draw_img(img, title='Original Image', save=f'../img/{title}_orig.pdf')

    ap = all_projections(img, n=n)
    fp = fft_projection(ap)

    draw_projections(ap, title='All Projections', save=f'../img/{title}_proj_all.pdf')
    draw_projections(fp, title='Filtered Projections (Ram-Lak)', save=f'../img/{title}_proj_ramlak.pdf')

    draw_sinogramm(ap, save=f'../img/{title}_sinogramm.pdf')

    all_img = transform_projections_to_xy(ap)
    filt_img = transform_projections_to_xy(fp)
    filt_img_log = np.sqrt(filt_img)
    draw_img(all_img, title='Reconstructed (all projections)', save=f'../img/{title}_reconstruct_all.pdf')
    draw_img(filt_img_log, title='Reconstructed (Ram-Lak)', save=f'../img/{title}_reconstruct_ram_lak.pdf')
