#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 18:26:54 2021

@author: lbo
"""

import math
import numpy as np
from numpy import fft
import matplotlib.pyplot as plt
import pandas as pd
from scipy import signal
import re

import ctfilereader as ctf
import cthelper as cth

FIGSIZE = (7,7)

MHZ1_CT_AMP = '../../data/DatensatzA/ct_scan/1MHz/both_amp.dat'
MHZ1_CT_VEL = '../../data/DatensatzA/ct_scan/1MHz/both_vel.dat'

def dropout(df, lines=1, cols=1):
    return df.iloc[0::lines, 0::cols]

def original_amplitude(df):
    return df

def original_speed(df):
    sos = 1500  # m/s
    const = 1  # mm / µs
    return const*sos/(df/100+1)

def subtract_background(df):
    return df - determine_background_amplitude(df)

def simple_ct(fname, ramlak=False, shepplo=False, shepplo_C=1,
              drop=(1,1), disable_preproc=False):
    df = ctf.read_processed_file(fname)

    is_vel = False
    s0 = 1
    if not disable_preproc:
        if '_vel' in fname:
            filterfunc = original_speed
            is_vel = True
            s0 = .13 * 1e6
        elif '_amp' in fname:
            filterfunc = original_amplitude
        else:
            filterfunc = lambda x: x

    df = filterfunc(dropout(df, *drop)) if filterfunc else dropout(df, *drop)

    delta_s = np.mean(df.columns[1:]-df.columns[:-1])

    if ramlak:
        df = filter_ct_data(df, ram_lak=True, shepplo=False)
    elif shepplo:
        df = filter_ct_data(df, ram_lak=False, shepplo=True, shepplo_C=shepplo_C)

    angles = list(df.index)
    lengs = list(df.columns)
    N = len(angles)

    img = np.zeros((len(lengs), len(lengs)))

    for angle in angles:
        xs = df.loc[angle].to_numpy()
        img += cth.project(xs, plot=False, angle=angle) * 1/(2*N*s0)

    # We have measured inverse velocity.
    if is_vel:
        img = 1/img

    fig = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = fig.add_subplot(111)
    s = p.imshow(img, origin='lower')

    filtertype = {
        (True, False): 'Ram-Lak',
        (False, True): 'Shepp-Logan',
        (False, False): 'Kein Filt.'}[(ramlak, shepplo)]
    measure_type = 'Amplitude / 1V' if 'amp' in fname else 'Geschwindigkeit / (m/s)'
    freq = re.search('/(\\dMHz)', fname)[1]
    filtername = str(filterfunc).split(' ')[1] + ', '

    p.set_title('{} ({}{}) \n {} -- drop {}'.format(freq, filtername, filtertype, measure_type, drop))
    fig.colorbar(s, ax=p, shrink=0.7)
    return fig

def determine_background_amplitude(df):
    return df.iloc[:,0].mean()

def filter_ct_data(df, ram_lak=True, shepplo=False, shepplo_C=1/2,
                   normalize=True):
    """Takes a dataframe read from file or mean amplitude series (is_raw==False)
    and applies Ram-Lak on it."""

    angles = list(df.index)
    lengs = list(df.columns)

    transformed = df.copy()

    filterfunc = lambda x, y: y
    if ram_lak and not shepplo:
        def filterfunc(freqs, fourier):
            return np.abs(freqs)*fourier
    elif shepplo and not ram_lak:
        def filterfunc(freqs, fourier):
            k = freqs
            factor = np.maximum(np.pi*k/(2*shepplo_C), 1e-6)
            return fourier * np.abs(k) * np.sin(factor)/factor
    else:
        raise Exception("filter_ct_data: no filter selected!")

    for angle in angles:
        # Projections contains all projections for a fixed angle.
        projections = df.loc[angle].to_numpy()
        fourier = fft.rfft(projections)
        freqs = fft.rfftfreq(projections.size)
        filtered_freqs = filterfunc(freqs, fourier)
        filtered_projection = fft.irfft(filtered_freqs)

        if normalize:
            filtered_projection -= filtered_projection.min()

        transformed.loc[angle].iloc[0:filtered_projection.size] = filtered_projection
        if (transformed.loc[angle].size % 2 == 1):
            transformed.loc[angle].iloc[-1] = 0

    return transformed


def simple_ct_render_all_plots():
    # Unfiltered back-projections
    FREQS = [1,2,4]
    AMP_PAT = lambda f: '../../data/DatensatzA/ct_scan/{}MHz/both_amp.dat'.format(f)
    VEL_PAT = lambda f: '../../data/DatensatzA/ct_scan/{}MHz/both_vel.dat'.format(f)
    SAVE_PATH = lambda s: '../../img/ct/{}.pdf'.format(s)

    configs = {
        'raw': {},
        'ram_lak': {'ramlak': True},
        'shepplo': {'shepplo': True, 'shepplo_C': .2},
    }

    drops = [(1,1), (2, 2), (5, 5)]

    for fr in FREQS:
        for title, cfg in configs.items():
            for drop in drops:
                fig = simple_ct(AMP_PAT(fr), drop=drop, **cfg)
                fig.savefig(SAVE_PATH('amplitude_{}MHz_{}_drop_{}_{}'.format(fr, title, *drop)))
                fig = simple_ct(VEL_PAT(fr), drop=drop, **cfg)
                fig.savefig(SAVE_PATH('velocity_{}MHz_{}_drop_{}_{}'.format(fr, title, *drop)))
