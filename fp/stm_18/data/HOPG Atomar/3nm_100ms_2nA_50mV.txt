-- Scan group --
Image size	3nm
Scan direction	Down
Time/Line	100ms
Points	128
Lines	128
X-Slope	0m°
Y-Slope	0m°
Rotation	0m°
X-Pos	0mm
Y-Pos	0mm
Z-Plane	0mm
Const.Height-Mode	Disabled
Date	25-01-2017
Time	16:18:12
-- Feedback group --
Setpoint	2nA
P-Gain	1k
I-Gain	2k
D-Gain	0
Tip voltage	50,1mV
Feedback mode	Free running
Feedback algo.	Adaptive PID
Error range	200nA
-- Module --
Controller Board	2
USB Module	4
-- Global --
Operating mode	STM
Head type	EZ2-STM
Scan head	3-09-994.hed
Software ver.	3.1.1.13
Firmware ver.	3.1.3.16
Controller S/N	023-09-715
