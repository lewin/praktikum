### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ bf9adf7c-8c8e-11eb-1f75-fd7ba58b9724
begin
	import Statistics
	import Plots
	using PyPlot
	PyPlot.svg()
	
	using Revise
	using MosaicViews
	
	push!(LOAD_PATH, pwd())
	using analyze.loading
	using analyze.colormap
	using analyze.display
	using analyze.filter
	using analyze.lr
end

# ╔═╡ dfdc596e-8c9d-11eb-305f-3f84439457ab
md"""## Analysis of Gold scans by time/line
"""

# ╔═╡ 7439db92-8e45-11eb-285f-970c86f956ad
md"""#### Given conditions
Image size	98,8nm


Scan direction	Idle


Time/Line	60,4ms


Points	300


Lines	300


Setpoint	1nA


P-Gain	1k


I-Gain	3k


D-Gain	0


Tip Voltage	1 V
"""

# ╔═╡ 3f691738-8e3c-11eb-0827-e3679007fa19
SCAN_TIME_SELECTION = [1,2,3,5]

# ╔═╡ 0a088a5a-8c9e-11eb-3dc7-879094adcd36
begin
	zscans = MD_T_ZOFF[SCAN_TIME_SELECTION,:]
	currentscans = MD_T_CURRENT[SCAN_TIME_SELECTION,:]
end

# ╔═╡ 71c629c0-8ca3-11eb-0f6d-9900c9b2df46
function load_gold_scan(md)
	img = drop_peaks(load_image(md), 99.5)
	return img
end

# ╔═╡ 229e4d5c-8ca1-11eb-250e-3d4b8dbff95a
md"""### Show images with different scan times

The following blocks load and render the raw images to PDFs on disk.
"""

# ╔═╡ 4eee62f4-8ca1-11eb-3500-cdaa54a555f0
begin
	img = load_gold_scan.(zscans)
	display_mpl.(img, save=true, prefix="zscan")
end

# ╔═╡ 70f154ce-8e36-11eb-23f0-f9efc10c6877
begin
	cimg = load_gold_scan.(currentscans)
	display_mpl.(cimg, save=true, prefix="currentscan")
end

# ╔═╡ ffb4df62-8ca3-11eb-0ecd-03402d56ea1d
md"""### Show crosssections with different scan times
"""

# ╔═╡ 76a66c2e-8ca6-11eb-01c0-cf3c4466b92c
@bind eycross html"""Y-Crosssection: <input type=range value=38>"""

# ╔═╡ 06e47f68-8ca4-11eb-017e-59da3bd8a376
crosssection(img[:,1], img[:,2],
	y=round(Int32, eycross/100 * size(img[1].data)[1]), 
	relative=nothing, save=true)

# ╔═╡ df011596-8ca4-11eb-0d3e-89841a5ee30b
draw_crosssection(img[:,1],
	y=round(Int32, eycross/100 * size(img[1].data)[1]),
	save="../img/gold_crosssec_overlay_time_$(convert(Int32, eycross))_fw.png")

# ╔═╡ 44bbe5d8-8fe0-11eb-179f-a7102f2cacfe
draw_crosssection(cimg[:,1],
	y=round(Int32, eycross/100 * size(cimg[1].data)[1]),
	save="../img/gold_crosssec_overlay_time_current_$(convert(Int32, eycross))_fw.png")

# ╔═╡ a4bcf15e-8edc-11eb-23f8-d9bb0a81a597
draw_crosssection(img[:,2],
	y=round(Int32, eycross/100 * size(img[1].data)[1]),
	save="../img/gold_crosssec_overlay_time_$(convert(Int32, eycross))_bw.png")

# ╔═╡ 1f94d9ac-8fcd-11eb-0427-11a3ae22a1d5
md"""Calculate filtered versions of the images to determine noise."""

# ╔═╡ 83af9476-8e23-11eb-394c-9393512fae01
begin
	for i = img
		csfig = crosssection([i, img_line_bandpass(i, .0001, .2)],
			y=round(Int32, eycross/100 * size(i.data)[1]))
		csfig.savefig(plot_path(i.desc, "crossection_lowpass"))
	end
end

# ╔═╡ 2bf24af2-8e36-11eb-326d-9dfce1191ac1
draw_contour_image.(
	img[:,1],
	img_line_bandpass.(img[:,1], .0001, .2),
	save=true,
	title="Contour Bandpass",
	prefix="contour_bandpass_20pct")

# ╔═╡ 5c7e440e-8e25-11eb-0db2-5d50d23f5028
# This function estimates the measurement uncertainty of the Z offset by only
# considering high-frequency parts (> 10% of nyquist frequency).
# Here, this means everything bigger than 20nm, with a Chebyshev filter form.
function noise_stddev_img(img::STImage, bandpass=(.2, .999); x=nothing, y=nothing)
	if x !== nothing
		series = img.data[:, x+1]
	elseif y !== nothing
		series = img.data[y+1, :]
	end
	off = 10
	filtered = line_bandpass(series, bandpass[1], bandpass[2])
	stddev = Statistics.std(filtered[off:end])
	clf()
	figure(figsize=(10,5), tight_layout=true)
		
	gca().plot(
		range(1, img.desc.dim, length=length(series)-off+1),
		filtered[off:end], alpha=1)
	gca().set_xlabel("x / nm")
	gca().set_ylabel("Δz / nm")
	gca().set_ylim(-1.5, 1.5)
	
	ax2 = gca().twiny()
	ax2.hist(filtered[off:end], bins=range(-1.5, 1.5, step=0.1),
		orientation=:horizontal, color=:red, alpha=0.5)
	title("Highpass (> 20%) $(short_desc(img.desc)): σ = $(round(stddev, digits=3)) nm")
	ax2.set_xlabel("n")


	gcf().savefig(plot_path(img.desc, "noise_bandpass_gt20pct"))
	stddev
end

# ╔═╡ 801a8476-8e26-11eb-1be3-f182c23c4707
# Calculate uncertainty from high-frequency noise.
zoff_uncertainties = noise_stddev_img.(img, y=round(Int32, eycross/100 * size(img[1].data)[1]))

# ╔═╡ ce0fd6b8-8fac-11eb-3d65-f7420305b2cb
function measure_steepness(imgs::Vector{STImage}, xmin, xmax;
		y=eycross, uncertainties::Vector{Float64}=[])
	img = imgs[1]
	if length(uncertainties) < length(imgs)
		push!(uncertainties, zeros(length(imgs)-length(uncertainties))...)
	end

	yix = round(Int32, eycross/100 * size(img.data)[1])
	
	to_x_ix = x -> round(Int32, x * size(img.data)[2] / img.desc.dim)
	xminix, xmaxix = to_x_ix(xmin), to_x_ix(xmax)
	
	xs_ = collect(range(0, img.desc.dim, length=size(img.data)[1]))
	xs = xs_[xminix:xmaxix]
	allxs = hcat(repeat([xs], length(imgs))...)
	allys = hcat([lr.mfl.(i.data[yix+1,xminix:xmaxix], uncertainties[ix])
			for (ix, i) in enumerate(imgs)]...)
	
	fits = mapslices(y -> linreg(xs[:,1], y), allys, dims=1)
	allfitys = lr.nv.(hcat([(xs .* f.m .+ f.b) for f in fits]...))
	
	resids = allys .- allfitys

	labels = permutedims(["$(short_desc(i.desc)): m = $(round(fits[ix].m, digits=4))"
		for (ix, i) in enumerate(imgs)])
	
	lay = Plots.grid(2, 1, heights=[0.7, 0.3])

	Plots.scatter(allxs, allys, t=:scatter, xlabel="x / nm", ylabel="z / nm",
               	label=labels, color=[:blue :orange :green :red], 
				title="Flank steepness (Time) @ y = $(round(xs_[yix], digits=1))")
    dataplot = Plots.plot!(allxs, allfitys, color=:black, label="")   
    
    Plots.plot(xs, resids, legend=false, t=:scatter,    
              ylabel="Daten - (mx + b)", color=[:blue :orange :green :red])    
    residplot = Plots.plot!((xs), zeros(size(xs)), color="gray")    
    Plots.plot(dataplot, residplot, layout=lay, size=(700,700), link=:x)
end

# ╔═╡ b0c10cfc-8fcd-11eb-28e7-3b27f9daea46
begin
	p =	measure_steepness(img[:,1], 80, 90, uncertainties=zoff_uncertainties[:,1])
	Plots.savefig(p,
		"../img/steepness_time_$(eycross)px.pdf")
	p
end

# ╔═╡ a5d0af72-8e37-11eb-208b-47f46610cc43
draw_contour_image.(img[:,1], img[:,2], save=true, prefix="contour_fw_bw")

# ╔═╡ 054ba79c-8e40-11eb-31f0-a56dd9209b13
draw_contour_image(img[2,1], img[2,2])

# ╔═╡ b2d6d8d8-8ee4-11eb-07e6-abeafa9668ae
elevation_distribution.(img)

# ╔═╡ 6e606dc8-8e43-11eb-0ce8-4d27ed83d7ee
md"""### Current images

Now we look at the current images instead of Z offset images. They are in `currentscans`:
"""

# ╔═╡ c577ff36-8e43-11eb-242b-c5842f48d096
cs = crosssection(cimg[:,1], cimg[:,2],
	y=round(Int32, eycross/100 * size(cimg[1].data)[1]), 
	save=true)

# ╔═╡ 465a175a-8e45-11eb-0127-29ca9f6b8b78
cs.savefig("../img/time_current_crosssection.pdf")

# ╔═╡ b1bb32ba-8e4f-11eb-2e4e-5b874f5626f8
md"""For larger time/line scantimes, we see that the current is a lot more steady. For short times, there are big variations in the current."""

# ╔═╡ baf1fb1c-8e4c-11eb-380d-259801a014e9
draw_contour_image.(cimg[:,1], cimg[:,2], save=true, prefix="contour_fw_bw", png=true)

# ╔═╡ Cell order:
# ╠═bf9adf7c-8c8e-11eb-1f75-fd7ba58b9724
# ╠═dfdc596e-8c9d-11eb-305f-3f84439457ab
# ╠═7439db92-8e45-11eb-285f-970c86f956ad
# ╠═3f691738-8e3c-11eb-0827-e3679007fa19
# ╠═0a088a5a-8c9e-11eb-3dc7-879094adcd36
# ╠═71c629c0-8ca3-11eb-0f6d-9900c9b2df46
# ╠═229e4d5c-8ca1-11eb-250e-3d4b8dbff95a
# ╠═4eee62f4-8ca1-11eb-3500-cdaa54a555f0
# ╠═70f154ce-8e36-11eb-23f0-f9efc10c6877
# ╠═ffb4df62-8ca3-11eb-0ecd-03402d56ea1d
# ╠═76a66c2e-8ca6-11eb-01c0-cf3c4466b92c
# ╠═06e47f68-8ca4-11eb-017e-59da3bd8a376
# ╠═df011596-8ca4-11eb-0d3e-89841a5ee30b
# ╠═44bbe5d8-8fe0-11eb-179f-a7102f2cacfe
# ╠═a4bcf15e-8edc-11eb-23f8-d9bb0a81a597
# ╠═1f94d9ac-8fcd-11eb-0427-11a3ae22a1d5
# ╠═83af9476-8e23-11eb-394c-9393512fae01
# ╠═2bf24af2-8e36-11eb-326d-9dfce1191ac1
# ╠═5c7e440e-8e25-11eb-0db2-5d50d23f5028
# ╠═801a8476-8e26-11eb-1be3-f182c23c4707
# ╠═ce0fd6b8-8fac-11eb-3d65-f7420305b2cb
# ╠═b0c10cfc-8fcd-11eb-28e7-3b27f9daea46
# ╠═a5d0af72-8e37-11eb-208b-47f46610cc43
# ╠═054ba79c-8e40-11eb-31f0-a56dd9209b13
# ╠═b2d6d8d8-8ee4-11eb-07e6-abeafa9668ae
# ╠═6e606dc8-8e43-11eb-0ce8-4d27ed83d7ee
# ╠═c577ff36-8e43-11eb-242b-c5842f48d096
# ╠═465a175a-8e45-11eb-0127-29ca9f6b8b78
# ╠═b1bb32ba-8e4f-11eb-2e4e-5b874f5626f8
# ╠═baf1fb1c-8e4c-11eb-380d-259801a014e9
