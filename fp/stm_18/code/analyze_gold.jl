### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 347f0012-8bd5-11eb-2002-ed0d67c64fc4
begin
	# Imports.
	import Images
	import MosaicViews
	import DelimitedFiles
	import DSP.Filters
	import Statistics
	using PlutoUI
	
	using Setfield
	
	using Revise
	
	# All the custom code is in the analyze module.
	push!(LOAD_PATH, pwd())
	using analyze.loading
	using analyze.colormap
	using analyze.display
	using analyze.filter
	
	using PyPlot
	PyPlot.svg(true)

end

# ╔═╡ 2118fcb2-8bd5-11eb-0e7c-45c134dba815
md"""# Gold STM
"""

# ╔═╡ 0302d252-8c7a-11eb-2478-c91fa37ada8a
md"""## Data Description

Here we generate the Measurement Descriptors (MDescriptors) for all available measurements.

As a result, we get Nx2 arrays for N measurements, with a forward and backward measurement each."""

# ╔═╡ 193bc8ae-8be3-11eb-09f0-830c019d5223
md"""## Loading and displaying functions.
"""

# ╔═╡ 25868a9c-8be3-11eb-3f7b-f137fbafc9be
md"""They allow us to load and present the images for interactive use.

Like here:
"""

# ╔═╡ c788aa60-8c80-11eb-2176-371709527271
md"""### Image Loading

Here, we determine if we look at varying-I or varying-T images, as well as current or offset pictures.
"""

# ╔═╡ 35e67528-8e2a-11eb-35ea-af59501c2fa1
IMAGESET = MD_T_ZOFF

# ╔═╡ c098474e-8bd9-11eb-122c-9bf28238275c
begin
	im0 = load_image.(reshape(IMAGESET[1:end, 1], :))
	imbw = drop_peaks.(load_image.(reshape(IMAGESET[1:end, 2], :)))
	im0 = drop_peaks.(im0, 99.9)
end

# ╔═╡ 5f1844b2-8c13-11eb-19cb-173ec2edf353
md"""
$(join([to_filename(img.desc, base="") for img in im0], ", "))

Forward/backward.

"""

# ╔═╡ 2e67ea92-8c0f-11eb-12c5-7d238af2b887
begin
	mat = display_viridis.(im0)
	MosaicViews.mosaicview(mat, nrow=1)
end

# ╔═╡ 030b54f0-8c0b-11eb-2274-ef7457bb2a9a
md"""## Filters
"""

# ╔═╡ a404008e-8be1-11eb-02b4-9715c3d23bed
md"""## Crosssections
"""

# ╔═╡ d3e4b43c-8e2b-11eb-25cd-3d7e0638a833
CROSSSECTION_SELECTION = [1, 3, 4]

# ╔═╡ 0d5e0b4a-8be4-11eb-258d-570f07ff325c
md"""### Horizontal Crosssection
"""

# ╔═╡ 13628ba6-8be4-11eb-1c7c-4d9f7ad32d83
@bind ycross html"""<input type=range>"""

# ╔═╡ 33c3a216-8ca4-11eb-0a98-7ba8604219e7
crosssection(im0[CROSSSECTION_SELECTION], y=round(Int32, ycross/100 * size(im0[1].data)[1]))

# ╔═╡ da09493a-8cab-11eb-381c-a5caa293a1ff
crosssection(img_line_bandpass.(im0[CROSSSECTION_SELECTION], .2, .999), y=round(Int32, ycross/100 * size(im0[1].data)[1]))

# ╔═╡ 5b05cf7e-8c0a-11eb-2960-ab48a18ff722
md"""## Vertical Crosssection"""

# ╔═╡ 2fef49e6-8c0a-11eb-35d8-1b5c3a964caf
@bind xcross html"""<input type=range>"""

# ╔═╡ 58626b54-8be4-11eb-0c74-d72aae7c5f44
MosaicViews.mosaicview(
	draw_crosssection(
		identity.(im0[CROSSSECTION_SELECTION]),
		y=round(Int32, ycross/100 * size(im0[1].data)[1]),
		x=round(Int32, xcross/100 * size(im0[1].data)[1])),
	nrow=1)

# ╔═╡ 3865b984-8c0a-11eb-1dcb-2f455f733c9d
crosssection(im0[CROSSSECTION_SELECTION], x=round(Int32, xcross/100 * size(im0[1].data)[1]))

# ╔═╡ 8db24656-8c81-11eb-178e-f1563ea03da2
md"""## Elevation Histogram
"""

# ╔═╡ f496c10a-8c82-11eb-1727-cf094746827f
begin
	function elevation_histogram(img::STImage;
			xlim=nothing, ylim=nothing,  # each in nm! (min, max) tuple
			bins=100)::PyPlot.Figure
		
		# Find window for histogram
		# images are quadratic
		steps = range(0, img.desc.dim, length=size(img.data)[1])
		xlim = xlim == nothing ? (0, img.desc.dim) : xlim
		ylim = ylim == nothing ? (0, img.desc.dim) : ylim

		xmin, xmax = argmin(abs.(steps.-xlim[1])), argmin(abs.(steps.-xlim[2]))
		ymin, ymax = argmin(abs.(steps.-ylim[1])), argmin(abs.(steps.-ylim[2]))
		
		fig = figure(figsize=(10, 7), tight_layout=true)
		p = fig.add_subplot(111)
		p.hist(reshape(img.data[xmin:xmax, ymin:ymax], :), bins=bins)
		p.set_title("Elevation Histogram ($(short_desc(img.desc)))")
		p.set_xlabel("z / nm")
		p.set_yscale(:log)
		fig
	end
end

# ╔═╡ 663e5a46-8c87-11eb-1861-2d41c20c561f
# Which image to analyze?
elimg = drop_peaks(load_image(IMAGESET[4,1]), 99.8)

# ╔═╡ 17506ae2-8c9d-11eb-1023-47dc2ab0a08f
@bind elxmin html"""XMin: <input type=range>"""

# ╔═╡ 22f7ceaa-8c9d-11eb-13c2-277e8e231198
@bind elxmax html"""XMax: <input type=range>"""

# ╔═╡ 2b2a3202-8c9d-11eb-3069-9f8a5e035ade
@bind elymin html"""YMin: <input type=range>"""

# ╔═╡ 2bc19408-8c9d-11eb-31cb-d3394f2a8b57
@bind elymax html"""YMax: <input type=range>"""

# ╔═╡ 2c7c590a-8c9d-11eb-2788-b58ac2cc1be1
function to_nm(img::STImage, cmin, cmax)
	return (cmin/100 * img.desc.dim, cmax/100*img.desc.dim)
end

# ╔═╡ c884bf64-8c83-11eb-2007-3335205cd4a7
begin
	filtered_img = img_line_bandpass(elimg, .5, .999)
	elevation_histogram(filtered_img, xlim=to_nm(elimg, elxmin, elxmax), ylim=to_nm(elimg, elymin, elymax))
end

# ╔═╡ 2cb805ae-8c84-11eb-156a-f9ea7f410dbe
MosaicViews.mosaicview(draw_window(elimg, xlim=to_nm(elimg, elxmin, elxmax), ylim=to_nm(elimg, elymin, elymax)),
	nrow=1)

# ╔═╡ dc72d534-8e28-11eb-05cf-33a47f126496
md"""## Forwards/backwards
"""

# ╔═╡ e15c3fd4-8e28-11eb-2ea8-49d758788d7d
@bind yfwbw html"""Y-location: <input type=range>"""

# ╔═╡ 03b4cf62-8e2c-11eb-1030-930a0de38e33
@bind SAMPLE_I Select(map(x -> string(x) => string(x), T_VALUES))

# ╔═╡ 99e656de-8e29-11eb-1502-b771dfe25ae3
FORWARD_BACKWARD_IMGIX = findnext(T_VALUES .== parse(Int64, SAMPLE_I), 1)

# ╔═╡ f1daba0e-8e28-11eb-2453-b510c78c79d7
crosssection([im0[FORWARD_BACKWARD_IMGIX], imbw[FORWARD_BACKWARD_IMGIX]], y=round(Int32, yfwbw/100 * size(im0[FORWARD_BACKWARD_IMGIX].data)[1]))

# ╔═╡ 2d219a18-8e29-11eb-0599-5923d5fd1744
MosaicViews.mosaicview(
	draw_crosssection(
		[im0[FORWARD_BACKWARD_IMGIX], imbw[FORWARD_BACKWARD_IMGIX]],
		y=round(Int32, yfwbw/100 * size(im0[1].data)[1])),
	nrow=1)

# ╔═╡ 4fe3d162-8e31-11eb-3d0a-21a1b33bdb92
draw_contour_image(im0[FORWARD_BACKWARD_IMGIX], imbw[FORWARD_BACKWARD_IMGIX])

# ╔═╡ Cell order:
# ╠═2118fcb2-8bd5-11eb-0e7c-45c134dba815
# ╠═347f0012-8bd5-11eb-2002-ed0d67c64fc4
# ╠═0302d252-8c7a-11eb-2478-c91fa37ada8a
# ╠═193bc8ae-8be3-11eb-09f0-830c019d5223
# ╠═25868a9c-8be3-11eb-3f7b-f137fbafc9be
# ╠═c788aa60-8c80-11eb-2176-371709527271
# ╠═35e67528-8e2a-11eb-35ea-af59501c2fa1
# ╠═c098474e-8bd9-11eb-122c-9bf28238275c
# ╠═5f1844b2-8c13-11eb-19cb-173ec2edf353
# ╠═2e67ea92-8c0f-11eb-12c5-7d238af2b887
# ╠═030b54f0-8c0b-11eb-2274-ef7457bb2a9a
# ╠═a404008e-8be1-11eb-02b4-9715c3d23bed
# ╠═d3e4b43c-8e2b-11eb-25cd-3d7e0638a833
# ╠═58626b54-8be4-11eb-0c74-d72aae7c5f44
# ╠═0d5e0b4a-8be4-11eb-258d-570f07ff325c
# ╠═13628ba6-8be4-11eb-1c7c-4d9f7ad32d83
# ╠═33c3a216-8ca4-11eb-0a98-7ba8604219e7
# ╠═da09493a-8cab-11eb-381c-a5caa293a1ff
# ╠═5b05cf7e-8c0a-11eb-2960-ab48a18ff722
# ╠═2fef49e6-8c0a-11eb-35d8-1b5c3a964caf
# ╠═3865b984-8c0a-11eb-1dcb-2f455f733c9d
# ╠═8db24656-8c81-11eb-178e-f1563ea03da2
# ╠═f496c10a-8c82-11eb-1727-cf094746827f
# ╠═663e5a46-8c87-11eb-1861-2d41c20c561f
# ╠═17506ae2-8c9d-11eb-1023-47dc2ab0a08f
# ╠═22f7ceaa-8c9d-11eb-13c2-277e8e231198
# ╠═2b2a3202-8c9d-11eb-3069-9f8a5e035ade
# ╠═2bc19408-8c9d-11eb-31cb-d3394f2a8b57
# ╠═2c7c590a-8c9d-11eb-2788-b58ac2cc1be1
# ╠═c884bf64-8c83-11eb-2007-3335205cd4a7
# ╠═2cb805ae-8c84-11eb-156a-f9ea7f410dbe
# ╠═dc72d534-8e28-11eb-05cf-33a47f126496
# ╠═e15c3fd4-8e28-11eb-2ea8-49d758788d7d
# ╠═03b4cf62-8e2c-11eb-1030-930a0de38e33
# ╠═99e656de-8e29-11eb-1502-b771dfe25ae3
# ╠═f1daba0e-8e28-11eb-2453-b510c78c79d7
# ╠═2d219a18-8e29-11eb-0599-5923d5fd1744
# ╠═4fe3d162-8e31-11eb-3d0a-21a1b33bdb92
