%
% Vorlage für Versuchsprotokolle im Grundpraktikum an der RWTH Aachen
% -------------------------------------------------------------------
%
% Bitte verwenden Sie diese Vorlage zur Erstellung Ihrer
% Protokolle.
%
% Wenn möglich, drucken Sie Ihre Protokolle bitte doppelseitig
% aus. Wenn Sie das Protokoll doppelseitig ausdrucken, verwenden Sie
% die twoside Option ...
\documentclass{protokoll}
% ... wenn Sie nicht doppelseitig ausdrucken können, lassen Sie sie
% weg:
%\documentclass{protokoll}

% Deutsche Anführungszeichen ,,'' automatisch
\usepackage{csquotes}
\MakeOuterQuote{"}

\usepackage{placeins}
\usepackage{physics}

\sisetup{range-phrase = --, range-units = single}

\usepackage[no-math]{fontspec}
\setmainfont{STIX Two Text}
\usepackage[notext]{stix}

\newcommand{\TODO}[1]{{\large \textcolor{red}{\textbf{TODO:}}} \emph{#1}}
\newcommand{\chisq}[3]{\ensuremath{\frac{\chi^2}{\mathrm{ndof}}
= \frac{#1}{#2} = #3}}
\newcommand{\uncertfrac}[1]{\ensuremath{\frac{\sigma_{#1}^2}{#1^2}}}

% Setzen Sie hier "I" für den Teil 1 oder "II" für den Teil 2 ein:
\praktikum{Fortgeschrittenenpraktikum}

% Setzen Sie hier das jeweilige Versuchsgebiet ein (Mechanik,
% Akustik, Thermodynamik, Elektrizitätslehre, usw.):
\versuchsgebiet{M21: PET}

% Setzen Sie hier Ihren Namen und Ihre Matrikelnummer sowie Ihre
% Gruppennummer ein:
\teilnehmer{Simon Neuland, Lewin Bormann}
\gruppe{18}

\begin{document}

% Ersetzen Sie den folgenden Text durch eine kurze Beschreibung der
% Versuchsziele.
\begin{versuchsziele}
Im Versuch M21 betrachten wir einen vereinfachten Positronen-Emissions-Tomografen (PET), und charakterisieren ihn durch Messung seiner (1) Dunkelstromwerte; (2) Energieauflösung; (3) Sensitivität; und (4) Zeitauflösung. Mit den gemessenen Eigenschaften berechnen wir die Position eines punktförmigen $\beta^+$-Strahlers innerhalb des PET-Scanners.
\end{versuchsziele}

\section{Grundlagen}

Die Positronen-Emissions-Tomographie ist ein Verfahren zur medizinischen Bildgebung. Im Gegensatz zu anderen Verfahren (CT, MRT) wird dabei der Patient nicht von außen "durchleuchtet", sondern erhält ein Radiopharmakon, also eine radioaktiv markierte Substanz. Die Markierung besteht meist in Substitution eines Atoms in einem Molekül der Substanz durch ein radioaktives Atom, das im $\beta^+$-Prozess zerfällt.

Ist dieses Molekül beispielsweise chemisch ähnlich zu Glukose, dann wird der Körper sie ähnlich verstoffwechseln. Bereiche mit hoher Stoffwechselrate, wie z.B. Tumoren, zeigen dann eine höhere Aktivität als umgebende Bereiche. \footnote{\emph{Experimentalphysik 4, Kern-, Teilchen- und Astrophysik}, W. Demtröder, Abschn. 8.1.4, S.\,203 ff.}

Die vom Radiopharmakon emittierten Positronen annihilieren nach kurzer Flugstrecke im Körper des Patienten mit Elektronen und senden dabei zwei Gamma-Photonen aus, die wegen der Impulserhaltung bei nicht allzu hohen Positronengeschwindigkeiten in einem Winkel von \SI{180}{\degree} abgestrahlt werden. Mit einer ringförmigen Anordnung von Detektoren, innerhalb derer sich der Patient oder die Quelle befindet, können diese Photonpaare nun detektiert werden.

Die Detektoren bestehen in diesem Experiment aus LYSO-Szintillatoren (Lutetiumyttriumoxoorthosilikat), die an Silicium-Photomultiplier gekoppelt sind. Diese sind als SPADs (\emph{Single Photon Avalanche Diode}s) realisiert. SPADs sind Halbleiterdioden, die in Sperrichtung betrieben werden. Die Biasspannung erzeugt ein elektrisches Feld in der Diode; aufgrund der Ladungsträgerverarmung fließt aber zunächst kein Strom. Sobald ein geladenes Teilchen die Diode passiert, werden Ladungsträger (Elektron-Loch-Paare) erzeugt, die wegen des elektrischen Felds zu den Elektroden wandern. Auf dem Weg erzeugen sie sekundäre Ladungsträger, und verstärken damit das ursprüngliche Signal. Die erzeugten Ladungsträger werden durch die Elektronik erfasst.

Einige tausend SPADs sind jeweils zu Kanälen (Pixeln) zusammengeschaltet. Einzelne SPADs können individuell ausgelesen oder deaktiviert werden.

Für jedes Photonpaar, das den Ort der Annihilation verlässt und detektiert wird, ergibt sich eine LOR (\emph{Line of Response}). Falls die Photonen ohne weitere Streuung den Annihilationsort verlassen konnten, verläuft die Linie durch diesen. Nach der Messung vieler solcher Photonpaare lassen sich die Entstehungsorte durch Algorithmen bestimmen; beispielsweise durch Überlagerung der LORs. Photonen, die einzeln ankommen -- für die also kein koinzidentes Photon detektiert wurde -- werden nicht betrachtet, da für sie kein Ursprungsort bestimmt werden kann.

Eine Verfeinerung des Verfahrens besteht in der Messung der ToF (\emph{Time of Flight}), also der Zeitdifferenz der Ankunft der beiden Photonen in Detektoren. Kommen sie zeitgleich an, so muss sich der Ursprung der Photonen in der Mitte der beiden Detektoren befinden; entsprechend lässt sich der Entstehungsort entlang der LOR eingrenzen. Einer Abweichung von \SI{1}{\cm} von der Mitte der LOR entspricht einer Zeitdifferenz der Ankunft von \SI{67}{\pico\second}.\footnote{\emph{M21 Medical Physics: Positron Emission Tomography (PET)}, Karl Krüger, 2021} Die Zeitauflösung der verwendeten Detektoren muss demnach auch in dieser Größenordnung liegen, um eine ToF-Messung überhaupt zu ermöglichen.

\section{Experiment}

In diesem Experiment charakterisieren wir zunächst die PET-Detektoren:

\begin{description}
\item[Dunkelstrom und Rauschen] Wir bestimmen, wie viele Photonen im leeren Aufbau gemessen werden, und wie viele Events die einzelnen SPADs ohne aktive Strahlungsquelle detektieren.
\item[Energieauflösung] Anhand des Elektronenmassen-Photopeaks bei $E = \SI{511}{\keV}$ bestimmen wir die Energieauflösung des Detektors, also den minimalen Abstand von Energiepeaks, die noch unterschieden werden können.
\item[Sensitivität] Wir berechnen die erwartete Sensitivität aus der geometrischen Anordnung und vergleichen sie mit der gemessenen Sensitivität (detektierte Events vs. erwartete Events).
\item[Zeitauflösung] Eine punktförmige $\beta^+$-Strahlungsquelle (Na-22) wird in die Apparatur eingesetzt. Wir betrachten die gemessenen ToF-Zeitdifferenzen und berechnen die Zeitauflösung des Detektors und der dahinterliegenden Messeinrichtung. Mit den Kalibrierungswerten eines mittig platzierten Strahlers korrigieren wir die Zeitmessungen für drei weitere Positionen des Strahlers und berechnen die Lage dieser drei Positionen.
\end{description}

\subsection{Aufbau}

Der verwendete Aufbau entspricht einem einzelnen Detektorpaar eines großen PET-Scanner, in dem viele solcher Paare ringförmig angeordnet sind. Die Sensoren (SPUs, \emph{Singles Processing Unit}s) stehen sich in einem Abstand von \SI{292}{\mm} gegenüber. Zwischen den Sensoren befindet sich ein automatisch steuerbarer Arm, der die Punktquelle an unterschiedlichen Positionen zwischen den Detektoren positionieren kann. Die Messung der Flugzeit basiert darauf, die Quelle zu verschieben.

\begin{figure}[h]
    \centering
    \includegraphics[scale=1.25]{aufbau.pdf}
    \caption{Skizze des Versuchsaufbaus. Bestandteile: D: Detektor; S: Quelle (Source); SPU: Singles Processing Unit, am Detektor angeordnet; M: Positioniereinheit (3D), B: Backbone.}
    \label{fig:aufbau}
\end{figure}

Die Detektoren sind in einen Rahmen aus Aluminium-Profilen eingebaut, der auch das \emph{backbone} enthält, das für die Zeitsynchronisation der Sensoren verantwortlich ist. Diese sind flüssigkeitsgekühlt (denn das Rauschen der SiPM-Sensoren ist temperaturabhängig), und liefern ihre Daten an einen Aufnahmeserver (DAPS, \emph{Data Acquisition and Processing Server}). Da die Detektoren Photonen messen, muss außerdem Umgebungslicht abgeschirmt werden. Je nach Aufbau geschieht dies durch Abschirmung der einzelnen Detektoren, oder des ganzen Aufbaus innerhalb einer \emph{dark box}.

In der verwendeten Quelle erzeugen zerfallende Na-22-Atome $\beta^+$-Strahlung.

\section{Dunkelstrom \& SPAD-Rauschen}

% TODO: Simon

\subsection{Rauschmessung}
	Zunächst wurde eine Rauschmessung durchgeführt. Dafür wurde der abgeschirmte Detektor ohne Quelle betrieben und die durchschnittliche Zählrate jedes einzelnen SPADs aufgenommen.
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm1_3.pdf}
		\caption{Dark Count Map der SPU 1\_3.}
		\label{fig:dcm1_3}
	\end{figure}
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm1_6.pdf}
		\caption{Dark Count Map der SPU 1\_6.}
		\label{fig:dcm1_6}
	\end{figure}
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm2_3.pdf}
		\caption{Dark Count Map der SPU 2\_3. Die weiße Fläche entspricht zwei defekten Sensor-Dies.}
		\label{fig:dcm2_3}
	\end{figure}
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm2_6.pdf}
		\caption{Dark Count Map der SPU 2\_6. Die weiße Fläche entspricht einem defekten Sensor-Die.}
		\label{fig:dcm2_6}
	\end{figure}
	
	In \autoref{fig:dcm1_3} bis \autoref{fig:dcm2_6} ist die Zählrate der einzelnen SPADs als logarithmisches Falschfarbenbild dargestellt. Die logarithmische Skala wurde gewählt, da die Zählraten einen Bereich von mehreren Größenordnungen umfassen. Umgekehrt bedeutet das auch, dass es ''Ausreißer'' mit besonders hohen Zählraten gibt -- diese Sensoren sind möglicherweise defekt. (Eine nicht-logarithmische Darstellung, bei der die hohen Zählraten besonders hervorstechen, findet sich unter \autoref{fig:dcm1_3_alt} bis \autoref{fig:dcm2_6_alt} im Anhang.)
	
	\subsection{Rauschreduktion}
	\begin{figure}
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm_inhibition.pdf}
		\caption{Einfluss, den das Abschalten der "schlechtesten" SPADs auf den gesamten Dunkelstrom hat.}
		\label{fig:dcm_inhibition}
	\end{figure}
	
	Die Änderung des Dunkelstroms durch Abschalten der am stärksten rauschenden SPADs ist in \autoref{fig:dcm_inhibition} dargestellt. Man erkennt das Pareto-Prinzip wieder: Ein gezieltes Ausschließen von 20\% der SPADs ist in der Lage, den Dunkelstrom um über 80\% zu reduzieren.

\FloatBarrier
\newpage

\section{Sensitivität}

% TODO: Simon

\subsection{Single-Messung}
	\autoref{fig:sens_s_emp} zeigt die Zahl aller aufgenommenen Messpunkte (''Singles'') im Verlauf einer Rauschmessung. Es wurde eine Dunkelzählrate von $\si{10014}\pm\SI{0,04}{\Hz}$ %Gibt es einen schöneren Weg, das mit siunitx zu schreiben? Das kommt mit ziemlich hacky vor.
	gemessen. Man erkennt an der eingefügten linearen Regression, dass die Messpunkte gut einer Gerade folgen. Die Struktur im Residuenplot und der damit zusammenhängende hohe $\chi^2$-Wert ergibt sich wahrscheinlich aus Ungenauigkeiten in der Zeitmessung (beispielsweise eine nicht exakte Zuordnung der Zeitstempel), die hier nicht berücksichtigt wurden. Als Unsicherheit auf die gemessene Anzahl wurde ein Poisson-Fehler angenommen: Die Zahl der Ereignisse $\delta N_{\mathrm{dunkel}}$, die während einem Zeitschritt geschehen, ist poissonverteilt, und die zugehörige Unsicherheit ist
	
	\begin{equation}
		\sigma_{N_{\mathrm{dunkel}}}=\sqrt{\delta N_{\mathrm{dunkel}}}.
	\end{equation}
	
	Der genaue Wert der Unsicherheit ist damit für jeden Messpunkt verschieden. Da die Länge der Zeitschritte allerdings immer ziemlich genau \SI{1}{\s} betrug und die Messpunkte sehr gut der Regression folgen ist \num{10000} ein guter Schätzwert für $\delta N_{\mathrm{dunkel}}$ und die Unsicherheit jedes Messpunktes liegt bei ungefähr \num{100}.
	
	\autoref{fig:sens_s_source} zeigt im Detektor mit Quelle die Zahl der Singles, die explizit von der Aktivität dieser Quelle ausgehen. Dazu wurde von den Messwerten des Detektors mit Quelle ($N_\mathrm{mit~Quelle} \approx \num{12500}$) die durchschnittliche Dunkelzählrate ($N_\mathrm{dunkel} \approx \num{10000}$) abgezogen. Daraus ergibt sich eine Zählrate von $(\num{2521}\pm\num{0.08})\,\si{\Hz}$. Auch hier zeigt sich eine lineare Abhängigkeit der Messpunkte von der Zeit, d.h. eine konstante Zählrate. In den Residuen erkennt man die gleiche Struktur wie bei der Rauschmessung. Auch hier wird eine Poisson-Verteilung mit entsprechender statistischer Unsicherheit angenommen. Da sich die untersuchten Daten aus der Differenz zweier Messungen ergeben (die gesamte Single-Rate im Detektor mit Quelle abzüglich der Dunkelmessung, $N_{\mathrm{Quelle}}=N_{\mathrm{mit~Quelle}}-N_{\mathrm{dunkel}}$), müssen hier zwei Fehler fortgepflanzt werden:
	\begin{equation}
		\sigma_{N_{\mathrm{Quelle}}}=\sqrt{\sigma_{N_{\mathrm{mit~Quelle}}}^2+\sigma_{N_{\mathrm{dunkel}}}^2}.
	\end{equation}
	Auch hier ist die Größe der Unsicherheit bei jedem Messpunkt verschieden, eine Abschätzung aus den durchschnittlichen Zählraten liefert als Schätzwerte
	\begin{equation}
		\sigma_{N_{\mathrm{mit~Quelle}}}\approx\sqrt{\num{12500}} \mathrm{\,\,und\,\,} \sigma_{N_{\mathrm{dunkel}}}\approx\sqrt{\num{10000}},
	\end{equation}
	woraus sich $\sigma_{N_\mathrm{Quelle}}\approx150$ ergibt. %Korrelation?
	
	\begin{figure}
		\centering
		\includegraphics[scale=.8]{sensitivity_images/sens_single_empty.pdf}
		\caption{Gesamtzahl der Singles, die bei einem leeren Detektor gemessen wurde, aufgetragen gegen die Zeit. Durch die Messpunkte wurde eine lineare Regression gelegt, deren Steigung die durchschnittliche Zählrate ist. \chisq{543380}{446}{1218}.}
		\label{fig:sens_s_emp}
	\end{figure}
	
	\begin{figure}
		\centering
		\includegraphics[scale=.8]{sensitivity_images/sens_single_with-source.pdf}
		\caption{Zahl der Singles im Detektor mit Probe abzüglich der Dunkelzählrate (siehe \autoref{fig:sens_s_emp}), aufgetragen gegen die Zeit. Durch die Messpunkte wurde eine lineare Regression gelegt, deren Steigung die durch die Quellenaktivität verursachte durchschnittliche Zählrate ist. \chisq{373788}{360}{1038}.}
		\label{fig:sens_s_source}
	\end{figure}
	
	Die errechneten Single-Zählraten sind also
	
	\begin{center}
	\begin{tabular}{|l|cc|}
		\hline
		&Hintergrund&Quelle (ohne Hintergrund)\\
		\hline
		Zählrate [Hz]&10014&2521\\
		Unsicherheit [Hz]&0,036&0,075\\
		\hline
	\end{tabular}
	\end{center}
	
	\subsection{Coincidence-Messung}
	\label{subsec:sens_coinc}
	\autoref{fig:sens_c_emp} bildet die Zahl der Coincidences -- d.h. die Ereignisse, bei denen innerhalb eines kurzen Zeitfensters (hier: \SI{10}{\ns}) auf den gegenüberliegenden Sensoren jeweils ein Single aufgenommen wird -- im Verlauf einer Messreihe mit leerem Detektor ab. Durch die Messpunkte wurde eine lineare Regression gelegt. Anhand der rein statistischen Streuung der Residuen um den Nullpunkt lässt sich erkennen, dass die Messpunkte sich gut durch ein lineares Modell darstellen lassen. Es ergab sich aus der Regression eine durchschnittliche Dunkelzählrate von $\si{9,996}\pm\SI{0,001}{\Hz}$. Die Unsicherheiten der einzelnen Messpunkte ergeben sich analog zu den Überlegungen zu \autoref{fig:sens_s_emp}, wobei $\sqrt{10}\approx3,61$ (Wurzel der Zählrate pro Messschritt) wieder einen sinnvollen Richtwert für $\sigma_{N}$ ergibt.
	
	\autoref{fig:sens_s_source} bildet die Zahl der Coincidences im Detektor mit Quelle abhängig von der Zeit ab, wobei erneut die zuvor bestimmte Dunkelzählrate abgezogen wurde. Auch hier wurde eine lineare Regression an die Messwerte angepasst, die dem Residuenplot nach zu urteilen eine sinnvolle Annahme ist. Die Regression ergab eine durch die Quelle verursachte Coincidence-Rate von $\si{201,90}\pm\SI{0,01}{\Hz}$. Der fortgepflanzte Poisson-Fehler auf die Messwerte wurde analog zur Diskussion von \autoref{fig:sens_s_source} berechnet; als Schätzwert ergibt sich
	\begin{equation}
		\sigma_\mathrm{coincidence} = \sqrt{\sqrt{10}^2+\sqrt{201.9}^2}\approx14,9.
	\end{equation}
	
	\begin{figure}
		\centering
		\includegraphics[scale=.8]{sensitivity_images/sens_coinc_empty.pdf}
		\caption{Gesamtzahl der Coincidences, die bei einem leeren Detektor gemessen wurden, aufgetragen gegen die Zeit. Durch die Messpunkte wurde eine lineare Regression gelegt, deren Steigung die durchschnittliche Coincidence-Zählrate ist. \chisq{14200}{467}{30,4}.}
		\label{fig:sens_c_emp}
	\end{figure}
	
	\begin{figure}
		\centering
		\includegraphics[scale=.8]{sensitivity_images/sens_coinc_with-source.pdf}
		\caption{Zahl der Coincidences im Detektor mit Probe abzüglich der Dunkelzählrate. Durch die Messpunkte wurde eine lineare Regression gelegt, deren Steigung die durch die Quellenaktivität verursachte durchschnittliche Coincidence-Zählrate ist. \chisq{8711}{344}{25,3}.}
		\label{fig:sens_c_source}
	\end{figure}
	
	Die errechneten Coincidence-Zählraten sind also\\
	\begin{tabular}{|l|cc|}
		\hline
		&Hintergrund&Quelle (ohne Hintergrund)\\
		\hline
		Zählrate [Hz]&9,996&21,902\\
		Unsicherheit [Hz]&0,001&0,008\\
		\hline
	\end{tabular}
	
	
	\subsection{Geometrische Effizienz und Erwartung}
	\label{subsec:sens_geometry}
	Wir wollen nun die gemessene Single-Rate mit der theoretischen Erwartung vergleichen, die sich aus den Dimensionen des Detektors ergibt.
	
	Die Fläche eines einzelnen SPADs ist laut Datenblatt $A_{\mathrm{SPAD}}=\SI{59,4}{\mu\m}\times\SI{64}{\mu\m}=3,81\cdot10^{-9}\,\mathrm{m}^2$. Ein dSiPM-Sensorkanal besteht aus 3200 SPADs, vier Sensorkanäle bilden einen Sensor-Die, auf jedem Sensor-Board befinden sich 16 Sensor-Dies und insgesamt werden vier Sensor-Boards verwendet. Damit ist die Gesamt-Detektorfläche
	\begin{equation}
		A_{\mathrm{Det}}=4\cdot16\cdot4\cdot3200\cdot A_{\mathrm{SPAD}}=3,11\cdot10^{-3}\,\mathrm{m}^2
	\end{equation}
	beziehungsweise
	\begin{equation}
		A_{\mathrm{Det}}^\prime = \frac{61}{64} A_\mathrm{Det} = 2,95\cdot10^{-3}\,\mathrm{m}^2
	\end{equation}
	wenn die drei defekten Dies berücksichtigt werden. Der Abstand der Sensoren ist \SI{292}{\mm}. Unter der Annahme dass die Quelle sich in der Mitte zwischen den Sensoren befindet und diese näherungsweise auf einer Kugelschale (Radius $r=\SI{0,146}{m}$) liegen ergibt sich eine Raumabdeckung von
	\begin{equation}
		\delta=\frac{A_\mathrm{Det}^\prime}{4\pi r^2}=0,01107.
	\end{equation}
	
	Die Aktivität der verwendeten Quelle wurde am 1. April 2013 zu $I_0=1,11\,\mathrm{MBq}$ gemessen. Na-22 hat eine Halbwertszeit von $t_{1/2}=2,60\,\mathrm{yr}$, womit die Aktivität zum Zeitpunkt der Versuchsdurchführung im Februar 2020
	\begin{equation}
		I=I_0\cdot\Big(\frac{1}{2}\Big)^{t/t_{1/2}}=135,5\,\mathrm{kBq}
	\end{equation}
	betrug. Da diese Aktivität der Rate an $\beta^+$-Zerfällen entspricht, von denen jeder zwei $\gamma$-Quanten erzeugt, ist die Photonen-Entstehungsrate
	\begin{equation*}
		I_\gamma=2\cdot I=271,0\,\mathrm{kHz}.
	\end{equation*}
	Da die Abstrahlung der Photonen räumlich isotrop ist (egal welchen Streuprozessen sie unterliegen) erwarten wir, dass das Verhältnis aus Photonen die den Detektor erreichen und der Gesamtzahl der abgestrahlten Photonen der räumlichen Abdeckung der Detektoren entspricht:
	\begin{equation}
		I_\mathrm{Det}=\delta\cdot I_\gamma=2999\,\mathrm{Hz}.
	\end{equation}
	Im Experiment wurde festgestellt, dass dort die durch die Na-22-Quelle verursachte Zählrate \SI{2521}{\Hz} betrug. Die tatsächliche Zählrate ist also 16\% geringer als die Erwartung aus der idealisierten geometrischen Betrachtung. Die Differenz könnte beispielsweise von Photonen verursacht werden, die den Detektor unentdeckt durchqueren, zum Beispiel in seiner Totfläche.
	
	\subsection{Sensitivität}
	Die Sensitivität eines Detektors drückt aus, wie effizient Strahlungsereignisse (hier: $\beta^+$-Zerfälle der Na-22-Probe) in brauchbare Detektor-Signale (hier: Coincidence-Counts) umgewandelt werden. Die Rate der $\beta^+$-Zerfälle wurde in \autoref{subsec:sens_geometry} zu $I=135,5\,\mathrm{kHz}$ bestimmt. Ebenso ist aus \autoref{subsec:sens_coinc} die durch die Quelle verursachte Coincidence-Zählrate als $201,9\,\mathrm{Hz}$ bekannt. Daraus ergibt sich die Sensitivität des Detektors:
	\begin{equation}
		S=\frac{201,9\,\mathrm{Hz}}{135,5\,\mathrm{kHz}}=1,49\cdot10^{-3}.
	\end{equation}

\FloatBarrier
\newpage


\section{Energieauflösung}

% TODO: Lewin

Die Detektoren erlauben die Messung des Energiespektrums der gemessenen Photonen. Wir werten das erhaltene Energiespektrum unter Berücksichtigung des Untergrunds aus.

In den folgenden Abbildungen sind jeweils Energiespektren zu sehen, und zwar

\begin{itemize}
    \item in \autoref{fig:energy_single_empty} das Energiespektrum der leeren Apparatur, in der der Untergrund gemessen wird, der vor allem aus dem Zerfall des Lutetiums im LYSO-Kristall des Szintillators stammt;
    \item in \autoref{fig:energy_single} das Energiespektrum der Apparatur mit eingesetzter Quelle;
    \item und in \autoref{fig:energy_coinc} das Energiespektrum der nach Koinzidenz gefilterten Events.
\end{itemize}

In \autoref{fig:energy_single} und \autoref{fig:energy_coinc} sind Normalverteilungen an die Peaks der Elektronenenergie angepasst (in \autoref{fig:energy_single_empty} war das nicht sinnvoll möglich, da das Modell nicht passt). Dabei haben wir jeweils Werte für $\chi^2/$Freiheitsgrad zwischen 2 und 5 erreicht, was für eine etwas zu klein eingeschätzte Messunsicherheit spricht. Unter der Annahme einer Poissonverteilung für die Anzahl der Events in jedem Energiebucket haben wir dabei die Unsicherheit der Anzahl $n_i$ der Events im i-ten Bucket zu $\sigma_{n_i} = \sqrt{n_i}$ geschätzt.

Für die Energieauflösung betrachten wir den FWHM-Wert der Verteilungen. Da die Energie bei $m_e$ scharf sein sollte, ist der FWHM-Wert als Auflösung der Energiemessung zu betrachten. Da wir dafür am Untergrund nicht interessiert sind, benutzen wir \autoref{fig:energy_coinc}, woraus sich
%
\begin{equation}\mathrm{FWHM}_{E} = \SI{69}{\kilo\eV} ~ \hat = ~\SI{14.3}{\percent}\end{equation}

ergibt. Das ist die Energieauflösung des Detektors; Peaks im Spektrum, die näher beieinander liegen, kann er nicht unterscheiden.

\subsection{Energiekalibrierung}

Wie in der Vorbesprechung angekündigt, wurden die Sensoren mit einem anderen Gerät kalibriert als dem, das die Messung hier durchgeführt hat. Daraus ergibt sich eine gemessene mittlere Elektronenmasse von \SI{519.5}{\kilo\eV}, die um \SI{8.5}{\kilo\eV} vom Literaturwert von $m_e = \SI{511}{\kilo\eV}$ abweicht. 

Um die Frage zu klären, ob sie wenigstens über den Messbereich gleichmäßig falsch ist -- der Offset also über den gesamten Messbereich konstant ist --, können wir den Gamma-Peak des $^{22}$Na-Zerfalls zu $^{22}$Ne bei einer Energie von \SI{1274.53}{\kilo\eV} (laut Praktikums-Anleitung) betrachten.

In \autoref{fig:energy_single} ist dieser Peak bereits zu erkennen; in \autoref{fig:na22peak} bestimmen wir das Maximum mittels einer angepassten Gauß-Verteilung zu \SI{1250}{\keV}. Der FWHM-Wert beträgt für diese Anpassung etwa \SI{208}{\keV}; das zeigt uns, dass die absolute Auflösung des Detektors bei hohen Energien niedriger ist. Relativ zur Photonenenergie ist sie mit \num{16.6}\,\% allerdings ähnlich groß wie die Auflösung am $m_e$-Peak (dort \SI{14.3}{\percent}). Die Anpassungsgüte ist bei einem $\chi^2$/Freiheitsgrad-Wert von 1.4 als gut zu bewerten.

Wir sehen, dass am $m_e$-Peak die Kalibrierung in etwa \SI{1.7}{\percent} zu hohen Energien resultiert (\SI{519.5}{\keV} vs. \SI{511}{\keV}), während im Bereich des $^{22}$Na-Photopeaks die gemessene Energie um \SI{2}{\percent} zu niedrig liegt (\SI{1250}{\keV} vs. \SI{1275}{\keV}). Die Kalibrierung ist über den gesamten Messbereich nicht konstant. Allerdings muss auch beachtet werden, dass die grobe Auflösung des Detektors die Aussagekraft dieses Vergleichs einschränkt.

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{energy_images/energy_single_empty.log_plot.pdf}
    \caption{Das Energiespektrum bei leerer Apparatur. Der Untergrund stammt von der Radioaktivität des Lutetiums im Szintillator: $^{176}$Lu hat eine Halbwertszeit von \num{3.8e10}\,yr. Wir haben zwar eine Normalverteilung an den Peak um \SI{641}{\keV} angepasst, allerdings ist der Peak nicht kompatibel mit den Daten.}
    \label{fig:energy_single_empty}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{energy_images/energy_single_no_background.pdf}
    \caption{Energiespektrum (a) der Singles in blau, (b) abzüglich Untergrund in orange. Es tritt der charakteristische Photopeak bei \SI{511}{\kilo\eV} hervor, und der $^{22}$Na-spezifische Photopeak bei \SI{1275}{\kilo\eV} ist zu erahnen. In \autoref{fig:na22peak} wird die Lage des Peaks durch einen Normalverteilungs-Fit bestimmt.}
    \label{fig:energy_single}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{energy_images/energy_coinc.log_plot.pdf}
    \caption{Energiespektrum der nach Koinzidenz gefilterten Singles: das für PET genutzte Spektrum. Der Photopeak bei der Elektronenmasse $m_e$ sticht heraus, und bei niedrigeren Energien sehen wir u.\,a. Compton-gestreute Photonen. Der Untergrund ist nicht mehr stark vorhanden (da er kaum koinzidente Photonen enthält). Für den Fit haben wir die Punkte \SIrange{450}{600}{\kilo\eV} benutzt.}
    \label{fig:energy_coinc}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{energy_images/na22_photopeak.pdf}
    \caption{Bestimmung des gemessenen Photopeaks um \SI{1275}{\kilo\eV}.}
    \label{fig:na22peak}
\end{figure}

\FloatBarrier
\newpage

\section{Time of Flight}

% TODO: Lewin

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{tof_images/all_time_diff.pdf}
    \caption{Übersicht der Time-of-Flight-Messungen}
    \label{fig:all_time_diff}
\end{figure}

In \autoref{fig:all_time_diff} sind alle vier gemessenen Zeithistogramme dargestellt. Dabei ist auf der x-Achse die Zeitdifferenz $\Delta t$ abzulesen, mit denen die Koinzidenz-Gamma-Photonen eingetroffen sind, und auf der y-Achse ist die Gesamtanzahl aufgetragen. Die blaue Messung 0 ist dabei die "Nullmessung", sie wurde mit der Quelle im Zentrum des Aufbaus aufgenommen. Das Ziel dieses Abschnitts ist es, die Zeitauflösung des Detektors, sowie die Lage $\Delta x$ der Punktquelle für drei weitere Messungen bezogen auf den Mittelpunkt der Anordnung zu bestimmen.

\subsection{Zeitauflösung}

Aus der 0-Messung lässt sich dann in \autoref{fig:time_0} die Zeitauflösung ablesen. Da eine Punktquelle vorliegt, ist eigentlich zu erwarten, dass der Peak eine Ausdehnung von nur wenigen Picosekunden ($\SI{1}{\mm} \Leftrightarrow \SI{6.7}{\ps}$) besitzt. Dies ist bereits auf den ersten Blick nicht der Fall; der Grund für den breiten Peak liegt bei der begrenzten Zeitauflösung der Detektoren: Zufällige Schwankungen (jitter), Abweichungen bei der Synchronisation, und Variationen des Pfads der Positronen und der Schauerbildung im Szintillator führen zu gestreuten Zeitstempeln der individuellen Messungen. Solche Unsicherheiten lassen sich nur begrenzt reduzieren.

Um die Auflösung zu quantifizieren, haben wir (wie bei den Energiemessungen oben) eine Modellfunktion $\lambda$ an die Daten angepasst; dabei werden drei freie Parameter ($\mu,~\sigma, \Delta$) benutzt, wobei $\Delta$ die vertikale Verschiebung und $f(x; \mu, \sigma)$ die Normalverteilung ist\footnote{Wir hätten alternativ auch einen Skalierungsparameter statt einen Offset einführen können. Das ändert jedoch nicht viel am Ergebnis, und zeigt weniger plausible Residuen; beide Modelle haben wenig Aussagekraft außerhalb des Peaks (siehe \autoref{fig:time_0_scale} im Anhang)}:

\begin{equation}\lambda(\Delta t; \mu, \sigma, \Delta) = f(\Delta t; \mu, \sigma) - \Delta\end{equation}

Damit stehen $N-3$ Freiheitsgrade zur Verfügung (bei $N$ Buckets mit Breite $\Delta t$). Zur Anpassung haben wir weiterhin angenommen, dass die Anzahl Events in jedem Bucket Poisson-verteilt ist; die zur Anpassung verwendeten und im Residuenplot dargestellten Messunsicherheiten sind also $\sigma_i = \sqrt{n_i}$, ($n_i$ Events im $i$-ten Bucket).

Außerdem haben wir die Anpassung nur mit einem Teil der Daten vorgenommen, nämlich jeweils den Punkten, die innerhalb der grünen Linien liegen, die den Peak jeweils bei 1250 Counts schneiden. Diese Auswahl haben wir zur Verbesserung der Anpassung vorgenommen: Die gesamte gemessene Verteilung ist nicht kompatibel mit einer Normalverteilung, der mittlere Teil mit hohen Event-Anzahlen jedoch schon. Wir sehen das auch daran, dass die Residuen bis auf lokale systematische Abweichungen wohlverteilt sind. Auch die Berechnung der $\chi^2$-Werte bezieht sich nur auf die betrachteten Daten im Peak. Für unsere Anpassungen ergeben sich $\chi^2$/Freiheitsgrad-Werte zwischen \num{1} und \num{2.5}, was für eine statistisch gute Anpassung spricht.

Aus den verschiedenen Messungen ergibt sich eine Zeitauflösung von etwa

\begin{equation}\mathrm{FWHM}_{\Delta t} = \SI{350}{\ps}\end{equation}

\subsection{Ortsauflösung}

Nun können wir die Ortsauflösung sowie die Ortslagen der Punktquelle in den Messungen 1 bis 3 bestimmen. Dabei gilt zunächst für die Bestimmung der Ortslage $\Delta x$ ganz allgemein:

\begin{equation}
    \Delta x = \frac{c}{2} \Delta t
    \label{eqn:location}
\end{equation}

Hier ist $\Delta x$ die Distanz zwischen der Quelle und dem Mittelpunkt der Verbindungslinie der beiden Detektoren; denn für jeden Schritt $\Delta x$ nach rechts (oder links) erreicht ein Signal den rechten (linken) Detektor um $\Delta x/c$ früher und den linken (rechten) Detektor um $\Delta x/c$ später. Die gemessene Zeitdifferenz $\Delta t$ entspricht also der doppelten Laufzeit der Verschiebung $\Delta x$. Es folgt \autoref{eqn:location}.

Die oben berechnete Zeitauflösung entspricht mit \autoref{eqn:location} einer Ortsauflösung von \begin{equation}\mathrm{FWHM}_x = \frac{c}{2}  \mathrm{FWHM}_{\Delta t} = \SI{5.2}{\cm}\end{equation}

Nun können wir die drei Positionen der Punktquelle näherungsweise bestimmen. Dazu wollen wir die Messungen durch die 0-Messung im Mittelpunkt korrigieren. Im folgenden definieren wir $\sigma_{\Delta t / x}$ als den FWHM-Wert der jeweiligen Zeit- bzw. Ortsmessung (und \emph{nicht} etwa die Standardabweichung der angepassten Normalverteilung).

Wir ziehen von den in Messungen $i = 1$ bis $3$ bestimmten $\Delta t_i \pm \sigma_{\Delta t_i}$ die Zeitdifferenz der 0-Messung $\Delta t_0 \pm \sigma_{\Delta t_0}$ ab, um die korrigierte Zeitdifferenz $\Delta t_i' \pm \sigma_{\Delta t_i'}$ zu erhalten. Es gilt dann mit \autoref{eqn:location}:

\begin{align}
    \Delta x_i &= \frac{c}{2} \Delta t_i' := \frac{c}{2} (\Delta t_i - \Delta t_0)\qquad\text{(Korrigierte Ortslage)} \\
    \sigma_{\Delta x_i} &= \frac{c}{2} \sigma_{\Delta t_i'} = \frac{c}{2} \sqrt{\sigma_{\Delta t_0}^2 + \sigma_{\Delta t_i}^2}\qquad\text{(Unsicherheit der korr. Ortslage)}
\end{align}

In der folgenden Tabelle sind die unkorrigierten und korrigierten Messwerte zu finden. \autoref{fig:location_prob} zeigt außerdem die Wahrscheinlichkeit für jede Messung, die Quelle an einem gegebenen Ort zu finden. Dafür haben wir die Anpassungen der ToF-Messung in den Ortsraum umgerechnet; im Plot sind die Wahrscheinlichkeitsverteilungen (PDF) für jede Position gezeichnet.

\begin{center}
    \begin{tabular}{c|cc|cc}
         Messung & Zeitdiff. & Zeitdiff., korrigiert & Ortslage &  Ortslage, korrigiert \\
         \hline
         0 & \SI{+9(357)}{\ps} & \emph{(Referenz)} & \SI{+1.3(535)}{\mm} & \emph{(Referenz)} $ = \SI{0}{\mm} $ \\
         1 & \SI{+372(330)}{\ps} & \SI{+363(485)}{\ps} & \SI{+56(49)}{\mm} & \SI{+54(73)}{\mm} \\
         2 & \SI{-103(355)}{\ps} & \SI{-112(503)}{\ps} & \SI{-16(53)}{\mm} & \SI{-17(75)}{\mm} \\
         3 & \SI{-189(348)}{\ps} & \SI{-198(498)}{\ps} & \SI{-28(52)}{\mm} & \SI{-30(75)}{\mm} \\
    \end{tabular}
\end{center}

Die großen Unsicherheiten der Zeitdifferenzen bzw. Ortslagen stammen von der Korrektur. Da sich die Unsicherheiten der Differenzen von Messung und Korrektur quadratisch addieren, steigt die Unsicherheit des korrigierten Werts um einen Faktor von etwa $\sqrt{2}$, da die FWHM-Unsicherheit für alle Zeitmessungen in etwa gleich ist (siehe Gleichungen oben).

Die erzielten Unsicherheiten von etwa \SI{8}{\cm} stimmen mit der im Skript angegebenen überein: \emph{"[...] the probable area of annihilation along the LOR can be limited to a length of \SIrange{6}{15}{\centi\meter}"}. (S.\,18 dort)

% Python-Skript:
% Zero-measure time: 8.609483100631476+/-356.94646664036554 zero-measure location: 0.0012905290504238859+/-0.0535049293042651
% 1 Original time: 371.9320340347157+/-329.79710352369324 Original location: 0.05575120934610354+/-0.04943534215332423 
%	Corrected time: 363.32255093408423+/-485.9803592118299 Corrected location: 0.05446068029567966+/-0.07284662321391873
% 2 Original time: -103.44515212980865+/-355.3341973902798 Original location: -0.015506038212589633+/-0.05326325622354459 
%	Corrected time: -112.05463523044013+/-503.6597779077022 Corrected location: -0.016796567263013523+/-0.07549670140734208
% 3 Original time: -189.41217726415323+/-347.6609719217679 Original location: -0.028392171098576106+/-0.05211306866154789 
%	Corrected time: -198.0216603647847+/-498.2759591277005 Corrected location: -0.029682700148999992+/-0.07468968727460044

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.7]{tof_images/time_0.log_plot.pdf}
    \caption{Time-of-Flight-Messung für Null-Position}
    \label{fig:time_0}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.7]{tof_images/time_1.log_plot.pdf}
    \caption{Time-of-Flight-Messung für erste Position}
    \label{fig:time_1}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.7]{tof_images/time_2.log_plot.pdf}
    \caption{Time-of-Flight-Messung für zweite Position}
    \label{fig:time_2}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.7]{tof_images/time_3.log_plot.pdf}
    \caption{Time-of-Flight-Messung für dritte Position}
    \label{fig:time_3}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{tof_images/location_prob.pdf}
    \caption{Wahrscheinlichkeitsverteilung (PDF) der korrigierten Ortslage der Punktquelle, modelliert als Normalverteilung aus den ToF-Daten mit entsprechender Messunsicherheit. Die blaue Nullmessung hat eine naturgemäß kleinere Messunsicherheit, da sie als Referenz dient.}
    \label{fig:location_prob}
\end{figure}

\FloatBarrier

\section{Anhang}
\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm1_3_alt.pdf}
		\caption{Dark Count Map der SPU 1\_3}
		\label{fig:dcm1_3_alt}
	\end{figure}
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm1_6_alt.pdf}
		\caption{Dark Count Map der SPU 1\_6}
		\label{fig:dcm1_6_alt}
	\end{figure}
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm2_3_alt.pdf}
		\caption{Dark Count Map der SPU 2\_3}
		\label{fig:dcm2_3_alt}
	\end{figure}
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.8]{dcm_images/dcm2_6_alt.pdf}
		\caption{Dark Count Map der SPU 2\_6}
		\label{fig:dcm2_6_alt}
	\end{figure}

    
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.7]{tof_images/time_0.log_scale_plot.pdf}
        \caption{Normalverteilungsfit an Zeitdifferenzdaten mit Skalierung (statt Offset; vgl. \autoref{fig:time_0})}
        \label{fig:time_0_scale}
    \end{figure}

%
% Ende des Protokolls
%
\end{document}
