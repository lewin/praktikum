#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 12:55:08 2021

Zu tun:

    - Potential improvements to the data to improve the analysis
    - How can energy resolution be improved

Nicht vergessen

    - Nichtlinearität (Verschiebung der Peaks bei 511/1275 keV)


@author: lbo
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from scipy import optimize

from os import path

FIGSIZE = (9,6)

BUCKET_WIDTH = 2

plt.rcParams['font.size'] = 12.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.weight'] = 'bold'


def ChiSq(xdata, ydata, fitdata, ddof=3, details=False):
    # Assuming poisson distribution: Variance is expected vallue
    diffsq = (fitdata-ydata)**2/ydata
    if details:
        return diffsq.sum()/(xdata.size-ddof), xdata.size-ddof, diffsq.sum()
    return diffsq.sum()/(xdata.size-ddof)


def parse_energy_log(filename):
    """Returns list of (energy, count) tuples with energy in keV.

    Expects filename of preprocessed energy histogram file, i.e. one line:
    Energy, count; Energy, count; ...
    """
    lines = []
    with open(filename, 'r') as f:
        lines = f.readlines()
    assert len(lines) == 1
    buckets = lines[0].split(';')
    parsed = []
    for b in buckets:
        if not ',' in b:
            continue
        E, c = b.split(',')
        parsed.append((int(E), int(c)))
    return sorted(parsed)


def find_true_m_e(hist_data, gaussrange, use_offset=True):
    low, high = gaussrange
    E = np.array([hd[0] for hd in hist_data if low <= hd[0] <= high])
    c = np.array([hd[1] for hd in hist_data if low <= hd[0] <= high])
    total_count = sum(c)
    normalized = np.array(c)/total_count

    if use_offset:
        result = optimize.curve_fit(
            lambda x, mu, sigma, off: stats.norm.pdf(x, mu, sigma)*BUCKET_WIDTH - off,
            E, normalized, [520, 20, -1e-2])
        fit_y = total_count*(stats.norm.pdf(E, result[0][0], result[0][1])*BUCKET_WIDTH-result[0][2])
    else:
        result = optimize.curve_fit(
            lambda x, mu, sigma, c: c*stats.norm.pdf(x, mu, sigma)*BUCKET_WIDTH,
            E, normalized, [520, 20, 1])
        fit_y = total_count*(stats.norm.pdf(E, result[0][0], result[0][1])*BUCKET_WIDTH*result[0][2])
    print(result)
    print(ChiSq(E, c, fit_y))
    return result, total_count


def find_na22_photopeak(hist_data, background=None, rng=(1170, 1340)):
    low, high = rng
    E = np.array([hd[0] for hd in hist_data if low <= hd[0] <= high])
    c = np.array([hd[1] for hd in hist_data if low <= hd[0] <= high])
    if background:
        bg_c = np.array([hd[1] for hd in background if low <= hd[0] <= high])

    # Fit Gauß bell
    (mu, sigma, off), _ = optimize.curve_fit(
        lambda x, mu, sigma, off: stats.norm.pdf(x, mu, sigma)*BUCKET_WIDTH - off,
        E, c/c.sum(), [1275, 40, 0])

    print('Na-22 Sigma params:', mu, sigma, off)

    max_loc = mu
    fit_y = c.sum()*(stats.norm.pdf(E, mu, sigma)*BUCKET_WIDTH-off)
    residuals = c - fit_y
    chisq, size, chisq_sum = ChiSq(E, c, fit_y, details=True)

    f, (p, res) = plt.subplots(2, 1, figsize=FIGSIZE, tight_layout=True,
                               sharex=True, gridspec_kw={'height_ratios': [5, 2]})

    print('ChiSq = ', chisq)

    p.errorbar(E, c, np.sqrt(c), fmt='.', label='Daten')
    res.errorbar(E, residuals, np.sqrt(c), fmt='.', label='Residuen')
    p.plot(E, fit_y, label='Fit', color='gray')
    p.plot([max_loc, max_loc], [min(c), 1.1*max(c)], color='green', label='Peak = {:.0f} keV'.format(max_loc))
    p.text(
        E.min(), c.max(),
        'FWHM = {:.1f} keV\n$\chi^2$ / ndof = {:.1f}/{} = {:.1f}'.format(
            2.35482*sigma, chisq_sum, size, chisq))

    p.set_xlabel('E / keV')
    res.set_xlabel('E / keV')
    p.set_ylabel('Anzahl (abs)')
    res.set_ylabel('Daten - Fit')
    p.set_title('Na-22 photopeak')

    p.grid()
    res.grid()
    p.legend()
    f.savefig('artifacts/na22_photopeak.pdf')


def paint_histogram(hist_data, title, file=None, gaussrange=None, use_offset=True,
                    **plotargs):
    """Paint a histogram with data from parse_energy_log()"""
    if gaussrange:
        f, (p, res) = plt.subplots(2, 1, figsize=FIGSIZE, tight_layout=True,
                                   sharex=True, gridspec_kw={'height_ratios': [5, 2]})
        res.grid()
    else:
        f = plt.figure(figsize=FIGSIZE, tight_layout=True)
        p = f.add_subplot(111)

    buckets = np.array([hd[0] for hd in hist_data])
    counts = np.array([hd[1] for hd in hist_data])
    if gaussrange:
        gaussmin, gaussmax = np.argmin(np.abs(buckets-gaussrange[0])), np.argmin(np.abs(buckets-gaussrange[1]))

    me_x = [511,511]
    me_y = [0, 1.2*max(counts)]

    p.errorbar(buckets, counts, np.sqrt(counts), label='Anzahl', **plotargs)
    p.plot(me_x, me_y, color='green', label='$m_{e,lit} = 511$ keV')
    p.grid()

    if gaussrange:
        # Determine location of 511-peak
        ((m_e, sigma_m_e, adj), _), total_count = find_true_m_e(
            hist_data,gaussrange, use_offset=use_offset)
        fwhm = 2.35482*sigma_m_e
        print('adj:', adj, sigma_m_e)

        if use_offset:
            fit_count = (stats.norm.pdf(buckets, m_e, sigma_m_e)*BUCKET_WIDTH - adj)*total_count
            p.plot([m_e-fwhm/2,m_e+fwhm/2], [(fit_count.max()-adj*total_count)/2]*2,
               color='#a000a0', label='FWHM = {:.0f} keV'.format(fwhm), marker='')
        else:
            fit_count = (stats.norm.pdf(buckets, m_e, sigma_m_e)*BUCKET_WIDTH*adj)*total_count
            p.plot([m_e-fwhm/2,m_e+fwhm/2], [(fit_count.max())/2]*2,
               color='#a000a0', label='FWHM = {:.0f} keV'.format(fwhm), marker='')
        resid = fit_count - counts
        chisq, chisq_dof, chisq_sum = ChiSq(
            buckets[gaussmin:gaussmax], counts[gaussmin:gaussmax],
            fit_count[gaussmin:gaussmax], details=True)

        print(title, ':: Gemessene Elektronenmasse: ', m_e, '±',
              sigma_m_e, 'keV', 'FWHM = 2.355σ =', fwhm, 'keV, ChiSq = ', chisq)

        p.errorbar(buckets, fit_count, np.sqrt(fit_count), color='gray', label='Peak fit ($m_e$)')
        p.text(100, 0.9*max(counts),
               ('$m_{{e,meas}}$ = {:.1f} keV\n$\sigma$ = {:.0f} keV\n$\chi^2/$ndof = {:.1f}/{} = {:.1f}'
                .format(m_e, sigma_m_e, chisq_sum, chisq_dof, chisq)))

        res.errorbar(buckets[gaussmin:gaussmax], resid[gaussmin:gaussmax], np.sqrt(counts[gaussmin:gaussmax]), fmt='.')
        res.set_ylabel('Modell - Daten')
        res.set_xlabel('E / keV')

    p.set_xlabel('E / keV')
    p.set_ylabel('Anzahl (abs.)')
    p.set_title(title)
    p.legend()

    if file:
        f.savefig(file)
    return p

def subtract_background_from_singles(title='', file=None, **plotargs):
    hd_single = parse_energy_log('../data/preproc/energy_single.log')
    hd_background = parse_energy_log('../data/preproc/energy_single_empty.log')

    E_single, c_single = np.array([t[0] for t in hd_single]), np.array([t[1] for t in hd_single])
    E_background, c_background = np.array([t[0] for t in hd_background]), np.array([t[1] for t in hd_background])

    # We have peaks around 200 keV that align. Find them
    left, right = np.argmin(np.abs(E_single-150)), np.argmin(np.abs(E_single-250))
    single_peak, background_peak = np.argmax(c_single[left:right]), np.argmax(c_single[left:right])

    print(E_single[single_peak], E_background[background_peak])
    print(c_single[single_peak], c_background[background_peak])

    c_single_corrected = c_single - c_background

    # plt.plot(E_single, c_single)
    # plt.plot(E_single, c_background)
    # plt.plot(E_single, c_single_corrected)

    f, (p, res) = plt.subplots(2, 1, figsize=FIGSIZE, tight_layout=True,
                               sharex=True, gridspec_kw={'height_ratios': [5, 2]})

    gaussrange = (470, 550)

    buckets = E_single
    counts = c_single_corrected
    gaussmin, gaussmax = np.argmin(np.abs(buckets-gaussrange[0])), np.argmin(np.abs(buckets-gaussrange[1]))

    me_x = [511,511]
    me_y = [0, 1.2*max(counts)]

    # Determine location of 511-peak
    hd = list(zip(E_single, c_single_corrected))
    ((m_e, sigma_m_e, off), _), total_count = find_true_m_e(hd, gaussrange)
    fit_count = (stats.norm.pdf(buckets, m_e, sigma_m_e)*BUCKET_WIDTH - off)*total_count
    resid = fit_count - counts
    chisq, chisq_dof, chisq_sum = ChiSq(
        buckets[gaussmin:gaussmax], counts[gaussmin:gaussmax],
        fit_count[gaussmin:gaussmax], details=True)
    fwhm = 2.35482*sigma_m_e
    print(title, ':: Gemessene Elektronenmasse: ', m_e, '±',
          sigma_m_e, 'keV', 'FWHM = 2.355σ =', fwhm, 'keV, ChiSq = ', chisq)

    p.errorbar(buckets, c_single, np.sqrt(c_single), label='Anzahl (abs)', **plotargs)
    p.errorbar(buckets, c_single_corrected, np.sqrt(c_single_corrected), label='Anzahl ohne Untergrund', **plotargs)
    p.plot(me_x, me_y, color='green', label='$m_{e,lit} = 511$ keV')
    p.errorbar(buckets, fit_count, np.sqrt(fit_count), color='gray', label='Peak fit ($m_e$)')
    p.text(100, 1.35*max(counts),
               ('$m_{{e,meas}}$ = {:.1f} keV\n$\sigma$ = {:.0f} keV\n$\chi^2/$ndof = {:.1f}/{} = {:.1f}'
                .format(m_e, sigma_m_e, chisq_sum, chisq_dof, chisq)))
    p.plot([m_e-fwhm/2,m_e+fwhm/2], [(fit_count.max()-off*total_count)/2]*2,
           color='#a000a0', label='FWHM = {:.0f} keV'.format(fwhm), marker='')

    res.errorbar(buckets[gaussmin:gaussmax], resid[gaussmin:gaussmax], np.sqrt(counts[gaussmin:gaussmax]), fmt='.')
    res.set_ylabel('Modell - Daten')
    res.set_xlabel('E / keV')
    res.grid()

    p.grid()
    p.set_xlabel('E / keV')
    p.set_ylabel('Anzahl (abs.)')
    p.set_title(title)
    p.legend()

    if file:
        f.savefig(file)
    return p


def paint_all_energy_histograms():
    data = [('energy_single.log', 'Energy (single)', (490,550)),
            ('energy_single_empty.log', 'Energy (single, empty)', (600, 700)),
            ('energy_coinc.log', 'Energy (coinc)', (470, 580))]

    for file, title, peaklim in data:
        hd = parse_energy_log(path.join('../data/preproc/', file))
        p = paint_histogram(hd, file=path.join('artifacts', file+'_plot.pdf'),
                            use_offset=True,
                            gaussrange=peaklim, title=title)

    subtract_background_from_singles('Singles ohne Untergrund', file='artifacts/energy_single_no_background.pdf')
