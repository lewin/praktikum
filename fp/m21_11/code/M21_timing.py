# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 11:31:28 2021

@author: 394493: Alexander Kohlgraf
"""

import os
import codecs
import numpy as np
import pandas as pd
import texttable
import latextable
import nice_plots
import matplotlib.pyplot as plt
import scipy as sp
import scipy.stats as stats
import scipy.optimize as opt
import scipy.constants as const

dir_path = os.path.dirname(os.path.realpath(__file__))

plt.rcParams['font.size'] = 24.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['figure.autolayout'] = True

def gaussFit(bins, counts, savename=None, p0=None, label="", labelArray=None, xLabel="", yLabel="", title="", FWHM=False):
    def gauss(x, mu, sig, A):
        return A * np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) / (np.sqrt(2*np.pi) * sig)
    
    plt.figure(figsize=(19,9))
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.title(title)
    plt.grid(True)
    
    if labelArray:
        for (x,y,lab) in zip(bins,counts,labelArray):
            popt, pcov = opt.curve_fit(gauss, bins, counts, p0=p0)
    else:
        popt, pcov = opt.curve_fit(gauss, bins, counts, p0=p0)
        plt.plot(bins,counts,label=label)
        plt.fill_between(bins,counts)
        plt.plot(bins, gauss(bins,*popt), label="mu= {0:.2f}+-{3:.2f}; sig= {1:.2f}+-{4:.2f}; Amplitude= {2:.2f}+-{5:.2f}".format(*popt,pcov[0][0], pcov[1][1], pcov[2][2]))
        print(popt,pcov)
        plt.legend()
    if savename:
        plt.savefig("../protokoll/images/" + savename +".pdf")
    plt.show()

def readLog(x):
    df = pd.read_csv("../data/Time_diff_pos" + str(x) + ".log", delimiter=";", skiprows=3, engine='python', index_col=False, header=None).T
    df = df[0].str.split(",", expand=True)
    df.drop(df.tail(1).index,inplace=True)
    df = df.astype(int)
    df.columns = ["time", "count"]
    return df

def gaussHist(xArrayArray, weightsArray, bins=None, xLabel="", yLabel="", title="", labelArray = None, savename = None, density=False, guesses=[]):
        plt.figure(figsize=(19,9))
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.title(title)
        poptArray=[]
        pcovArray=[]
        for (x, weights, label, p0) in zip(xArrayArray, weightsArray, labelArray, guesses):
            n, newBins, patches = plt.hist(x,label=label, weights=weights, bins=bins, density=density, alpha=0.9)
            gaussRange = 700
            popt, pcov = fitGauss(np.delete(newBins,0), n, p0, gaussRange)
            poptArray.append(popt)
            pcovArray.append(pcov)
            plt.plot(x[(x>=popt[0]-gaussRange) & (x<=popt[0]+gaussRange)], gaussian(x[(x>=popt[0]-gaussRange) & (x<=popt[0]+gaussRange)], *popt))
            plt.legend()
        plt.grid(True)
        if savename:
            plt.savefig("../protokoll/images/" + savename +".pdf")
        plt.show()
        return poptArray, pcovArray

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) / (np.sqrt(2*np.pi) * sig)

def fitGauss(x,y,p0,gaussRange):
    xCut = x[(x<=p0[0]+gaussRange) & (x>=p0[0]-gaussRange)]
    yCut = y[(x<=p0[0]+gaussRange) & (x>=p0[0]-gaussRange)]
    return opt.curve_fit(gaussian, xCut, yCut, p0=p0)

def FWHMcalc(sig, sig_sigma):
    FWHM = 2*np.sqrt(2*np.log(2))*sig
    FWHM_sigma = 2*np.sqrt(2*np.log(2))*sig_sigma
    return FWHM, FWHM_sigma

def timeToPos(FWHM, mu):
    return mu*10**(-9)*const.c/2, FWHM*10**(-9)*const.c/2

def drawer(name, caption, label, content, header, small=False):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(len(header)))
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(3)
        
        for i in content:
            table.add_row(i)
        if small: text += latextable.draw_latex_small(table, caption, label = "tab:"+label) +'\n'
        else: text += latextable.draw_latex(table, caption, label = "tab:"+label) +'\n'
    
        text_file = codecs.open(dir_path + "\\..\\protokoll\\tables\\" + "Tabelle_" + name + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()


x_axis = np.linspace(-1000,1000,201)
#FWHM
FWHM_log = readLog(0)
gaussFit(FWHM_log["time"], FWHM_log["count"], p0=[0,200,4500])
popt, pcov = gaussHist([FWHM_log["time"]], [FWHM_log["count"]], bins=x_axis, density=True ,labelArray=["FWHM"], guesses=[[0,180]], xLabel="Zeit [ps]", yLabel="Dichte", title="Bestimmung des FWHM via Gaussfit", savename = "FWHM")
FWHM, FWHM_sigma = FWHMcalc(popt[0][1], pcov[0][1][1])
print("FWHM:")
print(FWHM, FWHM_sigma)
drawer("gaussFWHM", "Gaussfit Ergebnisse für FWHM", "gaussFWHM", [["Erwartungswert", r"\SI{" + "{:.2f}".format(popt[0][0]) + r"}{\pico\second}",r"\SI{" + "{:.2f}".format(pcov[0][0,0]) + r"}{\pico\second}"], ["Standardabweichung", r"\SI{" + "{:.2f}".format(popt[0][1]) + r"}{\pico\second}",r"\SI{" + "{:.2f}".format(pcov[0][1,1]) + r"}{\pico\second}"]], ["Faktor", "Wert", "Fehler"])

#Position
position_logs = []
position_logs_time = []
position_logs_count = []
for x in range(3): 
    position_logs.append(readLog(x+1))
    position_logs_time.append(position_logs[x]["time"])
    position_logs_count.append(position_logs[x]["count"])
poptArray, pcovArray = gaussHist(position_logs_time, position_logs_count, bins=x_axis, density=True ,labelArray=["Pos1", "Pos2", "Pos3"], guesses=[[270,190],[-200,190],[-250,190]], xLabel="Zeit [ps]", yLabel="Dichte", title="Bestimmung der Positionen via Gaussfit für Mittelwerte", savename = "positions")
positions=[]
for (popt,pcov) in zip(poptArray, pcovArray):
    positions.append(["Position {:.1f}".format(len(positions)+1), popt[0], pcov[0][0], timeToPos(FWHM, popt[0])[0], timeToPos(FWHM, popt[0])[1]*2])
    print("Position: {:.3f}+-{:.3f}mm".format(*timeToPos(FWHM, popt[0])))
    
#drawer("gaussFWHM", "Gaussfit Ergebnisse für FWHM", "gaussFWHM", [["Erwartungswert", r"\SI{" + str(popt[0]) + r"}{\pico\second}",r"\SI{" + str(pcov[0][0]) + r"}{\pico\second}"], ["Standardabweichung", r"\SI{" + str(popt[1]) + r"}{\pico\second}",r"\SI{" + str(pcov[1][1]) + r"}{\pico\second}"]], ["Faktor", "Wert", "Fehler"])
drawer("FWHM", "FWHM Ergebnisse", "FWHM", [[r"\SI{" + "{:.2f}".format(FWHM) + r"}{\pico\second}", r"\SI{" + "{:.2f}".format(FWHM_sigma) + r"}{\pico\second}"],[r"\SI{" + "{:.2f}".format(timeToPos(FWHM,FWHM)[0]*2) + r"}{\milli\meter}", r"\SI{" + "{:.2f}".format(timeToPos(FWHM,FWHM_sigma)[0]*2) + r"}{\milli\meter}"]], [r"FWHM", "Unsicherheit"])
drawer("positions", "Positionen der Quelle bestimmt über ToF", "ToF", positions, ["Position", r"Erwartungswert $\Delta$t [\SI{}{\pico\second}]", r"Unsicherheit $\Delta$t [\SI{}{\pico\second}]", r"Auslenkung von der Mitte [\SI{}{\milli\meter}]", "FWHM [\SI{}{\milli\meter}]"], small=True)