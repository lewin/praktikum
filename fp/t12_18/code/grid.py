#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 22:27:34 2021

@author: lbo
"""

import drawSvg as ds
import math

def cross(draw, x, y, len=20):
    dx = len/math.sqrt(2)/2
    dy = dx
    l1 = ds.Line(x-dx, y-dy, x+dx, y+dy, stroke='black', stroke_width=2)
    l2 = ds.Line(x-dx, y+dy, x+dx, y-dy, stroke='black', stroke_width=2)

    draw.append(l1)
    draw.append(l2)

def line(draw, len, x=None, y=None):
    if x is not None:
        l = ds.Line(x, -len/2, x, len/2, stroke='black', stroke_width=1)
        draw.append(l)
    elif y is not None:
        l = ds.Line(-len/2, y, len/2, y, stroke='black', stroke_width=1)
        draw.append(l)

def text(draw, x, y, text):
    t = ds.Text(text, 22, x=x, y=y)
    draw.append(t)

def draw():
    dim = 500
    d = ds.Drawing(dim, dim, origin='center', displayInline=True)
    d.append(ds.Circle(0, 0, dim/2., stroke_width=2, stroke='black', fill='white'))
    cross(d, 0, 0)

    gridwith = dim/8.
    radius = dim/2.
    for i, (x, y) in enumerate(sorted(list(
            set([(x, x) for x in range(-2, 3)] + [(x, -x) for x in range(-2, 3)]))
            + [(0, -2)])):
         cross(d, gridwith*x, gridwith*y)
         text(d, gridwith*x + gridwith/4, gridwith*y + gridwith/6, str(i+1))


    for off in range(-4, 5):
        len = 2*math.sin(math.acos(off*gridwith/radius))*radius
        line(d, len, x=off*gridwith)
        line(d, len, y=off*gridwith)

    d.saveSvg('../img/grid.svg')
    return d
