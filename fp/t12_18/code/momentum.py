#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import matplotlib.pyplot as plt
import pandas
import uncertainties as unc
from uncertainties import umath, unumpy

import nice_plots as npl

DESCS = ['30s',
         '120s']
INTERVALS = [30, 120]
REGRESSION_RANGES = [(17, -6),
                     (8, -6)]
KURIE_REGRESSION_RANGES = [(8, -5), (8, -2)]
MOMENTUM_FILES = ['../data/momentum/momentum_30s.txt',
         '../data/momentum/momentum_120s.txt']

# If possible, use B/mT, r/mm, p/keV!

DETECTOR_APERTURE = 1.15
SOURCE_APERTURE = 1.55
RADIUS_UNCERTAINTY = (DETECTOR_APERTURE+SOURCE_APERTURE)/4
RADIUS = unc.ufloat(39.2/2, RADIUS_UNCERTAINTY)

# 0.2% of measurement range, and 0.3% of measure range for probe,
# measure range is mostly 0..2T. Plus one digit
B_SYSTEMATIC_ERROR = math.sqrt(0.002**2+0.003**2)*2e3
B_UNCERTAINTY = 1
ELECTRON_MASS = 511.0

# Calculated from last five points of high-res (120s) measurement.
# Hz = (c1+c2+c3+c4+c5)/(5*120) = 812/600 = 1.353;
# sigma_Hz = sqrt(812)/600 = 28.5/600 = 0.0475;
BACKGROUND_HZ = unc.ufloat(1.35, .05)

FIGSIZE = (8, 5)

nv = lambda x: unumpy.nominal_values(x) # if type(x) is np.ndarray else x.nominal_value
sd = lambda x: unumpy.std_devs(x) # if type(x) is np.ndarray else x.std_dev

B_to_E = lambda radius_mm: np.vectorize(lambda B_mT: umath.sqrt(ELECTRON_MASS**2+(0.3*B_mT*radius_mm)**2))
B_to_p = lambda radius_mm: np.vectorize(lambda B_mT: 0.3*B_mT*radius_mm)

def ChiSq(xdata, ydata, fitdata, ddof=3, details=False):
    # Assuming poisson distribution: Variance is expected value
    diffsq = (fitdata-ydata)**2/ydata
    if details:
        return diffsq.sum()/(xdata.size-ddof), xdata.size-ddof, diffsq.sum()
    return diffsq.sum()/(xdata.size-ddof)

def Fermi_correction(E_e, p_e, Z=38, alpha=1/137):
    eta = Z*alpha*E_e/p_e
    return 2*math.pi*eta/(1-unumpy.exp(-2*math.pi*eta))

def parse_magnetic_file(filename, initial_off=2):
    """Returns B/c (B/count) dataframe from filename"""
    df = pandas.read_csv(filename, names=('B','count'), sep='\t', skiprows=3)
    df['B'] = [unc.ufloat(b, B_UNCERTAINTY) for b in df['B']]
    df['count'] = [unc.ufloat(c, np.sqrt(c)) for c in df['count']]
    return df[initial_off:]

def correct_magnetic_background(df, interval):
    """Corrects for background in a parsed dataframe."""
    # count = [max(unc.ufloat((c - interval*BACKGROUND_HZ).nominal_value,
    #                         math.sqrt(abs(c.nominal_value - interval*BACKGROUND_HZ.nominal_value))),
    #              unc.ufloat(1, math.sqrt(abs(c.nominal_value - interval*BACKGROUND_HZ.nominal_value))))
    #          for c in df['count']]
    count = [max(c-interval*BACKGROUND_HZ, unc.ufloat(1, 2)) for c in df['count']]
    new = pandas.DataFrame({'B': df['B'], 'count': count})
    return new

def rescale_to_momentum(df, radius_mm=RADIUS):
    """Rescale B/c dataframe to columns (p, count) with p in keV"""
    new = pandas.DataFrame({'p': B_to_p(radius_mm)(df['B']), 'count': df['count']})
    return new

def rescale_to_energy(df, radius_mm=RADIUS):
    """Rescale B/c dataframe to columns (E, count) with E in keV"""
    new = pandas.DataFrame({'E': B_to_E(radius_mm)(df['B']), 'count': df['count']})
    return new

def plot_magnetic_measurement(df, title=''):
    f = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = f.add_subplot(111)

    p.errorbar(nv(df['B']), nv(df['count']),
               xerr=sd(df['B']), yerr=sd(df['count']), fmt='o')
    p.set_xlabel('B / mT')
    p.set_ylabel('Count')
    p.set_title(title)

    if title:
        f.savefig('../img/{}_mag.pdf'.format(title))


def plot_momentum_measurement(df, linreg_range=(0,-1), title=''):
    """Takes a rescaled dataframe (from rescale_to_momentum()) and plots it"""
    x, y = nv(df['p']), nv(df['count'])
    xerr, yerr = sd(df['p']), sd(df['count'])
    (m,em,b,eb,chiq,corr) = npl.nice_linear_regression_plot(
        x, y, yerr, xerror=xerr, xlabel='p / keV', ylabel='Count',
        ylabelresidue='$y_i - (mx + b)$', title=title,
        regrange=linreg_range, figsize=FIGSIZE, legend_x=.6,
        save='../img/{}_momentum.pdf'.format(title))

    m = unc.ufloat(m, em)
    b = unc.ufloat(b, eb)
    intersect = -b/m
    print('Momentum y=0 intersect: p =', repr(intersect), 'keV')


def plot_energy_measurement(df, linreg_range=(0,-1), title=''):
    """Takes a raw dataframe and plots it"""
    x, y = nv(df['E']), nv(df['count'])
    xerr, yerr = sd(df['E']), sd(df['count'])
    (m,em,b,eb,chiq,corr) = npl.nice_linear_regression_plot(
        x, y, yerr, xerror=xerr, xlabel='E / keV', ylabel='Count',
        ylabelresidue='$y_i - (mx + b)$', title=title,
        regrange=linreg_range, figsize=FIGSIZE, legend_x=.6,
        save='../img/{}_energy.pdf'.format(title.replace(' ', '_')))

    m = unc.ufloat(m, em)
    b = unc.ufloat(b, eb)
    intersect = -b/m
    print('Energy y=0 intersect: E =', repr(intersect), 'keV')


def plot_kurie_diagram(df, linreg_range=(0,-1), radius_mm=RADIUS,
                       abscissa='E', title=''):
    """Takes a raw dataframe to calculate and plot Kurie diagram"""
    assert abscissa in ('E', 'p')
    p = B_to_p(radius_mm)(df['B'])
    dp = (p[1:]-p[:-1])
    dp = np.array([p[0]] + list(dp))
    count = df['count']
    total = nv(count).sum()
    dw_dp = (count/dp) / (total)

    if abscissa == 'E':
        kurie_x = energies = B_to_E(radius_mm)(df['B'])
    elif abscissa == 'p':
        energies = B_to_E(radius_mm)(df['B'])
        kurie_x = p
    kurie_y_raw = unumpy.sqrt(dw_dp/p**2)
    fermi_corr = Fermi_correction(energies, p)
    kurie_y_fermi = unumpy.sqrt(dw_dp/(fermi_corr * p**2))

    # plt.figure(figsize=FIGSIZE).add_subplot(111).errorbar(nv(kurie_x), nv(fermi_corr), sd(fermi_corr), label='Fermi correction')

    (m,em,b,eb,chiq,corr), fig, ax = npl.nice_linear_regression_plot(
        nv(kurie_x), nv(kurie_y_raw),
        sd(kurie_y_raw), xerror=sd(kurie_x),
        figsize=FIGSIZE, legend_x=.59,
        regrange=linreg_range, # title=title+' without Fermi corr',
        xlabel='{} / keV'.format(abscissa), ylabel='$\\sqrt{\\frac{dw/dp}{[F(Z,p)] p^2}}$',
        save='../img/{}_kurie_both.pdf'.format(title).replace(' ', '_'),
        colors=('blue', 'orange'), ix='No-Fermi', return_axes=True)

    m = unc.ufloat(m, em)
    b = unc.ufloat(b, eb)
    intersect = -b/m
    print('Kurie w/o Fermi: y=0 intersect:', abscissa, '=', repr(intersect), 'keV')


    (m,em,b,eb,chiq,corr) = npl.nice_linear_regression_plot(
        nv(kurie_x), nv(kurie_y_fermi),
        sd(kurie_y_fermi), xerror=sd(kurie_x),
        use_axes=ax, figsize=FIGSIZE, legend_x=.59,
        regrange=linreg_range, title=title+' with(out) Fermi correction',
        xlabel='{} / keV'.format(abscissa), ylabel='$\\sqrt{\\frac{dw/dp}{F(Z,p) p^2}}$',
        save='../img/{}_kurie_fermi.pdf'.format(title).replace(' ', '_'),
        ix='Fermi')

    m = unc.ufloat(m, em)
    b = unc.ufloat(b, eb)
    intersect = -b/m
    print('Kurie w/ Fermi: y=0 intersect:', abscissa, '=', repr(intersect), 'keV')
    fig.savefig('../img/{}_kurie_both.pdf'.format(title).replace(' ', '_'))


def momentum_render_all(abscissa='E'):
    dfs = [parse_magnetic_file(f) for f in MOMENTUM_FILES]
    for i, df in enumerate(dfs):
        corrected = correct_magnetic_background(df, INTERVALS[i])
        plot_momentum_measurement(rescale_to_momentum(corrected),
                                  linreg_range=REGRESSION_RANGES[i],
                                  title=DESCS[i])
        plot_energy_measurement(rescale_to_energy(corrected),
                                linreg_range=REGRESSION_RANGES[i],
                                title=DESCS[i])
        plot_kurie_diagram(corrected, title='Kurie diagram: '+DESCS[i],
                           linreg_range=KURIE_REGRESSION_RANGES[i], abscissa=abscissa)
