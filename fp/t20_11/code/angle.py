# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 22:34:15 2021

@author: 394493: Alexander Kohlgraf
This should look into the angle dependance, with regard to the acceptance area
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import scipy as sc
import scipy.stats as stats

import nice_plots
import latextable

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

# Measurements of Cylinders
radius = 19.5 / 2
height = 30


def cos_fit(param, angle):
    return np.cos(angle * np.pi / 180) ** param[0]


class Angle:
    def __init__(self, radius, height, time: float = 30):
        self.radius = radius  # cm
        self.radius_error = 0.5  # cm
        self.height = height  # cm
        self.height_error = 0.5  # cm
        self.height_difference = 80  # cm
        self.height_difference_error = 0.5  # cm
        self.time = time  # Time Window for coinc, e.g. coincs per {time} Minutes
        self.abstand_error = 0.5  # cm
        df = self.df = pd.read_csv(
            r"data/Angle/angle.txt",
            delimiter="\t",
            decimal=",",
            engine="python",
            skiprows=1,
            names=[
                "time",
                "Abstand A",
                "Abstand B",
                "Winkel",
                "coinc",
            ],
        )
        df["coinc"] = df["coinc"] * time / (df["time"][0] / 60)
        df["coinc error"] = np.sqrt(df["coinc"])

        df["Winkel"] = (
            np.arctan((-df["Abstand A"] + df["Abstand B"]) / self.height_difference)
            * 180
            / np.pi
        )
        df["Winkel error"] = np.sqrt(
            1
            / (1 + ((-df["Abstand A"] + df["Abstand B"]) / self.height_difference) ** 2)
            ** 2
            * (
                2 * self.abstand_error ** 2
                + ((-df["Abstand A"] + df["Abstand B"]) / self.height_difference ** 2)
                ** 2
                * self.height_difference_error ** 2
            )
            ** 2
        )

        df["acceptance area"] = self.acceptance_area(df["Winkel"])
        df["acceptance area error"] = self.acceptance_area_error(df["Winkel"])

    def plot_area(self):
        plt.figure(figsize=(15, 8))
        plt.ylabel("Acceptance Area [cm^2]")
        plt.xlabel("Zenit angle [°]")
        plt.title("Plot of acceptance Area. Our highest Angle is below 50°")
        plt.grid(True)
        x = np.linspace(0, 90)
        plt.plot(x, self.acceptance_area(x), label="acceptance area")
        plt.plot(x, np.ones(len(x)) * np.pi * self.radius ** 2, label="circle area")
        plt.plot(
            x, np.ones(len(x)) * self.height * 2 * self.radius, label="rectangle area"
        )
        plt.axvline(
            x=self.df["Winkel"].max(),
            ymin=0,
            ymax=1,
            label="Maximum angle in measurement",
        )
        plt.legend()
        plt.savefig("protokoll/images/Angle/area_vs_angle.pdf")

    def plot_coinc(self, respect_acceptance=True):
        x = self.df["Winkel"]
        y = self.df["coinc"]
        yerr = self.df["coinc error"]
        xerr = self.df["Winkel error"]

        if respect_acceptance:
            y = y * self.df["acceptance area"][0] / (self.df["acceptance area"] * y[0])
            yerr = y * np.sqrt(
                (self.df["coinc error"] ** 2 / self.df["coinc"] ** 2)
                + (
                    self.df["acceptance area error"] ** 2
                    / self.df["acceptance area"] ** 2
                )
                + (self.df["coinc error"] ** 2 / self.df["coinc"] ** 2)
                + (
                    self.df["acceptance area error"] ** 2
                    / self.df["acceptance area"] ** 2
                )
            )

            reg = self.reg = nice_plots.nice_regression_plot_odr(
                cos_fit,
                np.array(x),
                y=np.array(y),
                xerror=np.array(xerr),
                yerror=np.array(yerr),
                xlabel="Zenit angle [°]",
                ylabel="cos(angle)^n",
                ylabelresidue="Residue",
                title="Fit of cos^n model",
                regrange=[0, -1],
                figsize=(15, 8),
                save="protokoll/images/Angle/cos_fit.pdf",
                plot_it=True,
                beta0=[2],
            )
            latextable.simple_draw(
                save="/../protokoll/tables/cos_fit.tex",
                content=[["n", reg[0][0], np.sqrt(reg[1][0][0])]],
                header=["Fit Parameter", "Wert", "Fehler"],
                caption=r"Fit Parameter für $\cos{\theta}^n$",
                label="cos_fit",
                precision=2,
                small=False,
            )
            print(reg)

        plt.figure(figsize=(15, 8))
        plt.ylabel(
            "Coincidences per {} Minutes{}".format(
                self.time, "*A_0/(A*f_0)" if respect_acceptance else ""
            )
        )
        plt.xlabel("Zenit angle in °")
        plt.title(
            "Rate vs Angle{}".format(
                " respecting acceptance area" if respect_acceptance else ""
            )
        )
        plt.grid(True)

        if respect_acceptance:
            plt.plot(
                np.linspace(0, max(x)),
                cos_fit(reg[0], np.linspace(0, max(x))),
                label="Cos^n fit with n={:.2f}".format(reg[0][0]),
            )
        plt.errorbar(x, y, yerr, xerr, fmt="none", marker="o", label="measured data")
        plt.legend()
        plt.savefig(
            "protokoll/images/Angle/coinc_vs_angle_{}_minutes{}.pdf".format(
                self.time, "_acceptance" if respect_acceptance else ""
            )
        )

    def acceptance_area(self, angle):
        area = 2 * self.radius * self.height * np.sin(
            angle * np.pi / 180
        ) + np.pi * self.radius ** 2 * np.cos(angle * np.pi / 180)
        return area

    def acceptance_area_error(self, angle):
        r = self.radius
        r_e = self.radius_error
        h = self.height
        h_e = self.height_error
        theta = (
            self.df["Winkel"] * np.pi / 180
        )  # umrechnen zu radian, damit nicht während der Rechnung
        theta_e = self.df["Winkel error"] * np.pi / 180  # "

        error = np.sqrt(
            (2 * h * np.sin(theta) + 2 * np.pi * r * np.cos(theta)) ** 2 * r_e ** 2
            + (2 * r * np.sin(theta)) ** 2 * h_e ** 2
            + (2 * r * h * np.cos(theta) - 2 * np.pi * r * np.sin(theta)) ** 2
            * theta_e ** 2
        )
        return error
