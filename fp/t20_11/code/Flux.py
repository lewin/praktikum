# -*- coding: utf-8 -*-
"""
Created on Wed Apr  7 12:01:57 2021

@author: 394493: Alexander Kohlgraf
Zeige Poisson statistik der Flussrate
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import scipy as sc
import scipy.stats as stats

import nice_plots

# import latextable

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


def poisson(mu, k):
    return stats.poisson.pmf(k, mu)


class Flux:
    def __init__(self, bin_size: int = 5):
        """
        bin size>=1
        """
        self.bin_size = bin_size

        self.df = pd.read_csv(
            r"data/flux/flux.txt",
            delimiter="\t",
            engine="python",
            skiprows=1,
            names=["time", "events", "rate", "bin_rate", "timestamp"],
        )
        self.df["time"] = self.df["time"] * 10 ** (-3)

        self.reduce(bin_size)

    def reduce(self, bin_size):
        "Reduces the bins so that each bin is the size of time steps if possible"
        rows = int(np.ceil(self.df["time"].size / bin_size))
        cols = 5
        df_new = pd.DataFrame(
            data=np.zeros((rows, cols)),
            columns=["time", "events", "rate", "bin_rate", "timestamp"],
        )
        for row in range(rows):
            time = 0
            events = 0
            for i in range(bin_size):
                if row * bin_size + i < self.df["time"].size:
                    df_row = int(row * bin_size + i)
                else:
                    continue
                events += self.df["events"].iloc[df_row]
                time = self.df["time"].iloc[df_row]

            df_new["time"].iloc[row] = time
            df_new["events"].iloc[row] = events
        df_new["rate"] = df_new["events"] / df_new["time"]
        df_new["bin_rate"] = df_new["events"] / (bin_size * 60)
        self.df = df_new

    def stem(self):
        plt.figure(figsize=(15, 8))
        plt.title("Measurement over night with Bin size {}".format(self.bin_size))
        plt.ylabel("Events per {} minutes".format(self.bin_size))
        plt.xlabel("time [s]")
        plt.stem(self.df["time"], self.df["events"])
        plt.savefig("protokoll/images/flux/flux_stem.pdf")

    def hist(self, bins=8):
        plt.figure(figsize=(15, 8))
        plt.title(
            "Measurement of Rate over night with Bin size {} Minutes".format(
                self.bin_size
            )
        )
        plt.ylabel("Percentage of Rate".format(self.bin_size))
        plt.xlabel("Rate per {} Minutes".format(self.bin_size))
        self.values, self.bins, patches = plt.hist(
            self.df["events"],
            bins=np.arange(-0.5, self.df["events"].max() + 1.5, 1),
            density=True,
        )
        mu = self.poisson_mu = self.df["events"].mean()
        plt.plot(
            np.arange(0, len(self.values), 1),
            poisson(mu, np.arange(0, len(self.values), 1)),
            label="Poisson Distribution with mu={:.2f} / {}Minutes".format(
                mu, self.bin_size
            ),
        )
        plt.legend()
        plt.savefig(
            "protokoll/images/flux/flux_hist_{}_Minutes.pdf".format(self.bin_size)
        )


a = Flux(4)
# a.hist()

for x in np.arange(1, 7, 1):
    Flux(x).hist()
