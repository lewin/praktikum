# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 22:33:43 2021

@author: 394493: Alexander Kohlgraf
This should look into the Rate of Coincidences
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import scipy as sc
import scipy.stats as stats

import nice_plots

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class coinc:
    def __init__(self, time: float = 20):
        """
        Parameters
        ----------
        time : float
            The time about which the rate is measured in Minutes. Originally 20 Minutes, it can be multiplied to be any factor.
        """

        self.time = time
        df = self.df = pd.read_csv(
            r"data/Coinc/coinc_Messung.txt",
            delimiter="\t",
            engine="python",
            skiprows=1,
            names=[
                "time",
                "Voltage A",
                "Thresh A",
                "Voltage B",
                "Thresh B",
                "coinc",
                "singles",
                "coinc/single",
            ],
        )
        df["coinc"] = df["coinc"] * time / (df["time"][0] / 60)
        df["singles"] = df["singles"] * time / (df["time"][0] / 60)

        df["coinc error"] = np.sqrt(df["coinc"])
        df["singles error"] = np.sqrt(df["singles"])

    def plot(self):
        plt.figure(figsize=(15, 8))
        plt.title("Koinzidenzrate gegen Singlesrate")
        plt.xlabel("Singles pro {} Minuten".format(self.time))
        plt.ylabel("Koinzidenzen pro {} Minuten".format(self.time))
        plt.grid(True)
        plt.errorbar(
            x=self.df["singles"],
            y=self.df["coinc"],
            yerr=self.df["coinc error"],
            xerr=self.df["singles error"],
            fmt="none",
            marker="o",
        )
        plt.savefig(
            "protokoll/images/Coinc/coinc_vs_singles_{}_minutes.pdf".format(self.time)
        )
