# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 22:34:46 2021

@author: 394386: Maximilian Bartel
This should look into the shielding dependance, with regards to passed matter
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from scipy import optimize
import nice_plots


def lin_fit(x, a, b):
    return a * x + b


def lin_fit_odr(A, x):
    return A[0] * x + A[1]


class Shielding:
    def __init__(self):
        self.shielding_array = np.array([0 * 207, 1 * 207, 2 * 207, 3 * 207, 4 * 207])
        self.atmosphere = np.array([0, 103.5, 2 * 103.5, 3 * 103.5, 4 * 103.5])
        self.coinc_array = np.array([34, 22, 18, 9, 16])
        self.min_energy = self.shielding_array + 160
        self.intensity = np.zeros(len(self.min_energy))
        self.intensity_normed = np.zeros(len(self.min_energy))

    def plot_raw_new(self):
        self.fig, self.ax = plt.subplots(figsize=(11, 6))
        plt.grid()
        plt.errorbar(
            self.atmosphere,
            self.coinc_array,
            yerr=np.sqrt(self.coinc_array),
            xerr=5 / 45 * self.shielding_array,
            label="Measurement",
        )
        plt.xlabel("density depth [$gcm^{-2}$]")
        plt.ylabel("count")
        plt.title("Count rate vs density depth")
        plt.savefig("density_raw.pdf")

    def plot_raw(self, show_plot=True):
        self.fig, self.ax = plt.subplots(figsize=(10, 10))
        plt.grid()
        plt.errorbar(
            self.shielding_array,
            self.coinc_array,
            yerr=np.sqrt(self.coinc_array),
            xerr=5 / 45 * self.shielding_array,
            label="Measurement",
            fmt="none",
        )
        plt.xlabel("minimum energy required [$MeV$]")
        plt.ylabel("count")
        (self.a, self.b), cov = optimize.curve_fit(
            lin_fit,
            self.shielding_array,
            self.coinc_array,
            sigma=np.sqrt(self.coinc_array),
            absolute_sigma=True,
        )
        plt.plot(
            self.shielding_array,
            self.shielding_array * self.popt[0] + self.popt[1],
            label="linfit",
        )
        error = np.sqrt(np.diag(cov))
        print(error)
        if show_plot:
            plt.legend()
            plt.title("count vs minimum energy required")
            plt.savefig("density_energy.pdf")
            plt.show()

    def plot_spectrum(self):
        x_axis = np.linspace(0, 10, 1000)
        alpha_1 = -0.5483
        alpha_2 = -0.3977
        a = 3.09e-3
        rate = a * x_axis ** (alpha_1 + alpha_2 * np.log(x_axis))
        plt.figure()
        plt.plot(x_axis, rate)
        plt.xscale("log")
        plt.yscale("log")

        plt.show()

    def integrate_and_get_percentages(self):
        alpha_1 = -0.5483
        alpha_2 = -0.3977
        a = 3.09e-3
        i_10 = 0.942e-3
        x2 = lambda x: a * x ** (alpha_1 + alpha_2 * np.log(x))

        for i, energy in enumerate(self.min_energy):
            self.intensity[i] = abs(integrate.quad(x2, 10, energy * 1e-3)[0]) + i_10

        self.intensity_normed = self.intensity / self.intensity[0]
        print(self.intensity_normed)

    def plot_theoretical(self, show_plot=True):
        plt.plot(
            self.shielding_array,
            self.intensity_normed * self.coinc_array[0],
            label="theory",
        )
        self.ax.fill_between(
            self.shielding_array,
            self.intensity_normed * self.coinc_array[0] * 1.08,
            self.intensity_normed * self.coinc_array[0] * 0.92,
            alpha=0.2,
            label="theory confidence",
        )

        if show_plot:
            plt.legend()
            plt.title("Theoretical values vs measurement")
            plt.savefig("density_all_combined.pdf")
            plt.show()

    def plot_fit(self):
        print(0.1 * self.shielding_array)
        self.popt, pcov, chiq, df = nice_plots.nice_regression_plot_odr(
            lin_fit_odr,
            self.shielding_array,
            self.coinc_array,
            yerror=np.sqrt(self.coinc_array),
            xerror=0.11 * self.shielding_array + 0.00001,
            beta0=[-1, 1],
            xlabel="minimum energy required [$MeV$]",
            ylabel="count",
            title="linfit of minimal energy",
            save="density_linfit.pdf",
        )
        print(self.popt, np.sqrt(np.diag(pcov)), chiq, df)


if __name__ == "__main__":
    shield = Shielding()
    # shield.plot_raw_new()
    shield.integrate_and_get_percentages()
    shield.plot_fit()
    shield.plot_raw(show_plot=False)
    shield.plot_theoretical()
    # shield.plot_spectrum()
