import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pandas as pd
import latextable
import texttable
import os
import nice_plots


def exp_fit(x, a, b, c):
    return a * np.exp(-b * x) + c


def lin_fit(x, a, b):
    return a * x + b


class ResistanceToTemperature:
    def __init__(self, material):
        self.current = 0.02
        self.material = material
        self.data_rt = self.read_values("room", self.material)
        self.data_he = self.read_values("He", self.material)
        self.data_ni = self.read_values("Ni", self.material)
        self.data_rt_cu = self.read_values("room", "cu")
        self.data_rt_ta = self.read_values("room", "tantal")
        self.data_rt_si = self.read_values("room", "si")
        self.temp_array = [296.45, 77, 4.2]
        self.data = [self.data_rt, self.data_ni, self.data_he]
        self.data_rt_cu_ta_si = [self.data_rt_cu, self.data_rt_ta, self.data_rt_si]
        self.mean_array = list(map(self.get_mean_resistance, self.data))
        self.std_array = list(map(self.get_std_resistance, self.data))
        self.std_array_cu_ta_si = list(
            map(self.get_std_resistance, self.data_rt_cu_ta_si)
        )
        self.popt = None
        self.pcov = None
        self.chiq = None
        self.ndof = None

    def read_values(self, series, frame):
        if series == "room":
            path = os.path.join(
                os.getcwd(),
                "data",
                "Kalibration_RT_23,3C",
            )
        elif series == "He":
            path = os.path.join(
                os.getcwd(),
                "data",
                "Kalibration_Helium",
            )
        elif series == "Ni":
            path = os.path.join(
                os.getcwd(),
                "data",
                "Kalibration_Stickstoff",
            )
        else:
            raise Exception("Seriesname not excepted!")
        data = pd.read_csv(
            path,
            sep=" ",
            decimal=",",
            skiprows=1,
            index_col=False,
            header=None,
            names=["platinum", "carbon", "cu", "tantal", "si", "1", "2"],
            engine="python",
        )
        return data[frame]

    def get_mean_resistance(self, data):
        return data.mean()

    def get_std_resistance(self, data):
        return data.std()

    def hist_series(self, series):
        plt.hist(series, bins=25)
        plt.title("{}".format(series.name))
        plt.xlabel(r"resistance [$\Omega$]")
        plt.ylabel("count")
        plt.show()

    def print_mean_std(self, latex=None):
        print("----------\n")
        print("order rt, ni, he\n")
        print(self.mean_array)
        print(self.std_array)
        if latex:
            latex_array = []
            for mean, std in zip(self.mean_array, self.std_array):
                latex_array.append([mean, std])
            header = [
                r"Mittelwerte in $\si{\ohm}$",
                r"Standardabweichung in $\si{\ohm}$",
            ]
            latextable.simple_draw(
                "/../protokoll/tables/" + latex,
                latex_array,
                header,
                caption="Messergebnisse von Platin",
                label="{}_mean_std".format(self.material),
            )

    def fit_data(self):
        if self.material == "carbon":
            self.popt, self.pcov = curve_fit(
                exp_fit,
                self.temp_array,
                self.mean_array,
                sigma=self.std_array,
                p0=[3000, 0.07, 1],
                absolute_sigma=True,
            )
            popt, pcov, self.chiq, self.ndof = nice_plots.nice_regression_plot(
                exp_fit,
                self.temp_array,
                self.mean_array,
                self.std_array,
                p0=[3000, 0.07, 1],
                absolute_sigma=True,
                title="carbon exponential fit",
                xlabel="$T [K]$",
                ylabel="$R [\Omega]$",
                save="protokoll/images/carbon_expfit.pdf",
            )
        elif self.material == "platinum":
            self.popt, self.pcov = curve_fit(
                lin_fit,
                self.temp_array[:2],
                self.mean_array[:2],
                sigma=self.std_array[:2],
                absolute_sigma=True,
            )
            popt, pcov, self.chiq, self.ndof = nice_plots.nice_regression_plot(
                lin_fit,
                self.temp_array[:2],
                self.mean_array[:2],
                self.std_array[:2],
                absolute_sigma=True,
                title="platin linear fit",
                xlabel="$T [K]$",
                ylabel="$R [\Omega]$",
                save="protokoll/images/platin_linfit.pdf",
            )

    def plot_fit(self, func):
        if self.popt is None or self.pcov is None:
            raise Exception("fit data first!")
        plt.figure()
        xaxis = np.linspace(0.0001, 300, 100)
        plt.plot(xaxis, func(xaxis, *self.popt))
        plt.plot(self.temp_array, self.mean_array)
        plt.title(self.material)
        plt.show()

    def print_popt_pcov(self, latex=None):
        if self.popt is None or self.pcov is None:
            raise Exception("fit data first!")
        else:
            print_list = [["", ""]]
            perr = np.sqrt(np.diag(self.pcov))
            print_list.extend(
                list(
                    [
                        "$" + chr(i + 97) + "$",
                        r"$\num"
                        + r"{"
                        + "{:.8f}".format(self.popt[i])
                        + r"}\pm\num{"
                        + "{:.8f}".format(perr[i])
                        + r"}$",
                    ]
                    for i in range(len(self.popt))
                )
            )
            print_list.append([r"$\chi^2$", self.chiq])
            print_list.append([r"$ndof$", self.ndof])
            print_list.append([r"$\frac{\chi^2}{ndof}$", self.chiq / self.ndof])
            table = texttable.Texttable()
            table.set_cols_align(["l", "r"])
            table.add_rows(print_list)
            header = ["Parameter", "Wert"]
            table.header(header)
            print(table.draw())
            if latex:
                latextable.simple_draw(
                    "/../protokoll/tables/" + latex,
                    print_list[1:],
                    header,
                    caption="Fitparameter Platin",
                    label="fitparameter_platin",
                    precision=5,
                )

    def getFitValues(self):
        return self.popt

    def getFitErrors(self):
        return np.sqrt(np.diag(self.pcov))

    def getResistanceError(self):
        return self.std_array[0]

    def get_calibration_cu_ta_si(self):
        return self.std_array_cu_ta_si


if __name__ == "__main__":
    resistance_platin = ResistanceToTemperature("platinum")
    resistance_platin.print_mean_std(latex="platin_table.tex")
    resistance_platin.fit_data()
    # resistance_platin.plot_fit(lin_fit)
    resistance_platin.print_popt_pcov(latex="platin_fitparameter.tex")
    resistance_carbon = ResistanceToTemperature("carbon")
    resistance_carbon.print_mean_std(latex="carbon_table.tex")
    resistance_carbon.fit_data()
    resistance_carbon.print_popt_pcov(latex="carbon_fitparameter.tex")
    # resistance_carbon.plot_fit(exp_fit)
    # resistance_carbon.print_popt_pcov()
