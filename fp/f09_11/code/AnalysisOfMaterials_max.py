import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from ResistanceToTemperature import ResistanceToTemperature
import texttable
import nice_plots
import latextable


dir_path = os.path.dirname(os.path.realpath(__file__))

pd.set_option("display.max_rows", None)
pd.set_option("display.max_columns", None)

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

os.chdir(dir_path + "/../")
resistance_platinum = ResistanceToTemperature("platinum")
resistance_carbon = ResistanceToTemperature("carbon")
resistance_platinum.fit_data()
resistance_carbon.fit_data()
resistance_error_materials = pd.DataFrame(
    [resistance_platinum.get_calibration_cu_ta_si()],
    columns=["copper", "tantal", "silicon"],
)
os.chdir(dir_path)


def lin_fit(x, A, B):
    return A * x + B


def KelvinToCelsius(K):
    return K - 273.15


def lin_res_fit(B, T):
    return B[1] * (1 + B[0] * T)


def exponent_fit(B, T):
    return B[0] * T ** B[1]


def ln_fit(B, T):
    return B[0] + B[1] * T


class AnalysisOfMaterial:
    def __init__(self, material):
        self.material = material
        df_nit = pd.read_csv(
            "../data/Stickstoff",
            decimal=",",
            delimiter=" ",
            skiprows=1,
            engine="python",
            index_col=False,
            header=None,
            names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"],
        )
        df_hel = pd.read_csv(
            "../data/Helium",
            decimal=",",
            delimiter=" ",
            skiprows=1,
            engine="python",
            index_col=False,
            header=None,
            names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"],
        )
        df_nit["measurement"] = "nitrogen"
        df_hel["measurement"] = "helium"

        df_plat = self.convert_platinum(
            pd.concat(
                [
                    df_nit[["platinum", material, "measurement"]],
                    df_hel[["platinum", material, "measurement"]],
                ],
                ignore_index=True,
            )
        )
        df_carb = self.convert_carbon(
            pd.concat(
                [
                    df_hel[["carbon", material, "measurement"]],
                    df_nit[["carbon", material, "measurement"]],
                ],
                ignore_index=True,
            )
        )

        # df2 = pd.read_csv("../data/Helium", decimal=",", delimiter=" ", skiprows=1, engine='python', index_col=False, header=None, names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"])
        # df2 = df2[["carbon", material]]
        # df2 = self.convert_carbon(df2)
        # self.beginHelium = df2["temperatures"].head(1)

        self.df = pd.concat([df_plat, df_carb])

        self.df[material + " error"] = float(resistance_error_materials[material])
        # self.df[material + " error"] =

        if material == "silicon":
            self.df = self.df.drop(self.df[self.df[self.material] > 10 ** 10].index)
        self.df = self.df.sort_values("temperatures", axis=0)
        # print(self.df[self.df[material] < 0.1])
        # print(self.df[self.df["measurement"] == "helium"])

    def getPlotVariables(self, material, measurement, start, stop):
        x = self.df["temperatures"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) < stop)
            & (np.array(self.df["temperatures"]) >= start)
        ]
        y = self.df[self.material][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) < stop)
            & (np.array(self.df["temperatures"]) >= start)
        ]
        yerr = self.df[self.material + " error"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) < stop)
            & (np.array(self.df["temperatures"]) >= start)
        ]
        xerr = self.df["temperatures error"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) < stop)
            & (np.array(self.df["temperatures"]) >= start)
        ]
        return x, y, yerr, xerr

    def plotRaw(self):
        plt.figure(figsize=(20, 10))
        plt.grid(True)
        plt.xlabel("Temperatur [K]")
        plt.title("Rohdaten für " + self.material)
        plt.ylabel("Widerstand [Ω]")
        x, y, yerr, xerr = self.getPlotVariables(
            "platinum", "helium", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of platinum of helium with errors",
            alpha=0.5,
            linestyle="-",
        )

        x, y, yerr, xerr = self.getPlotVariables(
            "platinum", "nitrogen", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of platinum of nitrogen with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "helium", start=0, stop=400)
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of carbon of helium with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables(
            "carbon", "nitrogen", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of carbon of nitrogen with errors",
            alpha=0.5,
            linestyle="-",
        )
        # plt.errorbar(self.df[self.df["material"] == "platinum"]["temperatures"], self.df[self.df["material"] == "platinum"][self.material], yerr=self.df[self.df["material"] == "platinum"][self.material + " error"], xerr = self.df[self.df["material"] == "platinum"]["temperatures error"], label=self.material + " Raw Data of platinum with errors")
        # plt.errorbar(self.df[self.df["material"] == "carbon"]["temperatures"], self.df[self.df["material"] == "carbon"][self.material], yerr=self.df[self.df["material"] == "carbon"][self.material + " error"], xerr = self.df[self.df["material"] == "carbon"]["temperatures error"], label=self.material + " Raw Data of carbon with errors")
        plt.legend()
        plt.savefig(dir_path + "/../protokoll/images/" + self.material + "_raw.pdf")
        plt.show()

    def plotLogarithmic(
        self,
        xInverse=False,
        yInverse=False,
        start=0,
        stop=300,
        xLog=True,
        yLog=True,
        **kwargs
    ):
        plt.figure(figsize=(20, 10))
        plt.grid(True)
        if xLog:
            plt.xlabel(
                "Logarithmus der {x}Temperatur".format(
                    x="inversen " if xInverse else ""
                )
            )
        else:
            plt.xlabel("Die {x}Temperatur".format(x="inverse " if xInverse else ""))
        plt.ylabel(
            "Logarithmus des {x}Widerstandes".format(x="inversen " if yInverse else "")
        )
        plt.title("Logarithmische darstellung für " + self.material)
        if yLog:
            plt.yscale("log")
        if xLog:
            plt.xscale("log")

        # x, y, yerr, xerr = self.getPlotVariables("platinum", "helium", start, stop)
        # plt.errorbar(
        #     1 / x if xInverse else x,
        #     1 / y if yInverse else y,
        #     yerr=1 / y ** 2 * yerr if yInverse else yerr,
        #     xerr=1 / x ** 2 * xerr if xInverse else xerr,
        #     label=self.material + " Logarithmic data of platinum of helium with errors",
        #     alpha=0.5,
        #     **kwargs
        # )

        x, y, yerr, xerr = self.getPlotVariables("platinum", "nitrogen", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material
            + " Logarithmic data of platinum of nitrogen with errors",
            **kwargs
        )

        # x, y, yerr, xerr = self.getPlotVariables("carbon", "helium", start, stop)
        # plt.errorbar(
        #     1 / x if xInverse else x,
        #     1 / y if yInverse else y,
        #     yerr=1 / y ** 2 * yerr if yInverse else yerr,
        #     xerr=1 / x ** 2 * xerr if xInverse else xerr,
        #     label=self.material + " Logarithmic data of carbon of helium with errors",
        #     **kwargs
        # )

        # x, y, yerr, xerr = self.getPlotVariables("carbon", "nitrogen", start, stop)
        # plt.errorbar(
        #     1 / x if xInverse else x,
        #     1 / y if yInverse else y,
        #     yerr=1 / y ** 2 * yerr if yInverse else yerr,
        #     xerr=1 / x ** 2 * xerr if xInverse else xerr,
        #     label=self.material + " Logarithmic data of carbon of nitrogen with errors",
        #     alpha=0.5,
        #     **kwargs
        # )

        # x = self.df["temperatures"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # y = self.df[self.material][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # yerr = self.df[self.material + " error"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # xerr = self.df["temperatures error"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # plt.errorbar(1/x if xInverse else x, 1/y if yInverse else y, yerr=1/y**2 * yerr if yInverse else yerr, xerr=1/x**2 * xerr if xInverse else xerr, label=self.material + " Logarithmic data of carbon with errors")

        # T_plat =self.platinumTemperatureTail
        # T_carb =self.carbonTemperatureHead
        # plt.axvline(1/T_plat if xInverse else T_plat, ymin=min(1/y) if yInverse else min(y), ymax = max(1/y) if yInverse else max(y), label="End of Platinum Data")
        # plt.axvline(1/T_carb if xInverse else T_carb, ymin=min(1/y) if yInverse else min(y), ymax = max(1/y) if yInverse else max(y), label="begin of Carbon Data")
        plt.legend()
        plt.savefig(dir_path + "/../protokoll/images/" + self.material + "_raw.pdf")
        plt.show()

    def convert_platinum(self, df):
        A, B = resistance_platinum.getFitValues()
        sigma_A, sigma_B = resistance_platinum.getFitErrors()
        sigma_R = resistance_platinum.getResistanceError()
        temp = []
        temp_err = []
        # temp_sys = []
        for R in df["platinum"]:
            T = R / A - B / A
            T_err = np.sqrt(
                sigma_B ** 2 + (sigma_R / A) ** 2 + (sigma_A * R / (A ** 2)) ** 2
            )
            # T_sys = A*sigma_R_sys
            temp.append(T)
            temp_err.append(T_err)
            # temp_sys.append(T_sys)
        df["temperatures"] = pd.Series(temp)
        df["temperatures error"] = pd.Series(temp_err)
        # df["temperatures systematic"] = pd.Series(temp_sys)
        df["material"] = "platinum"
        return df

    def convert_carbon(self, df):
        A, B, C = resistance_carbon.getFitValues()
        sigma_A, sigma_B, sigma_C = resistance_carbon.getFitErrors()
        sigma_R = resistance_carbon.getResistanceError()
        temp = []
        temp_err = []
        # temp_sys = []
        for R in df["carbon"]:
            T = -np.log(R / A - C / A) / (B)
            T_err = np.sqrt(
                (sigma_B * np.log((R - C) / A) / (B ** 2)) ** 2
                + (sigma_A / (B * A)) ** 2
                + (1 / (B * (R - C))) ** 2 * (sigma_R ** 2 + sigma_C ** 2)
            )
            # T_sys = np.abs(A*B*np.exp(-B*R)*sigma_R_sys)
            temp.append(T)
            temp_err.append(T_err)
            # temp_sys.append(T_sys)
        df["temperatures"] = pd.Series(temp)
        df["temperatures error"] = pd.Series(temp_err)
        # df["temperatures systematic"] = pd.Series(temp_sys)
        df["material"] = "carbon"
        return df

    def cutToLinear(self, df):
        pass

    def cutToNonLinear(self, df):
        pass

    def jumpTemperature(self):
        pass

    def print_popt_pcov_chiq_ndof(self, name, popt, pcov, chiq, ndof):
        if popt is None or pcov is None:
            raise Exception("fit data first!")
        else:
            print_list = [["", ""]]
            perr = np.sqrt(np.diag(pcov))
            print_list.extend(
                list(
                    [
                        "$" + chr(i + 97) + "$",
                        r"$\num"
                        + r"{"
                        + "{:.8f}".format(popt[i])
                        + r"}\pm{"
                        + "{:.8f}".format(perr[i])
                        + r"}$",
                    ]
                    for i in range(len(popt))
                )
            )
            # print_list.extend(
            #     ["$\sigma_{}$".format(chr(i + 97)), "{:.2e}".format(perr[i])]
            #     for i in range(len(popt))
            # )
            print_list.append([r"$\chi$", chiq])
            print_list.append([r"$ndof$", ndof])
            print_list.append([r"$\frac{\chi}{ndof}$", chiq / ndof])
            header = ["Parameter", "Wert"]
            table = texttable.Texttable()
            table.set_cols_dtype(["t", "e"])
            table.set_cols_align(["l", "r"])
            table.add_rows(print_list)

            table.header(header)
            print(name)
            print(table.draw())

            latextable.simple_draw(
                "/../protokoll/tables/{}_{}.tex".format(self.material, name),
                print_list[1:],
                header,
                caption="Fitparameter, der Einfachheit halber ohne Einheiten",
                label="{}_{}_paramter".format(self.material, name),
                precision=5,
                # col_dtypes=["t", "e"],
            )

    def task_c(self, cut_bottom=0, cut_top=np.inf):
        data_x, data_y, data_error_x, data_error_y = self.getPlotVariables(
            "platinum", "helium", cut_bottom, cut_top
        )
        data_x = KelvinToCelsius(data_x)
        data_x = data_x.to_numpy()
        data_y = data_y.to_numpy()
        data_error_x = data_error_x.to_numpy()
        data_error_y = data_error_y.to_numpy()
        popt, pcov, chiq, ndof = nice_plots.nice_regression_plot_odr(
            lin_res_fit,
            data_x,
            data_y,
            data_error_x,
            data_error_y,
            title=self.material + " linear fit",
            beta0=[20, 0.5],
            xlabel="$T [°C]$",
            ylabel="$R [\Omega]$",
            save="../protokoll/images/linear_fit_{}.pdf".format(self.material),
        )
        self.print_popt_pcov_chiq_ndof("linfit_c", popt, pcov, chiq, ndof)

    def task_d(self, cut_bottom=0, cut_top=np.inf):
        data_x, data_y, data_error_x, data_error_y = self.getPlotVariables(
            "carbon", "helium", cut_bottom, cut_top
        )
        # data_x = KelvinToCelsius(data_x)
        data_x = data_x.to_numpy()
        print("Restwiderstand: {:.3}".format(min(data_y)))
        print("min Temp: {:.3f}".format(min(data_x)))
        data_x -= min(data_x)
        data_x = data_x
        data_error_x = data_error_x.to_numpy()
        data_error_y = data_error_y.to_numpy()
        data_y = data_y.to_numpy()

        data_y -= min(data_y)

        data_y = data_y
        popt, pcov, chiq, ndof = nice_plots.nice_regression_plot_odr(
            exponent_fit,
            data_x,
            data_y,
            data_error_x,
            data_error_y,
            title=self.material + " fit",
            beta0=[1, 6],
            xlabel="$T - T_0 [K]$",
            ylabel="$R - R_0 [\Omega]$",
            save="../protokoll/images/t_5_fit_d_{}.pdf".format(self.material),
        )
        self.print_popt_pcov_chiq_ndof("t_5_fit_d", popt, pcov, chiq, ndof)

    def task_d_old(self, cut_bottom=0, cut_top=np.inf):
        data_x, data_y, data_error_x, data_error_y = self.getPlotVariables(
            "platinum", "helium", cut_bottom, cut_top
        )
        # data_x = KelvinToCelsius(data_x)
        data_x = data_x.to_numpy()
        data_y = data_y.to_numpy()
        data_error_x = data_error_x.to_numpy()
        data_error_y = data_error_y.to_numpy()
        popt, pcov, chiq, ndof = nice_plots.nice_regression_plot_odr(
            exponent_fit,
            data_x,
            data_y,
            data_error_x,
            data_error_y,
            title=self.material,
            beta0=[20, 6, 10, 4],
            save="../protokoll/images/t_5_fit_d_{}.pdf".format(self.material),
        )
        self.print_popt_pcov_chiq_ndof("t_5_fit_d", popt, pcov, chiq, ndof)

    def task_f(self, cut_bottom=0, cut_top=np.inf):
        data_x, data_y, data_error_x, data_error_y = self.getPlotVariables(
            "platinum", "nitrogen", cut_bottom, cut_top
        )
        # data_x = KelvinToCelsius(data_x)
        data_x = data_x.to_numpy()
        data_y = data_y.to_numpy()
        data_error_x = data_error_x.to_numpy()
        data_error_y = data_error_y.to_numpy()
        data_error_y = data_error_y / data_y ** 2
        data_y = 1 / data_y
        popt, pcov, chiq, ndof = nice_plots.nice_regression_plot_odr(
            exponent_fit,
            data_x,
            data_y,
            data_error_x,
            data_error_y,
            title=self.material,
            beta0=[2, 3, 1, 4],
        )
        self.print_popt_pcov_chiq_ndof(self.material, popt, pcov, chiq, ndof)


if __name__ == "__main__":
    analysis_cu = AnalysisOfMaterial("copper")
    analysis_ta = AnalysisOfMaterial("tantal")
    analysis_si = AnalysisOfMaterial("silicon")
    print("Tasks: ")
    print(
        "a)  R (T)-Kurve und ln(R) über ln(T) nur im nichtlinearen Bereich für Cu und Ta. "
    )
    # analysis_cu.plotRaw()
    # analysis_cu.plotLogarithmic(start=0, stop=75)
    # analysis_ta.plotRaw()
    # analysis_ta.plotLogarithmic(start=0, stop=75, xLog=False)
    print(
        "b) ln(1/R) über ln(1/T) und ln(1/R) über ln(T) im Bereich der Erschöpfung für SI."
    )
    # analysis_si.plotLogarithmic(True, True, xLog=False)
    # analysis_si.plotRaw()
    print("Weiterhin folgende Größen")
    print(
        "c) den linearen Widerstandskoeffizienten von Cu und Ta mit dem Ansatz: R(T) = R0 (1+ α T)wobei T in °C und R in Ω einzusetzen sind. Dabei ist R0 = R(T=0°C) ."
    )
    analysis_cu.task_c(100)
    analysis_ta.task_c(100)
    print(
        "d) für den nichtlinearen Bereich den Exponenten β mit dem Ansatz R ∝ T^β(Restwiderstand berücksichtigen!)"
    )
    # analysis_cu.task_d(35, 70)
    # analysis_ta.task_d(35, 70)

    print("e) die Sprungtemperatur von Ta bzw. von Nb3Sn ")
    print(
        "f) für den Bereich der Erschöpfung bei Si die Beweglichkeit µ (T) mit dem Ansatz 1/R ~ sigma ~ mü(T) ~ T^gamma"
    )

    print(
        "g) unter Vernachlässigung des Temperaturganges der Beweglichkeit die Aktivierungsenergieder Akzeptoren mit dem Ansatz: , wobei p(T) die Löcherkonzentration darstellt. "
    )
    print("sigma(T) ~ p(T) ~ exp(-(E_A - E_V)/2kT")