export mfl, mean, nv, sd, LinReg, linreg, linreg_residual_plot, linreg_residual_plot_mpl, M64

import Measurements
import Plots
import PyPlot

const M64 = Measurements.Measurement{Float64}

mfl = (v,e) -> Measurements.measurement(v, e)
mean = a -> sum(a)/length(a)

function nv(x)
    Measurements.value.(x)
end

function sd(x)
    Measurements.uncertainty.(x)
end

struct LinReg
    m::Measurements.Measurement
    b::Measurements.Measurement
    chisq::Float64
    samplesize::Float64
    chisq_dof::Float64
end

function linreg(xs::Vector{<:Real}, ys::Vector{<:Real}; allow_offset=true)
    N = length(xs)
    if length(ys) != N
        return nothing
    end
    if allow_offset
        # FWIW, this returns the same result as if we just calculate with automatic error propagation.
        m = (mean(xs.*ys)-mean(xs)*mean(ys))/(mean(xs.^2)-mean(xs)^2)
        b = mean(ys)-m*mean(xs)
    else
        m = mean(xs.*ys)/mean(xs.^2)
        b = 0
    end

    chisq = sum( (nv(ys) .- (nv(m) .*nv(xs) .+ nv(b))).^2 /
                (sd(ys).^2 .+ (nv(m) .* sd(xs)).^2) )
    samplesize = length(xs)
    chisq_dof = samplesize-2
    return LinReg(m, b, nv(chisq), samplesize, chisq_dof)
end

function linreg_residual_plot(xs, ys; figsize=(600,350), allow_offset=true,
        rng=nothing, step=1, kwargs...)
    if rng !== nothing
        xs, ys = xs[rng[1]:rng[2]], ys[rng[1]:rng[2]]
    end
    lr = linreg(xs, ys, allow_offset=allow_offset)
    fitys = nv(lr.m .* xs .+ lr.b)
    resids = nv(ys .- fitys)
    residerrs = sqrt.((sd(ys)).^2 + (nv(lr.m) .* sd(xs)).^2)
    resids = mfl.(resids, residerrs)

    lay = Plots.grid(2, 1, heights=[0.7, 0.3])
    m, b = round(lr.m, digits=3), round(lr.b, digits=3)

    Plots.plot(xs[1:step:end], ys[1:step:end], t=:scatter,
               label="Daten, χ²/ndof = $(round(lr.chisq, digits=1))/$(lr.chisq_dof) = $(round(lr.chisq/lr.chisq_dof, digits=1))";
               kwargs...)
    dataplot = Plots.plot!(xs[1:step:end], fitys[1:step:end], label="mx + b = ($(nv(m)) ± $(sd(m))) x + ($(nv(b)) ± $(sd(b)))")

    Plots.plot(nv(xs[1:step:end]), resids[1:step:end], legend=false, t=:scatter,
               ylabel="Daten - (mx + b)")
    residplot = Plots.plot!(nv(xs[1:step:end]), zeros(size(xs[1:step:end])), color="gray")
    Plots.plot(dataplot, residplot, layout=lay, size=figsize, link=:x)
end

function linreg_residual_plot_mpl(xs, ys;
        figsize=(8, 5), allow_offset=true, xlabel="", ylabel="",
        title="", kwargs...)
    lr = linreg(xs, ys)
    fitys = nv(lr.m .* xs .+ lr.b)
    resids = nv(ys .- fitys)
    residerrs = sqrt.((sd(ys)).^2 + (nv(lr.m) .* sd(xs)).^2)
    resids = mfl.(resids, residerrs)

    fig, axarr = PyPlot.subplots(2, 1, figsize=figsize, sharex=true, gridspec_kw=Dict("height_ratios" => [5,2]),
                                tight_layout=true)

    dataplot = axarr[1]
    residplot = axarr[2]

    m, b = round(lr.m, digits=3), round(lr.b, digits=3)

    dataplot.errorbar(nv(xs), nv(ys), xerr=sd(xs), yerr=sd(ys), color=:red, fmt=".", marker="o",
                     label="Daten, χ²/ndof = $(round(lr.chisq, digits=1))/$(lr.chisq_dof) = $(round(lr.chisq/lr.chisq_dof, digits=1))",
                    )
    dataplot.plot(nv(xs), nv(fitys), label="mx + b = ($(nv(m)) ± $(sd(m))) x + ($(nv(b)) ± $(sd(b)))", color=:green)
    dataplot.set_xlabel(xlabel)
    dataplot.set_ylabel(ylabel)
    dataplot.grid()
    dataplot.set_title(title)
    dataplot.legend()

    residplot.errorbar(nv(xs), nv(resids), yerr=sd(resids), color=:red, marker="o", fmt=".", markeredgecolor=:red)
    residplot.set_ylabel("Daten - (m x + b)")
    residplot.grid()
    fig
end
