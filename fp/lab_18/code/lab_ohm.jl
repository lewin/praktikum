### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# ╔═╡ d148d196-8d71-11eb-066c-c3bf8c74ac32
# Changing utility code
begin
	using Revise
	push!(LOAD_PATH, pwd())
	using analysis
end

# ╔═╡ 5ce4a610-8d6a-11eb-0d96-391b614bfec7
begin
	import CSV
	import DataFrames
	import Measurements
	import Statistics
	
	using Plots
	using StatsPlots
	
	const mea = Measurements
	const DF = DataFrames

	Plots.gr(html_output_format=:png)
	Plots.default(tickfont = ("sans-serif", 10), legendfontsize=10)
end

# ╔═╡ 47236dda-8d69-11eb-1581-89d9e5279f70
md"""## LABView overview"""

# ╔═╡ 2d229b58-8d6a-11eb-3608-9ff6784d07bc
begin
	BASE_PATH = "../data/"
	
	function to_path(fn::String)::String
		return string(BASE_PATH, fn)
	end
end

# ╔═╡ be972536-8d6a-11eb-1080-498c61a4b7a9
function read_csv(file::String)
	CSV.File(to_path(file), normalizenames=true) |> DataFrames.DataFrame
end

# ╔═╡ b2d2b5ee-8d6a-11eb-33ee-81ea93aa5a95
md"""### Resistor (Ohm law)

First some measurement characteristics.
"""

# ╔═╡ 8df63fb4-8d74-11eb-043b-6ffddff7f114
begin
	VOLTAGE_RANGE = 20  # V
	VOLTAGE_BINS = 2^16
	VOLTAGE_UNCERTAINTY = VOLTAGE_RANGE / VOLTAGE_BINS / sqrt(12)
	
	REFERENCE_RESISTOR = 220.5
	REFERENCE_RESISTOR_UNCERTAINTY = 220.5 * 1/1000  # 0.1 percent, last digit
end

# ╔═╡ 6911668c-8d95-11eb-099a-0d0c764d669a
const RESISTORS = [10, 47, 100, 220, 470, 1000, 4700, 10000]

# ╔═╡ 8d70b35e-8d70-11eb-3ba5-7f0e50211848
RESISTOR_FILES = ["Widerstand_$(i)Ohm_220OhmRRef.csv" for i = RESISTORS]

# ╔═╡ a3688e20-8d70-11eb-3c8b-e721e05f8d0b
RESISTOR_DATA = map(read_csv, RESISTOR_FILES);

# ╔═╡ 8f4859fa-9231-11eb-2a2f-49f17fffbd78
# Calculate and assign statistical uncertainty from measurement fluctuations.
# For each resistor, we record the voltage uncertainty measured at a single reference 
# voltage. It contains the combined uncertainty of reference and actual measurement
for df_ = RESISTOR_DATA
	df_.UNCERTAINTY = zeros(size(df_)[1])
	sort!(df_, :U_Ref_V_)
	grouped = DF.groupby(df_, :U_Ref_V_)
	unc_by_ibrb = [
		(g[1, :U_Ref_V_], Statistics.std(g[!, :U_R_V_])) for g = grouped]
	max_unc = maximum(
		filter(x -> !isnan(x), [Statistics.std(g[!, :U_R_V_]) for g = grouped]))
	for (v, u) = unc_by_ibrb
		df_[df_.U_Ref_V_ .== v, :UNCERTAINTY] .= isnan(u) ? 0 : u
	end
end

# ╔═╡ b6d6c1ea-9232-11eb-0287-ffa4f7c55062
# Calculate votlage uncertainties for measurements of each resistor.
VOLTAGE_UNCERTAINTIES = [Statistics.mean(RESISTOR_DATA[i][RESISTOR_DATA[i].UNCERTAINTY .> 0, :UNCERTAINTY]) for i = 1:length(RESISTORS)]

# ╔═╡ c4a8775d-5618-40cb-a685-1db4dd1e4c48
["$r Ω: σ_U = $(round(v*1e3, digits=2))\n" for (r, v) = zip(RESISTORS, VOLTAGE_UNCERTAINTIES)]

# ╔═╡ 17779e80-9232-11eb-3f84-8d10144c56f6
begin
	unchist = Plots.histogram([RESISTOR_DATA[i][RESISTOR_DATA[i].UNCERTAINTY .> 0, :UNCERTAINTY] for i = 1:length(RESISTORS)-3], t=:stephist, title="Verteilung Unsicherheit", xlabel="σ(U) / V", ylabel="n", labels=permutedims(RESISTORS))
	Plots.savefig(unchist, "../img/widerstand_fehler_verteilung.pdf")
	unchist
end

# ╔═╡ f29af1e4-8d76-11eb-34b7-47155ebdad98
function resistor_plot(df, R, voltage_uncertainty=0.; step=1)
	xs = mfl.(df.U_Ref_V_, VOLTAGE_UNCERTAINTY)
	I = xs ./ REFERENCE_RESISTOR
	U = mfl.(df.U_R_V_, voltage_uncertainty)
	
	linreg_residual_plot((I), (U), xlabel="I / A", ylabel="U / V",
		title="R = $(round(Int32, R)) Ohm - Regression - Messwerte: $(round(Int32, 100/step))%", step=step,
		figsize=(800, 400), grid=2, legend=:topleft)
end

# ╔═╡ 3769d857-fe1e-4678-8f07-1a5782ad3970
function multi_resistor_plot(dfs, Rs, voltage_uncertainties; step=10)
	xs = hcat([dfs[i].U_Ref_V_[3:end] / Rs[i] for i = 1:length(dfs)]...)
	ys = hcat([dfs[i].U_R_V_[3:end] for i = 1:length(dfs)]...)
	labels = ["$(R)Ω" for R = Rs]
	Plots.plot((xs), (ys), labels=permutedims(labels), xscale=:log,
	xlabel="I / A", ylabel="U(Mess) / V", legend=:outerright, size=(800,400),
	
	left_margin=20*Plots.px, bottom_margin=20*Plots.px)
end

# ╔═╡ 503bafdb-10c2-4378-a541-0de9bc521fda
Plots.savefig(multi_resistor_plot(RESISTOR_DATA, RESISTORS, VOLTAGE_UNCERTAINTIES),
	"../img/widerstand_vergleichalle_loglog.pdf")

# ╔═╡ ebd6bb25-33c0-4bfa-b268-43020110df5a
multi_resistor_plot(RESISTOR_DATA, RESISTORS, VOLTAGE_UNCERTAINTIES)

# ╔═╡ 5e9774e4-8d95-11eb-3907-831eff2f6a08
function analyze_resistor(df, R, voltage_uncertainty=0.; step=10)
	# Discard data at the end
	mask = df.U_Ref_V_ .< 0.95*maximum(df.U_Ref_V_)
	plot = resistor_plot(df[mask, :], R, voltage_uncertainty, step=step)

	# Shift method. Systematic error from reference resistor.
	xs = df.U_Ref_V_
	U = mfl.(df.U_R_V_, voltage_uncertainty)

	I_up, I_down = xs ./ (REFERENCE_RESISTOR-REFERENCE_RESISTOR_UNCERTAINTY), xs ./ (REFERENCE_RESISTOR+REFERENCE_RESISTOR_UNCERTAINTY)
	
	lr = linreg(xs/REFERENCE_RESISTOR, U)
	lr_up, lr_down = linreg(I_up, U), linreg(I_down, U)
	sys_m = (lr_up.m - lr_down.m)/2
	sys_b = (lr_up.b - lr_down.b)/2
		
	println("Info: $R Ohm: ", lr)
	println("R (m) = $(lr.m) :: m (sys. unc.) = $sys_m :: b (sys. unc.) = $sys_b")
	plot
end

# ╔═╡ 8bbfcade-8d95-11eb-34d3-23394a33ebe0
plots = analyze_resistor.(RESISTOR_DATA, RESISTORS, VOLTAGE_UNCERTAINTIES, step=10);

# ╔═╡ 604ebf50-2367-4c09-af00-94b0a35713fc
[size(rd) for rd = RESISTOR_DATA]

# ╔═╡ f9994f4c-8d97-11eb-07d4-29d0e0ed302f
#[p.savefig("../img/widerstand_$(r).pdf") for (r, p) = zip(RESISTORS, plots)]

# ╔═╡ 2dc10c24-90b3-11eb-320b-c9ffe679bfb2
[Plots.savefig(p, "../img/widerstand_$(r).pdf") for (r, p) = zip(RESISTORS, plots)]

# ╔═╡ c21edeac-90cf-11eb-374f-9daa83b95224
md"""## Hypothetical measurement code"""

# ╔═╡ c7f88c1a-90cf-11eb-2c54-39cd155398fa
struct State
	voltage_out::Float64
	ref_r::Float64
	r::Float64
end

# ╔═╡ db254e0c-90cf-11eb-3399-3d67ddea593b
struct VoltageMeasurement
	U_Ref::Float64  # Reference resistor voltage (for current)
	U_R::Float64    # Resistor voltage
end

# ╔═╡ 728077ba-90d0-11eb-3b3a-6d85ba9727db
function initial_state()::State
	State(0, REFERENCE_RESISTOR, 10)
end

# ╔═╡ a4e9e042-90d0-11eb-0067-570183c47d25
function update_voltage(st::State)::State
	State(st.voltage_out+0.05, st.ref_r, st.r)
end

# ╔═╡ b85f96da-90d0-11eb-26fa-d37e357a9e2a
# Simulate taking a measurement for an ohmic resistor.
function take_measurement(max_voltage::Float64=.5)::DF.DataFrame
	n_meas = round(Int64, max_voltage/0.05) # step width
	df = DF.DataFrame(U_Ref=zeros(Float64, n_meas), U_R=zeros(Float64, n_meas))
	
	st = initial_state()
	i = 1
	# Start measurement by ramping up voltage
	while nothing != (st = update_voltage(st))
		I = st.voltage_out / (st.ref_r+st.r)
		df[i, :] .= [st.ref_r*I, st.r*I]

		if i >= n_meas
			break
		end
		i += 1
		
	end
	df
end

# ╔═╡ 844d3c5c-90d1-11eb-111a-5b4b13ee2fd6
begin
	df = take_measurement()
	I = df.U_Ref / REFERENCE_RESISTOR
	@df df plot(I, :U_R,
		title="Simulierte Ohm-Messung", label="U_R", xlabel="I / A", ylabel="U_R / V", legend=:topleft)
end

# ╔═╡ Cell order:
# ╠═47236dda-8d69-11eb-1581-89d9e5279f70
# ╠═5ce4a610-8d6a-11eb-0d96-391b614bfec7
# ╠═d148d196-8d71-11eb-066c-c3bf8c74ac32
# ╠═2d229b58-8d6a-11eb-3608-9ff6784d07bc
# ╠═be972536-8d6a-11eb-1080-498c61a4b7a9
# ╠═b2d2b5ee-8d6a-11eb-33ee-81ea93aa5a95
# ╠═8df63fb4-8d74-11eb-043b-6ffddff7f114
# ╠═6911668c-8d95-11eb-099a-0d0c764d669a
# ╠═8d70b35e-8d70-11eb-3ba5-7f0e50211848
# ╠═a3688e20-8d70-11eb-3c8b-e721e05f8d0b
# ╠═8f4859fa-9231-11eb-2a2f-49f17fffbd78
# ╠═b6d6c1ea-9232-11eb-0287-ffa4f7c55062
# ╠═c4a8775d-5618-40cb-a685-1db4dd1e4c48
# ╠═17779e80-9232-11eb-3f84-8d10144c56f6
# ╠═f29af1e4-8d76-11eb-34b7-47155ebdad98
# ╠═3769d857-fe1e-4678-8f07-1a5782ad3970
# ╠═503bafdb-10c2-4378-a541-0de9bc521fda
# ╠═ebd6bb25-33c0-4bfa-b268-43020110df5a
# ╠═5e9774e4-8d95-11eb-3907-831eff2f6a08
# ╠═8bbfcade-8d95-11eb-34d3-23394a33ebe0
# ╠═604ebf50-2367-4c09-af00-94b0a35713fc
# ╠═f9994f4c-8d97-11eb-07d4-29d0e0ed302f
# ╠═2dc10c24-90b3-11eb-320b-c9ffe679bfb2
# ╠═c21edeac-90cf-11eb-374f-9daa83b95224
# ╠═c7f88c1a-90cf-11eb-2c54-39cd155398fa
# ╠═db254e0c-90cf-11eb-3399-3d67ddea593b
# ╠═728077ba-90d0-11eb-3b3a-6d85ba9727db
# ╠═a4e9e042-90d0-11eb-0067-570183c47d25
# ╠═b85f96da-90d0-11eb-26fa-d37e357a9e2a
# ╠═844d3c5c-90d1-11eb-111a-5b4b13ee2fd6
