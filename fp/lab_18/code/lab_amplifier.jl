### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# ╔═╡ e6129662-8d98-11eb-1aa7-1d81065bd20e
# Changing utility code
begin
	using Revise
	push!(LOAD_PATH, pwd())
	using analysis
end

# ╔═╡ d2f1d098-8d98-11eb-09fc-fd66808c6bf3
begin
	import StatsPlots
	import Statistics
	import CSV
	import DataFrames
	import Measurements
	import LsqFit
	import FFTW

	using Plots
	using LinearAlgebra
	
	const mea = Measurements
	const DF = DataFrames

	Plots.gr(html_output_format="png")
	Plots.default(tickfont = ("sans-serif", 10), legendfontsize=10)
end

# ╔═╡ e8f38b20-8d98-11eb-1b38-fb6306463607
begin
	BASE_PATH = "../data/"
	
	function to_path(fn::String)::String
		return string(BASE_PATH, fn)
	end
end

# ╔═╡ ebb2b502-8d98-11eb-038f-c113db7716b5
function read_csv(file::String)
	CSV.File(to_path(file), normalizenames=true) |> DataFrames.DataFrame
end

# ╔═╡ eedfc774-8d98-11eb-199e-bb08abb3a3a0
AMPLIFIER_WITH_CAP = read_csv("Verstärker_mitKondensator.csv");

# ╔═╡ b23db930-918e-11eb-36db-f921604a37d0
AMPLIFIER_WO_CAP = read_csv("Verstärker_ohneKondensator.csv");

# ╔═╡ bba4bd78-918e-11eb-151e-612ee72a55f5
begin
	@StatsPlots.df AMPLIFIER_WO_CAP plot(:Zeit_ms_, :U_A_V_,
		title="Verstärker", xlabel="t / ms", ylabel="U / V",
		label="\$U_A\$", color=:red,
		legend=:bottomright, grid=2, size=(600, 300))
	@StatsPlots.df AMPLIFIER_WO_CAP plot!(
		:Zeit_ms_, :U_E_V_, label="\$U_E\$", style=:dash, color=:red)
	@StatsPlots.df AMPLIFIER_WITH_CAP plot!(
		:Zeit_ms_, :U_A_V_, label="\$U_A, 47 \\mu F\$", color=:blue)
	@StatsPlots.df AMPLIFIER_WITH_CAP plot!(
		:Zeit_ms_, 10 .* :U_E_V_, label="\$10 U_E, 47 \\mu F\$", style=:dash,
	color=:blue)
	Plots.savefig(Plots.current(), "../img/verstaerker_rohdaten_vergleich.pdf")
	Plots.current()
end

# ╔═╡ b3032650-9190-11eb-041d-17aba357f485
begin
	@StatsPlots.df AMPLIFIER_WITH_CAP plot(:Zeit_ms_, :U_A_V_, label="\$U_A, C = 47 \\mu F\$", title="Verstärker mit Kondensator")
	@StatsPlots.df AMPLIFIER_WITH_CAP plot!(:Zeit_ms_, 1e2 .* :U_E_V_, label="\$10^2 \\cdot U_E, C = 47 \\mu F\$", legend=:bottomright, grid=2, size=(600,300))
	Plots.savefig(Plots.current(), "../img/verstaerker_mit_c.pdf")
	Plots.current()
end

# ╔═╡ f004f058-91f9-11eb-07a2-1fe0949f7dff
function sin_model(x, p)
	p[1] .* sin.(p[2] .* x .+ p[3]) .+ p[4]
end

# ╔═╡ 00f32630-9204-11eb-2e47-272298f2b9c5
function cos_model(x, p)
	p[1] .* cos.(p[2] .* x .+ p[3]) .+ p[4]
end

# ╔═╡ 712ca068-91fb-11eb-3e5c-25900a6a9229
struct WaveFitParams
	amplitude::M64
	frequency::M64
	phaseshft::M64
	vertoffst::M64
end

# ╔═╡ 2f830218-91ff-11eb-2d09-1f5b2ce97fa8
function sin_model(x, p::WaveFitParams)
	p.amplitude .* sin.(2pi/1000 * p.frequency .* x .+ p.phaseshft * pi/180) .+ p.vertoffst
end

# ╔═╡ be467fe6-91fc-11eb-39a4-0d1e26334254
function fit_model(df, ysym, xsym=:Zeit_ms_; initial=[0., 0., 0.,], model=sin_model)
	LsqFit.curve_fit(model, df[!, xsym], df[!, ysym], initial)
end

# ╔═╡ d743512c-9201-11eb-3b5a-db173b65fdbe
"""
Use Fourier analysis and statistics to find the best monochromatic sine wave parameters for data.
"""
function estimate_wave_params(df, ysym, xsym=:Zeit_ms_)
	ydata = df[!, ysym]
	xdata = df[!, xsym]
	sample_rate = 1 / Statistics.mean(xdata[2:end] - xdata[1:end-1])
	
	# TODO: This is not always a good way to calculate offset
	offset = abs(maximum(ydata))-abs(minimum(ydata))
	ampl = Statistics.mean(Statistics.quantile(abs.(ydata .- offset), 99/100)) - offset
	fft = FFTW.rfft(ydata)
	fftfreq = FFTW.rfftfreq(length(ydata), sample_rate)
	maxfreqloc = argmax(abs.(fft))
	maxfreq = fftfreq[maxfreqloc]
	shift = mod(atan(imag(fft[maxfreqloc]), real(fft[maxfreqloc])) + pi/2, 2pi)
	freq = maxfreq * 2pi

	[ampl, freq, shift, offset]
end

# ╔═╡ 8394cde2-91fb-11eb-06a9-c9c26711245b
"""
Extract wave parameters (WaveFitParams) from raw data.

If `fft == true`, use an FFT to directly find wave parameters.

Otherwise, use a least-squares fit.
"""
function calculate_wave_shape(df, ysym, xsym=:Zeit_ms_;
		initial=[0., 0., 0., 0.,], model=sin_model,
		fft=false)::WaveFitParams
	ydata = df[!, ysym]
	xdata = df[!, xsym]

	if fft || all(initial .== 0.)
		initial = estimate_wave_params(df, ysym, xsym)
	end
	
	if fft
		unc_params = initial
	end
	
	if !fft
		fit = LsqFit.curve_fit(model, df[!, xsym], df[!, ysym], initial)
		covar = LsqFit.estimate_covar(fit)
		uncerts = sqrt.(covar[diagind(covar, 0)])
		unc_params = mfl.(fit.param, uncerts)
	end
	
	# Convert to useful units
	unc_params[2] = unc_params[2]/(2pi) * 1e3 # millisecond
	unc_params[3] *= (180/pi)
	if unc_params[1] < 0
		unc_params[1] *= -1
		unc_params[3] -= 180
	end
	println(unc_params)
	unc_params[3] = mod(unc_params[3], 360)
	if unc_params[3] > 180
		unc_params[3] = unc_params[3] - 360
	elseif unc_params[3] < -180
		unc_params[3] = unc_params[3] + 360
	end
	
	WaveFitParams(unc_params...)
end

# ╔═╡ f5ce81dc-9201-11eb-11b0-bf0b391ba22c
wp = estimate_wave_params(AMPLIFIER_WITH_CAP, :U_E_V_)

# ╔═╡ b1419012-91ff-11eb-2309-b18d29f5ca62
calculate_wave_shape(AMPLIFIER_WO_CAP, :U_E_V_, fft=true)

# ╔═╡ 77f4d084-9214-11eb-23bd-d136f0401706
calculate_wave_shape(AMPLIFIER_WO_CAP, :U_E_V_, fft=false)

# ╔═╡ 5d796a9a-91ff-11eb-1f27-831e9a99a9f7
function plot_and_compare(df, E=:U_E_V_, A=:U_A_V_;
		amplifications=[1,1], ylim=(-4, 7))
	plot()
	for (i, symbol) = enumerate([E, A])
		if symbol === nothing
			continue
		end
		
		lo, hi = minimum(df[!, symbol]), maximum(df[!, symbol])
		plot!(df.Zeit_ms_,
			amplifications[i] .* df[!, symbol],
			label="\$ $(amplifications[i]) \\cdot $(string(symbol)[1:end-3]), \\mathrm{min} = $(round(lo, digits=3)), \\mathrm{max} = $(round(hi, digits=3))\$",
			legend=:topright, grid=2, legendfontsize=12, left_margin=10*Plots.px,
			size=(800,500), ylim=ylim, xlabel="t / ms", ylabel="U / V")

		wp = calculate_wave_shape(df, symbol, fft=false)
		println(wp)
		
		rnd(x) = round(x, digits=3)
		rndnv = rnd ∘ nv
		rndsd = rnd ∘ sd
		amp, freq, ps, off = rndnv(wp.amplitude), rndnv(wp.frequency), rndnv(wp.phaseshft), rndnv(wp.vertoffst)
		sdamp, sdfreq, sdps, sdoff = map(rndsd, (wp.amplitude, wp.frequency, wp.phaseshft, wp.vertoffst))
		
		println("$i $symbol -- $amp +/- $sdamp -- $freq +/- $sdfreq -- $ps +/- $sdps -- $off +/- $sdoff")
		sgnoff, sgnshft = (sign(off) >= 0 ? "+" : ""), (sign(ps) >= 0 ? "+" : "")

		sin_formula = "\$$(amplifications[i]) \\cdot $(amp) \\cdot \\sin($freq \\, \\mathrm{Hz} \\cdot 2 \\pi t $sgnshft $(ps) \\degree) $sgnoff $off\$" 
		plot!(df.Zeit_ms_, amplifications[i] .* nv(sin_model(df.Zeit_ms_, wp)),
			label=sin_formula)
	end
	Plots.current()
end

# ╔═╡ 66c88aa0-9208-11eb-167b-7be2ac473240
begin
	pc = plot_and_compare(AMPLIFIER_WITH_CAP, amplifications=[1e2, 1])
	Plots.savefig(pc, "../img/verstaerker_signalfit_mit_kond.pdf")
	pc
end

# ╔═╡ ed15660c-9215-11eb-2c62-e1d863c60e9e
begin
	p = plot_and_compare(AMPLIFIER_WO_CAP, amplifications=[1, 1], ylim=(-2.5, 5))
	Plots.savefig(p, "../img/verstaerker_signalfit_ohne_kond.pdf")
	p
end

# ╔═╡ 1d93ede6-920a-11eb-3005-ad360bb4499c
calculate_wave_shape(AMPLIFIER_WO_CAP, :U_E_V_)

# ╔═╡ 4d88e3f8-9205-11eb-1188-cf37e0782e93
function analyse_spectrum(df)
	xdata = df.Zeit_ms_
	ydata = df.U_A_V_
	edata = df.U_E_V_
	sample_rate = 1000/Statistics.mean(xdata[2:end]-xdata[1:end-1])
	fft = FFTW.rfft(ydata)
	infft = FFTW.rfft(edata)
	fftfreq = FFTW.rfftfreq(length(ydata), sample_rate)
	a = Plots.plot(fftfreq, abs.(fft)/maximum(abs.(fft)), label="\$\\mathcal{F}(U_A)\$",
		xlim=(0,5e3), xlabel="f / Hz", ylabel="Amplitude (rel.)",
		size=(600,300))
	e = Plots.plot!(fftfreq, abs.(infft)/maximum(abs.(infft)), label="\$\\mathcal{F}(U_E)\$")

	Plots.savefig("../img/verstaerker_ausgang_fft.pdf")
	Plots.current()
end

# ╔═╡ 43cecd83-44d1-4038-bc93-8538824e09b3
analyse_spectrum(AMPLIFIER_WITH_CAP)

# ╔═╡ c5b974bb-b887-4e4d-938a-5c4737419dee
analyse_spectrum(AMPLIFIER_WO_CAP)

# ╔═╡ a3c2c10a-7090-4c58-8b1b-098ffc3836de
FFTW.rfftfreq(2000, 1e6)

# ╔═╡ ad1d0010-9228-11eb-07e0-c1c7318f4e1d
AMPLIFIER_WO_CAP

# ╔═╡ Cell order:
# ╠═d2f1d098-8d98-11eb-09fc-fd66808c6bf3
# ╠═e6129662-8d98-11eb-1aa7-1d81065bd20e
# ╠═e8f38b20-8d98-11eb-1b38-fb6306463607
# ╠═ebb2b502-8d98-11eb-038f-c113db7716b5
# ╠═eedfc774-8d98-11eb-199e-bb08abb3a3a0
# ╠═b23db930-918e-11eb-36db-f921604a37d0
# ╠═bba4bd78-918e-11eb-151e-612ee72a55f5
# ╠═b3032650-9190-11eb-041d-17aba357f485
# ╠═f004f058-91f9-11eb-07a2-1fe0949f7dff
# ╠═2f830218-91ff-11eb-2d09-1f5b2ce97fa8
# ╠═00f32630-9204-11eb-2e47-272298f2b9c5
# ╠═712ca068-91fb-11eb-3e5c-25900a6a9229
# ╠═be467fe6-91fc-11eb-39a4-0d1e26334254
# ╠═d743512c-9201-11eb-3b5a-db173b65fdbe
# ╠═8394cde2-91fb-11eb-06a9-c9c26711245b
# ╠═f5ce81dc-9201-11eb-11b0-bf0b391ba22c
# ╠═b1419012-91ff-11eb-2309-b18d29f5ca62
# ╠═77f4d084-9214-11eb-23bd-d136f0401706
# ╠═5d796a9a-91ff-11eb-1f27-831e9a99a9f7
# ╠═66c88aa0-9208-11eb-167b-7be2ac473240
# ╠═ed15660c-9215-11eb-2c62-e1d863c60e9e
# ╠═1d93ede6-920a-11eb-3005-ad360bb4499c
# ╠═4d88e3f8-9205-11eb-1188-cf37e0782e93
# ╠═43cecd83-44d1-4038-bc93-8538824e09b3
# ╠═c5b974bb-b887-4e4d-938a-5c4737419dee
# ╠═a3c2c10a-7090-4c58-8b1b-098ffc3836de
# ╠═ad1d0010-9228-11eb-07e0-c1c7318f4e1d
