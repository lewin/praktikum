### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ e6129662-8d98-11eb-1aa7-1d81065bd20e
# Changing utility code
begin
	using Revise
	push!(LOAD_PATH, pwd())
	using analysis
end

# ╔═╡ d2f1d098-8d98-11eb-09fc-fd66808c6bf3
begin
	import PyPlot
	import CSV
	import DataFrames
	import Measurements
	
	using Plots
	
	const plt = PyPlot
	const mea = Measurements
	const DF = DataFrames

	Plots.gr()
end

# ╔═╡ e8f38b20-8d98-11eb-1b38-fb6306463607
begin
	BASE_PATH = "../data/"
	
	function to_path(fn::String)::String
		return string(BASE_PATH, fn)
	end
end

# ╔═╡ ebb2b502-8d98-11eb-038f-c113db7716b5
function read_csv(file::String)
	CSV.File(to_path(file), normalizenames=true) |> DataFrames.DataFrame
end

# ╔═╡ eedfc774-8d98-11eb-199e-bb08abb3a3a0


# ╔═╡ Cell order:
# ╠═d2f1d098-8d98-11eb-09fc-fd66808c6bf3
# ╠═e6129662-8d98-11eb-1aa7-1d81065bd20e
# ╠═e8f38b20-8d98-11eb-1b38-fb6306463607
# ╠═ebb2b502-8d98-11eb-038f-c113db7716b5
# ╠═eedfc774-8d98-11eb-199e-bb08abb3a3a0
