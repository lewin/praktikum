# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 16:06:15 2021

@author: 394493: Alexander Kohlgraf
"""
import numpy as np
import pandas as pd
import os
import nice_plots
import matplotlib.pyplot as plt
from scipy import constants as const
import texttable
import latextable
from matplotlib.patches import Rectangle


dir_path = os.path.dirname(os.path.realpath(__file__))
plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
# plt.rcParams["figure.autolayout"] = True
GdFe_names = [
    # "GdFe -32.0Degree.dat",
    "GdFe -32.0Degree(2).dat",
    "GdFe -31.4--31.7Degree.dat",
    "GdFe -30.2--30.0Degree.dat",
    "GdFe -28.8--28.8Degree.dat",
    "GdFe -27.8--27.8Degree.dat",
    "GdFe -26.0--25.9Degree.dat",
    "GdFe -21.8--22.0Degree.dat",
    "GdFe -20.0--19.8Degree.dat",
    # "GdFe -17.1--16.9Degree.dat",
    # "GdFe -14.9--14.6Degree.dat",
    "GdFe 18.4Degree.dat",
    "GdFe 19.5-19.6Degree.dat",
    "GdFe 25.6-25.7Degree.dat",
    "GdFe 31,2-31,2Degree.dat",
    # "GdFe 35.5-35.6Degree.dat",
    "GdFe 39.5-39.3Degree.dat",
    "GdFe 43.6-43.5Degree.dat",
    "GdFe 47.5-47.5Degree.dat",
    # "GdFe 53.3-54.0Degree.dat",
    "GdFe 57.0-56.8Degree.dat",
    # "GdFe 62.5-62.3Degree.dat",
    "GdFe 64.4-64.6Degree.dat",
    # "GdFe 65.7-65.9Degree.dat",
    "GdFe 71.6-71.7Degree.dat",
    # "GdFe 75,5-75,6Degree.dat",
    "GdFe 78.8-78.8Degree.dat",
    "GdFe 84.7-84.3Degree.dat",
]

temperatures = [
    [-32, -32],
    [-32, -32],
    [-31.4, -31.7],
    [-30.2, -30.0],
    [-28.8, -28.8],
    [-27.8, -27.8],
    [-26, -25.9],
    [-21.8, -22.0],
    [-20.0, -19.8],
    # [-17.1, -16.9],
    # [-14.9, -14.6],
    [18.4, 18.4],
    [19.5, 19.6],
    [25.6, 25.7],
    [31.2, 31.2],
    # [35.5, 35.6],
    [39.5, 39.3],
    [43.6, 43.5],
    [47.5, 47.5],
    # [53.3, 54.0],
    [57.0, 56.8],
    # [62.5, 62.3],
    [64.4, 64.6],
    # [65.7, 65.9],
    [71.6, 71.7],
    # [75.5, 75.6],
    [78.8, 78.8],
    [84.7, 84.3],
]

cut_above_left_array = [
    1.5,
    1,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1,
    # 1.3,
    1.3,
    0.8,
    1.3,
    1.5,
]

cut_above_right_array = [
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    1.8,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    # 2,
    2,
    0.3,
    0.35,
    0.5,
]

cut_below_left_array = [
    1.9,
    1.9,
    1.9,
    1.9,
    1.9,
    1.9,
    1.9,
    2,
    0.8,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    # 1.5,
    0.3,
    0.15,
    1.5,
    1,
]

cut_below_right_array = [
    1,
    1,
    1,
    1,
    1,
    1.2,
    1,
    1.5,
    1,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    # 1.5,
    1.5,
    1.5,
    1.2,
    1.3,
]
"""
1. read data xxx
2. get data split
3. do linfit
4. cut schlauch linfit all data
5. split positive and negative magnet field data
6. make linfit for rest, almost vertical
7. get coerzitiv field with linear through middle
"""


def lin_fit(x, A, B):
    return A * x + B


def print_popt_pcov_chiq_ndof(name, popt, pcov, chiq, ndof):
    if popt is None or pcov is None:
        raise Exception("fit data first!")
    else:
        print_list = [["", ""]]
        perr = np.sqrt(np.diag(pcov))
        print_list.extend(
            list(
                [
                    "$" + chr(i + 97) + "$",
                    r"$\num"
                    + r"{"
                    + "{:.3e}".format(popt[i])
                    + r"}\pm\num{"
                    + "{:.3e}".format(perr[i])
                    + r"}$",
                ]
                for i in range(len(popt))
            )
        )
        # print_list.extend(
        #     ["$\sigma_{}$".format(chr(i + 97)), "{:.2e}".format(perr[i])]
        #     for i in range(len(popt))
        # )
        print_list.append([r"$\chi^2$", chiq])
        print_list.append([r"$ndof$", ndof])
        print_list.append([r"$\frac{\chi^2}{ndof}$", chiq / ndof])
        header = ["Parameter", "Wert"]
        table = texttable.Texttable()
        table.set_cols_dtype(["t", "e"])
        table.set_cols_align(["l", "r"])
        table.add_rows(print_list)

        table.header(header)
        print(name)
        print(table.draw())

        latextable.simple_draw(
            "/../protokoll/tables/{}.tex".format(name),
            print_list[1:],
            header,
            caption="Fitparameter, der Einfachheit halber ohne Einheiten",
            label="{}_paramter".format(name),
            precision=2,
            # col_dtypes=["t", "e"],
        )


class Hysterese:
    def __init__(
        self, name, temperature=[24.9, 26.1], error_above=0.0001, error_below=0.0001
    ):
        self.name = name.replace(".dat", "")
        self.temperature = (temperature[0] + temperature[1]) / 2
        self.temperature_error = max(np.abs(temperature[0] - temperature[1]) / 2, 0.01)
        print(os.getcwd())
        self.df = pd.read_csv(
            os.getcwd() + "/data/" + name,
            decimal=".",
            delimiter="\t",
            engine="python",
            index_col=False,
            header=None,
            names=["magn", "A+B", "A-B"],
        )
        self.df["kerr"] = self.df["A-B"] / (2 * self.df["A+B"])
        self.error_above = error_above
        self.error_below = error_below

    def scatter(self, save=False):
        plt.figure(figsize=(20, 10))
        plt.xlabel("Flussdichte [G]")
        plt.ylabel("Kerr Winkel")
        plt.title("Kerr Winkel von " + self.name)
        plt.scatter(self.df["magn"], self.df["kerr"], label="Rohdaten")
        plt.legend()
        plt.grid(True)
        plt.savefig(dir_path + "/../protokoll/images/" + self.name + ".pdf")
        plt.show()

    def do_the_magic_hc(
        self, cut_above_left=2, cut_above_right=2, cut_below_left=2, cut_below_right=2
    ):
        self.split_data()
        self.get_lin_reg_above()
        self.get_lin_reg_below()
        self.cut_near_values_above(cut_above_left, cut_above_right)
        self.cut_near_values_below(cut_below_left, cut_below_right)
        self.get_lin_reg_above_clean()
        self.get_lin_reg_below_clean()
        self.get_vertical_together()
        self.split_vertical_data()
        self.get_lin_reg_left()
        self.get_lin_reg_right()
        self.middel_line()
        self.get_cross_left()
        self.get_cross_right()
        self.get_cross_error_left()
        self.get_cross_error_right()
        return self.h_c_together()
        # self.plot_all()

    def compensation(
        self,
        cut_above_left,
        cut_above_right,
        cut_below_left,
        cut_below_right,
    ):
        self.do_the_magic_hc(
            cut_above_left=cut_above_left,
            cut_above_right=cut_above_right,
            cut_below_left=cut_below_left,
            cut_below_right=cut_below_right,
        )
        H_c = self.h_c
        H_c_error = self.h_c_error
        self.compensation = 1 / H_c
        self.compensation_error = np.abs(H_c_error / H_c ** 2)

    def split_data(self):
        cut_line = self.df["kerr"].mean()
        self.above = self.df.drop(self.df[self.df.kerr < cut_line].index)
        self.below = self.df.drop(self.df[self.df.kerr > cut_line].index)
        # plt.figure(figsize=(12, 8))
        # plt.scatter(self.below["magn"], self.below["kerr"], color="red")
        # plt.scatter(self.above["magn"], self.above["kerr"])
        # plt.show()

    def get_lin_reg_above(self):
        yerror = [self.error_above] * len(self.above)
        above_kerr = self.above["kerr"].to_numpy()
        above_magn = self.above["magn"].to_numpy()
        (
            self.above_popt,
            self.above_pcov,
            self.above_chiq,
            self.above_df,
        ) = nice_plots.get_regression_parameter(lin_fit, above_magn, above_kerr, yerror)

    def get_lin_reg_below(self):
        yerror = [self.error_below] * len(self.below)
        below_kerr = self.below["kerr"].to_numpy()
        below_magn = self.below["magn"].to_numpy()
        (
            self.below_popt,
            self.below_pcov,
            self.below_chiq,
            self.below_df,
        ) = nice_plots.get_regression_parameter(lin_fit, below_magn, below_kerr, yerror)

    def cut_near_values_above(self, cut_left=2, cut_right=2):
        perr = np.sqrt(np.diag(self.above_pcov))
        above_left = self.above.drop(self.above[self.above.magn > 0].index)
        above_right = self.above.drop(self.above[self.above.magn < 0].index)
        above_left = above_left.drop(
            above_left[
                abs(above_left.kerr - lin_fit(above_left.magn, *self.above_popt))
                > self.error_above * cut_left
            ].index
        )

        above_right = above_right.drop(
            above_right[
                abs(above_right.kerr - lin_fit(above_right.magn, *self.above_popt))
                > self.error_above * cut_right
            ].index
        )
        self.above_clean = pd.concat([above_left, above_right])
        # plt.figure()
        # plt.scatter(self.above_clean["magn"], self.above_clean["kerr"])
        # plt.show()

    def cut_near_values_below(self, cut_left=2, cut_right=2):
        perr = np.sqrt(np.diag(self.below_pcov))
        below_left = self.below.drop(self.below[self.below.magn > 0].index)
        below_right = self.below.drop(self.below[self.below.magn < 0].index)
        below_left = below_left.drop(
            below_left[
                abs(below_left.kerr - lin_fit(below_left.magn, *self.below_popt))
                > self.error_below * cut_left
            ].index
        )
        below_right = below_right.drop(
            below_right[
                abs(below_right.kerr - lin_fit(below_right.magn, *self.below_popt))
                > self.error_below * cut_right
            ].index
        )
        self.below_clean = pd.concat([below_left, below_right])

        # plt.figure()
        # plt.scatter(below_left["magn"], below_left["kerr"])
        # plt.show()

    def get_lin_reg_above_clean(self):
        yerror = [self.error_above] * len(self.above_clean)
        above_kerr = self.above_clean["kerr"].to_numpy()
        above_magn = self.above_clean["magn"].to_numpy()
        (
            self.above_popt,
            self.above_pcov,
            self.above_chiq,
            self.above_df,
        ) = nice_plots.nice_regression_plot(
            lin_fit,
            above_magn,
            above_kerr,
            yerror,
            title="Fit der oberen Werte",
            ylabel="Kerr Winkel $\Theta$",
            xlabel="magnetische Flussdichte [G]",
            save=os.getcwd() + "/protokoll/images/hysterese/above_linfit.pdf",
        )
        print_popt_pcov_chiq_ndof(
            "linfit_above",
            self.above_popt,
            self.above_pcov,
            self.above_chiq,
            self.above_df,
        )

    def get_lin_reg_below_clean(self):
        yerror = [self.error_below] * len(self.below_clean)
        below_magn = self.below_clean["magn"].to_numpy()
        below_kerr = self.below_clean["kerr"].to_numpy()
        (
            self.below_popt,
            self.below_pcov,
            self.below_chiq,
            self.below_df,
        ) = nice_plots.nice_regression_plot(
            lin_fit,
            below_magn,
            below_kerr,
            yerror,
            title="Fit der unteren Werte",
            ylabel="Kerr Winkel $\Theta$",
            xlabel="magnetische Flussdichte [G]",
            save=os.getcwd() + "/protokoll/images/hysterese/below_linfit.pdf",
        )
        print_popt_pcov_chiq_ndof(
            "linfit_below",
            self.below_popt,
            self.below_pcov,
            self.below_chiq,
            self.below_df,
        )

    def get_vertical_together(self):
        temp = self.df.drop(self.above_clean.index)
        temp = temp.drop(self.below_clean.index)
        temp = temp.drop(
            temp[temp.kerr - lin_fit(temp.magn, *self.above_popt) > 0].index
        )
        temp = temp.drop(
            temp[temp.kerr - lin_fit(temp.magn, *self.below_popt) < 0].index
        )

        self.vertical_data = temp
        # plt.figure()
        # plt.scatter(self.vertical_data["magn"], self.vertical_data["kerr"])
        # plt.show()

    def split_vertical_data(self, above_right=True):
        self.left = self.vertical_data.drop(
            self.vertical_data[self.vertical_data.magn > 0].index
        )
        left_mean = self.left["magn"].mean()
        left_std = self.left["magn"].std()
        self.left = self.left.drop(
            self.left[abs(self.left.magn - left_mean) > 2 * left_std].index
        )
        self.right = self.vertical_data.drop(
            self.vertical_data[self.vertical_data.magn < 0].index
        )
        right_mean = self.right["magn"].mean()
        right_std = self.right["magn"].std()
        self.right = self.right.drop(
            self.right[abs(self.right.magn - right_mean) > 2 * right_std].index
        )

    def get_lin_reg_left(self):
        yerror = [(self.error_below + self.error_above) / 2] * len(self.left)
        magn = self.left["magn"].to_numpy()
        kerr = self.left["kerr"].to_numpy()
        (
            self.left_popt,
            self.left_pcov,
            self.left_chiq,
            self.left_ndof,
        ) = nice_plots.nice_regression_plot(
            lin_fit,
            magn,
            kerr,
            yerror,
            title="Fit der linken Werte",
            ylabel="Kerr Winkel $\Theta$",
            xlabel="magnetische Flussdichte [G]",
            save=os.getcwd() + "/protokoll/images/hysterese/left_linfit.pdf",
        )
        print_popt_pcov_chiq_ndof(
            "left_linfit",
            self.left_popt,
            self.left_pcov,
            self.left_chiq,
            self.left_ndof,
        )

    def get_lin_reg_right(self):
        yerror = [(self.error_below + self.error_above) / 2] * len(self.right)
        magn = self.right["magn"].to_numpy()
        kerr = self.right["kerr"].to_numpy()
        (
            self.right_popt,
            self.right_pcov,
            self.right_chiq,
            self.right_ndof,
        ) = nice_plots.nice_regression_plot(
            lin_fit,
            magn,
            kerr,
            yerror,
            title="Fit der rechten Werte",
            ylabel="Kerr Winkel $\Theta$",
            xlabel="magnetische Flussdichte [G]",
            save=os.getcwd() + "/protokoll/images/hysterese/right_linfit.pdf",
        )
        print_popt_pcov_chiq_ndof(
            "right_linfit",
            self.right_popt,
            self.right_pcov,
            self.right_chiq,
            self.right_ndof,
        )

    def middel_line(self):
        weights_above = np.sqrt(np.diag(self.above_pcov))
        weights_below = np.sqrt(np.diag(self.below_pcov))
        weights = [weights_above[0], weights_below[0]]
        means = [self.above_popt[0], self.below_popt[0]]
        self.middel_gradient, self.middel_gradient_error = np.average(
            means, weights=weights, returned=True
        )
        weights = [weights_above[1], weights_below[1]]
        means = [self.above_popt[1], self.below_popt[1]]
        self.middel_intercept, self.middel_intercept_error = np.average(
            means, weights=weights, returned=True
        )
        print(
            "steigung {:.3e} +- {:.3e}".format(
                self.middel_gradient, self.middel_gradient_error
            )
        )
        print(
            "Mitte  {:.3e} +- {:.3e}".format(
                self.middel_intercept, self.middel_intercept_error
            )
        )

    def get_cross_left(self):
        self.cross_left = (self.left_popt[1] - self.middel_intercept) / (
            self.middel_gradient - self.left_popt[0]
        )

    def get_cross_right(self):
        self.cross_right = (self.right_popt[1] - self.middel_intercept) / (
            self.middel_gradient - self.right_popt[0]
        )

    def get_cross_error_left(self):
        error_left = np.sqrt(np.diag(self.right_pcov))
        self.cross_error_left = (
            (error_left[1] / (self.middel_gradient - self.left_popt[0])) ** 2
            + (self.middel_intercept_error / (self.middel_gradient - self.left_popt[0]))
            ** 2
            + (
                self.middel_gradient_error
                * (self.left_popt[1] - self.middel_intercept)
                / (self.middel_gradient - self.left_popt[0]) ** 2
            )
            ** 2
            + (
                error_left[0]
                * (self.left_popt[1] - self.middel_intercept)
                / (self.middel_gradient - self.left_popt[0]) ** 2
            )
            ** 2
        )
        self.cross_error_left = np.sqrt(self.cross_error_left)

    def get_cross_error_right(self):
        error_right = np.sqrt(np.diag(self.right_pcov))
        self.cross_error_right = (
            (error_right[1] / (self.middel_gradient - self.right_popt[0])) ** 2
            + (
                self.middel_intercept_error
                / (self.middel_gradient - self.right_popt[0])
            )
            ** 2
            + (
                self.middel_gradient_error
                * (self.right_popt[1] - self.middel_intercept)
                / (self.middel_gradient - self.right_popt[0]) ** 2
            )
            ** 2
            + (
                error_right[0]
                * (self.right_popt[1] - self.middel_intercept)
                / (self.middel_gradient - self.right_popt[0]) ** 2
            )
            ** 2
        )
        self.cross_error_right = np.sqrt(self.cross_error_right)

    def h_c_together(self):
        self.h_c = ((self.cross_right - self.cross_left) / 2) * 10 ** (-4) / const.mu_0
        self.h_c_error = (
            np.sqrt(self.cross_error_left ** 2 + self.cross_error_right ** 2)
            * 10 ** (-4)
            / const.mu_0
        )
        return self.h_c, self.h_c_error

    def plot_all(self, save=False):
        fig, ax = plt.subplots(figsize=(12, 8))
        plt.scatter(self.df["magn"], self.df["kerr"])
        min_x_data = self.df["magn"].min()
        max_x_data = self.df["magn"].max()
        min_y_data = self.df["kerr"].min()
        max_y_data = self.df["kerr"].max()
        vertical_x_array = np.linspace(min_x_data, max_x_data, 1000)
        plt.plot(
            self.above_clean["magn"],
            lin_fit(self.above_clean["magn"], *self.above_popt),
            color="red",
            linestyle="dotted",
        )
        plt.plot(
            self.below_clean["magn"],
            lin_fit(self.below_clean["magn"], *self.below_popt),
            color="red",
            linestyle="dotted",
        )
        plt.plot(
            vertical_x_array,
            lin_fit(vertical_x_array, *self.left_popt),
            color="red",
            linestyle="dotted",
            scalex=False,
            scaley=False,
        )
        plt.plot(
            vertical_x_array,
            lin_fit(vertical_x_array, *self.right_popt),
            color="red",
            linestyle="dotted",
            scalex=False,
            scaley=False,
        )
        plt.plot(
            vertical_x_array,
            lin_fit(vertical_x_array, self.middel_gradient, self.middel_intercept),
            color="red",
        )
        plt.xlim(min_x_data - 0.1 * max_x_data, 1.1 * max_x_data)
        plt.ylim(min_y_data - 0.1 * max_y_data, 1.1 * max_y_data)
        extra1 = Rectangle(
            (0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0
        )
        extra2 = Rectangle(
            (0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0
        )
        extra3 = Rectangle(
            (0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0
        )

        plt.legend(
            [extra1, extra2, extra3],
            (
                "cross left: {:.2f} $\pm$ {:.2f}".format(
                    self.cross_left, self.cross_error_left
                ),
                "cross right: {:.2f} $\pm$ {:.2f}".format(
                    self.cross_right, self.cross_error_right
                ),
                "H$_c$: {:.2f} $\pm$ {:.2f}".format(
                    const.mu_0 * self.h_c / 10 ** (-4),
                    const.mu_0 * self.h_c_error / 10 ** (-4),
                ),
            ),
        )
        plt.title("GdFe {:.1f}°C".format(self.temperature))
        plt.ylabel("Kerr Winkel $\Theta$")
        plt.xlabel(r"magnetische Flussdichte [G]")
        plt.grid(True)
        if save:
            plt.savefig(
                os.getcwd()
                + "/protokoll/images/hysterese/GdFe_{:.0f}_all.pdf".format(
                    self.temperature
                )
            )
        plt.show()

    def plot_raw(self, save):
        fig, ax = plt.subplots(figsize=(12, 8))
        plt.scatter(self.df["magn"], self.df["kerr"])
        plt.title("GdFe {:.1f}°C".format(self.temperature))
        plt.ylabel("Kerr Winkel $\Theta$")
        plt.xlabel(r"magnetische Flussdichte [G]")
        plt.grid(True)
        if save:
            plt.savefig(
                os.getcwd()
                + "/protokoll/images/hysterese/GdFe_{:.0f}_raw.pdf".format(
                    self.temperature
                )
            )
        plt.show()


# for (
#     files,
#     temperature,
#     cut_above_left,
#     cut_above_right,
#     cut_below_left,
#     cut_below_right,
# ) in zip(
#     GdFe_names,
#     temperatures,
#     cut_above_left_array,
#     cut_above_right_array,
#     cut_below_left_array,
#     cut_below_right_array,
# ):
#     data = Hysterese(files, temperature)
#     data.do_the_magic_hc(
#         cut_above_left=cut_above_left,
#         cut_above_right=cut_above_right,
#         cut_below_left=cut_below_left,
#         cut_below_right=cut_below_right,
#     )
#     data.plot_all()

index = 18

data = Hysterese(GdFe_names[index], temperatures[index])
data.plot_raw(save=True)
data.do_the_magic_hc(
    cut_above_left=cut_above_left_array[index],
    cut_above_right=cut_above_right_array[index],
    cut_below_left=cut_below_left_array[index],
    cut_below_right=cut_below_right_array[index],
)
# data.do_the_magic_hc(
#     cut_above_left=3,
#     cut_above_right=3,
#     cut_below_left=3,
#     cut_below_right=3,
# )
data.plot_all(save=True)
