module T8

using Setfield

import CSV
import DataFrames
import PyPlot

const DF = DataFrames

include("util.jl")

using .util

include("fit.jl")
include("gain.jl")

## Organize our data ##

data_path() = "../data/"
img_path() = "../img/"

struct GasMix
    argon::Int
    CO2::Int
end

struct Voltage
    u::Int
end

@enum MeasureMode Drift Layer Spectrum
@enum Isotope cd109 sr90 fe55 am241

struct FileDesc
    gm::GasMix
    iso::Isotope
    mode::MeasureMode
    volt::Voltage
    suff::String
    index::Int
end

## Produce file path

function filename_for(fd::FileDesc)::String
    gm = "gas$(fd.gm.argon)_$(fd.gm.CO2)"
    iso = string(fd.iso)
    mode = string(fd.mode)

    if fd.mode in [Drift, Layer]
        return "$(gm)_$(iso)_$(mode)/$(gm)_$(iso)_$(fd.volt.u)v_$(fd.index)$(fd.suff).csv"
    elseif fd.mode == Spectrum
        return "$(gm)_$(mode)/$(gm)_$(iso)_$(fd.volt.u)v_$(fd.index)$(fd.suff).csv"
    end
end

## Open files.

function sanitize_dataframe!(df; max_voltage=2.)
    mask = isnan.(df[!,:Max_Voltage_V_])

    df[mask, :Max_Voltage_V_] .= 2
end

function discover_files(template; sanitize=true)::DF.DataFrame
    df = DF.DataFrame()
    for i = 1:1000
        d = @set template.index = i
        fn = string(data_path(), filename_for(d))
        if stat(fn).size > 0
            append!(df, DF.DataFrame(CSV.File(fn, normalizenames=true, select=["Buffer_No_","Cycle_No_", "Max_Voltage_V_"])))
        else
            break
        end
    end
    if sanitize
        sanitize_dataframe!(df)
    end
    df
end

## Describe our data

# First measurement: gain by drift voltage

CD_DRIFT_VOLTAGES = map(Voltage, [3200, 3250, 3300, 3350, 3400])
CD_DRIFT_TEMPLATES = [i => FileDesc(GasMix(70,30), cd109, Drift, i, "", 0) for i = CD_DRIFT_VOLTAGES]

"""Returns an array of pairs (Voltage, DataFrame)"""
CD_DRIFT_FILES = () -> [i => discover_files(t) for (i, t) = CD_DRIFT_TEMPLATES]
CD_DRIFT_LIMITS = [(-Inf, Inf), (-Inf, Inf), (0.18, 0.4), (0.25, .5), (.39, .65)]

CD_LAYER1_VOLTAGES = map(Voltage, [2571, 2576, 2581, 2586, 2596, 2601])
CD_LAYER1_TEMPLATES = [i => FileDesc(GasMix(70,30), cd109, Layer, i, "", 0) for i = CD_LAYER1_VOLTAGES]

"""Returns an array of pairs (Voltage, DataFrame)"""
CD_LAYER1_FILES = () -> [i => discover_files(t) for (i, t) = CD_LAYER1_TEMPLATES]

SPECTRUM_ISOTOPES = [cd109, sr90, fe55, am241]
SPECTRUM_TEMPLATES = [i => FileDesc(GasMix(70, 30), i, Spectrum, Voltage(3400), "", 0) for i = SPECTRUM_ISOTOPES]

"""Returns an array of pairs (Isotope => DataFrame)"""
SPECTRUM_FILES = () -> [i => discover_files(t) for (i, t) = SPECTRUM_TEMPLATES]
SPECTRUM_5V = () -> [am241 => discover_files(FileDesc(GasMix(70,30), am241, Spectrum, Voltage(3400), "_5vmax", 0))]

GAS_MIXES = [GasMix(66, 34), GasMix(70, 30), GasMix(74, 26)]
GAS_MIX_TEMPLATES = [gm => FileDesc(GasMix(gm.argon, gm.CO2), cd109, Drift, Voltage(3400), "", 0) for gm = GAS_MIXES]

"""Returns an array of pairs (GasMix => DataFrame)"""
GAS_MIX_FILES = () -> [gm => discover_files(fd) for (gm, fd) = GAS_MIX_TEMPLATES]
GAS_MIX_74_5V = () -> [GasMix(74, 26) => discover_files(FileDesc(GasMix(74, 26), cd109, Drift, Voltage(3400), "_max5v", 0))]
GAS_MIX_LIMITS = [(.15, .4), (.39, .65), (.8, 1.35)]

## Constants

const N_CD = 322
const P = 1.164e15 # mV / C
const e = 1.602176634e-19

const NPE_CD = N_CD*P*e

## Some overview

import Plots

"""Draw max voltage histograms for all measurements."""
function compare_all_measurements(files; title="", normalize=false)
    labels = [f[1] for f = files]
    frames = [f[2] for f = files]

    maxvolts = [frame.Max_Voltage_V_ for frame = frames]

    Plots.histogram(maxvolts, labels=map(string, hcat(labels...)),
                    xlabel="U / V", ylabel="n", title=title,
                    size=(600,300), normalize=normalize,
                    t=:stephist, bins=100)
end

function plot_all_overviews()
    for (dfs, title, filename) = [
        (CD_DRIFT_FILES(), "Cd-109 Drift Voltage", "overview_cd109_drift.pdf"),
        (CD_LAYER1_FILES(), "Cd-109 GEM Layer 1 Voltage", "overview_cd109_gem1.pdf"),
        (SPECTRUM_FILES(), "Spectra", "overview_spectrum.pdf"),
        (GAS_MIX_FILES(), "Gas Mix", "overview_gas_mix.pdf"),
    ]
        p = compare_all_measurements(dfs, title=title)
        Plots.savefig(p, string(img_path(), filename))
    end
end

# Drift voltage analysis
#

function cd109_peaks()
    data = CD_DRIFT_FILES()
    limits = CD_DRIFT_LIMITS

    PyPlot.pygui(false)
    for (i, (v,d)) = enumerate(data)
        if limits[i] == (-Inf, Inf)
            lim = nothing
        else
            lim = limits[i]
        end
        fig = gain.find_peak_scatter(d, limit=lim)
        fig.savefig("../img/cd109_drift_dist_$(v.u).pdf")
    end
    PyPlot.pygui(true)
end

CD_109_PEAKS = [
                3250 => mfl(.150, .1),
                3300 => mfl(.246, .089),
                3350 => mfl(.364, .104),
                3400 => mfl(.518, .111),
               ]

function gm_peaks()
    data = GAS_MIX_FILES()
    limits = GAS_MIX_LIMITS

    PyPlot.pygui(false)
    for (lim, (v,d)) = zip(limits, data)
        fig = gain.find_peak_scatter(d, limit=lim)
        fig.savefig("../img/gas_mix_dist_$(v.argon).pdf")
    end
    PyPlot.pygui(true)
end

GM_PEAKS = [GasMix(66,34) => mfl(.24,0.091), GasMix(70,30) => mfl(.519,.117),
            GasMix(74,26) => mfl(1.065, .227)]
GM_PEAKS_FOR_PLOT = () -> [gm.argon => v for (gm, v) = GM_PEAKS]

function iso_peaks()
    data = SPECTRUM_FILES()
    #data = SPECTRUM_5V()

    #PyPlot.pygui(false)
    for (v,d) = data
        fig = gain.find_peak_scatter(d, limit=(0.14, 0.25))
        fig.savefig("../img/spectrum_dist_$(v).pdf")
    end
    #PyPlot.pygui(true)
end

function gain_vs_voltage(peaks::Vector{Pair{T, M64}}; name="cd109_gain_regression", filt=log, legend=:bottomright ) where {T}
    xs = [p[1] for p = peaks]
    ys = [p[2] for p = peaks]
    gains = ys ./ NPE_CD

    # formula: g = a exp(b x)
    # log'ed: ln(g) = b x + ln(a)
    lf = util.linreg(xs, filt.(gains))
    println(collect(zip(xs, gains)))
    println(lf)

    util.linreg_residual_plot(xs, filt.(gains),
        xlabel="\$U / V\$", ylabel="\$\\ln(G)\$",
        figsize=(600,350), legend=legend,
        save="../img/$(name).pdf")
end

end # module
