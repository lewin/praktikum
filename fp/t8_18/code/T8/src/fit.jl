module fit

import Plots
import LsqFit
import DataFrames

const DF = DataFrames

function gauss_model(xs, ps)
    (a, sigma, mu) = ps
    a/sqrt(2pi*sigma^2) .* exp.(-(xs .- mu).^2 ./ (2 * sigma^2))
end

"""For fitting a gauss distribution, to partial data."""
function partial_gauss_model(xs, ps; limits=(-Inf, Inf), ys=[])
    (a, sigma, mu) = ps
    fitys = a/sqrt(2pi*sigma^2) .* exp.(-(xs .- mu).^2 ./ (2 * sigma^2))

    if length(ys) > 0
        mask =  .!((xs .> limits[1]) .& (xs .< limits[2]))
        fitys[mask] = ys[mask]
    end

    fitys
end

function fit_partial_gauss(xs, ys; limits=(.15, Inf), ps=[30, 0.2, 0.1])
    model = (x, p) -> partial_gauss_model(x, p, limits=limits, ys=ys)
    model = gauss_model

    LsqFit.curve_fit(model, xs, ys, ps)
end

function fit_to_histogram(df; limits=(.145, Inf))
    DF.sort!(df, :Max_Voltage_V_)
    grouped = DF.groupby(df, :Max_Voltage_V_)
    counts_by_voltage = [(f[1, :Max_Voltage_V_], size(f)[1]) for f = grouped]
    volts = map(x -> x[1], counts_by_voltage)[2:end-1]
    counts = map(x -> x[2], counts_by_voltage)[2:end-1]

    fitp = fit_partial_gauss(volts, counts, limits=limits)
    println(fitp.param)

    Plots.plot(volts, counts)
    Plots.plot!(volts, partial_gauss_model(volts, fitp.param))
end

end # module
