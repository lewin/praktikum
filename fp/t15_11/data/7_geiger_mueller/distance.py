"""
Created on Mon Mar 25 14:14:29 2021

@author: 394493: Alexander Kohlgraf
"""

import serial as serial
import time

ser = serial.Serial("COM3", 9600)
print(ser.name)
time.sleep(1)

outName = "test" + ".txt"
outFile = open(outName, "w")
outFile.write("duration, time, distance_guess")

while True:
    line = ser.readline()
    list_of_data = line.split(", ")

    duration = list_of_data[0]
    time = list_of_data[1]
    distance = duration / 58

    outFile.write(str(duration) + ", " + str(time) + ", " + str(distance))

    if False:
        ser.close()
        outFile.close()
        break
