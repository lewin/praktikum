/*
   Author: Alexander Kohlgraf

   Date: 25.03.2021

   This Code sends the time difference recorded by an ultrasonic
   sensor to the serial. as well as timestamp

*/

#define TXpin 2
#define RXpin 3

const int SENSOR_MAX_RANGE = 400; // in cm
unsigned long duration;
unsigned long distance;

void setup() {
  Serial.begin(9600);
  pinMode(TXpin, OUTPUT);
  pinMode(RXpin, INPUT);
}

void loop() {
  digitalWrite(TXpin, LOW);
  delayMicroseconds(2);

  digitalWrite(TXpin, HIGH);
  delayMicroseconds(10);

  duration = pulseIn(RXpin, HIGH);

  Serial.println(String(duration) + ", " + String(micros()));

  delay(10);
}
