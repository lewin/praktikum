"""
Created on Mon Mar 26 14:14:29 2021

@author: 394493: Alexander Kohlgraf

This code saves data
"""

import serial as serial
import time


def heart_rate(name):
    ser = serial.Serial("COM3", 9600)
    print(ser.name)
    outName = "heart_rate_raw_" + str(name) + ".txt"
    outFile = open(outName, "w")
    outFile.write("raw, time\n")

    counter = 1000
    while True:
        try:
            line = ser.readline().decode("UTF-8")
            print(line)

            list_of_data = str(line).split(", ")

            if len(list_of_data) == 2:
                outFile.write(line)

                counter = counter - 1
                print(counter)
                if counter <= 0:
                    ser.close()
                    outFile.close()
                    break
        except:
            print("conversion error with UTF-8")
