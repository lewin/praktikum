"""
Created on Fri Mar 26.03.2021

@author: 394386: Maximilian Bartel
gets the voltage values from the arduino and saves it
"""

import serial as serial
import time

ser = serial.Serial("/dev/tty.usbmodem11401", 9600)

outName = input("textfile name: ") + ".csv"
outFile = open(outName, "w")
outFile.write("voltage, voltage_on_led, voltage_on_resistor \n")

time.sleep(4.0)

while True:
    while ser.in_waiting:
        line = ser.readline().decode("utf-8")

        outFile.write(line)
        print(line)
        if line == "break":
            break
    if line == "break":
        break

ser.close()
outFile.close()