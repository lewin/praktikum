import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import nice_plots
import os
from scipy.interpolate import UnivariateSpline
import matplotlib.dates as mdates
from datetime import datetime
from matplotlib.patches import Rectangle

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


beta0_array = [
    [01e-1, 5],
    [01e-12, 0.05],
    [01e-20, 0.04],
    [01e-12, 0.05],
    [01e-12, 0.05],
    [01e-12, 0.06],
    [01e-12, 0.05],
    [01e-12, 0.05],
    [01e-12, 0.09],
    [0e-20, 0.05],
]
wavelength = np.array(
    [
        687,
        940,
        568,
        498,
        687,
        479,
        702,
        722,
        413,
        431,
    ]
)


def exp_model(B, V):
    return B[0] * (np.exp(V / B[1]) - 1)


def exp_model_alt(V, A, B):
    return A * (np.exp(V / B) - 1)


class U_I_Diagramm:
    def __init__(self, measurement):
        self.measurement = measurement
        self.data = pd.read_csv(
            r"data/5_planck/data/led_" + str(measurement) + ".csv",
            delimiter=", ",
            engine="python",
        )
        self.resistor_ohm = 1000
        # self.digital_error = 2 * 4.096 / 65536
        self.digital_error = 0.01 / np.sqrt(3)
        self.resistor_ohm_error = 1000 * 0.0
        self.data["current"] = self.data["voltage_on_resistor"] / self.resistor_ohm
        # self.data["current_error"] = np.sqrt(
        #     (self.digital_error / self.resistor_ohm) ** 2
        #     + (
        #         self.resistor_ohm_error
        #         * self.data["voltage_on_resistor"]
        #         / self.resistor_ohm ** 2
        #     )
        #     ** 2
        # )
        self.data["current_error"] = self.data["current"] * np.sqrt(
            (self.digital_error / self.data["voltage_on_resistor"]) ** 2
            + (self.resistor_ohm_error / self.resistor_ohm) ** 2
        )
        self.data["current_error"] = self.data["current_error"].fillna(
            self.digital_error / self.resistor_ohm
        )

    def get_activation_voltage(self):
        return self.data[self.data.current > 1e-4].head(1)["voltage_on_led"]

    def check_import(self):
        print(self.data.head())

    def plot_all(self):
        plt.figure(figsize=(10, 8))
        plt.plot(
            self.data["voltage_on_led"],
            self.data["voltage_on_resistor"] / self.resistor_ohm,
        )
        # plt.plot(self.data["voltage"], self.data["voltage_on_resistor"])
        plt.show()

    def plot_all_error(self):
        fig, ax = plt.subplots(figsize=(12, 8))
        plt.errorbar(
            self.data["voltage_on_led"],
            self.data["voltage_on_resistor"] / self.resistor_ohm,
            xerr=self.digital_error,
            yerr=self.data["current_error"],
            ecolor="red",
            fmt="none",
            marker="o",
        )
        plt.grid(True)
        if self.get_activation_voltage().to_numpy().size > 0:
            extra1 = Rectangle(
                (0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0
            )
            plt.legend(
                [extra1],
                (
                    r"$V_{TH}=$ "
                    + "{} $\pm$ {:.3f} V".format(
                        self.get_activation_voltage().to_numpy()[0], self.digital_error
                    ),
                ),
            )
        plt.title("I-V Diagramm {} nm LED".format(wavelength[self.measurement - 1]))
        plt.xlabel("Voltage on LED [V]")
        plt.ylabel("Current through LED [A]")
        # plt.xlim((2.2, 2.4))
        # plt.ylim((0.000, 0.0002))
        plt.savefig("protokoll/images/led/led_{}.pdf".format(self.measurement))

    def plot_fit_data(self):
        data_x = self.data["voltage_on_led"].to_numpy()
        data_y = self.data["voltage_on_resistor"].to_numpy() / self.resistor_ohm
        data_error_x = np.array([self.digital_error] * len(data_x))
        data_error_y = self.data["current_error"].to_numpy()
        (
            self.popt,
            self.pcov,
            self.chiq,
            self.ndof,
        ) = nice_plots.nice_regression_plot_odr(
            exp_model,
            data_x,
            data_y,
            data_error_x,
            data_error_y,
            plot_it=True,
            beta0=beta0_array[self.measurement - 1],
        )
        print(self.popt)
        self.perr = np.sqrt(np.diag(self.pcov))
        # print(self.perr)
        return self.popt, self.perr, self.chiq, self.ndof, self.pcov

    def plot_fit_without_xerror_data(self):
        data_x = self.data["voltage_on_led"].to_numpy()
        data_y = self.data["voltage_on_resistor"].to_numpy() / self.resistor_ohm
        data_error_y = self.data["current_error"]
        self.popt, self.pcov, self.chiq, self.ndof = nice_plots.nice_regression_plot(
            exp_model_alt,
            data_x,
            data_y,
            data_error_y,
            save="protokoll/images/led/led_{}.pdf".format(self.measurement),
            p0=beta0_array[self.measurement - 1],
            title=str(self.measurement),
        )
        print(self.popt)
        self.perr = np.sqrt(np.diag(self.pcov))
        # print(self.perr)
        return self.popt, self.perr, self.chiq, self.ndof, self.pcov

    def get_variables(self):
        return self.popt, self.perr, self.chiq, self.ndof, self.pcov


if __name__ == "__main__":
    for i in range(10):
        # if i != 2:
        #     continue
        u_i_1 = U_I_Diagramm(i + 1)

        # u_i_1.plot_all()
        u_i_1.plot_all_error()
