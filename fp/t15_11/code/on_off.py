import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import os
from scipy.interpolate import UnivariateSpline
import matplotlib.dates as mdates
from datetime import datetime

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class On_off:
    def __init__(self):
        self.measurement = "on_off"
        self.data = pd.read_csv(
            r"data/1_blink/data/" + str(self.measurement) + ".csv",
            delimiter=",",
            engine="python",
        )
        self.data["time"] -= self.data["time"][0]
        self.data["time"] = pd.to_datetime(self.data["time"], unit="us")

    def check_import(self):
        print(self.data.head())

    def plot_raw(self):
        plt.figure(figsize=(10, 8))
        plt.step(self.data["time"], self.data["state"], color="red")

        plt.gcf().autofmt_xdate()
        myFmt = mdates.DateFormatter("%H:%M:%S")
        plt.gca().xaxis.set_major_formatter(myFmt)
        plt.grid(True)
        plt.ylabel("Status [1/0]")
        plt.xlabel("Zeit [h:min:s]")
        plt.title("LED-Status gegen Zeit")
        plt.savefig("protokoll/images/led/on_off.pdf")


if __name__ == "__main__":
    on_off = On_off()
    on_off.check_import()
    on_off.plot_raw()
