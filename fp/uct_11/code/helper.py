import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from scipy import signal, ndimage

import file_reader


def _to_corner_coords(index: pd.Index) -> np.ndarray:
    """Helper function to transform N coordinates pointing at centers of bins
    to N+1 coords pointing to the edges"""
    coords = index.values

    delta = coords[-1] - coords[-2]

    return np.concatenate((coords, [coords[-1] + delta])) - delta / 2


def plot_2d_dataframe(
    df: pd.DataFrame,
    ax: plt.Axes = None,
    square=False,
    column=None,
    index_is_y=True,
    c_unit=None,
) -> plt.Axes:
    """Plot pandas data frames using pcolormesh. This function expects numeric
    labels. Have a look at seaborn.heatmap if you need something else.

    Details:
     - plotted meshes are stored in ax.meshes
     - The colorbar is stored in ax.custom_colorbar (DO NOT RELY ON THIS)
     - If the plotted data is already present we just shift it to the top using set_zorder
     - Uses _data_hash(x, y, c) to identify previously plotted data

    :param df: pandas dataframe to plot
    :param ax: Axes object
    :param square:
    :param column: Select this column from the dataframe and unstack the index
    :param index_is_y: If true the index are on the y-axis and the columns on the x-axis
    :param c_unit: If not None a colorbar will be drawn with this label
    :return:
    """
    if ax is None:
        ax = plt.gca()

    if square:
        ax.set_aspect("equal")

    if column is None and len(df.columns) == 1 and len(df.index.levshape) == 2:
        column = df.columns[0]

    ax_kwargs = {}
    if column is not None:
        ax_kwargs["title"] = column
        series = df[column]
        df = series.unstack()

    c = df.values
    x_idx = df.columns
    y_idx = df.index

    if not index_is_y:
        c = np.transpose(c)
        x_idx, y_idx = y_idx, x_idx

    x_label = x_idx.name
    y_label = y_idx.name

    y = _to_corner_coords(y_idx)
    x = _to_corner_coords(x_idx)

    current_mesh = ax.pcolormesh(x, y, c)
    # can also be found in ax.collections[-1]

    if c_unit is not None:
        cb = plt.colorbar(ax=ax, mappable=current_mesh)
        cb.set_label(c_unit)

        # you can also get the colorbar with
        # ax.collections[-1].colorbar

    ax.set(ylabel=y_label, xlabel=x_label, **ax_kwargs)

    return ax


def plot_amplitude_scan(filename):
    """Example function that shows how an A-Scan CAN be plotted. This is just
    an example for coding, not for the presentation!"""

    df, meta = file_reader.read_amplitude_scan(filename)
    frequency = meta["front_panel"]["probe1"]
    out_gain = meta["front_panel"]["output"]
    in_gain = meta["front_panel"]["gain"]

    title = f"Frequenz: {frequency}, Ausgang: {out_gain}, Eingang: {in_gain}, Modus: {meta['front_panel']['mode']}"

    df["Unfiltered Amplitude"] = np.abs(
        # the hilbert transform computes the analytic signal from a real signal
        signal.hilbert(df["HF"])
    )

    df.plot(title=title)


def project(x, angle=0.0, plot=True):
    """Project the one-dimensional array x along the given angle in degrees
    to create an image of size (x.size, x.size)."""

    if plot:
        _, (ax_raw, ax_rot) = plt.subplots(1, 2)

    x = np.asarray(x)

    if len(x.shape) != 1:
        raise ValueError("Expecting 1-d input")

    # create an image with x projected along y direction
    x_img = np.tile(x, (x.size, 1))

    if plot:
        ax_raw.imshow(x_img)

    # use scipy.ndimage to rotate the projection
    rotated = ndimage.rotate(
        x_img,
        angle,
        # If reshape is true, the output shape is adapted so that
        # the input array is contained completely in the output.
        reshape=False,
        # extrapolate x to take the edge values
        mode="nearest",
        # do not blur the output
        prefilter=False,
        order=1,
    )

    if plot:
        ax_rot.imshow(rotated)

    return rotated


def gaussian_wave_conv(
    data: np.ndarray,
    sample_rate: float,
    frequency: float,
    relative_width: float,
    n_points: int = None,
    axis=-1,
):
    """Convolute with morlet wavelet along given axis and
    return absolute value
    relative_width: width of filter window relative to period"""
    s = sample_rate * relative_width / frequency
    w = 2 * np.pi * relative_width
    if n_points is None:
        n_points = 2 * (int(s * 4) // 2) + 1

    # x = np.arange(0, M) - (M - 1.0) / 2
    # x = x / s
    # wavelet = np.exp(1j * w * x) * np.exp(-0.5 * x**2) * np.pi**(-0.25)
    # output = np.sqrt(1/s) * wavelet
    wvlt = signal.morlet2(n_points, w=w, s=s)

    morlet = ndimage.convolve1d(data, wvlt, output=complex, mode="nearest", axis=axis)

    return np.abs(morlet)
    # x = np.arange(0, M) - (M - 1.0) / 2
    # x = x / s
    # wavelet = np.exp(1j * w * x) * np.exp(-0.5 * x**2) * np.pi**(-0.25)
    # output = np.sqrt(1/s) * wavelet
    wvlt = signal.morlet2(n_points, w=w, s=s)

    morlet = ndimage.convolve1d(data, wvlt, output=complex, mode="nearest", axis=axis)

    return np.abs(morlet)
