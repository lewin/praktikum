# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 15:39:55 2021

@author: 394493: Alexander Kohlgraf
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import nice_plots
from numpy import fft

import helper
import file_reader

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class CT:
    def __init__(
        self,
        degree_step: int,
        MHz: int,
        ram_lak=True,
        shep_logan=False,
        velocity=False,
        attenuation=False,
        cutoff=0.1,
    ):
        self.degree_step = degree_step
        self.degrees = np.arange(0, 360, degree_step, dtype=int)
        self.positions = np.arange(
            -45,
            45.5,
            0.5,
            dtype=float,
        )  # pos in mm
        self.MHz = MHz
        self.MHz_name = str(MHz) + "MHz"
        self.shep_logan_factor = cutoff
        path = (
            "data/ct_scan/"
            + self.MHz_name
            + "/{}.bin".format("duration" if MHz == 4 else "both")
        )

        self.df = file_reader.read_org_file(path).T

        self.reduce_time_axis(velocity)
        print("Reduction done")
        self.filter_ct(ram_lak, shep_logan, shep_logan_factor=cutoff)
        print("Filtering done")
        self.image(velocity)
        print("Image done")

    def reduce_time_axis(self, velocity):
        if velocity:

            self.reduced = self.df.idxmax()
            # self.reduced = 1 / (self.df.T.iloc[0::1, 0::1] / 100 + 1)
            # self.reduced = self.reduced.T
        else:
            amplitudes = (
                self.df.abs().max()
            )  # Amplitudes of all positions over the time
            self.reduced = np.log(amplitudes.max() / amplitudes)  # log(A_0/A)

    def filter_ct(self, Ram_Lak=True, Shep_Logan=False, shep_logan_factor=0.1):
        self.ram_lak = Ram_Lak
        self.shep_logan = Shep_Logan
        if Ram_Lak and not Shep_Logan:

            def filter_function(frequencies, fourier):
                return np.abs(frequencies) * fourier

        elif Shep_Logan and not Ram_Lak:

            def filter_function(frequencies, fourier):
                factor = np.maximum(np.pi * frequencies / (2 * shep_logan_factor), 1e-9)
                return fourier * np.sin(factor) / factor

        elif Shep_Logan and Ram_Lak:

            def filter_function(frequencies, fourier):
                factor = np.maximum(np.pi * frequencies / (2 * shep_logan_factor), 1e-9)
                return fourier * np.abs(frequencies) * np.sin(factor) / factor

        else:

            def filter_function(frequencies, fourier):
                return fourier

        self.filtered = self.reduced.copy()
        for angle in self.degrees:
            projections = self.reduced[angle].to_numpy()
            fourier = fft.rfft(projections)
            frequencies = fft.rfftfreq(projections.size)
            filtered_frequencies = filter_function(frequencies, fourier)
            filtered_projection = fft.irfft(filtered_frequencies)

            normalize = False
            if normalize:
                filtered_projection -= filtered_projection.min()

            self.filtered[angle].iloc[
                0 : filtered_projection.size
            ] = filtered_projection
            (
                projections,
                fourier,
                frequencies,
                filtered_frequencies,
                filtered_projection,
            ) = (0, 0, 0, 0, 0)

    def image(self, velocity=False):
        dim = self.filtered.loc[self.degrees[0]].size
        img = np.zeros((dim, dim))

        norm = 1 / (2 * len(self.degrees))

        if velocity:
            s = 100
            norm = norm / s

        for angle in self.degrees:
            img += (
                helper.project(self.filtered.loc[angle], plot=False, angle=angle) * norm
            )
        img = img.flatten()
        if velocity:
            img = 1 / img
        # img = np.log(abs(img))
        # img[abs(img) > 10] = 0
        img = img.reshape((dim, dim))
        # print(img)
        print("angle projection done")

        # img = imgmap(img)
        fig = plt.figure(figsize=(8, 8), tight_layout=True)
        sub = fig.add_subplot(111)
        image = sub.imshow(img, origin="lower", extent=[-45, 45, -45, 45])
        filters = "{}{} ".format(
            "Ram-Lak" if self.ram_lak else "",
            ", Shep-Logan C={},".format(self.shep_logan_factor)
            if self.shep_logan
            else "",
        )
        plt.title(
            "{} ".format("Speed of Sound" if velocity else "Absorption")
            + filters
            + self.MHz_name
        )
        # fig.colorbar(image, ax=sub, shrink=0.7)
        savename = "{}_{}_{}_{}".format(
            self.MHz_name,
            "speed" if velocity else "absorption",
            "ramlak" if self.ram_lak else "",
            "sheplogan" if self.shep_logan else "",
        )
        plt.savefig("protokoll/images/ct_scan/" + savename + ".pdf")

    def sinogramm(self):
        plt.figure(figsize=(4, 12))
        dim = len(self.reduced[0])
        img = np.zeros((360, dim))
        for i in range(0, 360):
            img[i] += self.reduced[i].to_numpy()
        a = plt.imshow(img, extent=[-45, 45, 0, 360])

        plt.xlabel("Position")
        plt.ylabel("Angle")
        plt.title(self.MHz_name)
        plt.savefig("protokoll/images/ct_scan/sinogramm_{}.pdf".format(self.MHz_name))


# a = CT(1, 1)
def task():
    CT(1, 1, False, False, False)
    CT(1, 1, True, False, False)
    CT(1, 1, True, True, False)

    CT(1, 2, False, False, False)
    CT(1, 2, True, False, False)
    CT(1, 2, True, True, False)

    CT(1, 4, False, False, False)
    CT(1, 4, True, False, False)
    CT(1, 4, True, True, False)
