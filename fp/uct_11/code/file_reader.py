# -*- coding: utf-8 -*-
import io
import configparser
import pathlib

import pandas as pd
import numpy as np

POS_LABEL = "pos[mm]"
ANGLE_LABEL = "angle[°]"
TIME_LABEL = "time[µs]"

# the software changed how it formats ascii data between versions
# please adjust the decimal argument accordingly
DEFAULT_ASCII_KWARGS = dict(delimiter="\t", encoding="latin-1")

BIN_FILE_TYPE = {"bin", "binary", "binear"}
DAT_FILE_TYPE = {"ascii", "dat"}


def _read_ascii(fname: pathlib.Path, **kwargs):
    """All ascii files are read via this function. It replaces ',' with '.'
    before parsing because EchoView tends to change its ascii formatting."""
    kwargs = {**DEFAULT_ASCII_KWARGS, **kwargs}
    with open(fname, "r", encoding=kwargs.pop("encoding")) as f:
        return pd.read_csv(io.StringIO(f.read().replace(",", ".")), **kwargs)


def _read_org_bin_file(fname: pathlib.Path):
    """Read and parse bin file to a pandas DataFrame where the columns
    are position angle and all measurement times"""
    as_float = np.fromfile(str(fname), dtype=np.float32)

    n_points_per_scan = int(as_float[0])
    line_len = n_points_per_scan + 2
    if as_float.size % line_len:
        raise TypeError("File is not a valid binary file")

    as_float = as_float.reshape(-1, line_len)
    columns = as_float[0].tolist()
    columns[0] = POS_LABEL
    columns[1] = ANGLE_LABEL

    return pd.DataFrame(as_float[1:], columns=columns)


def _read_org_ascii_file(fname: pathlib.Path, **kwargs):
    """Read and parse dat file to a pandas DataFrame where the columns
    are position angle and all measurement times"""
    data = _read_ascii(fname, **kwargs)
    data.drop(columns=data.columns[-1:], inplace=True)
    return data


def read_processed_file(fname: pathlib.Path, **kwargs):
    """either amp or vel file"""
    data = _read_ascii(fname, **kwargs)

    angles = data.columns[0]
    pos = data.columns[1:]

    index = data[angles]

    columns = pos.astype(float)
    index.name = ANGLE_LABEL
    columns.name = POS_LABEL

    return pd.DataFrame(data[pos].values, index=data[angles], columns=columns)


def read_org_file(fname: str, file_type=None, **kwargs):
    """Read orf data (dat/ascii or bin/binary) file and return content as
    pandas.DataFrame with
    columns = time
    index = [angle, position]"""

    fname = pathlib.Path(fname)

    if file_type is None:
        file_type = fname.suffix.lstrip(".")

    if file_type.lower() in BIN_FILE_TYPE:
        data = _read_org_bin_file(fname)
    elif file_type.lower() in DAT_FILE_TYPE:
        data = _read_org_ascii_file(fname, **kwargs)
    else:
        raise ValueError(
            "Unkown file type. " "Not in {}.".format(DAT_FILE_TYPE | BIN_FILE_TYPE)
        )

    data.set_index([ANGLE_LABEL, POS_LABEL], inplace=True)
    data.sort_index(axis=0, inplace=True)

    data.columns = data.columns.astype(float)
    data.columns.name = TIME_LABEL

    return data


def read_amplitude_scan(filename) -> pd.DataFrame:
    """Read the data from an A-Scan *.dat file save with version 4.2.1.19318.

    Returns:
        pandas.DataFrame with the measured data
        dict with metadata
    """
    with open(filename, "r", encoding="latin-1") as f:
        raw = f.read()

    # dynamically find the beginning of data section
    us_header = "Depth [µs]	HF	Amplitude"
    mm_header = "Depth [mm]	HF	Amplitude"
    if us_header in raw:
        columns = us_header.split("\t")
        header, data = raw.split(us_header)
    elif mm_header in raw:
        columns = mm_header.split("\t")
        header, data = raw.split(mm_header)
    else:
        ValueError("Function is too stupid to parse this file")

    # extract metadata from header
    cp = configparser.ConfigParser()
    cp.read_string(header)
    meta = {k: dict(v) for k, v in cp.items() if dict(v)}

    data = data.strip().replace(",", ".")

    df = pd.read_csv(
        io.StringIO(data), sep="\t", names=columns, index_col=0, usecols=[0, 1, 2]
    )
    return df, meta
