# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 15:39:39 2021

@author: 394493: Alexander Kohlgraf
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import nice_plots
import latextable

import helper
import file_reader

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


def lin_fit(param, x):
    return param[0] * x + param[1]


def exp_fit(param, x):
    return param[0] * np.exp(-param[1] * x) + param[2]


class a_scan:
    def __init__(self, MHz):
        self.length_kurz = 40
        self.length_mittel = 79.7
        self.length_lang = 120.1
        self.length_error = 0.05

        self.MHz = MHz
        self.MHz_name = str(MHz) + " MHz"

        self.df_kurz, self.meta_kurz = file_reader.read_amplitude_scan(
            "data/a_scan/" + self.MHz_name + "/kurz.dat"
        )
        self.df_kurz_max = self.df_kurz["Amplitude"].max()
        self.df_kurz_max_time = self.df_kurz["Amplitude"].idxmax()

        self.df_mittel, self.meta_mittel = file_reader.read_amplitude_scan(
            "data/a_scan/" + self.MHz_name + "/mittel.dat"
        )
        self.df_mittel_max = self.df_mittel["Amplitude"].max()
        self.df_mittel_max_time = self.df_mittel["Amplitude"].idxmax()

        self.df_lang, self.meta_lang = file_reader.read_amplitude_scan(
            "data/a_scan/" + self.MHz_name + "/lang.dat"
        )
        self.df_lang_max = self.df_lang["Amplitude"].max()
        self.df_lang_max_time = self.df_lang["Amplitude"].idxmax()

        self.df_list = [self.df_kurz, self.df_mittel, self.df_lang]
        self.meta_list = [self.meta_kurz, self.meta_mittel, self.meta_lang]
        self.names_list = ["kurz", "mittel", "lang"]
        self.max_list = [self.df_kurz_max, self.df_mittel_max, self.df_lang_max]
        self.max_time_list = [
            self.df_kurz_max_time,
            self.df_mittel_max_time,
            self.df_lang_max_time,
        ]
        self.length_list = [self.length_kurz, self.length_mittel, self.length_lang]

        print(self.MHz_name)
        self.plot_raw()
        self.speed_of_sound()
        self.dampening()

    def plot_raw(self):
        plt.figure(figsize=(15, 8))
        for (df, meta, name, color, max_value, max_time) in zip(
            self.df_list,
            self.meta_list,
            self.names_list,
            ["b", "r", "g"],
            self.max_list,
            self.max_time_list,
        ):
            plt.plot(df.index, df["Amplitude"], label=name, color=color)
            plt.scatter(max_time, max_value, color="black", marker="x", s=100)
        plt.ylabel("Amplitude")
        plt.xlabel("Time [µs]")
        plt.title("Amplituden der Transmitierten Signale bei " + self.MHz_name)
        plt.grid(True)
        plt.legend()
        plt.savefig("protokoll/images/a_scan/" + str(self.MHz) + "_MHz_amplitudes.pdf")

        plt.figure(figsize=(15, 8))
        for (df, meta, name, color) in zip(
            self.df_list, self.meta_list, self.names_list, ["b", "r", "g"]
        ):
            plt.plot(df.index, df["HF"], label=name, color=color)
        plt.ylabel("HF")
        plt.xlabel("Time [µs]")
        plt.title("Amplituden der Transmitierten Signale bei " + self.MHz_name)
        plt.grid(True)
        plt.legend()
        plt.savefig("protokoll/images/a_scan/" + str(self.MHz) + "_MHz_HF.pdf")

    def speed_of_sound(self):
        x = self.max_time_list
        y = self.length_list
        self.reg = reg = nice_plots.nice_regression_plot_odr(
            lin_fit,
            x,
            y,
            np.zeros(len(x)) + 2,  # Fehler von 2 us nach augenmaß bestimmt
            np.ones(len(x)) * self.length_error,
            beta0=[1, 1],
            ylabel="Length [mm]",
            xlabel="Time [µs]",
            figsize=(15, 8),
            title="Lineare Regression der Schallgeschwindigkeit in Acrylglas bei "
            + self.MHz_name,
            save="protokoll/images/a_scan/"
            + str(self.MHz)
            + "_MHz_speed_of_sound_fit.pdf",
        )
        print(reg)
        latextable.simple_draw(
            save="/../protokoll/tables/"
            + "schallgeschwindigkeit_"
            + str(self.MHz)
            + "_fit.tex",
            content=[
                ["a", reg[0][0], np.sqrt(reg[1][0][0])],
                ["b", reg[0][1], np.sqrt(reg[1][1][1])],
            ],
            header=["Fit Parameter", "Wert", "Fehler"],
            caption="Fit Parameter für die Schallgeschwindigkeit " + self.MHz_name,
            label="schallgeschwindigkeit_fit_" + str(self.MHz),
            precision=3,
            small=False,
        )
        # Todo: Tabelle

    def dampening(self):
        y = self.max_list
        x = self.length_list
        reg = nice_plots.nice_regression_plot_odr(
            exp_fit,
            x,
            y,
            np.ones(len(x)) * self.length_error,
            np.zeros(len(x))
            + 0.05,  # Fehler von 0.05 Amp nach augenmaß bestimmt im Vergleich mit HF
            beta0=[0.5, 0.03, 0],
            ylabel="Amplitude",
            xlabel="Length [mm]",
            figsize=(15, 8),
            title="Lineare Regression der Dämpfung in Acrylglas bei " + self.MHz_name,
            save="protokoll/images/a_scan/" + str(self.MHz) + "_MHz_dampening_fit.pdf",
        )
        print(reg)
        latextable.simple_draw(
            save="/../protokoll/tables/" + "absorption_" + str(self.MHz) + "_fit.tex",
            content=[
                ["a", reg[0][0], np.sqrt(reg[1][0][0])],
                ["b", reg[0][1], np.sqrt(reg[1][1][1])],
                ["c", reg[0][2], np.sqrt(reg[1][2][2])],
            ],
            header=["Fit Parameter", "Wert", "Fehler"],
            caption="Fit Parameter für die Absorption " + self.MHz_name,
            label="absorption_fit_" + str(self.MHz),
            precision=3,
            small=False,
        )


# a1 = a_scan(1)
# a2 = a_scan(2)
# a4 = a_scan(4)
