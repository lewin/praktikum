# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 16:34:09 2020

@author: 394493: Alexander Kohlgraf
"""

import os
import codecs
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import nice_plots
import texttable
import latextable
from praktikum import analyse, literaturwerte

dir_path = os.path.realpath(__file__)

sigma_s = 10**(-5) / np.sqrt(12)
lambda_rot = 632.8 * 10**(-9)
lambda_rot_sys = 0.1 * 10**(-9)

class tableDrawer:
    def drawKappaRaw(self, s,m,name):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["maxima $m$","Auslenkung $s$ in mm"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        
        table.add_rows(np.column_stack([m,s]) ,False)
        

        text += latextable.draw_latex(table, caption="Rohdaten für " + name + ". Messung $\kappa$", label = "tab:" + "kappa_raw_" + name) +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "kappa_raw_" + name + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawKappaReg(self, kappa1, kappa2, kappa3):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(3))
        header = ["Messreihe","Steigung $a$ in m","y-Achsenabschnitt $b$ in m"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(6)
        table.add_row(["1. Messung", "$({:.3f} \pm {:.3f})e-06$".format(kappa1[0]/10**(-6),kappa1[1]/10**(-6)), "$({:.3f} \pm {:.3f})e-03$".format(kappa1[2]/10**(-3),kappa1[3]/10**(-3))])
        table.add_row(["2. Messung", "$({:.3f} \pm {:.3f})e-06$".format(kappa2[0]/10**(-6),kappa2[1]/10**(-6)), "$({:.3f} \pm {:.3f})e-03$".format(kappa2[2]/10**(-3),kappa2[3]/10**(-3))])
        table.add_row(["defekte Messung", "$({:.3f} \pm {:.3f})e-06$".format(kappa3[0]/10**(-6),kappa3[1]/10**(-6)), "$({:.3f} \pm {:.3f})e-03$".format(kappa3[2]/10**(-3),kappa3[3]/10**(-3))])

        text += latextable.draw_latex(table, caption='Regression für $\kappa$', label = "tab:" + "kappa_reg") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "kappa_reg.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawKappa(self, kappa1, kappa2, kappa3):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(4))
        header = ["Messreihe","$\kappa$","$\sigma_{\kappa}$","$\sigma_{\kappa,sys}$"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(7)
        kappa1.insert(0,"1. Messung")
        table.add_row(kappa1)
        kappa2.insert(0,"2. Messung")
        table.add_row(kappa2)
        kappa3.insert(0,"defekte Messung")
        table.add_row(kappa3)
        
        text += latextable.draw_latex(table, caption='Ergebnisse $\kappa$', label = "tab:" + "kappa") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "kappa.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawLambdaRaw(self, s,m,name):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["maxima $m$","Auslenkung $s$ in mm"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        
        table.add_rows(np.column_stack([m,s]) ,False)
        

        text += latextable.draw_latex(table, caption="Rohdaten für " + name + ". Messung $\lambda$", label = "tab:" + "lambda_raw_" + name) +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "lambda_raw_" + name + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawLambdaReg(self, lamb1, lamb2, lamb3):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(3))
        header = ["Messreihe","Steigung $a$ in m","y-Achsenabschnitt $b$ in m"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(6)
        table.add_row(["1. Messung", "$({:.3f} \pm {:.3f})e-06$".format(lamb1[0]/10**(-6),lamb1[1]/10**(-6)), "$({:.3f} \pm {:.3f})e-03$".format(lamb1[2]/10**(-3),lamb1[3]/10**(-3))])
        table.add_row(["2. Messung", "$({:.3f} \pm {:.3f})e-06$".format(lamb2[0]/10**(-6),lamb2[1]/10**(-6)), "$({:.3f} \pm {:.3f})e-03$".format(lamb2[2]/10**(-3),lamb2[3]/10**(-3))])
        table.add_row(["3. Messung", "$({:.3f} \pm {:.3f})e-06$".format(lamb3[0]/10**(-6),lamb3[1]/10**(-6)), "$({:.3f} \pm {:.3f})e-03$".format(lamb3[2]/10**(-3),lamb3[3]/10**(-3))])

        text += latextable.draw_latex(table, caption='Regression für $\lambda$', label = "tab:" + "lambda_reg") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "lambda_reg.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawLambda(self, lamb1, lamb2, lamb3):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(4))
        header = ["Messreihe","$\lambda$ in nm","$\sigma_{\lambda}$ in nm","$\sigma_{\lambda,sys}$ in nm"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(7)
        lamb1 = np.array(lamb1) * 10**9
        lamb2 = np.array(lamb2) * 10**9
        lamb3 = np.array(lamb3) * 10**9
        table.add_row(["1. Messung", "${:.2f}$".format(lamb1[0]), "${:.2f}$".format(lamb1[1]), "${:.2f}$".format(lamb1[2])])
        table.add_row(["2. Messung", "${:.2f}$".format(lamb2[0]), "${:.2f}$".format(lamb2[1]), "${:.2f}$".format(lamb2[2])])
        table.add_row(["3. Messung", "${:.2f}$".format(lamb3[0]), "${:.2f}$".format(lamb3[1]), "${:.2f}$".format(lamb3[2])])
        
        text += latextable.draw_latex(table, caption='Ergebnisse $\lambda$', label = "tab:" + "lambda") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "lambda.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
   
    def drawDruckRaw(self, alle):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(8))
        header = ["Maxima"]
        for i in range(7):
            header.append("$P_{}".format(i+1) + "$ in $\SI{}{\hecto\pascal}$")
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(0)
        table.add_rows(alle, False)
        
        text += latextable.draw_latex_small(table, caption='Rohdaten P', label = "tab:" + "druckRaw") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "druckRaw.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

    def drawDruckMittel(self, alle):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(3))
        header = ["Maxima", "$\overline{P}$ in Pa", "$\sigma_{\overline{P}}$ in Pa"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(0)
        table.add_rows(alle, False)
        
        text += latextable.draw_latex_small(table, caption='Mittelwerte P', label = "tab:" + "druckAverage") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "druckAverage.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

    def drawDruckReg(self, druck, name):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["Steigung $a$ in Pa","y-Achsenabschnitt $b$ in Pa"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(6)
        table.add_row(["$({:.3f} \pm {:.3f})e+04$".format(druck[0]/10**(4),druck[1]/10**(4)), "${:.3f} \pm {:.3f}$".format(druck[2]/10**(0),druck[3]/10**(0))])
        
        text += latextable.draw_latex(table, caption='Regression ' + name +' für $P$', label = "tab:" + "druck_reg_" + name) +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "druck_reg_"+name+".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

    def drawDruck(self, druck):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(3))
        header = ["$\\frac{\Delta n}{\Delta P}$ in 1/Pa","$\sigma_{\\frac{\Delta n}{\Delta P}}$ in 1/Pa","$\sigma_{\\frac{\Delta n}{\Delta P},sys}$ in 1/Pa"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(7)
        druck = np.array(druck)/10**(-9)
        table.add_row(["${:.4f}e-09$".format(druck[0]),"${:.4f}e-09$".format(druck[1]),"${:.4f}e-09$".format(druck[2])])
        
        text += latextable.draw_latex(table, caption='Ergebnisse $\\frac{\Delta n}{\Delta P}$', label = "tab:" + "delNDelP") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "delNDelP.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

def kappa(auslenkung, maxima, name):
    x = maxima
    y = auslenkung
    yerr = np.ones([len(maxima)]) * sigma_s
    m,em,b,eb,chiq,corr = nice_plots.nice_regression_plot(x,y,yerr, xlabel='Maxima',
                         ylabel='Mikrometerschraube in m', ylabelresidue='Residuen', title='kappa {}. Messung'.format(name), save=dir_path + "/../../Grafiken/kappa_regression_" + str(name) +".pdf")
    kappa = lambda_rot / (2 * m)
    kappa_err = (lambda_rot / 2) * (1/m**2) * em
    kappa_err_sys = lambda_rot_sys / (2 * m)
    return kappa, kappa_err, kappa_err_sys, (m,em,b,eb)

def gruenLambda(auslenkung, maxima, kappa, kappa_err, kappa_err_sys, name):
    x = maxima
    y = auslenkung
    yerr = np.ones([len(auslenkung)]) * sigma_s
    m,em,b,eb,chiq,corr = nice_plots.nice_regression_plot(x,y,yerr, xlabel='Maxima',
                         ylabel='Mikrometerschraube in m', ylabelresidue='Residuen', title='Lambda grün {}. Messung mit {}. kappa'.format(name[0], name[1]), save=dir_path + "/../../Grafiken/lambda_regression_" + str(name[0]) + "_kappa_" + str(name[1]) + ".pdf")
    lamb = 2 * kappa * m
    lamb_err = (2 * kappa * em)
    lamb_err_sys = 2 * m * np.sqrt(kappa_err**2 + kappa_err_sys**2)
    return lamb, lamb_err, lamb_err_sys, (m,em,b,eb)

def druck(P_Mittel, P_Mittel_err, maxima, lamb, lamb_err, lamb_err_sys, L, name, name2=""):
    x = maxima
    y = P_Mittel
    yerr = P_Mittel_err
    m,em,b,eb,chiq,corr = nice_plots.nice_regression_plot(x,y,yerr, xlabel='Maxima',
                         ylabel='Druck in Pa', ylabelresidue='Residuen', title='Druck mit {}. kappa'.format(name) + " " + name2, save=dir_path + "/../../Grafiken/druck_regression_lambda_" + str(name) + name2+ ".pdf")
    delNdelP = (lamb/(2*L*m))
    delNdelP_err = (lamb/(2*L))/(m**2) * em
    delNdelP_err_sys = (np.sqrt(lamb_err**2 + lamb_err_sys**2)/(2*L))/(m)
    
    return delNdelP, delNdelP_err, delNdelP_err_sys, (m,em,b,eb)

k_Messung_alt = np.array([50,56,64,71,78,86,94,101,107,114,121,129,138,146]) / 100 + 7
k_Messung_alt_m = np.arange(0,131,10)

k_Messung_1_komisch = np.array([50,56,62,68,74,80,86,92,98,104,109,116,122,128,134,140,146,152]) / 100 + 7
k_Messung_1 = np.array([50,56,62,68,74,80,86,92,98,104,110,116,122,128,134,140,146,152]) / 100 + 7
k_Messung_1_m = np.arange(0,171,10)
k_Messung_2 = np.array([50,56,62,69,75,81,87,93,99,105,111,117,123,129,136,142,148,154]) / 100 + 7
k_Messung_2_m_verzählt = np.arange(0,171,10)
k_Messung_2_m = np.arange(0,171,10)
for x in range(len(k_Messung_2_m)):
    if x>2:
        k_Messung_2_m[x] += 1
    if x>13:
       k_Messung_2_m[x] += 1 

lambda_Messung_1 = np.array([50,55,60,65,70,76,80,86,91,96,101,106,111,116,122,127,132,137,142,147]) / 100 + 7
lambda_Messung_1_m = np.arange(0,191,10)
lambda_Messung_1_m_verzählt = np.arange(0,191,10)
#for x in range(len(lambda_Messung_1_m)):
  #  if x>10:
  #      lambda_Messung_1_m[x] += 1
 #   if x == 5:
  #      lambda_Messung_1_m[x] += 1
  #  if x == 9:
  #      lambda_Messung_1_m[x] += 1
 #   if x>9 and x<14:
 #       lambda_Messung_1_m[x] -= 2  
 #   if x>5 and x<10:
  #      lambda_Messung_1_m[x] += 1 
#2. Messung verwerfen
lambda_Messung_2 = np.array([50,55,60,65,70,75,80,85,91,96,101,107,113,118,124,129,134,140,146,151]) / 100 + 7
lambda_Messung_2_m = np.arange(0,191,10)

lambda_Messung_3 = np.array([50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,130,136,141,146]) / 100 + 7
lambda_Messung_3_m = np.arange(0,191,10)


P_Messung_1 = np.array([998,915,800,700,602,512,412,310,202]) * 100
P_Messung_1_m = np.arange(0,9,1) * -1
P_Messung_2 = np.array([997.000001,896,790,688,596,481,389,286,182]) * 100
P_Messung_2_m = np.arange(0,9,1) * -1
P_Messung_3 = np.array([997,895,803,693,590,484,382,288,188]) * 100
P_Messung_3_m = np.arange(0,9,1) * -1
P_Messung_4 = np.array([997,883,781,680,582,472,373,272,178]) * 100
P_Messung_4_m = np.arange(0,9,1) * -1
P_Messung_5 = np.array([997,899,783,696,592,498,389,273,183]) * 100
P_Messung_5_m = np.arange(0,9,1) * -1
P_Messung_6 = np.array([997,883,785,680,572,475,383,279,173]) * 100
P_Messung_6_m = np.arange(0,9,1) * -1
P_Messung_7 = np.array([998,896,800,696,603,497,392,295,187]) * 100
P_Messung_7_m = np.arange(0,9,1) * -1
#Messung 1,7 verworfen
P_Messungen = np.column_stack((P_Messung_2,P_Messung_3,P_Messung_4,P_Messung_5,P_Messung_6))
print(P_Messungen)
P_Mittel = np.array([])
P_Mittel_err = np.array([])
for x in P_Messungen:
    tmp = analyse.mittelwert_stdabw(x)
    P_Mittel = np.append(P_Mittel, tmp[0])
    P_Mittel_err = np.append(P_Mittel_err, tmp[1] / np.sqrt(5))

print(P_Mittel)
print(P_Mittel_err)



#Berechnungen
kappa_alt, kappa_alt_err, kappa_alt_err_sys, kappa_reg_alt = kappa(k_Messung_alt * 10**(-3), k_Messung_alt_m, "defekt")

kappa_1, kappa_1_err, kappa_1_err_sys, kappa_reg_1 = kappa(k_Messung_1 * 10**(-3), k_Messung_1_m, 1)
kappa_2, kappa_2_err, kappa_2_err_sys, kappa_reg_2 = kappa(k_Messung_2 * 10**(-3), k_Messung_2_m, 2)

kappa_1_verzählt = kappa(k_Messung_1_komisch * 10**(-3), k_Messung_1_m, "verlesen")

#kappa_mittel, kappa_mittel_err = analyse.gewichtetes_mittel(np.array([kappa_1, kappa_2]), np.array([kappa_1_err+kappa_1_err_sys,kappa_2_err+kappa_2_err_sys]))
#print(kappa_mittel, kappa_mittel_err)

#lambda_1_kappa_mittel = np.array(gruenLambda(lambda_Messung_1* 10**(-3), lambda_Messung_1_m, kappa_mittel, kappa_mittel_err, 0, (1,"mittel")))
#lambda_2_kappa_mittel = np.array(gruenLambda(lambda_Messung_2* 10**(-3), lambda_Messung_2_m, kappa_mittel, kappa_mittel_err, 0, (2,"mittel")))
#lambda_3_kappa_mittel = np.array(gruenLambda(lambda_Messung_3* 10**(-3), lambda_Messung_3_m, kappa_mittel, kappa_mittel_err, 0, (3,"mittel")))
#print(lambda_1_kappa_mittel)
#print(lambda_2_kappa_mittel)
#print(lambda_3_kappa_mittel)

lambda_1_kappa_1 = (gruenLambda(lambda_Messung_1* 10**(-3), lambda_Messung_1_m, kappa_1, kappa_1_err, kappa_1_err_sys, (1,1)))
lambda_1_kappa_2 = (gruenLambda(lambda_Messung_1* 10**(-3), lambda_Messung_1_m, kappa_2, kappa_2_err, kappa_2_err_sys, (1,2)))

lambda_2_kappa_1 = (gruenLambda(lambda_Messung_2* 10**(-3), lambda_Messung_2_m, kappa_1, kappa_1_err, kappa_1_err_sys, (2,1)))
lambda_2_kappa_2 = (gruenLambda(lambda_Messung_2* 10**(-3), lambda_Messung_2_m, kappa_2, kappa_2_err, kappa_2_err_sys, (2,2)))

lambda_3_kappa_1 = (gruenLambda(lambda_Messung_3* 10**(-3), lambda_Messung_3_m, kappa_1, kappa_1_err, kappa_1_err_sys, (3,1)))
lambda_3_kappa_2 = (gruenLambda(lambda_Messung_3* 10**(-3), lambda_Messung_3_m, kappa_2, kappa_2_err, kappa_2_err_sys, (3,2)))


druck_result = druck(P_Mittel, P_Mittel_err, np.arange(0,9,1) * -1, lambda_3_kappa_1[0], lambda_3_kappa_1[1], lambda_3_kappa_1[2], 0.01, 1)


#Darstellungen
drawer = tableDrawer()

#Grafik
plt.figure(figsize=(20,10))
plt.xlabel("Maxima")
plt.ylabel("Mikrometerschraube in mm")
plt.title("Rohdaten und Bereinigung, kappa")
plt.scatter( k_Messung_1_m,k_Messung_1_komisch, label="1. Messung Ablesefehler")
plt.scatter( k_Messung_1_m,k_Messung_1, label="1. Messung Bereinigt")
#plt.scatter( k_Messung_2_m,k_Messung_2, label="2. Messung Bereinigt")
plt.scatter( k_Messung_2_m_verzählt,k_Messung_2, label="2. Messung Verzählt")
plt.scatter (k_Messung_alt_m, k_Messung_alt, label = "Defekter Aufbau")
plt.grid(True)
plt.legend()
plt.savefig(dir_path + "/../../Grafiken/kappa_raw.pdf")
plt.show()

#Tabelle
drawer.drawKappa([kappa_1, kappa_1_err, kappa_1_err_sys],[kappa_2, kappa_2_err, kappa_2_err_sys],[kappa_alt, kappa_alt_err, kappa_alt_err_sys])
drawer.drawKappaReg(kappa_reg_1, kappa_reg_2, kappa_reg_alt)
drawer.drawKappaRaw(k_Messung_alt,k_Messung_alt_m,"defekte")
drawer.drawKappaRaw(k_Messung_1_komisch,k_Messung_1_m,"1")
drawer.drawKappaRaw(k_Messung_2,k_Messung_2_m_verzählt,"2")

#Ausgabe
print("kappas")
print(kappa_1, kappa_1_err, kappa_1_err_sys)
print(kappa_2, kappa_2_err, kappa_2_err_sys)
print(kappa_alt, kappa_alt_err, kappa_alt_err_sys)


#Grafik
plt.figure(figsize=(20,10))
plt.xlabel("Maxima")
plt.ylabel("Mikrometerschraube in mm")
plt.title("Rohdaten Lambda")
plt.scatter( lambda_Messung_1_m_verzählt,lambda_Messung_1, label="1. Lambda Messung")
plt.scatter( lambda_Messung_2_m,lambda_Messung_2, label="2. Lambda Messung")
plt.scatter( lambda_Messung_3_m,lambda_Messung_3, label="3. Lambda Messung")
plt.grid(True)
plt.legend()
plt.savefig(dir_path + "/../../Grafiken/lambda_raw.pdf")
plt.show()

#Tabelle
drawer.drawLambda([lambda_1_kappa_1[0], lambda_1_kappa_1[1], lambda_1_kappa_1[2]],[lambda_2_kappa_1[0], lambda_2_kappa_1[1], lambda_2_kappa_1[2]],[lambda_3_kappa_1[0], lambda_3_kappa_1[1], lambda_3_kappa_1[2]])
drawer.drawLambdaReg(lambda_1_kappa_1[3], lambda_2_kappa_1[3], lambda_3_kappa_1[3])
drawer.drawLambdaRaw(lambda_Messung_1,lambda_Messung_1_m_verzählt,"1")
drawer.drawLambdaRaw(lambda_Messung_2,lambda_Messung_2_m,"2")
drawer.drawLambdaRaw(lambda_Messung_3,lambda_Messung_3_m,"3")

#Ausgabe
print("lamb 1")
print(lambda_1_kappa_1)
print(lambda_1_kappa_2 )

print("lamb 2, verworfen")
print(lambda_2_kappa_1)
print(lambda_2_kappa_2)

print("lamb 3")
print(lambda_3_kappa_1)
print(lambda_3_kappa_2)



print("Druck")
print(druck_result)
print(druck_result[0] * P_Mittel + 1)
print(literaturwerte.brechungsindex_luft(lambda_3_kappa_1[0]*10**6, p = 0.01*P_Mittel))
plt.figure(figsize=(20,10))
plt.xlabel("Maxima")
plt.ylabel("Druck in Pascal")
plt.title("Rohdaten Druck")
plt.scatter(P_Messung_1_m, P_Messung_1, label = "1. Messung")
plt.scatter(P_Messung_2_m, P_Messung_2, label = "2. Messung")
plt.scatter(P_Messung_3_m, P_Messung_3, label = "3. Messung")
plt.scatter(P_Messung_4_m, P_Messung_4, label = "4. Messung")
plt.scatter(P_Messung_5_m, P_Messung_5, label = "5. Messung")
plt.scatter(P_Messung_6_m, P_Messung_6, label = "6. Messung")
plt.scatter(P_Messung_7_m, P_Messung_7, label = "7. Messung")
plt.grid(True)
plt.legend()
plt.savefig(dir_path + "/../../Grafiken/druck_raw.pdf")
plt.show()

plt.figure(figsize=(20,10))
plt.xlabel("Druck in Pascal")
plt.ylabel("Brechungsindex von Luft")
plt.title("Modell Brechungsindex")
x = np.arange(10000,100000)
plt.xlim(10000,100000)
plt.plot(x, (druck_result[0]*x + 1), label="Modell")
plt.plot(x, literaturwerte.brechungsindex_luft(lambda_3_kappa_1[0]*10**6, p = 0.01*x), label="Literatur")
plt.grid(True)
plt.legend()
plt.savefig(dir_path + "/../../Grafiken/druck_vergleich.pdf")
plt.show()

#Tabellen
drawer.drawDruckRaw(np.column_stack((np.array(P_Messung_1_m)*100,P_Messung_1,P_Messung_2,P_Messung_3,P_Messung_4,P_Messung_5,P_Messung_6,P_Messung_7))/100)
drawer.drawDruckMittel(np.column_stack((np.array(P_Messung_1_m),P_Mittel,P_Mittel_err)))
drawer.drawDruckReg(druck_result[3], "gemittelt")
drawer.drawDruck([druck_result[0], druck_result[1], druck_result[2]])

a = np.append(P_Messung_2, (P_Messung_3, P_Messung_4,P_Messung_5,P_Messung_6))
b = np.append(P_Messung_2_m, (P_Messung_2_m, P_Messung_2_m,P_Messung_2_m,P_Messung_2_m))
c = np.append(P_Mittel_err, (P_Mittel_err, P_Mittel_err, P_Mittel_err, P_Mittel_err))
print(a)
print(b)
print(c)
druckTest = druck(a, c,b, lambda_3_kappa_1[0], lambda_3_kappa_1[1], lambda_3_kappa_1[2], 0.01, "ungemittelt")
print(druckTest)
drawer.drawDruckReg(druckTest[3], "alle")