import os
import codecs
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import nice_plots
import texttable
import latextable
from praktikum import analyse, literaturwerte, cassy1

dir_path = os.path.realpath(__file__)

#Konstanten im Versuchsaufbau
C = 10.6 * 10**(-6)
L = 1.292 * 10**(-3)
R_L = 1.1

#Fehler
sigma_U = -1
sigma_I = -1
sigma_R_sys = 0.1/np.sqrt(12)
sigma_L_sys = 0.001 * 10**(-3)/np.sqrt(12)
sigma_R_L_sys = 0.1/np.sqrt(12)
sigma_C_sys = 0.1 * 10**(-6)/np.sqrt(12)
sigma_U_ablese = 0.01 
sigma_phi_ablese = 5
sigma_oszi = 1


#Rauschmessung für Fehler
def rauschMessung():
    reader = fileReader()
    n, t, U_C, U_L, U_0, I_0, phi, f = reader.readFile("394493_2E1_offset_korrektur", "Kallibrierung")
    
    sigma_U = analyse.mittelwert_stdabw(U_C)[1]/np.sqrt(len(U_C))
    
    sigma_I = analyse.mittelwert_stdabw(I_0)[1]/np.sqrt(len(I_0))
    
    plt.figure()
    plt.xlabel("Spannung in V")
    plt.ylabel("Anzahl")
    plt.title("Rauschmessung U")
    #plt.figure(figsize=(19,9))
    plt.hist(U_C)
    #plt.hist(I_0)
    plt.show()
    
    plt.figure()
    plt.xlabel("Strom in A")
    plt.ylabel("Anzahl")
    plt.title("Rauschmessung I")
    #plt.figure(figsize=(19,9))
    #plt.hist(U_C)
    plt.hist(I_0)
    plt.show()
    return sigma_U, sigma_I

class tableDrawer:
    def drawResultByCalc(self,Q,Q_err,measurement,errorType,whichCalc):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(3))
        header = ["Messung","Güte durch " + whichCalc, errorType + " Fehler auf Güte"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(3)
        
        for i in range(len(Q)):
            table.add_row([measurement[i],"$\\num{" + "{:.3f}".format(Q[i]) + "}$","$\\num{" + "{:.3f}".format(Q_err[i]) + "}$"])
        #table.add_rows(np.column_stack([measurement, Q, Q_err]), False)
        
        text += latextable.draw_latex(table, caption="Güte bestimmt über " + whichCalc + " für alle Widerstände", label = "tab:" + "GüteGroup" + whichCalc) +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Tabellen/" + "Güte" + whichCalc + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
   
class fileReader:
    def readFile(self, name, directory):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        data = np.array(cassy1.lese_lab_datei(dir_path + "/../" + directory + "/" + name + ".lab"))
        n = data[:,0]
        t = data[:,1]
        U_C = data[:,3]
        U_L = data[:,2]
        U_0 = data[:,5]
        I_0 = data[:,6]
        phi = data[:,7]
        f = data[:,8]
        
        return n, t, U_C, U_L, U_0, I_0, phi, f

class displayer:
    def scatter(self, xArrayArray, yArrayArray, xLabel="", yLabel="", title="", labelArray = None, savename = None):
        plt.figure(figsize=(19,9))
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.title(title)
        if labelArray:
            if len(xArrayArray) != len(yArrayArray) or len(xArrayArray) != len(labelArray):
                raise ValueError('A Not same size') 
            for (x, y, label) in zip(xArrayArray, yArrayArray, labelArray):
                plt.scatter(x,y,label=label)
                plt.legend()
        else:
            if len(xArrayArray) != len(yArrayArray):
                raise ValueError('A Not same size at') 
            for (x, y) in zip(xArrayArray, yArrayArray):
                plt.scatter(x,y)
        plt.grid(True)
        if savename:
            plt.savefig(dir_path + "/../../Grafiken/" + savename +".pdf")
        plt.show()
        
    def plot(self, xArrayArray, yArrayArray, xLabel="", yLabel="", title="", labelArray = None, savename = None):
        plt.figure(figsize=(19,9))
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.title(title)
        if labelArray:
            if len(xArrayArray) != len(yArrayArray) or len(xArrayArray) != len(labelArray):
                raise ValueError('A Not same size') 
            for (x, y, label) in zip(xArrayArray, yArrayArray, labelArray):
                plt.plot(x,y,label=label)
                plt.legend()
        else:
            if len(xArrayArray) != len(yArrayArray):
                raise ValueError('A Not same size at') 
            for (x, y) in zip(xArrayArray, yArrayArray):
                plt.plot(x,y)
                print(x)
                print(y)
        plt.grid(True)
        if savename:
            plt.savefig(dir_path + "/../../Grafiken/" + savename +".pdf")
        plt.show()

class measurement:
    def __init__(self, name, R, resistors, resonanzAbgelesen, PhasenbreiteAbgelesen, onlyOne = False):
        self.R = R
        self.R_total = R + R_L
        self.R_total_err = np.sqrt((resistors * sigma_R_sys)**2 + (sigma_R_L_sys)**2)
        self.resFreqRead = resonanzAbgelesen
        self.deltaFreqPhi = PhasenbreiteAbgelesen
        self.resistors = resistors
        reader = fileReader()
        if not onlyOne:
            self.Fein_n, self.Fein_t, self.Fein_U_C, self.Fein_U_L, self.Fein_U_0, self.Fein_I_0, self.Fein_phi, self.Fein_f = reader.readFile(name + "Fein", "Messungen")
            self.Grob_n, self.Grob_t, self.Grob_U_C, self.Grob_U_L, self.Grob_U_0, self.Grob_I_0, self.Grob_phi, self.Grob_f = reader.readFile(name + "Grob", "Messungen")
        if onlyOne:
            self.Fein_n, self.Fein_t, self.Fein_U_C, self.Fein_U_L, self.Fein_U_0, self.Fein_I_0, self.Fein_phi, self.Fein_f = reader.readFile(name + "Fein", "Messungen")
            self.Grob_n, self.Grob_t, self.Grob_U_C, self.Grob_U_L, self.Grob_U_0, self.Grob_I_0, self.Grob_phi, self.Grob_f = reader.readFile(name + "Fein", "Messungen")
        self.name = name.replace("394493_2E1_", "")
        

        #Functions
        #self.scatterSelf()
        #self.readFrequency()
        self.printGüte()
        self.errorPlot()
        

    def scatterSelf(self):
        display = displayer()
        display.scatter([self.Grob_f,self.Grob_f,self.Grob_f], [self.Grob_U_0, self.Grob_U_L, self.Grob_U_C],"Frequenz in Hz", "Spannung in V", "Grobe Messung Spannungsffektivwerte: " + self.name, ["Erzeugerspannung Grob",  "Spulenspannung Grob",  "Kondensatorspannung Grob"], "Spannung_Grob_" + self.name)
        display.scatter([self.Fein_f,self.Fein_f,self.Fein_f], [self.Fein_U_0,self.Fein_U_L,self.Fein_U_C], "Frequenz in Hz", "Spannung in V", "Feine Messung Spannungsffektivwerte: " + self.name, ["Erzeugerspannung Fein","Spulenspannung Fein", "Kondensatorspannung Fein"], "Spannung_Fein_" + self.name)
        display.scatter([self.Grob_f],[self.Grob_I_0], "Frequenz in Hz", "Strom in A", "Grobe Messung Stromeffektivwerte: " + self.name, ["Strom Grob"], "Strom_Grob_" + self.name)
        display.scatter([self.Fein_f],[self.Fein_I_0], "Frequenz in Hz", "Strom in A", "Feine Messung Stromeffektivwerte: " + self.name, ["Strom Fein"], "Strom_Fein_" + self.name)
        display.scatter([self.Grob_f],[self.Grob_phi], "Frequenz in Hz", "Winkel in Grad", "Grobe Messung Phasenverschiebung: " + self.name, ["Phasenverschiebung Grob"], "Phasenverschiebung_Grob_" + self.name)
        display.scatter([self.Fein_f],[self.Fein_phi], "Frequenz in Hz", "Winkel in Grad", "Feine Messung Phasenverschiebung: " + self.name, ["Phasenverschiebung Fein"], "Phasenverschiebung_Fein_" + self.name)

    def readFrequency(self):
        display = displayer()
        bool_arr_down = self.Fein_f >1379
        bool_arr_up = self.Fein_f<1421
        bool_arr = np.all([bool_arr_down, bool_arr_up], axis=0)
        display.plot([self.Fein_f[bool_arr],self.Fein_f[bool_arr]], [self.Fein_U_L[bool_arr],self.Fein_U_C[bool_arr]], "Frequenz in Hz", "Spannung in V", "Vergrößerung von Feine Messung Spannungsffektivwerte um Resonanzfrequenz: " + self.name, ["Spulenspannung Fein", "Kondensatorspannung Fein"], "Vergrößerung_Spannung_Fein_" + self.name)
        bool_arr_down = self.Fein_f >1370
        bool_arr_up = self.Fein_f<1430
        bool_arr = np.all([bool_arr_down, bool_arr_up], axis=0)
        display.scatter([self.Fein_f[bool_arr]],[self.Fein_phi[bool_arr]], "Frequenz in Hz", "Winkel in Grad", "Vergrößerung von Feine Messung Phasenverschiebung: " + self.name, ["Phasenverschiebung Fein"], "Vergrößerung_Phasenverschiebung_Fein_" + self.name)
        bool_arr_down = self.Fein_f >1300
        bool_arr_up = self.Fein_f<1500
        bool_arr = np.all([bool_arr_down, bool_arr_up], axis=0)
        display.scatter([self.Fein_f[bool_arr]],[self.Fein_I_0[bool_arr]], "Frequenz in Hz", "Strom in Ampere", "Vergrößerung von Feine Messung Resonanzkurve: " + self.name, ["Resonanzkurve Fein"], "Vergrößerung_Resonanzkurve_Fein_" + self.name)
        
        

    def GüteSoll(self):
        self.Q_soll = 1/(self.R + R_L) * np.sqrt(L / C)
        #Da Messung sind s hie rnoch stat Fehler. Sys fehler des Multimeter sind 0. Eventuell spielen noch widerstände von anderen Bauteilen eine Rolle
        R_R_L_stat = np.sqrt( (self.resistors * sigma_R_sys)**2 + (sigma_R_L_sys)**2 )
        self.Q_soll_stat = self.Q_soll*np.sqrt( ( -1 * R_R_L_stat / (self.R + R_L) )**2 + (1/2 * sigma_C_sys/C)**2 + (1/2 * sigma_L_sys/L)**2 )
        self.Q_soll_sys = 0
        return self.Q_soll, self.Q_soll_stat, self.Q_soll_sys

    def GüteStrom(self):
        I_sqrt = np.amax(self.Fein_I_0) / np.sqrt(2)
        
        n_I_max = np.argmax(self.Fein_I_0)
        
        I_split = np.split(self.Fein_I_0, [n_I_max,len(self.Fein_I_0)])

        n_left_sqrt = self.closestIndex(I_split[0], I_sqrt)
        n_right_sqrt = self.closestIndex(I_split[1], I_sqrt) + n_I_max

        f_I_max = self.Fein_f[np.argmax(self.Fein_I_0)]
        f_delta = abs(self.Fein_f[n_left_sqrt] - self.Fein_f[n_right_sqrt])

        
        self.Q_Strom = f_I_max / f_delta
        #Fehler
        self.Q_Strom_stat = self.Q_Strom * np.sqrt( (sigma_phi_ablese/f_I_max)**2 + (2*sigma_phi_ablese/f_delta)**2 )
        self.Q_Strom_sys = 0
        
        return self.Q_Strom, self.Q_Strom_stat, self.Q_Strom_sys
    
    def GütePhase(self):
        self.Q_Phase = self.resFreqRead /  self.deltaFreqPhi
        
        #Fehler
        self.Q_Phase_stat = self.Q_Phase * np.sqrt( (sigma_phi_ablese/self.resFreqRead)**2 + (sigma_phi_ablese/self.deltaFreqPhi)**2)
        self.Q_Phase_sys = 0
        return self.Q_Phase, self.Q_Phase_stat, self.Q_Phase_sys
    
    def GüteSpannungC(self):
        U_C_max_res  = self.Fein_U_C[self.closestIndex(self.Fein_f, self.resFreqRead)]
        U_0_res = self.Fein_U_0[self.closestIndex(self.Fein_f, self.resFreqRead)]
        
        self.Q_spannung_C = U_C_max_res / U_0_res
        
        #Fehler: ablesefehler UND statistsicher Fehler
        self.Q_spannung_C_stat = self.Q_spannung_C * np.sqrt( (np.sqrt(sigma_U**2 + sigma_U_ablese**2)/U_C_max_res)**2 + (np.sqrt(sigma_U**2 + sigma_U_ablese**2)/U_0_res)**2 )
        self.Q_spannung_C_sys = 0
        return self.Q_spannung_C, self.Q_spannung_C_stat, self.Q_spannung_C_sys
    
    def GüteSpannungL(self):
        U_L_max_res  = self.Fein_U_L[self.closestIndex(self.Fein_f, self.resFreqRead)]
        U_0_res = self.Fein_U_0[self.closestIndex(self.Fein_f, self.resFreqRead)]
        
        self.Q_spannung_L = U_L_max_res / U_0_res
        
        #Fehler: ablesefehler UND statistsicher Fehler
        self.Q_spannung_L_stat = self.Q_spannung_L * np.sqrt( (np.sqrt(sigma_U**2 + sigma_U_ablese**2)/U_L_max_res)**2 + (np.sqrt(sigma_U**2 + sigma_U_ablese**2)/U_0_res)**2 )
        self.Q_spannung_L_sys = 0
        return self.Q_spannung_L, self.Q_spannung_L_stat, self.Q_spannung_L_sys
    
    def closestIndex(self, arr, value):
        return np.abs(arr-value).argmin()
    
    def printGüte(self):
        print("Güte Soll " + self.name +":", self.GüteSoll())
        print("Güte Strom " + self.name +":", self.GüteStrom())
        print("Güte Phase " + self.name +":", self.GütePhase())
        print("Güte Spannung C " + self.name +":", self.GüteSpannungC())
        print("Güte Spannung L " + self.name +":", self.GüteSpannungL())
        print("")
        print("Güte gewichtetes Mittel (ohne Phase): " + self.name +":", self.gewichtetesMittel())
        print("________________________________________________________________________________________")

        
    def errorPlot(self):
        plt.figure(figsize=(19,9))
        plt.xlabel("Güte")
        plt.ylabel("Unterschiedliche Messverfahren")
        plt.title("Vergleich der Messverfahren mit Fehlern von: " + self.name)
        
        plt.errorbar(self.Q_soll, 0, 0, self.Q_soll_stat, label = "Q soll", fmt='.', marker='o')
        plt.errorbar(self.Q_Strom, 1, 0, self.Q_Strom_stat, label = "Q Strom", fmt='.', marker='o')
        plt.errorbar(self.Q_Phase, 2, 0, self.Q_Phase_stat, label = "Q Phase", fmt='.', marker='o')
        plt.errorbar(self.Q_spannung_C, 3, 0, self.Q_spannung_C_stat, label = "Q spannung C", fmt='.', marker='o')
        plt.errorbar(self.Q_spannung_L, 4, 0, self.Q_spannung_L_stat, label = "Q spannung L", fmt='.', marker='o')
        plt.errorbar(self.Q_mittel, 5, 0, self.Q_mittel_stat, label = "Q gewichtetes Mittel ohne Phase", fmt='.', marker='o')
        
        plt.legend()
        plt.savefig(dir_path + "/../../Grafiken/errorplot_" + self.name +".pdf")
        plt.show()
        
    def gewichtetesMittel(self):
        data = analyse.gewichtetes_mittel(np.array([self.Q_Strom, self.Q_spannung_L, self.Q_spannung_C]), np.array([self.Q_Strom_stat, self.Q_spannung_L_stat, self.Q_spannung_C_stat]))
        self.Q_mittel = data[0]
        self.Q_mittel_stat = data[1]
        return self.Q_mittel,self.Q_mittel_stat
        
class group:
    def __init__(self, measurementArray):
        self.measurements = measurementArray
        
        
    def resonanzkurvenGrob(self):
        display = displayer()
        x = []
        y = []
        label = []
        for i in self.measurements:
            x.append(i.Grob_f)
            y.append(i.Grob_I_0)
            label.append(i.name)
        display.scatter(x,y,"Frequenz in Hz", "Strom in A", "Resonanzkurven Grob",label, "group_Resonanzkurven_Grob")
        x=[]
        y=[]
        for i in self.measurements:
            x.append(i.Grob_f)
            y.append(i.Grob_phi)
        display.scatter(x,y,"Frequenz in Hz", "Phasenverschiebung in Grad", "Phasenverschiebung Grob",label, "group_Phasenverschiebung_Grob")
        x=[]
        y=[]
        for i in self.measurements:
            f = i.Grob_f
            x.append(np.append(f, f))
            U_C = i.Grob_U_C
            y.append(np.append(U_C, i.Grob_U_L))
        display.scatter(x,y,"Frequenz in Hz", "Spannung in V", "Kondensator und Spulenspannung Grob",label, "group_Spannungen_Grob")
        
    def resonanzkurvenFein(self):
        display = displayer()
        x = []
        y = []
        label = []
        for i in self.measurements:
            x.append(i.Fein_f)
            y.append(i.Fein_I_0)
            label.append(i.name)
        display.scatter(x,y,"Frequenz in Hz", "Strom in A", "Resonanzkurven Fein",label, "group_Resonanzkurven_Fein")
        x=[]
        y=[]
        for i in self.measurements:
            x.append(i.Fein_f)
            y.append(i.Fein_phi)
        display.scatter(x,y,"Frequenz in Hz", "Phasenverschiebung in Grad", "Phasenverschiebung Fein",label, "group_Phasenverschiebung_Fein")
        x=[]
        y=[]
        for i in self.measurements:
            f = i.Fein_f
            x.append(np.append(f, f))
            U_C = i.Fein_U_C
            y.append(np.append(U_C, i.Fein_U_L))
        display.scatter(x,y,"Frequenz in Hz", "Spannung in V", "Kondensator und Spulenspannung Fein",label, "group_Spannungen_Fein")
    
    def linReg(self):
        R = []
        R_err = []
        Q = []
        Q_err = []
        for x in self.measurements:
            for y in range(3):
                R.append(x.R_total)
                R_err.append(x.R_total_err)
            Q.append(x.Q_spannung_C)
            Q.append(x.Q_spannung_L)
            Q.append(x.Q_Strom)
            Q_err.append(x.Q_spannung_C_stat)
            Q_err.append(x.Q_spannung_L_stat)
            Q_err.append(x.Q_Strom_stat)
        R = np.array(R)
        R_err = np.array(R_err)
        Q = np.array(Q)
        Q_err = np.array(Q_err)
        (m,em,b,eb,chiq,corr) = nice_plots.nice_regression_plot(1/R, Q, Q_err, xerror=np.zeros(1), xlabel='1/(R+R_L)',
                         ylabel='Güte Q', ylabelresidue='Residuen', title='Lineare Regression mit allen Messungen', save=dir_path + "/../../Grafiken/lineareRegressionGüte"+".pdf")
        
    def printAllByCalc(self):
        drawer = tableDrawer()
        
        Q = []
        Q_err = []
        name = []
        for x in self.measurements:
            Q.append(x.Q_Strom)
            Q_err.append(x.Q_Strom_stat)
            name.append(x.name)
        drawer.drawResultByCalc(Q,Q_err,name,"statistischer","Resonanzkurve")
        
        Q = []
        Q_err = []
        name = []
        for x in self.measurements:
            Q.append(x.Q_Phase)
            Q_err.append(x.Q_Phase_stat)
            name.append(x.name)
        drawer.drawResultByCalc(Q,Q_err,name,"statistischer","Phasenverschiebung")
        
        Q = []
        Q_err = []
        name = []
        for x in self.measurements:
            Q.append(x.Q_spannung_C)
            Q_err.append(x.Q_spannung_C_stat)
            name.append(x.name)
        drawer.drawResultByCalc(Q,Q_err,name,"statistischer","Spulenspannungsüberhöhung")
        
        Q = []
        Q_err = []
        name = []
        for x in self.measurements:
            Q.append(x.Q_spannung_L)
            Q_err.append(x.Q_spannung_L_stat)
            name.append(x.name)
        drawer.drawResultByCalc(Q,Q_err,name,"statistischer","Kondensatorspannungsüberhöhung")
        
        Q = []
        Q_err = []
        name = []
        for x in self.measurements:
            Q.append(x.Q_soll)
            Q_err.append(x.Q_soll_stat)
            name.append(x.name)
        drawer.drawResultByCalc(Q,Q_err,name,"systematischer","Impedanz")

class oszi:
    def __init__(self, resFreq, highFreq, highFreq_t, lowFreq, lowFreq_t, resistor):
       self.resonance_Frequency = resFreq
       self.high_Frequency = highFreq
       self.high_Frequency_del_t = highFreq_t
       self.low_Frequency = lowFreq
       self.low_Frequency_del_t = lowFreq_t
       
       self.del_Frequency = highFreq - lowFreq
       
       self.Q = resFreq / self.del_Frequency
       self.Q_err = self.Q * np.sqrt( (sigma_oszi/resFreq)**2 + (2*sigma_oszi/self.del_Frequency)**2 )
       
       self.high_Phase = highFreq * highFreq_t * 360
       self.low_Phase = lowFreq * lowFreq_t * 360
       
       self.printResults()
       
    def printResults(self):
        print("________________________________________________________________________________________")
        print("Güte Oszillator 51dOhm: " + str((self.Q, self.Q_err)))
        print("Resonanzfrequenz bei " + str(self.resonance_Frequency) + "Hz")
        print("Phasenverschiebung bei " + str(self.low_Frequency) + "Hz: " + str(self.low_Phase))
        print("Phasenverschiebung bei " + str(self.high_Frequency) + "Hz: " + str(self.high_Phase))

#Begin
#Rauschmessung für Fehler
sigma_U, sigma_I= rauschMessung()

print(sigma_U, sigma_I)

#Einlesen der Daten + Plotten + ablesen
m_51dOhm = measurement("394493_2E1_51dOhm", 5.3, 1, 1394, 1865-1048)
m_61dOhm = measurement("394493_2E1_61dOhm", 6.4, 2, 1392.5, 1950-1006)
m_100dOhm = measurement("394493_2E1_100dOhm", 10.1, 1, 1396, 2319-857)
m_151dOhm = measurement("394493_2E1_151dOhm", 15.35, 2, 1405, 2850-708)
m_200dOhm = measurement("394493_2E1_200dOhm", 19.9, 1, 1402,3480-595, True)

#Gemeinsame Darstellungen
allItems = group([m_51dOhm, m_61dOhm, m_100dOhm, m_151dOhm, m_200dOhm])
allItems.resonanzkurvenGrob()
allItems.resonanzkurvenFein()
allItems.linReg()
allItems.printAllByCalc()

#Oszi Daten

n_51dOhm = oszi(1390.6, 1820.9, 68*10**(-6), 1040.9, 120*10**(-6), 5.3)
g_51dOhm = oszi(1390.6, 1840.6, 0, 1030.6,0,5.3)