# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 11:15:20 2020

@author: 394493: Alexander Kohlgraf
"""

from praktikum import analyse, cassy1
import numpy as np
import matplotlib.pyplot as plt
import nice_plots
import scipy.constants as const
import texttable
import latextable
import codecs
import os
x=1
y=1
ex=1

amplification = 10**4
#SerialNumber = 130810
sensorSensitivity = 0.198
sensorSystematic = 0.0059
diameterEmitter = 0.035
diameterEmitterError = 0.0005
diameterSensor = 0.023
diameterSensorError = 0.0005
distance = 0.108
r = distance
r_err = 0.0005

def systematicError(U_max, U_current):
    return 0.01 * U_current + 0.005 * U_max

def latexTableRaw(name, T_0, T_0_err, T, T_err, U, U_err):
    """automated tables for latex """
    text = ''
    table = texttable.Texttable()
    table.set_cols_align(["c"]*4)
    table.header(["Soll Temp. in K | °C", "Wassertemp. in K", "Cassy Spannung in V", "Umgebungstemp. in K"])
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    table.set_precision(3)
    for i in range(8):
        T_soll = 50 + i*5
        if T_soll == 85:    T_soll = 90
        if T_soll == 80: T_soll = 85
        table.add_row([str(T_soll + 273.15) + " | " + str(T_soll), '$ \\num{{ {:.2e} }}'.format(T[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(T_err[i]), '$ \\num{{ {:.2e} }}' .format( U[i])+ " \pm " + ' \\num{{ {:.2e} }}$'.format(U_err[i]), '$ \\num{{ {:.2e} }}'.format(T_0[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(T_0_err[i])])
    text += latextable.draw_latex_small(table, caption='Gemittelte Daten für die Seite: ' + name, label = "tab:" + name + "Raw") +'\n'

    dir_path = os.path.dirname(os.path.realpath(__file__))
    text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + name + "Raw.tex", "w", 'utf-8')
    text_file.write(text)
    text_file.close()
    
def latexTableReg(name, m, em, b, eb, chiq, corr):
    """automated tables for latex """
    text = ''
    table = texttable.Texttable()
    table.set_cols_align(["c"]*2)
    table.header([r"Steigung in $VK^{-4}$", r"y-Achsenabschnitt in $V$"])
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    table.set_precision(3)
    table.add_row(['$ \\num{{ {:.2e} }}'.format(m) + " \pm " + ' \\num{{ {:.2e} }}$'.format(em), '$ \\num{{ {:.2e} }}' .format( b)+ " \pm " + ' \\num{{ {:.2e} }}$'.format(eb)])
    text += latextable.draw_latex(table, caption='Regressionsfaktoren für die Seite: ' + name, label = "tab:" + name + "Reg") +'\n'
    
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + name + "Reg.tex", "w", 'utf-8')
    text_file.write(text)
    text_file.close()
    
def latexTableBoltzConst(name, eps, eps_std, eps_sys, P_mess, P_mess_std, P_mess_sys, P_ideal, P_ideal_std):
    """automated tables for latex """
    eps_aver = analyse.gewichtetes_mittel(eps, eps_std)
    
    text = ''
    table = texttable.Texttable()
    table.set_cols_align(["c"]*4)
    table.header(["°C", "P gemessen in W", "P ideal in W", "Emissionsgrad"])
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    table.set_precision(3)
    for i in range(8):
        T_soll = 50 + i*5
        if T_soll == 85: T_soll = 90
        if T_soll == 80: T_soll = 85
        table.add_row([T_soll, '$ \\num{{ {:.2e} }}'.format(P_mess[i]) + " \pm " + ' \\num{{ {:.2e} }}'.format(P_mess_std[i]) + "$stat$ \pm " + ' \\num{{ {:.2e} }}$sys'.format(P_mess_sys[i]), '$ \\num{{ {:.2e} }}' .format( P_ideal[i])+ " \pm " + ' \\num{{ {:.2e} }}$stat'.format(P_ideal_std[i]), '$ \\num{{ {:.2e} }}'.format(eps[i]) + " \pm " + ' \\num{{ {:.3f} }}'.format(eps_std[i]) + "$stat$ \pm " + ' \\num{{ {:.3f} }}$sys'.format(eps_sys[i])])
    text += latextable.draw_latex_small(table, caption='Gewichtetes Mittel Emissionsgrad ' + name + ": $\epsilon_{" + name + "}" + " = {:.3f}".format(eps_aver[0]) + "\pm" + ' {:.3f}$'.format(eps_aver[1]), label = "tab:" + name + "Emission") +'\n'

    dir_path = os.path.dirname(os.path.realpath(__file__))
    text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + name + "Emission.tex", "w", 'utf-8')
    text_file.write(text)
    text_file.close()

def latexTableBoltzConstAveraged(names, listOfEpsAver, listOfEpsAverErr):
    """automated tables for latex """
    text = ''
    table = texttable.Texttable()
    table.set_cols_align(["c"]*2)
    table.header(["Material", "Emissionsgrad"])
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    table.set_precision(3)
    for i in range(len(listOfEpsAver)):
        table.add_row([names[i], '$ \\num{{ {:.3f} }}'.format(listOfEpsAver[i]) + " \pm " + ' \\num{{ {:.3f} }}$'.format(listOfEpsAverErr[i])])
    text += latextable.draw_latex(table, caption= "Gewichtete Mittel", label = "tab:" + "GewichtetesMittelEmissionen") +'\n'

    dir_path = os.path.dirname(os.path.realpath(__file__))
    text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "GewichtetesMittelEmissionen.tex", "w", 'utf-8')
    text_file.write(text)
    text_file.close()
    
    
def averagedValues(name):
    T = []
    T_err = []
    U = []
    U_err = []
    T_0 = []
    T_0_err = []
    for x in range(50, 90, 5):
        file = name + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        t = measure[:,3]
        t_average = analyse.mittelwert_stdabw(t)
        T.append(t_average[0])
        T_err.append(t_average[1])
        
        u = measure[:,2]
        u_average = analyse.mittelwert_stdabw(u)
        U.append(u_average[0])
        U_err.append(u_average[1])
        
        file = "Umgebungstemperatur" + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        t_0 = measure[:,3]
        t_0_average = analyse.mittelwert_stdabw(t_0)
        T_0.append(t_0_average[0])
        T_0_err.append(t_0_average[1])
        
    return name,T_0,T_0_err,T,T_err,U,U_err

def EmissionDegree(name):
    realEmmission = RealEmmission(name)
    idealEmmission = IdealEmission(name)
    emissionDegrees = realEmmission[0] / idealEmmission[0]
    
    emissionDegreesError = emissionDegrees * np.sqrt((realEmmission[1] / realEmmission[0])**2 + (idealEmmission[1] / idealEmmission[0])**2)

    
    emissionDegreesErrorSystematic = realEmmission[2] / idealEmmission[0]
    
    latexTableBoltzConst(name, emissionDegrees, emissionDegreesError, emissionDegreesErrorSystematic, realEmmission[0], realEmmission[1], realEmmission[2], idealEmmission[0], idealEmmission[1])
    
    return emissionDegrees, emissionDegreesError, emissionDegreesErrorSystematic

def EmissionDegreeAverage(name):
    realEmmission = RealEmmission(name)
    idealEmmission = IdealEmission(name)
    emissionDegrees = realEmmission[0] / idealEmmission[0]

    emissionDegreesError = emissionDegrees * np.sqrt((realEmmission[1] / realEmmission[0])**2 + (idealEmmission[1] / idealEmmission[0])**2)

    EmissionDegree = analyse.gewichtetes_mittel(emissionDegrees, emissionDegreesError)
    return EmissionDegree[0], EmissionDegree[1]

def IdealEmission(name):
    Temperatures = []
    TemperaturesErrors = []
    surroundingTemperatures = []
    surroundingTemperaturesErrors = []

    for x in range(50, 90, 5):
        file = name + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        temperature = measure[:,3]
        averageTemperature = analyse.mittelwert_stdabw(temperature)[0]
        averageTemperatureError = analyse.mittelwert_stdabw(temperature)[1]
        Temperatures.append(averageTemperature)
        TemperaturesErrors.append(averageTemperatureError)

        file = "Umgebungstemperatur" + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        surroundingTemperature = measure[:,3]
        averageSurroundingTemperature = analyse.mittelwert_stdabw(surroundingTemperature)[0]
        averageSurroundingTemperatureError = analyse.mittelwert_stdabw(surroundingTemperature)[1]
        surroundingTemperatures.append(averageSurroundingTemperature)
        surroundingTemperaturesErrors.append(averageSurroundingTemperatureError)

    areaEmitter = CalculateArea(diameterEmitter, diameterEmitterError)[0]
    areaEmitterError = CalculateArea(diameterEmitter, diameterEmitterError)[1]
    areaSensor = CalculateArea(diameterSensor, diameterSensorError)[0]
    areaSensorError = CalculateArea(diameterSensor, diameterSensorError)[1]

    idealEmission = areaEmitter * areaSensor * const.Stefan_Boltzmann * 1/np.pi * 1/(distance**2) * (np.array(Temperatures)**4 - np.array(surroundingTemperatures)**4)
    A_e = CalculateArea(diameterEmitter, diameterEmitterError)[0]
    A_e_err = CalculateArea(diameterEmitter, diameterEmitterError)[1]
    A_s = CalculateArea(diameterSensor, diameterSensorError)[0]
    A_s_err = CalculateArea(diameterSensor, diameterSensorError)[1]
    
    T_0 = np.array(surroundingTemperatures)
    T = np.array(Temperatures)
    T_0_err = np.array(surroundingTemperaturesErrors)
    T_err = np.array(TemperaturesErrors)

    idealEmission = A_e * A_s * const.Stefan_Boltzmann * 1/np.pi * 1/(distance**2) * (T**4 -T_0**4)
    idealEmissionError = np.sqrt( (A_e * 1/r**2 * (T**4 - T_0**4) * const.Stefan_Boltzmann/np.pi * A_s_err)**2 +
                                 (A_s * 1/r**2 * (T**4 - T_0**4) * const.Stefan_Boltzmann/np.pi * A_e_err)**2 + 
                                 (2* A_e * A_s * 1/r**3 * (T**4 - T_0**4) * const.Stefan_Boltzmann/np.pi * r_err)**2 + 
                                 (A_e * A_s * 1/r**2 * const.Stefan_Boltzmann/np.pi)**2 * ( (4 * T**3 * T_err)**2 + (4 * T_0**3 * T_0_err)**2 ) )
    
    return (idealEmission, idealEmissionError)
   

def CalculateArea(diameter, diameterError):
    area = np.pi * (diameter/2)**2
    areaError = np.pi * diameter/2 * diameterError
    return [area, areaError]

def RealEmmission(name):
    Voltages = []
    VoltagesErrors = []
    RealEmmissionSystematic = []
    for x in range(50, 90, 5):
        file = name + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        voltage = measure[:,2]
        averageVoltage = analyse.mittelwert_stdabw(voltage)[0]
        averageVoltageError = analyse.mittelwert_stdabw(voltage)[1]
        Voltages.append(averageVoltage)
        VoltagesErrors.append(averageVoltageError)
        if x <= 75:
            systematic = (averageVoltage*amplification**-1 / sensorSensitivity) * np.sqrt( (systematicError(10,averageVoltage) / averageVoltage )**2 + (sensorSystematic / sensorSensitivity)**2 )
            RealEmmissionSystematic = np.append(RealEmmissionSystematic, systematic)
        else:
            systematic = (averageVoltage*amplification**-1 / sensorSensitivity) * np.sqrt( (systematicError(30,averageVoltage) / averageVoltage )**2 + (sensorSystematic / sensorSensitivity)**2 )
            RealEmmissionSystematic = np.append(RealEmmissionSystematic, systematic)
    realVoltages = np.array(Voltages) * amplification**-1
    realVoltagesErrors = np.array(VoltagesErrors) * amplification**-1
    RealEmmission = realVoltages / sensorSensitivity
    RealEmmissionError = realVoltagesErrors / sensorSensitivity
    return (RealEmmission, RealEmmissionError, RealEmmissionSystematic)

def Regression(name):
    Temperatures = []
    Voltages = []
    TemperaturesErrors = []
    VoltagesErrors = []
    surroundingTemperatures = []
    surroundingTemperaturesErrors = []
    for x in range(50, 90, 5):
        file = name + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        temperature = measure[:,3]
        voltage = measure[:,2] 
        Temperatures = np.append(Temperatures, temperature)
        Voltages = np.append(Voltages, voltage)
        if x <= 75:
            VoltagesErrors = np.append(VoltagesErrors, np.ones((len(voltage),))* (20/4096 + 0.01 * voltage))
        else:
            VoltagesErrors = np.append(VoltagesErrors, np.ones((len(voltage),))* (60/4096 * 10 + 0.01 * voltage))
        
        file = "Umgebungstemperatur" + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        surroundingTemperature = measure[:,3]
        averageSurroundingTemperature = analyse.mittelwert_stdabw(surroundingTemperature)[0]
        averageSurroundingTemperatureError = analyse.mittelwert_stdabw(surroundingTemperature)[1]
        surroundingTemperatures = np.append(surroundingTemperatures, np.ones((len(temperature),)) * averageSurroundingTemperatureError)
       

    TemperaturesErrors = np.ones((len(Temperatures),))
    for i in range(len(Temperatures)):
        if Temperatures[i] > 70 + 273.15:
            TemperaturesErrors[i] *= 0.4
        else:
            TemperaturesErrors[i] *= 0.2
    
    
    x = np.array(Temperatures)**4
    x_err = 4 * np.array(Temperatures)**3 * np.array(TemperaturesErrors)
    y = np.array(Voltages) * amplification**-1
    y_err = np.array(VoltagesErrors) * amplification**-1 
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + name + "Raw.tex", "w", 'utf-8')
    
    m, em, b, eb, chiq, corr = nice_plots.nice_regression_plot(x,y,y_err, x_err, 'T^4 / [K^4]',
                         'U(T) / [V]', "Residuen", name, dir_path + "/../Protokoll/regression/" + name + ".pdf")
    
    latexTableReg(name, m, em, b, eb, chiq, corr)
    
    return m, em, b, eb, chiq, corr
    
def RegressionAverages(name):
    Temperatures = []
    Voltages = []
    TemperaturesErrors = []
    VoltagesErrors = []
    surroundingTemperatures = []
    surroundingTemperaturesErrors = []
    for x in range(50, 90, 5):
        file = name + "/" + str(x) + "Grad.lab"
        measure = cassy1.lese_lab_datei(file)
        temperature = measure[:,3]
        voltage = measure[:,2] 
        averageTemperature = analyse.mittelwert_stdabw(temperature)
        averageVoltage = analyse.mittelwert_stdabw(voltage)
        Temperatures.append(averageTemperature[0])
        Voltages.append(averageVoltage[0]) 
    Voltages = Voltages * amplification**-1
    TemperaturesForth = Temperatures**4
    TemperaturesErrors.append(averageTemperature[1])
    VoltagesErrors.append(averageVoltage[1])
                        
    file = "Umgebungstemperatur" + "/" + str(x) + "Grad.lab"
    measure = cassy1.lese_lab_datei(file)
    surroundingTemperature = measure[:,3]
    averageSurroundingTemperature = analyse.mittelwert_stdabw(surroundingTemperature)[0]
    averageSurroundingTemperatureError = analyse.mittelwert_stdabw(surroundingTemperature)[1]
    surroundingTemperatures.append(averageSurroundingTemperature)
    surroundingTemperaturesErrors.append(averageSurroundingTemperatureError)
        
    x = np.array(Temperatures)**4 - np.array(surroundingTemperatures)**4
    x_err = np.sqrt((4 * np.array(Temperatures)**3 * np.array(TemperaturesErrors))**2 + (4 * np.array(surroundingTemperatures) * np.array(surroundingTemperaturesErrors))**2)
    y = np.array(Voltages) * amplification**-1
    y_err = np.array(VoltagesErrors) * amplification**-1 / sensorSensitivity
        
    nice_plots.nice_regression_plot(x,y,y_err, x_err)
    
    m, em, b, eb, chiq, corr = nice_plots.nice_regression_parameters(x,y,x_err,y_err)
    return m, em, b, eb, chiq, corr


print(EmissionDegree("Weiss"))
print(EmissionDegree("Messing"))
print(EmissionDegree("Schwarz"))
print(EmissionDegree("Silber"))

Regression("Weiss")
Regression("Messing")
Regression("Schwarz")
Regression("Silber")


We = EmissionDegreeAverage("Weiss")
Me = EmissionDegreeAverage("Messing")
Sw = EmissionDegreeAverage("Schwarz")
Si = EmissionDegreeAverage("Silber")
latexTableBoltzConstAveraged(["Weiss","Messing","Schwarz", "Silber"], [We[0], Me[0], Sw[0], Si[0]], [We[1], Me[1], Sw[1], Si[1]])


a = averagedValues("Weiss")
latexTableRaw(a[0], a[1], a[2], a[3], a[4], a[5], a[6])
a = averagedValues("Messing")
latexTableRaw(a[0], a[1], a[2], a[3], a[4], a[5], a[6])
a = averagedValues("Schwarz")
latexTableRaw(a[0], a[1], a[2], a[3], a[4], a[5], a[6])
a = averagedValues("Silber")
latexTableRaw(a[0], a[1], a[2], a[3], a[4], a[5], a[6])


file = "Umgebungstemperatur" + "/" + "45" + "Grad.lab"
measure = cassy1.lese_lab_datei(file)
voltage = measure[:,3]
averageVoltage = analyse.mittelwert_stdabw(voltage)[0]
averageVoltageError = analyse.mittelwert_stdabw(voltage)[1]

print(averageVoltage, averageVoltageError)