#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 10:58:27 2020

@author: lbo
"""
import math

import matplotlib.pyplot as plt
import numpy as np


from praktikum import literaturwerte, analyse

fig = plt.figure(figsize=(15,10))

for i,f in enumerate([literaturwerte.n_schott_f2, literaturwerte.n_schott_nsf10]):
    p = fig.add_subplot(2,1,i+1)
    wl = np.arange(0.400,0.800,0.025)
    wl_isq = 1/wl**2
    n = np.vectorize(f)(wl)

    params = analyse.quadratische_regression(wl_isq*1e-6, n, np.full_like(n, 0.00001))
    print('quad', params)
    (a,_,b,_,c,_,_,_) = params

    p.plot(wl, n, label=str(f), marker='o')
    # p.plot(wl_isq, np.vectorize(lambda x: a*x**2+b*x+c)(wl_isq), label='Quad.')
    # p.plot(wl, n, label=str(f))
    # p.secondary_xaxis('top', functions=(lambda l: 1/l**2, lambda l: 1/np.sqrt(l)))
    (a, ea, b, eb, chisq, corr) = analyse.lineare_regression(wl_isq, n, np.full_like(n, 0.00001))
    print((a, ea, b, eb, chisq, corr))
    # p.plot(wl_isq, np.vectorize(lambda x: a*x+b)(wl_isq), label='Linear')
    p.legend()
    p.grid()

fig.savefig('litregress.pdf')
