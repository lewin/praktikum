#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 17:05:03 2020

All calculations in rad! All stored values in degrees!

Local paths are only set as defaults. To run, give datapath arguments according
to where your CSV files are.

@author: lbo
"""

import math

from praktikum import analyse, literaturwerte
import numpy as np
import matplotlib.pyplot as plt
import pandas

from uncertainties import ufloat
import uncertainties.umath as umath
import uncertainties.unumpy as unp

from pylatex import table

class Util:

    def dms_deg(d, m=0, s=0):
        return d+m/60+s/3600

    def degrad(d):
        return d/180 * math.pi

    def raddeg(r):
        return r/math.pi * 180

    def deg_dms(d):
        negate = 1
        if d < 0:
            negate = -1
        d = abs(d)
        deg = int(d)
        rem = d-deg
        m = 0
        if abs(rem) > 1/60:
            m = round(rem / (1/60), 0)
            rem = d - (deg+m/60)
        s = round(rem / (1/3600), 0) if rem > 0 else 0
        return (negate*deg, negate*int(m), negate*int(s))

class Fmt:
    def format_latex_uncertainty(u):
        v = u.n
        s = u.s
        v_p = int(math.log10(v))
        v_s = v / 10**v_p
        s_s = s / 10**v_p
        s_unc_dig = int(round(s_s * 10**(4), 0))

        return '\\num{{ {:.4f}({:04d})e{:d}  }}'.format(v_s, s_unc_dig, v_p)

    def fmt(f):
        return '\\num{{ {:.2f} }}'.format(f)
    def rmt(f):
        return '\\num{{{:.5f}}}'.format(Util.degrad(f))
    def dmt(d):
        return Angle(deg=d).latex()
    def nmt(n):
        return Fmt.format_latex_uncertainty(n)
        return '$\\num{{ {:.4f} }} \\pm \\num{{ {:.4f} }}$'.format(n.n, n.s)


class Angle:

    def __init__(self, deg=None, rad=None):
        deg %= 360
        if deg:
            self.val = deg
        elif rad:
            self.val = Util.raddeg(rad)

    def deg(self):
        return self.val

    def rad(self):
        return Util.degrad(self.val)

    def latex(self):
        d,m,s = Util.deg_dms(self.val)
        return '$\\ang{{{:d};{};{}}}$'.format(d,str(m) if m != 0 else '',str(s) if s != 0 else '')

    def __str__(self):
        d,m,s = Util.deg_dms(self.val)
        if s == 0 and m == 0:
            return '{:d}°'.format(d).replace('.', ',')
        if s == 0:
            return '{:d}° {:d}\''.format(d,m).replace('.', ',')
        return '{:d}° {:d}\' {:d}"'.format(*Util.deg_dms(self.val)).replace('.', ',')

    __repr__ = __str__



class AngleUncertainties:

    def __init__(self, datapath='../daten/winkelunsicherheit.csv'):
        # Expecting columns PSI1_angle, PSI2_angle

        dataframe = pandas.read_csv(datapath)
        psi1 = dataframe['PSI1_angle'].to_numpy()
        psi2 = dataframe['PSI2_angle'].to_numpy()

        self.measurements = [psi1, psi2]

    def _centered(self):
        # "Center" measurements around 0, then take standard deviation,
        # which is unaffected by absolute values
        centered = []
        for i, m in enumerate(self.measurements):
            centered.extend(list(m-m.mean()))
        return np.array(centered)

    def uncertainty(self):
        """Calculate uncertainty over all uncertainty/noise measurements"""
        return max(np.std(self._centered(), ddof=1), Util.dms_deg(0, 1, 0))
        # return Util.dms_deg(0,2,0)

    def histogram(self):
        centered = self._centered()
        mincentered = np.vectorize(lambda d: Util.deg_dms(d)[1]+Util.deg_dms(d)[2]/60)(centered)

        fig = plt.figure(figsize=(10, 7))
        p = fig.add_subplot(111)
        p.hist(mincentered, bins=np.arange(-4.5,5,1), align='mid')
        p.set_title('Winkelverteilung Rauschmessung: n = {:d}'.format(mincentered.size))
        p.set_xlabel('Winkelabweichung / \'')
        p.set_ylabel('Häufigkeit')
        p.grid()
        fig.savefig('../img/winkelrauschen.pdf')

    def gen_latex(self):
        """Format measurements as a Latex table."""
        tab = table.Tabular('r|ll|ll', booktabs=True)
        tab.escape = False
        tab.add_row('', '$\\psi_1 / \\si{\\degree}$', '$\\psi_1 / \\si{\\radian}$', '$\\psi_2 / \\si{\\degree}$', '$\\psi_2 / \\si{\\radian}$')
        tab.add_hline()

        for i, m in enumerate(zip(*self.measurements)):
            i += 1
            tab.add_row(i, Angle(m[0]).latex(), Fmt.rmt(m[0]), Angle(m[1]).latex(), Fmt.rmt(m[1]))

        tab.generate_tex('../img/uncertainties')


class DispersionRelation:

    def __init__(self, datapath='../daten/spektrallinien.csv', uncertainty=None):
        """Initialize a new DispersionRelation calculator.

        This class deals with degree values, until actual calculations happen.
        """
        if not uncertainty:
            uncertainty = AngleUncertainties().uncertainty()
        # angle_avg_uncert = min(uncertainty / math.sqrt(2), (Util.dms_deg(0,1)))
        angle_avg_uncert = uncertainty / math.sqrt(2)

        # Expecting PSI1_1, PSI1_2, PSI2_1, PSI2_2 columns
        dataframe = pandas.read_csv(datapath)

        psi1 = (dataframe['PSI1_1']+dataframe['PSI1_2'])/2
        self.psi1 = psi1.to_numpy()
        psi2 = (dataframe['PSI2_1']+dataframe['PSI2_2'])/2
        self.psi2 = psi2.to_numpy()
        self.sources = dataframe['Quelle']
        self.lambdas = dataframe['WL'].to_numpy()
        assert self.psi1.size == self.psi2.size == self.sources.size

        self.uncertainties = np.full_like(self.psi1, angle_avg_uncert)
        self.delta_mins = unp.uarray(np.abs((self.psi2-self.psi1)/2), self.uncertainties/math.sqrt(2))
        self.delta_min_vals = np.abs((self.psi2-self.psi1)/2)

        # Calculate refractive indices with uncertainties
        lambda_invsq = 1/self.lambdas**2
        delta_mins_rad = self.delta_mins / 180 * math.pi
        self.ns = self.calculate_refr_index(delta_mins_rad)

        self.quad_params = None
        self.lin_params = None

        # Check uncertainty calculation manually.
        # epsilon = Util.degrad(60)
        # delta_mins = np.vectorize(Util.degrad)(self.delta_min_vals)
        # sigma_n = 1/(2*math.sin(epsilon/2)) * np.cos((delta_mins+epsilon)/2) * Util.degrad(uncertainty)/2
        # print(sigma_n)

    def calculate_refr_index(self, delta_min_rad):
        epsilon = Util.degrad(60)
        return unp.sin((delta_min_rad+epsilon)/2)/np.sin(epsilon/2)

    def gen_latex(self):
        # Element, wavelength, degrees, radians
        tab = table.Tabular('rc|lll', booktabs=True)
        tab.escape = False
        tab.add_row('Element', '$\\lambda / \\si{\\nm}$', '$\\delta_{min}/\\si{\\degree}$', '$\\delta_{min}/\\si{\\radian}$', '$n$')
        tab.add_hline()

        lambdas = np.vectorize(Fmt.fmt)(self.lambdas)
        dmind = np.vectorize(Fmt.dmt)(self.delta_min_vals)
        dminr = np.vectorize(Fmt.rmt)(self.delta_min_vals)
        ns = np.vectorize(Fmt.nmt)(self.ns)

        for i, d in enumerate(zip(self.sources,lambdas,dmind, dminr, ns)):
            tab.add_row(d)

        tab.generate_tex('../img/measured_spectrals')

    def gen_plot(self):
        """Plot the dispersion curve measurements."""
        ns = np.vectorize(lambda n: n.n)(self.ns)

        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_title('$n$ vs. $\\lambda$')
        p.set_xlabel('$\\lambda$/nm' )
        p.set_ylabel('$n$')
        p.plot(self.lambdas, ns, marker='o', color='red')
        p.grid()
        fig.savefig('../img/brechungsindices.pdf')

    def calculate_regressions(self):
        """Returns (quad, lin) parameters tuples."""
        lambda_invsq = 1/self.lambdas**2
        ns = np.vectorize(lambda n: n.n)(self.ns)
        ns_std = np.vectorize(lambda n: n.s)(self.ns)

        (a, ea, b, eb, c, ec, chiq, (corr_ab, corr_ac, corr_bc)) = analyse.quadratische_regression(lambda_invsq, ns, ns_std)
        (la, lea, lb, leb, lchiq, lcorr) = analyse.lineare_regression(lambda_invsq, ns, ns_std)

        print('Quad. Reg.:', a, ea, b, eb, c, ec)
        print('Quad X^2/DoF:', chiq/(ns.size-3))
        print('Lin. Reg.:', la, lea, lb, leb)
        print('Lin X^2/DoF:', lchiq/(ns.size-2))

        self.quad_params = (a, ea, b, eb, c, ec, chiq, (corr_ab, corr_ac, corr_bc))
        self.lin_params = (la, lea, lb, leb, lchiq, lcorr)


        return ((a, ea, b, eb, c, ec, chiq, (corr_ab, corr_ac, corr_bc)),
                (la, lea, lb, leb, lchiq, lcorr))

    def interpolate_dispersion(self, wl):
        """Calculate the expected value of the dispersion curve at lambda = wl."""
        if not self.quad_params:
            self.calculate_regressions()
        return self.quad_params[0]/wl**4 + self.quad_params[2]/wl**2 + self.quad_params[4]

    def interpolate_dispersion_diff(self, wl):
        """Calculate the expected value of the first derivative of the dispersion curve at lambda = wl.
        """
        if not self.quad_params:
            self.calculate_regressions()
        return -4*self.quad_params[0]/wl**5 - 2*self.quad_params[2]/wl**3

    def regression(self):
        """Plot the linear and quadratic regressions for the dispersion curve."""
        lambda_invsq = 1/self.lambdas**2
        ns = np.vectorize(lambda n: n.n)(self.ns)
        ns_std = np.vectorize(lambda n: n.s)(self.ns)

        ((a, ea, b, eb, c, ec, chiq, (corr_ab, corr_ac, corr_bc)),
         (la, lea, lb, leb, lchiq, lcorr)) = self.calculate_regressions()

        def lin(x):
            return la*x+lb
        def quad(x):
            return a*x**2+b*x+c

        frm, to = lambda_invsq[-1], lambda_invsq[0]
        xs = np.linspace(frm, to, 100)
        lin_ys = np.vectorize(lin)(xs)
        quad_ys = np.vectorize(quad)(xs)

        params = {}

        # Plot two residual plots for linear and quadratic regression
        for typ, ys, model, dof, chiq in [
                ('Lin', lin_ys, lin, 2, lchiq),
                ('Quad', quad_ys, quad, 3, chiq)]:
            plt.tight_layout()
            fig, (p, residp) = plt.subplots(2, 1, figsize=(15,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
            fig.tight_layout()
            fig.subplots_adjust(hspace=0.)
            p.set_title('$n$ vs. $\\lambda^{-2}$')
            p.set_ylabel('$n$')
            p.scatter(lambda_invsq, ns, marker='x', label='Messung', color='red')
            p.plot(xs, ys, label='{}. Reg.'.format(typ), color='green')
            p.text(0.5, 0.1, '$\\chi^2$/ndof = {:.2e} / {:d} = {:.3f}'.format(chiq, ns.size-dof, chiq/(ns.size-dof)), transform=p.transAxes)
            p.grid()
            p.legend()

            # Calculate and plot residual plot
            resid = ns - np.vectorize(model)(lambda_invsq)

            residp.errorbar(lambda_invsq, resid, yerr=ns_std, label='{}. Reg.'.format(typ), fmt='or')
            residp.axhline(y=0., color='black', linestyle='--')
            residp.set_xlabel('$\\lambda^{-2}$/nm$^{-2}$' )
            residp.set_ylabel('$n - n_{modell}$')
            ymax = max([abs(x) for x in residp.get_ylim()])
            residp.set_ylim(-ymax, ymax)
            residp.grid()

            fig.savefig('../img/regression_{}.pdf'.format(typ))

        def fmt(n):
            return Fmt.format_latex_uncertainty(n)
            return '$\\num{{ {:.4e} }} \\pm \\num{{ {:.4e} }}$'.format(n.n, n.s)
        def num(n):
            return '\\num{{ {:.2f} }}'.format(n)
        def intt(n):
            return '\\num{{ {:d} }}'.format(n)

        # And format as LaTeX
        tab = table.Tabular('rll', booktabs=True)
        tab.escape = False
        tab.add_row('Parameter', 'Linear', 'Quadratisch')
        tab.add_hline()
        tab.add_row('$b\'$', '--', fmt(ufloat(a, ea)))
        tab.add_row('$b$', fmt(ufloat(la, lea)), fmt(ufloat(b, eb)))
        tab.add_row('$a$', fmt(ufloat(lb, leb)), fmt(ufloat(c, ec)))
        tab.add_hline()
        tab.add_row('$\\chi^2$', num(lchiq), num(chiq))
        tab.add_row('FG', intt(ns.size-2), intt(ns.size-3))
        tab.add_row('$\\chi^2/$FG', num(lchiq/(ns.size-2)), num(chiq/(ns.size-3)))
        tab.generate_tex('../img/regressionsparameter')

    def invert(self, n=None, rng=(1.71,1.79), title=None, plot=True):
        """If n not None, return lambda (in nm) of n. Generate plot of inverted curve."""

        ((c, _, b, _, a, _, chiq, (corr_ab, corr_ac, corr_bc)), _) = self.calculate_regressions()

        def inv(n):
            return math.sqrt(
                (math.sqrt(-4*a*c+b**2+4*c*n) + b)/(2*n - 2*a))

        if plot:
            xs = np.linspace(*rng, 50)
            ys = np.vectorize(inv)(xs)

            fig = plt.figure(figsize=(15,10))
            p = fig.add_subplot(111)
            p.plot(xs, ys)

            wl = None
            if n:
                wl = inv(n)
                p.scatter(n, wl, color='red', marker='o')
                p.text(n+.005, wl+10, '$\\lambda \\approx {:d}$ nm'.format(int(round(wl, 0))))

            p.set_title('$\\lambda(n)$')
            p.set_xlabel('$n$')
            p.set_ylabel('$\\lambda /$nm')

            if title:
                fig.savefig('../img/dispersion_invertiert_{}.pdf'.format(title))
            else:
                fig.savefig('../img/dispersion_invertiert.pdf')
        return inv(n) if n else None

    def determine_lines_wavelengths(self, datapath='../daten/linienbestimmung.csv'):
        """Read measured angles for unknown spectral lines from CSV and determine the wavelengths.
        """
        df = pandas.read_csv(datapath)

        tab = table.Tabular('rccccc', booktabs=True)
        tab.escape = False
        tab.add_row('Linie', '$\\delta_{min}$', '$n$', '$\\lambda_{messung} / \\si{\\nm}$', '$\\Delta \\lambda / \\si{\\nm}$', '$\\lambda_{literatur} / \\si{\\nm}$')
        tab.add_hline()

        for i, line in enumerate(df['Linie']):
            psi1 = (df['PSI1_1'][i]+df['PSI1_2'][i])/2
            psi2 = (df['PSI2_1'][i]+df['PSI2_2'][i])/2
            delta_min = abs((psi2-psi1)/2)
            delta_min_rad = Util.degrad(delta_min)
            # using 1' as uncertainty here, due to bad angle reading
            delta_min_rad = ufloat(delta_min_rad, max(Util.degrad(1/60),
                                                      Util.degrad(AngleUncertainties().uncertainty())/2))
            n = self.calculate_refr_index(delta_min_rad)
            print('n = ', n)
            l = self.invert(n=n.n, title=line)
            l_upper = self.invert(n=n.n+n.s, plot=False)
            l_lower = self.invert(n=n.n-n.s, plot=False)
            print('Wellenlänge = ', l, '/ +', l_upper, '/ -', l_lower)
            tab.add_row(line, Fmt.dmt(delta_min), Fmt.nmt(n),
                        Fmt.format_latex_uncertainty(ufloat(l,abs(l_upper-l_lower)/2)),
                        Fmt.fmt(l-df['soll'][i]),
                        Fmt.fmt(df['soll'][i]))

        tab.generate_tex('../img/linienbestimmung')

    def spectral_resolution(self, wl=579.4, epsilon=60, a=0.0015, delta_min=Util.dms_deg(59,39,18)):
        """Calculate the spectral resolution using the specified arguments."""

        epsilon = Util.degrad(epsilon)
        delta_min = Util.degrad(delta_min)
        return abs(self.interpolate_dispersion_diff(wl))*2*a*math.sin(epsilon/2)/math.cos((delta_min+epsilon)/2) * 1e9


    def refraction_compare(self, datapath='../daten/literaturbrechung.csv'):
        """Compare the measured dispersion curve with the manufacturer's data sheet values.
        """
        df = pandas.read_csv(datapath)

        wl = df['WL']
        lit_n_nsf10 = df['n_N-SF10']
        lit_n_sf10 = df['n_SF10']
        meas_n = df['gemessen']

        (a, ea, b, eb, c, ec, _, _) = self.calculate_regressions()[0]

        def dispersion(l):
            return ufloat(a, ea)/l**4 + ufloat(b, eb)/l**2 + ufloat(c, ec)

        # Present as a residue plot
        xs = np.linspace(400, 800, 100)
        ys = np.vectorize(dispersion)(xs)
        stddevs = unp.std_devs(ys)
        ys = unp.nominal_values(ys)

        diff_regr_nsf10 = lit_n_nsf10 - np.vectorize(dispersion)(wl)
        diff_regr_sf10 = lit_n_sf10 - np.vectorize(dispersion)(wl)

        plt.tight_layout()
        fig, (p, residp) = plt.subplots(2, 1, figsize=(15,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
        fig.tight_layout()
        fig.subplots_adjust(hspace=0.)
        p.set_title('$n$ vs. $\\lambda$')
        p.set_ylabel('$n$')
        p.plot(xs, ys, label='Quad. Regr.', color='green')
        # p.plot(xs, np.vectorize(literaturwerte.n_schott_nsf10)(xs*1e-3), color='violet',
               # label='p.lw.n_schott_nsf10')
        p.scatter(wl, lit_n_nsf10, color='blue', label='Literatur N-SF10')
        p.scatter(wl, lit_n_sf10, color='orange', label='Literatur SF10')
        p.scatter(wl, meas_n, color='red', label='Messungen')
        p.grid()
        p.legend()

        # Calculate and plot residual plot
        residp.scatter(wl, unp.nominal_values(diff_regr_nsf10), # yerr=unp.std_devs(diff_regr_lit),
                       label='Diff. Regr. N-SF10', color='blue')
        residp.scatter(wl, unp.nominal_values(diff_regr_sf10), # yerr=unp.std_devs(diff_regr_lit),
                       label='Diff. Regr. SF10', color='orange')
        residp.plot(xs,
                    unp.nominal_values(np.vectorize(literaturwerte.n_schott_nsf10)(xs*1e-3)-np.vectorize(dispersion)(xs)),
                    color='violet', label='p.lw.n_schott_nsf10')
        # Add error estimates for regression
        exs = np.arange(400,675,5)
        eys = np.vectorize(dispersion)(exs)
        # residp.errorbar(exs, np.zeros_like(eys), yerr=unp.std_devs(eys),
                        # label='Unsich. d. Regr.', fmt='g.')
        residp.plot(exs, unp.std_devs(eys), 'g-', label='Unsich. d. Regr.')
        residp.plot(exs, -unp.std_devs(eys), 'g-')

        residp.axhline(y=0., color='black', linestyle='--')
        residp.set_xlabel('$\\lambda$/nm' )
        residp.set_ylabel('$n_{modell} - n_{lit}$')
        ymax = max([abs(x) for x in residp.get_ylim()])
        residp.set_ylim(-ymax, ymax)
        # residp.text(610, -7.5e-5, '> <', rotation='vertical')
        # residp.text(620, 1e-4, 'Lit.wert')
        residp.grid(True, 'both')
        residp.legend()

        fig.savefig('../img/literaturvsmessung.pdf')


au = AngleUncertainties()
au.gen_latex()
au.histogram()

dr = DispersionRelation()
dr.gen_plot()
dr.regression()
dr.invert()
dr.determine_lines_wavelengths()
print("Auflösung:", dr.spectral_resolution())
dr.refraction_compare()
dr.gen_latex()
